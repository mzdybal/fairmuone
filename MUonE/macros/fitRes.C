void fitRes(TString file, std::string name_add, Int_t nr_events, Double_t range = 0.05, Int_t bins = 200) {

    TFile input_file(file);
    TTree* input_tree = (TTree*) input_file.Get("cbmsim");

    MUonERecoOutput* event = nullptr;
    input_tree->SetBranchAddress("ReconstructionOutput", &event);

    Int_t MAX_EVENT = input_tree->GetEntriesFast();
    if(nr_events > 0)
    	MAX_EVENT = std::min(nr_events, MAX_EVENT);

    gStyle->SetOptFit(1011);


    TH1D h_x1("h_x1", ("x1 residuum " + name_add).c_str(), bins, -range, range);
    TH1D h_y1("h_y1", ("y1 residuum " + name_add).c_str(), bins, -range, range);
    TH1D h_u("h_u", ("u residuum " + name_add).c_str(),  bins, -range, range);
    TH1D h_v("h_v", ("v residuum " + name_add).c_str() , bins, -range, range);
    TH1D h_x2("h_x2", ("x2 residuum " + name_add).c_str(), bins, -range, range);
    TH1D h_y2("h_y2", ("y2 residuum " + name_add).c_str(), bins, -range, range);  


    for(int i = 0; i < MAX_EVENT; ++i) {

        input_tree->GetEntry(i);

        if(event->reconstructedTracksMultiplicity() != 1) continue;

        auto const track = event->reconstructedTracks()[0];
        if(track.numberOfStereoHits() != 2) continue;

        auto const hits = track.hits();

        for(auto const& h : hits) {

            auto const mid = h.moduleID();

            if(mid == 0)
                h_x1.Fill(h.perpendicularResiduum());
            if(mid == 1)
                h_y1.Fill(h.perpendicularResiduum());
            if(mid == 2)
                h_u.Fill(h.perpendicularResiduum());
            if(mid == 3)
                h_v.Fill(h.perpendicularResiduum());
            if(mid == 4)
                h_x2.Fill(h.perpendicularResiduum());
            if(mid == 5)
                h_y2.Fill(h.perpendicularResiduum());
        }
    }    


    TCanvas c;

    h_x1.DrawNormalized();
    h_x1.Fit("gaus");
    c.SaveAs("x1.pdf");
    c.Clear();

    h_y1.DrawNormalized();
    h_y1.Fit("gaus");
    c.SaveAs("y1.pdf");
    c.Clear();

    h_u.DrawNormalized();
    h_u.Fit("gaus");
    c.SaveAs("u.pdf");
    c.Clear();

    h_v.DrawNormalized();
    h_v.Fit("gaus");
    c.SaveAs("v.pdf");
    c.Clear();

    h_x2.DrawNormalized();
    h_x2.Fit("gaus");
    c.SaveAs("x2.pdf");
    c.Clear();

    h_y2.DrawNormalized();
    h_y2.Fit("gaus");
    c.SaveAs("y2.pdf");    
    c.Clear();

}
