#!/bin/sh

#set the environment
pwd
source ../../../../bin/FairRootConfig.sh

YAML_RECO_it0=$PWD/trackReco_forAlignment_it0.yaml
YAML_ALIGNMENT_it0=$PWD/simpleAlignment_it0.yaml

YAML_RECO=$PWD/trackReco_forAlignment.yaml
YAML_ALIGNMENT=$PWD/simpleAlignment.yaml

#do first iteration
#run reconstruction
root -l -b -q runProductionJob.C'("'${YAML_RECO_it0}'")'

#run alignment job
root -l -b -q runAlignmentJob.C'("'${YAML_ALIGNMENT_it0}'")'


for ITER in {1..10}
do

	#update the yaml files
	source makeRecoYaml.sh ${YAML_RECO} $ITER
	source makeAlignYaml.sh ${YAML_ALIGNMENT} $ITER
	#run reconstruction
	root -l -b -q runProductionJob.C'("'${YAML_RECO}'")'

	#run alignment job
	root -l -b -q runAlignmentJob.C'("'${YAML_ALIGNMENT}'")'

done


#run last trackReco with best alignment
LAST_ITER=$((ITER+=1))
source makeRecoYaml.sh ${YAML_RECO} $LAST_ITER
root -l -b -q runProductionJob.C'("'${YAML_RECO}'")'


echo "Done."







