this directory contains the macros that I use to do an iterative alignment.

You need to change some paths in some files before running the iterations...
1) trackReco_forAlignment_it0.yaml:
	you need to change the path to the outputFile (1st line), inputDirectory (7th line) and inputFiles (8th line) according to your needs.

2) simpleAlignment_it0.yaml:
	you need to change path to the inputFile (1st line) to be the same of the outputFile name in trackReco_forAlignment_it0.yaml.

3) makeRecoYaml.sh:
	you need to change the path to the outputFile (9th line), inputDirectory (15th line), inputFiles (16th line) according to your needs.

4) makeAlignYaml.sh:
	you need to change the path to the inputFile (9th line) to be the same of the outputFile name in makeRecoYaml.sh.


You are ready to lauch the alignment.
Usage:
bash do_all_jobs.sh

