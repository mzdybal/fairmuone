#!/bin/sh

FILE=$1
COUNTER=$2
PREVIOUS_COUNTER=$((COUNTER-=1))
COUNTER=$((COUNTER+=1))

cat << EOF > ${FILE}
outputFile: /eos/user/r/rpilato/TR2023/run_3220-3221/merged_3220-3221_goodDecoding_eventsWith12Stubs_1Mevts_it${COUNTER}.root  #used as output filename

detectorConfiguration: TR2023_geometry_forAlignment

numberOfEvents: 500000

inputDirectory: /eos/user/r/rpilato/TR2023/run_3220-3221/
inputFiles: [merged_3220-3221_goodDecoding_eventsWith12Stubs_1Mevts.root] #if inputDirectory set, use "*" for all files (note "", as * is yaml's special symbol)

saveParametersFile: false

runGeneration: false
runDigitization: false
runEventFilter: false

runReconstruction: true
reconstructionConfiguration:

  isMC: false
  verbose: false

  useSeedingSensorOnly: false #true #don't use bend

  runAdaptiveVertexFitter: false

  xyHitAssignmentWindow: 0.2 #cm
  uvHitAssignmentWindow: 0.3 #cm

  alignmentFile: alignParams_run3220_3221_FixedBend_it${PREVIOUS_COUNTER}
  
  #no chi2 selection, since you can get very high chi2 without alignment
  #savedTracksChi2PerNdfThreshold: 100 
  #savedVerticesChi2PerNdfThreshold: 100 

  maxOutlierChi2: -1  #disables outlier hit removal during 3D track fitting (high chi2 due to no alignment)

  maxNumberOfSharedHits: -1  #strictly disables hit sharing between reconstructed tracks (but if I'm selecting events with only 1 stub/module probably this is not needed...)

  weightLinkedTracksByDepositedCharge: false
  allowTrivialIncomingTracks: false #allows for tracks without stereo hits; implemented for testrun


 
EOF
