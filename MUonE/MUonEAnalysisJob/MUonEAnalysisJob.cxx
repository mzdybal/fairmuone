#include "MUonEAnalysisJob.h"

#include <fairlogger/Logger.h>


MUonEAnalysisJob::MUonEAnalysisJob()
{
    resetDefaultConfiguration();
}

Bool_t MUonEAnalysisJob::readConfiguration(TString config_name) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    LOG(info) << "";
    LOG(info) << "Reading job configuration: " << config_name.Data();

    YAML::Node job;

    if(!loadConfigFile(config_name, job))
        return false;


    if(job["inputFile"])
        m_inputFile = job["inputFile"].as<std::string>();
    else {
        LOG(error) << "Variable 'inputFile' not set.";
        m_configuredCorrectly = false;
    }


    if(job["outputFile"])
        m_outputFile = job["outputFile"].as<std::string>();
    else {
        LOG(info) << "Variable 'outputFile' not set. Defaulting to inputFile+_analysis.";
        m_outputFile = m_inputFile + "_analysis";
    }



    if(job["numberOfEvents"]) {
        m_numberOfEvents = job["numberOfEvents"].as<Int_t>();
        m_numberOfEventsSet = true;
    }
    else {
        m_numberOfEventsSet = false;
    }


    if(job["runBasicPlots"])
        m_runBasicPlots = job["runBasicPlots"].as<Bool_t>();
    else {
        m_runBasicPlots = false;            
    }

    if(m_runBasicPlots) {

        if(job["basicPlotsConfiguration"]) {

            if(!m_basicPlotsConfiguration.readConfiguration(job["basicPlotsConfiguration"]))
                m_configuredCorrectly = false;
        }
        else {
            LOG(error) << "Variable 'runBasicPlots' set to true, but 'basicPlotsConfiguration' not provided.";
            m_configuredCorrectly = false;                
        }
    }


    if(m_configuredCorrectly)
        LOG(info) << "Job configuration read successfully.";
    else
        LOG(info) << "Job configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}


void MUonEAnalysisJob::logCurrentConfiguration() const {

    LOG(info) << "";
    LOG(info) << "";
    LOG(info) << "Summary of analysis job configuration";
    LOG(info) << "Input file: " << m_inputFile;
    LOG(info) << "Output file: " << m_outputFile;

    if(m_runBasicPlots) {
        LOG(info) << "The job will run BasicPlots analysis task.";           
    }
    else {
        LOG(info) << "All analysis tasks run flags are set to false. The job will do nothing.";
    }

    if(m_numberOfEventsSet)
        LOG(info) << "First " << m_numberOfEvents << " events will be processed.";
    else
        LOG(info) << "All input events will be processed.";
            

}


void MUonEAnalysisJob::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_inputFile = "";
    m_outputFile = "";

    m_numberOfEvents = 0;
    m_numberOfEventsSet = false;


    m_runBasicPlots = false;
    m_basicPlotsConfiguration.resetDefaultConfiguration();

}

Bool_t MUonEAnalysisJob::loadConfigFile(TString config_name, YAML::Node& config) {

    if(!config_name.EndsWith(".yaml") && !config_name.EndsWith(".YAML"))
        config_name.Append(".yaml");

    try {

        config = YAML::LoadFile(config_name.Data());
        LOG(info) << "Configuration file found in the path provided.";

    } catch(...) {

        TString jobs_dir = getenv("JOBSPATH");
        jobs_dir.ReplaceAll("//", "/"); //taken from FR examples, not sure if necessary
        if(!jobs_dir.EndsWith("/"))
            jobs_dir.Append("/");  

        TString full_path = jobs_dir + config_name;

        LOG(info) << "Configuration file in the given path not found or failed to open. Trying in JOBSPATH directory (" << full_path << ").";

        try {

            config = YAML::LoadFile(full_path.Data());
            LOG(info) << "File found.";

        } catch(...) {

            LOG(fatal) << "Failed to load job configuration.";
            return false;
        }
    }   

    return true; 
}

ClassImp(MUonEAnalysisJob)