#ifndef MUONEANALYSISJOB_H
#define MUONEANALYSISJOB_H

#include <yaml-cpp/yaml.h>
#include "TString.h"
#include <string>

#include "Rtypes.h"

#include "MUonEAnalysisBasicPlotsConfiguration.h"

class MUonEAnalysisJob {


public:

    MUonEAnalysisJob();

    Bool_t readConfiguration(TString config_name);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}


    std::string inputFile() const {return m_inputFile;}
    std::string outputFile() const {return m_outputFile;}

    Bool_t numberOfEventsSet() const {return m_numberOfEventsSet;}
    Int_t numberOfEvents() const {return m_numberOfEvents;}


    Bool_t runBasicPlots() const {return m_runBasicPlots;}
    MUonEAnalysisBasicPlotsConfiguration const& basicPlotsConfiguration() const {
        if(m_runBasicPlots)
            return m_basicPlotsConfiguration;
        else
            throw std::logic_error("Trying to access basicPlotsConfiguration, but runBasicPlots is set to false.");
    }        


    void resetDefaultConfiguration();

private:

    Bool_t loadConfigFile(TString config_name, YAML::Node& config);

    Bool_t m_configuredCorrectly{false};

    std::string m_inputFile;
    std::string m_outputFile;

    Bool_t m_numberOfEventsSet{false};
    Int_t m_numberOfEvents{0};

    Bool_t m_runBasicPlots{false};
    MUonEAnalysisBasicPlotsConfiguration m_basicPlotsConfiguration;



    ClassDef(MUonEAnalysisJob, 2)
};

#endif //MUONEANALYSISJOB_H

