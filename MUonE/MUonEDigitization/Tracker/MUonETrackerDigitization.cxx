#include "MUonETrackerDigitization.h"

#include "MUonETrackerPoint.h"
#include "MUonEChargeDeposit.h"
#include "MUonESignalPoint.h"

#include "FairRootManager.h"
#include "FairRunSim.h"
#include "FairRunAna.h"

#include <fairlogger/Logger.h>
#include "TRandom.h"

#include "TClonesArray.h"
#include "TMath.h"

#include <iostream>
#include <array>
#include <vector>
#include <stdlib.h>


MUonETrackerDigitization::MUonETrackerDigitization()
	: FairTask("MUonETrackerDigitization", 0), 
    m_trackerStripDigis(new TClonesArray("MUonETrackerStripDigi")),
    m_trackerStubs(new TClonesArray("MUonETrackerStub"))
	{}


MUonETrackerDigitization::~MUonETrackerDigitization()
{}


Bool_t MUonETrackerDigitization::setConfiguration(MUonEDetectorConfiguration const& detectorConfig, MUonETrackerDigitizationConfiguration const& digitizationConfig) {

	resetDefaultConfiguration();

	m_detectorConfiguration = detectorConfig;

	m_configuredCorrectly = true;

	LOG(info) << "";
	LOG(info) << "Creating TrackerDigitization object.";

	m_digitizationConfiguration = digitizationConfig;
    m_configuredCorrectly = m_digitizationConfiguration.configuredCorrectly();


    if(m_configuredCorrectly)
        LOG(info) << "TrackerDigitization object created successfully.";
    else
        LOG(info) << "Digitization configuration contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}

void MUonETrackerDigitization::logCurrentConfiguration() {

    m_digitizationConfiguration.logCurrentConfiguration();
}

void MUonETrackerDigitization::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_detectorConfiguration.resetDefaultConfiguration();
	m_digitizationConfiguration.resetDefaultConfiguration();
}


InitStatus MUonETrackerDigitization::ReInit()
{

    if(m_trackerStripDigis) 
        m_trackerStripDigis->Clear();
    

    if(m_trackerStubs) 
        m_trackerStubs->Clear();
    
	return kSUCCESS;
}

InitStatus MUonETrackerDigitization::Init()
{
    LOG(info) << "Initializing tracker digitization.";
	FairRootManager* ioman = FairRootManager::Instance();

	if(!ioman) 
	{ 
		LOG(fatal) << "No FairRootManager"; 
		return kERROR;
	} 

	m_trackerPoints = static_cast<TClonesArray*>(ioman->GetObject("TrackerPoints"));
    Register();
    
    return kSUCCESS;
}

void MUonETrackerDigitization::Exec(Option_t* option)
{
    
    auto n_points = m_trackerPoints->GetEntriesFast();
    
    m_trackerStripDigis->Clear();
    m_trackerStubs->Clear();

    DigisMap digis_map;


    //initialize all strips with 0 charge
    auto const stations = m_detectorConfiguration.stations();
    for(Int_t station_index = 0; station_index < stations.size(); ++station_index) {

        auto const& modules = stations[station_index].modules();
        for(Int_t module_index = 0; module_index < modules.size(); ++module_index) {

            auto const& module = modules[module_index];
            auto number_of_strips = module.configuration().geometry().numberOfStrips();
            Int_t number_of_sensors = module.configuration().geometry().twoSensorsPerModule() ? 2 : 1;

            for(Int_t sensor_index = 0; sensor_index < number_of_sensors; ++sensor_index) {
                for(Int_t sensor_half = 0; sensor_half < 2; ++sensor_half) {
                    for(Int_t strip_index = 0; strip_index < number_of_strips; ++strip_index) {

                        digis_map[station_index][module_index][sensor_index][sensor_half][strip_index] = MUonEStripDeposits();
                    }
                }//!sensor half
            }//!sensor
        }//!modules
    }//!stations
        
    for(Int_t i = 0; i < n_points; ++i) {
        

        auto const* point = static_cast<MUonETrackerPoint*>(m_trackerPoints->At(i));

        std::vector<MUonESignalPoint> collection_points;
        std::vector<MUonEChargeDeposit> ionisation_points;
            
        //transportation model:
        // take each SimHit and generate ionisation points every 10um along the
        // track path, fluctuate the energy loss on each ionisation point, per SimHit
        primaryIonisation(point, ionisation_points);
            
        // drift each ionisation point to the sensor surface, using a gaussian smear
        // based on a charge diffusion parameter and the distance of ionisation point to
        // surface to generate set of charge cloud objects, per SimHit
        drift(ionisation_points, collection_points, point);
            
        // accumulate the charge points on a rectilinear grid of strips at the surface,
        // so that each strip has a charge associated with it, per SimHit
        induceSignal(collection_points, digis_map, point);  
    }
        
    //digitisation here:
    // apply a CRRC distribution for every strip and SimHit and, depending on CBC mode
    // chosen, define whether there is a binary hit for each of the strips above
    // selected threshold and within the time window, then generate StripDigis
    createDigis(digis_map);
        
    createStubs();
    
}



void MUonETrackerDigitization::primaryIonisation(const MUonETrackerPoint* point, std::vector<MUonEChargeDeposit>& ionisation_points) {

    const Double_t segment_length = 1e-3; // 10 um in cm
    Int_t number_of_segments = point->distanceTraveledInside() / segment_length;
    if(number_of_segments < 1)
        number_of_segments = 1;

    Double_t energy_loss = point->totalEnergyDeposit();

    auto direction = point->exitingPositionLocalCoordinates() - point->enteringPositionLocalCoordinates();


    ionisation_points.reserve(number_of_segments);

    for(Int_t i = 0; i < number_of_segments; ++i) {

        auto position = point->enteringPositionLocalCoordinates() + ((i + 0.5)/number_of_segments) * direction;
        auto charge = energy_loss / number_of_segments / GEV_PER_ELECTRON;

        ionisation_points.emplace_back(charge, position);
    }
}

void MUonETrackerDigitization::drift(std::vector<MUonEChargeDeposit> const& ionisation_points, std::vector<MUonESignalPoint>& collection_points, const MUonETrackerPoint* point) {

    collection_points.reserve(ionisation_points.size());

    auto const& module = m_detectorConfiguration.stations()[point->stationID()].modules()[point->moduleID()];
    auto const& module_geometry = module.configuration().geometry();
    auto const& module_digitization = module.configuration().digitization();

    auto drift_direction = point->isUpstream() ? module_digitization.driftDirectionUpstream() : module_digitization.driftDirectionDownstream();
    auto sensor_thickness = module_geometry.sensorThickness();
    auto sensor_pitch = module_geometry.stripPitch();

    auto sigma0 = module_digitization.sigma0();
    auto sigma_coefficient = module_digitization.sigmaCoefficient();

    for(auto const& charge : ionisation_points) {

        Double_t drift_distance = 0.5 * sensor_thickness - drift_direction * charge.position().Z();

        if(drift_distance < 0)
            drift_distance = 0;
        else if (drift_distance > sensor_thickness)
            drift_distance = sensor_thickness;

        
        auto cloud_center = charge.position().XYvector();

        //diffusion sigma assumed to be the same in both directions

        // sigma0=0.00037 is for 300um thickness (make sure thickness is in [cm])
        Double_t sigma = TMath::Sqrt(drift_distance / sensor_thickness) * sigma0 * sensor_thickness / 0.3;

        // sigma_coefficient   
        // delta in the diffusion across the strip pitch
        // (set between 0 to 0.9,  0-->flat Sigma0, 1-->Sigma_min=0 & Sigma_max=2*Sigma0      
        //measurement direction in local coordinates is always along the X axis
        if(0 != sigma_coefficient)
            sigma *= sigma_coefficient * TMath::Power(TMath::Cos(charge.position().X() * TMath::Pi() / sensor_pitch), 2) + 1;

        // No charge loss due to radiation damage implemented
        // energy on collector == charge.charge()
        collection_points.emplace_back(cloud_center, sigma, charge.charge());
    }
}


void MUonETrackerDigitization::induceSignal(std::vector<MUonESignalPoint> const& collection_points, DigisMap& digis_map, const MUonETrackerPoint* point) {

    auto const& module = m_detectorConfiguration.stations()[point->stationID()].modules()[point->moduleID()];
    auto const& module_geometry = module.configuration().geometry();
    auto const& module_digitization = module.configuration().digitization();

    for(auto const& collection_point : collection_points) {

        auto cloud_center = collection_point.position();
        auto sigma = collection_point.sigma();
        auto charge = collection_point.amplitude();
        auto sensor_size = module_geometry.sensorSizeMeasurementDirection();
        auto number_of_strips = module_geometry.numberOfStrips();
        auto strip_pitch = module_geometry.stripPitch();

        //reject points outside of the sensor's sensitive area
        if(cloud_center.X() <= -0.5*sensor_size || 0.5*sensor_size <= cloud_center.X())
            continue;

        //determine strips hit by charge cloud, x == measurement direction
        auto positionToStrip = [sensor_size, number_of_strips, strip_pitch](Double_t position) -> Int_t {
            
            if(position <= - 0.5 * sensor_size)
                return 0;
            else if(position >= 0.5 * sensor_size)
                return number_of_strips - 1;
            else
                return std::min(static_cast<Int_t>(position/strip_pitch + 0.5 * number_of_strips), number_of_strips - 1);
        };


        //assuming max spread of 3 sigma
        //strip range in measurement direction
        auto min_strip_index = positionToStrip(cloud_center.X() - 3 * sigma);
        auto max_strip_index = positionToStrip(cloud_center.X() + 3 * sigma);

        //cloud range in perpendicular direction
        Double_t cloud_lower_bound = cloud_center.Y() - 3 * sigma;
        Double_t cloud_upper_bound = cloud_center.Y() + 3 * sigma;


        auto stripToPosition = [number_of_strips, strip_pitch](Int_t strip_index) -> Double_t {

            return (strip_index - 0.5*number_of_strips)*strip_pitch + 0.5*strip_pitch;
        };

        auto calcQ = [](Double_t x) {

            Double_t xx = std::min(0.5*x*x, 12.5);
            return 0.5*(1. - std::copysign(std::sqrt(1. - std::exp(-xx*(1. + 0.2733/(1. + 0.147*xx)))), x));
        };

        //create signal on the strips
        for(Int_t strip_index = min_strip_index; strip_index <= max_strip_index; ++strip_index) {

            Double_t charge_lower_bound, position_lower_bound;

            if(0 == strip_index || sigma == 0) {

                charge_lower_bound = 0;
            }
            else {

                position_lower_bound = stripToPosition(strip_index) - 0.5*strip_pitch;
                charge_lower_bound = 1 - calcQ((position_lower_bound - cloud_center.X())/sigma);
            }


            Double_t charge_upper_bound, position_upper_bound;

            if(number_of_strips - 1 == strip_index || sigma == 0) {

                charge_upper_bound = 1;
            }
            else {

                position_upper_bound = stripToPosition(strip_index) + 0.5*strip_pitch;
                charge_upper_bound = 1 - calcQ((position_upper_bound - cloud_center.X())/sigma);
            }

            Double_t charge_fraction = charge * (charge_upper_bound - charge_lower_bound);

            Int_t channel_fired = -1;
            Int_t sensor_half = -1;

            if(charge_fraction > 0)
                channel_fired = strip_index;

            //divide into two sets of strips of the 2S module
            if(cloud_lower_bound < 0 && cloud_upper_bound < 0) {

                sensor_half = 0;
                digis_map[point->stationID()][point->moduleID()][point->sensorID()][sensor_half][channel_fired].addDeposit(point->trackID(), charge_fraction);
            }
            else if(cloud_lower_bound >= 0 && cloud_upper_bound >= 0) {

                sensor_half = 1;
                digis_map[point->stationID()][point->moduleID()][point->sensorID()][sensor_half][channel_fired].addDeposit(point->trackID(), charge_fraction);
            }
            else {

                Double_t cloud_width = cloud_upper_bound - cloud_lower_bound;

                Double_t fraction_lower = std::abs(cloud_lower_bound)/cloud_width;
                sensor_half = 0;
                digis_map[point->stationID()][point->moduleID()][point->sensorID()][sensor_half][channel_fired].addDeposit(point->trackID(), fraction_lower*charge_fraction);

                Double_t fraction_upper = cloud_upper_bound/cloud_width;
                sensor_half = 1;
                digis_map[point->stationID()][point->moduleID()][point->sensorID()][sensor_half][channel_fired].addDeposit(point->trackID(), fraction_upper*charge_fraction); 
            }
        }
    }
}


void MUonETrackerDigitization::createDigis(DigisMap const& digis_map) {


    for(auto const& station : digis_map) {

        auto station_id = station.first;
        auto modules_map = station.second;

        for(auto const& mod : modules_map) {

            auto module_id = mod.first;
            auto sensors_map = mod.second;

            auto const& module = m_detectorConfiguration.stations()[station_id].modules()[module_id];
            auto const& module_digitization = module.configuration().digitization();

            for(auto const& sensor : sensors_map) {

                auto sensor_id = sensor.first;
                auto sensor_halfs_map = sensor.second;

                for(auto const& sensor_half : sensor_halfs_map) {

                    auto sensor_half_id = sensor_half.first;
                    auto strips_map = sensor_half.second;

                    for(auto& strip : strips_map) {

                        auto strip_id = strip.first;
                        auto strip_charge = strip.second.totalCharge();

                        //add readout noise
                        auto readout_noise = module_digitization.readoutNoise();
                        if(0 != readout_noise) {

                            strip_charge += gRandom->Gaus(0, readout_noise);

                            if(strip_charge < 0) 
                                strip_charge = 0;
                        }

                        auto strip_threshold = module_digitization.stripThreshold();
                        if(strip_charge > strip_threshold) {

                            auto digi = AddStripDigi(station_id, module_id, sensor_id, sensor_half_id, strip_id, strip_charge, strip.second);
                        }

                    }//!strip : strips_map
                }//!sensor_half : sensor_halfs_map
            }//!sensor : sensors_map
        }//!mod : modules_map
    }//!station : digis_map
}

void MUonETrackerDigitization::createStubs() {
    
    HitMap hit_map;
    auto n_digis = m_trackerStripDigis->GetEntriesFast();
    for(Int_t i = 0; i < n_digis; ++i) {

        auto const* digi = static_cast<MUonETrackerStripDigi*>(m_trackerStripDigis->At(i));
        hit_map[digi->stationID()][digi->moduleID()][digi->sensorHalf()][digi->sensorID()].insert(digi); 
    }

    for(auto const& station : hit_map) {

        auto station_id = station.first;
        auto modules_map = station.second;

        for(auto const& mod : modules_map) {

            auto module_id = mod.first;
            auto sensor_halfs_map = mod.second;

            auto const& module = m_detectorConfiguration.stations()[station_id].modules()[module_id];
            auto const& module_geometry = module.configuration().geometry();
            auto const& module_digitization = module.configuration().digitization();            

            for(auto const& sensor_half : sensor_halfs_map) {

                auto sensor_half_id = sensor_half.first;
                auto sensors_map = sensor_half.second;

                //create clusters
                ClusterSets cluster_sets;

                for(auto const& sensor : sensors_map) {

                    auto sensor_id = sensor.first;
                    auto hits_set = sensor.second;


                    //make clusters
                    std::vector<MUonETrackerCluster> clusters;
                    Int_t previous_strip = -1;

                    for(auto const& digi : hits_set) {

                        auto hit_strip = digi->stripID();

                        if(clusters.empty() || hit_strip - previous_strip != 1) {

                            clusters.emplace_back(station_id, module_id, sensor_id, sensor_half_id, module_digitization.halfChannelStripOrder(), digi);    
                        }
                        else {

                            clusters.back().addHitStrip(digi);
                        }

                        previous_strip = hit_strip;
                    } //!digi : hits_set


                    //computing cluster values and cluster width cut
                    for(auto& cluster : clusters) {

                        cluster.compute(module_geometry.numberOfStrips());

                        if(cluster.isValid(module_digitization.maxClusterWidth())) {

                            cluster_sets[sensor_id].insert(cluster);
                        }
                    } //!cluster : clusters


                }//!sensor : sensors_map

                //create stubs

                StubSets stub_sets;

                if(module_geometry.twoSensorsPerModule()) {

                    int seed_sensor = module_digitization.seedSensor();

                    for(auto const& seeding_cluster : cluster_sets[seed_sensor]) {

                        auto window_offset = module_digitization.windowOffset();
                        auto pt_width = module_digitization.ptWidth();

                        Double_t width_low = seeding_cluster.centerStrip() + window_offset - pt_width;
                        Double_t width_high = seeding_cluster.centerStrip() + window_offset + pt_width;

                        for(auto const& correlation_cluster : cluster_sets[1 - seed_sensor]) {

                            if(correlation_cluster.centerStrip() < width_low || width_high < correlation_cluster.centerStrip()) continue;

                            MUonETrackerStub stub(station_id, module_id, sensor_half_id, seeding_cluster, correlation_cluster, window_offset);

                            std::pair<StubSet::iterator, bool> ret = stub_sets[seeding_cluster.cbcID()].insert(stub);

                            //stub already exists in a set
                            if(!ret.second) {

                                //if new stub is better than old one (smaller bend)
                                //remove old and insert new
                                if(stub.seedReducedChannel() == ret.first->seedReducedChannel() && std::abs(stub.bend()) < std::abs(ret.first->bend())) {

                                    stub_sets[seeding_cluster.cbcID()].erase(ret.first);
                                    stub_sets[seeding_cluster.cbcID()].insert(stub);
                                }
                            }
                        }//!correlation_cluster : cluster_sets[1 - seed_sensor]
                    }//!seeding_cluster : cluster_sets[seed_sensor]

                } else {

                    //1 sensor/module
                    for(auto const& cluster : cluster_sets[0]) {

                        stub_sets[cluster.cbcID()].insert(MUonETrackerStub(station_id, module_id, sensor_half_id, cluster));
                    }
                }

                Int_t ns_cic{0};
                for(auto const& set : stub_sets) {

                    if(ns_cic > 8) break;

                    auto const& stubs = set.second;

                    Int_t ns_cbc{0};
                    for(auto const& stub : stubs) {

                        if(ns_cbc < 3 && ns_cic < 8) {

                            if(module_digitization.isLUTEnabled()) {
                                MUonETrackerStub stub_bendLUT = ApplyLUT(stub);
                                stub_bendLUT = CorrectBendOffset(stub_bendLUT);
                                AddStub(stub_bendLUT);
                            }
                            else {
                                MUonETrackerStub stub_correctOffset = CorrectBendOffset(stub);
                                AddStub(stub_correctOffset);
                            }

                            ++ns_cbc;
                            ++ns_cic;         
                        } else break;
                    }
                }
                
            }//!sensor_half : sensor_halfs_map
        }//!mod : modules_map
    }//!station : hit_map



}



void MUonETrackerDigitization::Finish()
{

    if(m_trackerStripDigis) {

        m_trackerStripDigis->Delete();
        delete m_trackerStripDigis;
        m_trackerStripDigis = nullptr;
    }

    if(m_trackerStubs) {

        m_trackerStubs->Delete();
        delete m_trackerStubs;
        m_trackerStubs = nullptr;
    }
}

void MUonETrackerDigitization::Register()
{

    FairRootManager::Instance()->Register("TrackerStripDigis", FairTask::GetName(), m_trackerStripDigis, m_digitizationConfiguration.saveStripDigis() ? kTRUE : kFALSE);
    FairRootManager::Instance()->Register("TrackerStubs", FairTask::GetName(), m_trackerStubs, m_digitizationConfiguration.saveStubs() ? kTRUE : kFALSE);

}


MUonETrackerStripDigi* MUonETrackerDigitization::AddStripDigi(Int_t station, Int_t module, Int_t sensor, Int_t sensorHalf, Int_t strip, Double_t charge, MUonEStripDeposits& deposits) {

	TClonesArray& clref = *m_trackerStripDigis;
	Int_t size = clref.GetEntriesFast();
	return new(clref[size]) MUonETrackerStripDigi(station, module, sensor, sensorHalf, strip, charge, deposits);	
}

MUonETrackerStub MUonETrackerDigitization::ApplyLUT(MUonETrackerStub const& stub) {

	auto const& module = m_detectorConfiguration.stations()[stub.stationID()].modules()[stub.moduleID()];
	auto const& module_digitization = module.configuration().digitization();

	std::unordered_multimap<uint8_t, Double_t> LUTmap = module_digitization.LUTmap();//key = 4bits bend code; value = bend value from digitization
	Double_t pt_width = module_digitization.ptWidth();

	auto iter = LUTmap.begin();
	while(stub.bend() != iter->second) iter++;
	uint8_t LUT_bendCode = iter->first;
	auto result = LUTmap.equal_range(LUT_bendCode);
	Double_t new_bend = 0;
	Int_t n_bends = 0;
	for(auto it = result.first; it != result.second; it++) {
		if(fabs(it->second) <= pt_width) {
		    new_bend += it->second;
		    n_bends++;
		}
	}
	new_bend /= n_bends;

	MUonETrackerStub stub_afterLUT = stub;
	stub_afterLUT.setBend(new_bend);
	return stub_afterLUT;
}

MUonETrackerStub MUonETrackerDigitization::CorrectBendOffset(MUonETrackerStub const& stub) {

	auto const& module = m_detectorConfiguration.stations()[stub.stationID()].modules()[stub.moduleID()];
	auto const& module_digitization = module.configuration().digitization();
	Double_t window_offset = module_digitization.windowOffset();

	MUonETrackerStub stub_afterOffset = stub;
	stub_afterOffset.setBend(stub_afterOffset.bend() + window_offset);
	return stub_afterOffset;

}

MUonETrackerStub* MUonETrackerDigitization::AddStub(MUonETrackerStub const& stub) {

	TClonesArray& clref = *m_trackerStubs;
	Int_t size = clref.GetEntriesFast();
	return new(clref[size]) MUonETrackerStub(stub);	
}



ClassImp(MUonETrackerDigitization)


