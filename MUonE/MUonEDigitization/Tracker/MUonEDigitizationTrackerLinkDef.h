#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonEChargeDeposit+;
#pragma link C++ class MUonESignalPoint+;
#pragma link C++ class MUonETrackerCluster+;
#pragma link C++ class MUonEStripDeposits+;
#pragma link C++ class MUonETrackerStripDigi+;
#pragma link C++ class MUonETrackDeposit+;
#pragma link C++ class MUonETrackerStub+;
#pragma link C++ class MUonETrackerDigitization+;



#endif