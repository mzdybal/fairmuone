#ifndef MUONETRACKERCLUSTER_H
#define MUONETRACKERCLUSTER_H

#include "MUonETrackerStripDigi.h"

#include "Rtypes.h"
#include "TMath.h"

#include <set>


class MUonETrackerCluster {

public:

	MUonETrackerCluster() = default;

	MUonETrackerCluster(Int_t stationID, Int_t moduleID, Int_t sensorID, Int_t sensorHalf, Int_t stripOrder, const MUonETrackerStripDigi* digi)
		: m_stationID(stationID), m_moduleID(moduleID), m_sensorID(sensorID), m_sensorHalf(sensorHalf), m_hitStrips({digi}), 
		m_channelStripOrderMatch(sensorHalf == stripOrder)
		{}


	void addHitStrip(const MUonETrackerStripDigi* digi) {m_hitStrips.insert(digi);}


	Int_t stationID() const {return m_stationID;}
	Int_t moduleID() const {return m_moduleID;}
	Int_t sensorID() const {return m_sensorID;}
	Int_t sensorHalf() const {return m_sensorHalf;}

	std::set<const MUonETrackerStripDigi*> hitStrips() const {return m_hitStrips;}
	Int_t width() const {return m_hitStrips.size();}

	Bool_t channelStripOrderMatch() const {return m_channelStripOrderMatch;}

	Double_t centerStrip() const {return m_centerStrip;}
	Int_t cbcID() const {return m_cbcID;}
	Double_t centerReducedChannel() const {return m_centerReducedChannel;}


	void compute(Double_t total_number_of_strips) {

		if(m_hitStrips.empty()) return;

		//calculate mean of stripIds to get the center position
		m_centerStrip = 0;
		for(auto const& hit : m_hitStrips)
			m_centerStrip += hit->stripID();

		m_centerStrip /= m_hitStrips.size();


		if(m_channelStripOrderMatch) {

			m_cbcID = TMath::Floor(m_centerStrip / 127);
			m_centerReducedChannel = m_centerStrip - m_cbcID * 127;
		} else {
			//count position from the other side, stips id in (0, N - 1)
			Double_t cs_swapped = (total_number_of_strips - 1) - m_centerStrip;
			m_cbcID = TMath::Floor(cs_swapped / 127);
			m_centerReducedChannel = cs_swapped - m_cbcID * 127;
		}
	}

	Bool_t isValid(Double_t max_cluster_width) {

		return max_cluster_width > 0 ? m_hitStrips.size() <= max_cluster_width : true;
	}


	bool operator< (const MUonETrackerCluster& cl) const {

		if(m_channelStripOrderMatch) 
			return centerStrip() < cl.centerStrip();
		else 
			return cl.centerStrip() < centerStrip();
	}	

private:

	Int_t m_stationID{-1};
	Int_t m_moduleID{-1};
	Int_t m_sensorID{-1};
	Int_t m_sensorHalf{-1};

	std::set<const MUonETrackerStripDigi*> m_hitStrips;

	Bool_t m_channelStripOrderMatch{true}; //SensorHalf which strip order matches to the order of CBC channels.

	Double_t m_centerStrip{0};
	Int_t m_cbcID{-1};
	Double_t m_centerReducedChannel{-1}; //real channel [1:254]. odd for bottom, even for top. Reduced channel [0:126.5] (both layers)


	ClassDef(MUonETrackerCluster,2)

};

#endif //MUONETRACKERCLUSTER_H

