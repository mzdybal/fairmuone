#ifndef MUONETRACKERSTRIPDIGI_H
#define MUONETRACKERSTRIPDIGI_H

#include "Rtypes.h"
#include "TObject.h"

#include <unordered_set>
#include <map>

class MUonETrackDeposit {

public:

    MUonETrackDeposit(Int_t track_id = 0, Double_t deposit = 0)
        : TrackID(track_id), DepositedCharge(deposit)
        {}   

    void addDeposit(Double_t deposit) {DepositedCharge += deposit;}

    Int_t trackID() const {return TrackID;}
    Double_t depositedCharge() const {return DepositedCharge;}

private:

    Int_t TrackID{-2};
    Double_t DepositedCharge{0};

	ClassDef(MUonETrackDeposit,2)
};

class MUonEStripDeposits {

public:

    MUonEStripDeposits() = default;

    void addDeposit(Int_t trackID, Double_t deposit) {

        TotalCharge += deposit;
        TrackDeposits.emplace_back(trackID, deposit);
    }

    std::vector<MUonETrackDeposit> const& trackDeposits() const {return TrackDeposits;}
    Double_t totalCharge() const {return TotalCharge;}

private:
    
    std::vector<MUonETrackDeposit> TrackDeposits;
    Double_t TotalCharge{0}; //total charge

	ClassDef(MUonEStripDeposits,2)
};

class MUonETrackerStripDigi : public TObject {

public:

    MUonETrackerStripDigi() = default;

    MUonETrackerStripDigi(Int_t station, Int_t module, Int_t sensor, Int_t sensorHalf, Int_t strip, Double_t charge, MUonEStripDeposits const& deposits)
        :  TObject(), 
        StationID(station), ModuleID(module), SensorID(sensor), SensorHalf(sensorHalf), StripID(strip), Charge(charge), Deposits(deposits)
        {}

    Int_t stationID() const {return StationID;}
    Int_t moduleID() const {return ModuleID;}
    Int_t sensorID() const {return SensorID;}    
    Int_t sensorHalf() const {return SensorHalf;}
    Int_t stripID() const {return StripID;}
    Double_t charge() const {return Charge;}  

    MUonEStripDeposits const& deposits() const {return Deposits;}  
    

private:

    Int_t StationID{-1};
    Int_t ModuleID{-1};
    Int_t SensorID{-1};
    Int_t SensorHalf{-1};
    Int_t StripID{-1};
    Double_t Charge{-1}; //charge modified by detector effects, in units of electron charge 

    MUonEStripDeposits Deposits; //track ids and individual deposits of all tracks, charge without detector effects


	ClassDef(MUonETrackerStripDigi,2)

};

#endif //MUONETRACKERSTRIPDIGI_H

