#include "MUonECalorimeterDigitization.h"

#include "MUonECalorimeterPoint.h"

#include "FairRootManager.h"
#include "FairRunSim.h"
#include "FairRunAna.h"

#include <fairlogger/Logger.h>
#include "TRandom.h"

#include "TClonesArray.h"
#include "TMath.h"

#include <iostream>
#include <array>
#include <stdlib.h>

MUonECalorimeterDigitization::MUonECalorimeterDigitization()
	: FairTask("MUonECalorimeterDigitization", 0),
	m_calorimeterDigiDeposit(new MUonECalorimeterDigiDeposit())
	{}


MUonECalorimeterDigitization::~MUonECalorimeterDigitization()
{}

Bool_t MUonECalorimeterDigitization::setConfiguration(MUonEDetectorConfiguration const& detectorConfig, MUonECalorimeterDigitizationConfiguration const& digitizationConfig) {

	resetDefaultConfiguration();

	m_detectorConfiguration = detectorConfig;

	m_configuredCorrectly = true;

	LOG(info) << "";
	LOG(info) << "Creating CalorimeterDigitization object.";

	m_digitizationConfiguration = digitizationConfig;
	m_configuredCorrectly = m_digitizationConfiguration.configuredCorrectly();


    if(m_configuredCorrectly)
        LOG(info) << "CalorimeterDigitization object created successfully.";
    else
        LOG(info) << "Digitization configuration contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}

void MUonECalorimeterDigitization::logCurrentConfiguration() {

	m_digitizationConfiguration.logCurrentConfiguration();
}

void MUonECalorimeterDigitization::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_detectorConfiguration.resetDefaultConfiguration();
	m_digitizationConfiguration.resetDefaultConfiguration();


}

InitStatus MUonECalorimeterDigitization::ReInit()
{

	if(m_calorimeterDigiDeposit) 
		m_calorimeterDigiDeposit->resetEnergyDeposit();
	

	return kSUCCESS;
}

InitStatus MUonECalorimeterDigitization::Init()
{
	
	LOG(info) << "Initializing calorimeter digitization.";
	FairRootManager* ioman = FairRootManager::Instance();

	if(!ioman) 
	{ 
		LOG(fatal) << "No FairRootManager"; 
		return kERROR;
	} 

	m_calorimeterPoints = static_cast<TClonesArray*>(ioman->GetObject("CalorimeterPoints"));

    Register();

    return kSUCCESS;
}

void MUonECalorimeterDigitization::Exec(Option_t* option)
{

	m_calorimeterDigiDeposit->resetEnergyDeposit();

	//loop on all points registered in the calorimeter and add up their energy deposits
	auto n_points = m_calorimeterPoints->GetEntriesFast();

	for(Int_t i = 0; i < n_points; ++i){

		auto const* point = static_cast<MUonECalorimeterPoint*>(m_calorimeterPoints->At(i));

		m_calorimeterDigiDeposit->addEnergyDeposit(point->crystalColumnID(), point->crystalRowID(), point->totalEnergyDeposit());
	}

	// apply energy resolution
	double resolution = m_detectorConfiguration.calorimeterConfiguration().digitization().resolution();
	m_calorimeterDigiDeposit->smearEnergies(resolution);

}

void MUonECalorimeterDigitization::Finish()
{

	if(m_calorimeterDigiDeposit) {

		delete m_calorimeterDigiDeposit;
		m_calorimeterDigiDeposit = nullptr;
	}
}

void MUonECalorimeterDigitization::Register()
{
	FairRootManager::Instance()->RegisterAny("CalorimeterDigiDeposit", m_calorimeterDigiDeposit, m_digitizationConfiguration.saveDeposit() ? kTRUE : kFALSE);
}



ClassImp(MUonECalorimeterDigitization)
