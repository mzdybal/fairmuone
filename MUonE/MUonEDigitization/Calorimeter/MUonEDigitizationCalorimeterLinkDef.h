#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonECalorimeterDigiDeposit+;
#pragma link C++ class MUonECalorimeterCrystalDigiDeposit+;
#pragma link C++ class MUonECalorimeterDigitization+;

#endif
