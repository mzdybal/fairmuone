#ifndef MUONECALORIMETERDIGIDEPOSIT_H
#define MUONECALORIMETERDIGIDEPOSIT_H

#include "MUonECalorimeterCrystalDigiDeposit.h"
#include "TClonesArray.h"

class MUonECalorimeterDigiDeposit {

public:

    MUonECalorimeterDigiDeposit() = default;

    void resetEnergyDeposit() {Crystals.Clear(); }
    void addEnergyDeposit(Int_t crystalColumnID, Int_t crystalRowID, Double_t d); 

    Double_t energyDeposit(Int_t crystalColumnID, Int_t crystalRowID) const;

    void smearEnergies(Double_t resolutionPercentOverRootE);

    Double_t totalEnergyDeposit() const;
    Double_t totalSmearedEnergy() const;

private:

    TClonesArray Crystals{"MUonECalorimeterCrystalDigiDeposit"};

    MUonECalorimeterCrystalDigiDeposit *findCrystal(Int_t crystalColumnID, Int_t crystalRowID) const;

    ClassDef(MUonECalorimeterDigiDeposit, 3)
};

#endif //MUONECALORIMETERDIGIDEPOSIT_H



