#ifndef MUONECALORIMETERDIGITIZATION_H
#define MUONECALORIMETERDIGITIZATION_H


#include "MUonEDetectorConfiguration.h"
#include "MUonECalorimeterDigiDeposit.h"
#include "MUonECalorimeterDigitizationConfiguration.h"

#include "FairTask.h"
#include "Rtypes.h"
#include "TClonesArray.h"

class MUonECalorimeterDigitization : public FairTask {


public:

    MUonECalorimeterDigitization();
    virtual ~MUonECalorimeterDigitization();

	Bool_t setConfiguration(MUonEDetectorConfiguration const& detectorConfig, MUonECalorimeterDigitizationConfiguration const& digitizationConfig);

	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

    virtual InitStatus ReInit();
    virtual InitStatus Init();
	void Register();

    virtual void Exec(Option_t* option);
    virtual void Finish();    


private:
    
    Bool_t m_configuredCorrectly{false};

    MUonEDetectorConfiguration m_detectorConfiguration;
    MUonECalorimeterDigitizationConfiguration m_digitizationConfiguration;

    const TClonesArray* m_calorimeterPoints{nullptr};

    MUonECalorimeterDigiDeposit* m_calorimeterDigiDeposit{nullptr};

    ClassDef(MUonECalorimeterDigitization,2)
};

#endif //MUONECALORIMETERDIGITIZATION_H



