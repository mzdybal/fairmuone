#include "MUonETrackerDigitizationConfiguration.h"


#include <fairlogger/Logger.h>


MUonETrackerDigitizationConfiguration::MUonETrackerDigitizationConfiguration() {

    resetDefaultConfiguration();
}

Bool_t MUonETrackerDigitizationConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    if(config["saveStripDigis"])
        m_saveStripDigis = config["saveStripDigis"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'saveStripDigis' not set. They will be saved by default.";
        m_saveStripDigis = true;            
    }

    if(config["saveStubs"])
        m_saveStubs = config["saveStubs"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'saveStubs' not set. They will be saved by default.";
        m_saveStubs = true;            
    }


    if(!m_configuredCorrectly)
        LOG(info) << "Tracker digitization configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonETrackerDigitizationConfiguration::logCurrentConfiguration() const {

    LOG(info) << "";
	LOG(info) << "Tracker digitization configuration:";
    if(m_saveStripDigis)
        LOG(info) << "Strip digis will be saved.";
    else
        LOG(info) << "Strip digis will not be saved."; 
    if(m_saveStubs)
        LOG(info) << "Stubs will be saved.";
    else
        LOG(info) << "Stubs will not be saved."; 

}

void MUonETrackerDigitizationConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_saveStripDigis = true;
    m_saveStubs = true;
}

ClassImp(MUonETrackerDigitizationConfiguration)


