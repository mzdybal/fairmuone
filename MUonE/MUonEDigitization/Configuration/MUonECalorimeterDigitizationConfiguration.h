#ifndef MUONECALORIMETERDIGITIZATIONCONFIGURATION_H
#define MUONECALORIMETERDIGITIZATIONCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

class MUonECalorimeterDigitizationConfiguration {

public:

    MUonECalorimeterDigitizationConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t saveDeposit() const {return m_saveDeposit;}

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Bool_t m_saveDeposit{true};

    ClassDef(MUonECalorimeterDigitizationConfiguration, 3)
};

#endif //MUONECALORIMETERDIGITIZATIONCONFIGURATION_H

