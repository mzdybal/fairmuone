#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonEDigitizationConfiguration+;
#pragma link C++ class MUonETrackerDigitizationConfiguration+;
#pragma link C++ class MUonECalorimeterDigitizationConfiguration+;



#endif