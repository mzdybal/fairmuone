#ifndef MUONETRACKERDIGITIZATIONCONFIGURATION_H
#define MUONETRACKERDIGITIZATIONCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

class MUonETrackerDigitizationConfiguration {

public:

    MUonETrackerDigitizationConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t saveStripDigis() const {return m_saveStripDigis;}
    Bool_t saveStubs() const {return m_saveStubs;}

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Bool_t m_saveStripDigis{true};
    Bool_t m_saveStubs{true};

    ClassDef(MUonETrackerDigitizationConfiguration, 2)
};

#endif //MUONETRACKERDIGITIZATIONCONFIGURATION_H

