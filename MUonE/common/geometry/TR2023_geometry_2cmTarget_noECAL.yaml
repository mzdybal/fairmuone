---

#Test Run config with module tilt
#all dimensions are in centimeters and angles in degrees


modulesConfiguration:

  geometry:

    sensorMaterial: silicon
    sensorSizeMeasurementDirection: 9.144
    sensorSizePerpendicularDirection: 9.144
    sensorThickness: 0.032
    distanceBetweenSensorCenters: 0.18
    numberOfStrips: 1016


  digitization:

    driftDirectionUpstream: -1 #default -1, >= 0 -> 1, < 0 -> -1
    driftDirectionDownstream: 1 #default 1, >= 0 -> 1, < 0 -> -1
 
    sigma0: 0.00037
    sigmaCoefficient: 1.80
    readoutNoise: 1000 #readout gaussian noise from the electronics in front of the ADC (in units of the electron charge)
    stripThreshold: 6000 #strip threshold in electron units
    halfChannelStripOrder: 1 #the sensor half id which channel order of CBCs are the same as the strip number ordering. 
    maxClusterWidth: 4 #max. cluster width for modules reading. 0: no rejection, [1,4]: clusters with more than max hit strips are rejected.
    ptWidth: 7 #bend for modules reading.   0 to 7 in 0.5 step.
    windowOffset: 3 #window layer offset for modules reading.   [-3,3] in 0.5 step. Need to change sign when changing sign to seedSensor.
    seedSensor: 0 #seed sensor for the stub finding logic. 0 (1) upstream (downstream) sensor. If you change seedSensor you need to change the LUTmap, and change the sign of windowOffset as well
    enableLUT: true #bool value. true (false) enable (disable) LookUp table decoding of the bend information
    LUTmap: #Optimized LookUp table (used for the XY modules in TB22). This LUTmap can be used only with seedSensor = 0. Need to change the LUTmap if the seedSensor is changed
      - {binaryBendCode: 0x7, bendInStripUnits: -7  }
      - {binaryBendCode: 0x7, bendInStripUnits: -6.5}
      - {binaryBendCode: 0x7, bendInStripUnits: -6  }
      - {binaryBendCode: 0x7, bendInStripUnits: -5.5}
      - {binaryBendCode: 0x7, bendInStripUnits: -5  }
      - {binaryBendCode: 0x6, bendInStripUnits: -4.5}
      - {binaryBendCode: 0x5, bendInStripUnits: -4  }
      - {binaryBendCode: 0x4, bendInStripUnits: -3.5}
      - {binaryBendCode: 0x3, bendInStripUnits: -3  }
      - {binaryBendCode: 0x2, bendInStripUnits: -2.5}
      - {binaryBendCode: 0x1, bendInStripUnits: -2  }
      - {binaryBendCode: 0x0, bendInStripUnits: -1.5}
      - {binaryBendCode: 0x9, bendInStripUnits: -1  }
      - {binaryBendCode: 0xA, bendInStripUnits: -0.5}
      - {binaryBendCode: 0xB, bendInStripUnits:  0  }
      - {binaryBendCode: 0xC, bendInStripUnits:  0.5}
      - {binaryBendCode: 0xD, bendInStripUnits:  1  }
      - {binaryBendCode: 0xE, bendInStripUnits:  1.5}
      - {binaryBendCode: 0xF, bendInStripUnits:  2  }
      - {binaryBendCode: 0xF, bendInStripUnits:  2.5}
      - {binaryBendCode: 0xF, bendInStripUnits:  3  }
      - {binaryBendCode: 0xF, bendInStripUnits:  3.5}
      - {binaryBendCode: 0xF, bendInStripUnits:  4  }
      - {binaryBendCode: 0xF, bendInStripUnits:  4.5}
      - {binaryBendCode: 0xF, bendInStripUnits:  5  }
      - {binaryBendCode: 0xF, bendInStripUnits:  5.5}
      - {binaryBendCode: 0xF, bendInStripUnits:  6  }
      - {binaryBendCode: 0xF, bendInStripUnits:  6.5}
      - {binaryBendCode: 0xF, bendInStripUnits:  7  }

targetsConfiguration:

  targetMaterial: GraphiteR4550
  width: 9
  height: 9
  thickness: 2


calorimeterConfiguration:

  geometry:

    crystalMaterial: PbWO4

    crystalWidthUpstream: 2.86
    crystalHeightUpstream: 2.86

    crystalWidthDownstream: 3.00
    crystalHeightDownstream: 3.00

    crystalThickness: 22

    numberOfColumns: 5
    numberOfRows: 5

    distanceBetweenCrystalCenters: 3

  digitization:

    resolution: 4.2 #percent over sqrt(E)

detector:

  projections: 
  #the directions are fixed to angles wrt. x axis (x,y,u,v) = (0, 90, 135, 45)
  #negateDataHitPosition flag can be used to change the sign of hit position for data reconstruction
  #it's ignored for MC
    
    x:
      tilt: 13.35 
      negateDataHitPosition: true

    y: 
      tilt: 13.35


  stations:

    - #distance from beam profile to muone trolley... 8.1 m;
      origin: 810 #absolute position along the z axis of the origin of local coordinate system
      #targetRelativePosition: 0 #all other positions are relative to origin and are positions of the midpoints
      modules:
        - {projection: x, relativePosition: 18.0218, hitResolution: 0.0012, seedSensor: 0, ptWidth: 7, windowOffset: -3, 
                          LUTmap: [{binaryBendCode: 0x7, bendInStripUnits: -7  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -6  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -5  },
                                   {binaryBendCode: 0x6, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0x5, bendInStripUnits: -4  },
                                   {binaryBendCode: 0x4, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0x3, bendInStripUnits: -3  },
                                   {binaryBendCode: 0x2, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0x1, bendInStripUnits: -2  },
                                   {binaryBendCode: 0x0, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0x9, bendInStripUnits: -1  },
                                   {binaryBendCode: 0xA, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0xB, bendInStripUnits:  0  },
                                   {binaryBendCode: 0xC, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0xD, bendInStripUnits:  1  },
                                   {binaryBendCode: 0xE, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  2  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  3  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  4  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  5  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  6  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  7  }]
          }
        - {projection: y, relativePosition: 21.8693, hitResolution: 0.0012, seedSensor: 1, ptWidth: 7, windowOffset: 3,
                          LUTmap: [{binaryBendCode: 0xF, bendInStripUnits: -7  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -6  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -5  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -4  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -3  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -2  },
                                   {binaryBendCode: 0xE, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0xD, bendInStripUnits: -1  },
                                   {binaryBendCode: 0xC, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0xB, bendInStripUnits:  0  },
                                   {binaryBendCode: 0xA, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0x9, bendInStripUnits:  1  },
                                   {binaryBendCode: 0x0, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0x1, bendInStripUnits:  2  },
                                   {binaryBendCode: 0x2, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0x3, bendInStripUnits:  3  },
                                   {binaryBendCode: 0x4, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0x5, bendInStripUnits:  4  },
                                   {binaryBendCode: 0x6, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  5  },
                                   {binaryBendCode: 0x7, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  6  },
                                   {binaryBendCode: 0x7, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  7  }]
          }

        - {projection: u, relativePosition: 55.3635, hitResolution: 0.0026, seedSensor: 1, ptWidth: 7, windowOffset: 0,
                          LUTmap: [{binaryBendCode: 0x7, bendInStripUnits: -7  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -6  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -5  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -4  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0x6, bendInStripUnits: -3  },
                                   {binaryBendCode: 0x5, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0x4, bendInStripUnits: -2  },
                                   {binaryBendCode: 0x3, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0x2, bendInStripUnits: -1  },
                                   {binaryBendCode: 0x1, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0x0, bendInStripUnits:  0  },
                                   {binaryBendCode: 0x9, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0xA, bendInStripUnits:  1  },
                                   {binaryBendCode: 0xB, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0xC, bendInStripUnits:  2  },
                                   {binaryBendCode: 0xD, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0xE, bendInStripUnits:  3  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  4  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  5  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  6  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  7  }]
          }
        - {projection: v, relativePosition: 56.6205, hitResolution: 0.0026, seedSensor: 0, ptWidth: 7, windowOffset: 0,
                          LUTmap: [{binaryBendCode: 0x7, bendInStripUnits: -7  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -6  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -5  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -4  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0x6, bendInStripUnits: -3  },
                                   {binaryBendCode: 0x5, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0x4, bendInStripUnits: -2  },
                                   {binaryBendCode: 0x3, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0x2, bendInStripUnits: -1  },
                                   {binaryBendCode: 0x1, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0x0, bendInStripUnits:  0  },
                                   {binaryBendCode: 0x9, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0xA, bendInStripUnits:  1  },
                                   {binaryBendCode: 0xB, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0xC, bendInStripUnits:  2  },
                                   {binaryBendCode: 0xD, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0xE, bendInStripUnits:  3  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  4  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  5  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  6  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  7  }]
          }

        - {projection: x, relativePosition: 89.9218, hitResolution: 0.0012, seedSensor: 0, ptWidth: 7, windowOffset: -3, 
                          LUTmap: [{binaryBendCode: 0x7, bendInStripUnits: -7  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -6  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -5  },
                                   {binaryBendCode: 0x6, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0x5, bendInStripUnits: -4  },
                                   {binaryBendCode: 0x4, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0x3, bendInStripUnits: -3  },
                                   {binaryBendCode: 0x2, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0x1, bendInStripUnits: -2  },
                                   {binaryBendCode: 0x0, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0x9, bendInStripUnits: -1  },
                                   {binaryBendCode: 0xA, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0xB, bendInStripUnits:  0  },
                                   {binaryBendCode: 0xC, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0xD, bendInStripUnits:  1  },
                                   {binaryBendCode: 0xE, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  2  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  3  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  4  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  5  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  6  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  7  }]
          }
        - {projection: y, relativePosition: 93.7693, hitResolution: 0.0012, seedSensor: 1, ptWidth: 7, windowOffset: 3,
                          LUTmap: [{binaryBendCode: 0xF, bendInStripUnits: -7  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -6  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -5  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -4  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -3  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -2  },
                                   {binaryBendCode: 0xE, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0xD, bendInStripUnits: -1  },
                                   {binaryBendCode: 0xC, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0xB, bendInStripUnits:  0  },
                                   {binaryBendCode: 0xA, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0x9, bendInStripUnits:  1  },
                                   {binaryBendCode: 0x0, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0x1, bendInStripUnits:  2  },
                                   {binaryBendCode: 0x2, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0x3, bendInStripUnits:  3  },
                                   {binaryBendCode: 0x4, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0x5, bendInStripUnits:  4  },
                                   {binaryBendCode: 0x6, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  5  },
                                   {binaryBendCode: 0x7, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  6  },
                                   {binaryBendCode: 0x7, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  7  }]
          }







    - #distance from beam profile to muone trolley... 8.1 m + 1.012m; 
      origin: 911.2 #absolute position along the z axis of the origin of local coordinate system
      targetRelativePosition: 1 #all other positions are relative to origin and are positions of the midpoints
      modules:
        - {projection: x, relativePosition: 18.0218, hitResolution: 0.0012, seedSensor: 0, ptWidth: 7, windowOffset: -3, 
                          LUTmap: [{binaryBendCode: 0x7, bendInStripUnits: -7  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -6  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -5  },
                                   {binaryBendCode: 0x6, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0x5, bendInStripUnits: -4  },
                                   {binaryBendCode: 0x4, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0x3, bendInStripUnits: -3  },
                                   {binaryBendCode: 0x2, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0x1, bendInStripUnits: -2  },
                                   {binaryBendCode: 0x0, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0x9, bendInStripUnits: -1  },
                                   {binaryBendCode: 0xA, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0xB, bendInStripUnits:  0  },
                                   {binaryBendCode: 0xC, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0xD, bendInStripUnits:  1  },
                                   {binaryBendCode: 0xE, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  2  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  3  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  4  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  5  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  6  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  7  }]
          }
        - {projection: y, relativePosition: 21.8693, hitResolution: 0.0012, seedSensor: 1, ptWidth: 7, windowOffset: 3,
                          LUTmap: [{binaryBendCode: 0xF, bendInStripUnits: -7  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -6  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -5  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -4  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -3  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -2  },
                                   {binaryBendCode: 0xE, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0xD, bendInStripUnits: -1  },
                                   {binaryBendCode: 0xC, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0xB, bendInStripUnits:  0  },
                                   {binaryBendCode: 0xA, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0x9, bendInStripUnits:  1  },
                                   {binaryBendCode: 0x0, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0x1, bendInStripUnits:  2  },
                                   {binaryBendCode: 0x2, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0x3, bendInStripUnits:  3  },
                                   {binaryBendCode: 0x4, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0x5, bendInStripUnits:  4  },
                                   {binaryBendCode: 0x6, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  5  },
                                   {binaryBendCode: 0x7, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  6  },
                                   {binaryBendCode: 0x7, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  7  }]
          }
  
        - {projection: u, relativePosition: 55.2535, hitResolution: 0.0026, distanceBetweenSensorCenters: 0.4, seedSensor: 1, ptWidth: 7, windowOffset: 0,
                          LUTmap: [{binaryBendCode: 0x7, bendInStripUnits: -7  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -6  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -5  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -4  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0x6, bendInStripUnits: -3  },
                                   {binaryBendCode: 0x5, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0x4, bendInStripUnits: -2  },
                                   {binaryBendCode: 0x3, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0x2, bendInStripUnits: -1  },
                                   {binaryBendCode: 0x1, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0x0, bendInStripUnits:  0  },
                                   {binaryBendCode: 0x9, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0xA, bendInStripUnits:  1  },
                                   {binaryBendCode: 0xB, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0xC, bendInStripUnits:  2  },
                                   {binaryBendCode: 0xD, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0xE, bendInStripUnits:  3  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  4  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  5  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  6  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  7  }]
          }
        - {projection: v, relativePosition: 56.6205, hitResolution: 0.0026, seedSensor: 0, ptWidth: 7, windowOffset: 0,
                          LUTmap: [{binaryBendCode: 0x7, bendInStripUnits: -7  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -6  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -5  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -4  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0x6, bendInStripUnits: -3  },
                                   {binaryBendCode: 0x5, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0x4, bendInStripUnits: -2  },
                                   {binaryBendCode: 0x3, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0x2, bendInStripUnits: -1  },
                                   {binaryBendCode: 0x1, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0x0, bendInStripUnits:  0  },
                                   {binaryBendCode: 0x9, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0xA, bendInStripUnits:  1  },
                                   {binaryBendCode: 0xB, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0xC, bendInStripUnits:  2  },
                                   {binaryBendCode: 0xD, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0xE, bendInStripUnits:  3  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  4  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  5  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  6  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  7  }]
          }

        - {projection: x, relativePosition: 89.9218, hitResolution: 0.0012, seedSensor: 0, ptWidth: 7, windowOffset: -3, 
                          LUTmap: [{binaryBendCode: 0x7, bendInStripUnits: -7  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -6  },
                                   {binaryBendCode: 0x7, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits: -5  },
                                   {binaryBendCode: 0x6, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0x5, bendInStripUnits: -4  },
                                   {binaryBendCode: 0x4, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0x3, bendInStripUnits: -3  },
                                   {binaryBendCode: 0x2, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0x1, bendInStripUnits: -2  },
                                   {binaryBendCode: 0x0, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0x9, bendInStripUnits: -1  },
                                   {binaryBendCode: 0xA, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0xB, bendInStripUnits:  0  },
                                   {binaryBendCode: 0xC, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0xD, bendInStripUnits:  1  },
                                   {binaryBendCode: 0xE, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  2  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  3  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  4  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  5  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  6  },
                                   {binaryBendCode: 0xF, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits:  7  }]
          }
        - {projection: y, relativePosition: 93.7693, hitResolution: 0.0012, seedSensor: 1, ptWidth: 7, windowOffset: 3,
                          LUTmap: [{binaryBendCode: 0xF, bendInStripUnits: -7  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -6.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -6  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -5.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -5  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -4.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -4  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -3.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -3  },
                                   {binaryBendCode: 0xF, bendInStripUnits: -2.5},
                                   {binaryBendCode: 0xF, bendInStripUnits: -2  },
                                   {binaryBendCode: 0xE, bendInStripUnits: -1.5},
                                   {binaryBendCode: 0xD, bendInStripUnits: -1  },
                                   {binaryBendCode: 0xC, bendInStripUnits: -0.5},
                                   {binaryBendCode: 0xB, bendInStripUnits:  0  },
                                   {binaryBendCode: 0xA, bendInStripUnits:  0.5},
                                   {binaryBendCode: 0x9, bendInStripUnits:  1  },
                                   {binaryBendCode: 0x0, bendInStripUnits:  1.5},
                                   {binaryBendCode: 0x1, bendInStripUnits:  2  },
                                   {binaryBendCode: 0x2, bendInStripUnits:  2.5},
                                   {binaryBendCode: 0x3, bendInStripUnits:  3  },
                                   {binaryBendCode: 0x4, bendInStripUnits:  3.5},
                                   {binaryBendCode: 0x5, bendInStripUnits:  4  },
                                   {binaryBendCode: 0x6, bendInStripUnits:  4.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  5  },
                                   {binaryBendCode: 0x7, bendInStripUnits:  5.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  6  },
                                   {binaryBendCode: 0x7, bendInStripUnits:  6.5},
                                   {binaryBendCode: 0x7, bendInStripUnits:  7  }]
          }

#8.1m + 1.012m + 1.1125 = 10.2245
#  calorimeterPosition: 1022.45 #remove this line to remove calorimeter
