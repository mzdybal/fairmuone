
#run with runProductionJob.C macro (root -l 'runProductionJob.C("exampleProductionJob")'
#the yaml extension is not required, will be added automatically
#the job file is first looked for in the run directory and if not found, in the default path (common/jobs)

#the options to save different containers only affect the final ntuple, they are still passed from stage to stage in memory
#i.e. the reconstruction can still be performed just fine even if saveStubs is set to false
#the final ntuple will then contain the reconstruction output, but not the stubs container

outputFile: MCsignal_SIM-DIGI.root #used as output filename
saveParametersFile: false #required for event display to draw detector geometry, not used otherwise
saveGeoTracks: false #required for event display to draw tracks, off by default as the current default Geant physics list produces accurate cascades in the calo and thus A LOT of tracks
numberOfEvents: 10 #required for generation, you can also set it for jobs without generation if you want to process only the first N events
detectorConfiguration: TR2023_geometry_3cmTarget_noECAL #no need to give extension (added automatically), configs in run directory are prioritized, if not found, they are looked for in the default directory (common/geometry)

#inputFiles #in case you're not running generation

runGeneration: true
generationConfiguration:

  geantConfiguration: geant4DefaultConfiguration #remove to use default configuration, the one set here explicitly, as with detector configurations, these are first looked for in the run directory and then in (common/gconfig)

  saveTrackerPoints: false #save container of tracker Geant-level hits
  saveCalorimeterPoints: false #save container of calorimeter Geant-level hits
  saveMCTracks: true #save containers of MCTracks, off by default as the current default Geant physics list produces accurate cascades in the calo and thus A LOT of tracks
  saveSignalTracks: false #save a separate container with MCTracks of signal particles, including the beam particle # all infos are already in MCTracks
  savePileupTracks: false #save a separate container with MCTracks of pileup particles # all infos are already in MCTracks

  pileupMean: 0 #delete or set to <= 0 to remove pileup, used as a mean for Poisson distribution


  #in general adding particles requires momentum and vertex calculations require beam energy
  #in case of generic generators like ParticleGunBox, you can either provide the momentum or energy (but not both)
  #in the latter case you also need to provide particle mass in GeV, in the former you don't have to
  #but you will not be able to force the interaction if you don't

  #BeamProfile generators (like ParticleGunBeamProfile) get the momentum from beam profile, 
  #so you only need to provide mass if you want to force interaction

  #for convenience, muon and pion guns have been also defined
  #they use the same base classes but have the pdg and mass hardcoded,
  #so you only need to provide momentum or energy for Box guns
  #or nothing at all for BeamProfile guns

  #available generators: ParticleGunBox, ParticleGunBeamProfile, MuonGunBox, PionGunBox, MuonGunBeamProfile, PionGunBeamProfile

  #beamGenerator: ParticleGunBox
  #beamGeneratorConfiguration:
  #  xMin: -4
  #  xMax: 4
  #  yMin: -4
  #  yMax: 4
  #  z: -30
  #  pdg: 13
  #  energy: 167
  #  mass: 0.1056583715
  #  thetaMin: 0
  #  thetaMax: 1e-10

#  beamGenerator: MuonGunBox #in case of box generators, the values are drawn with uniform probability from the range (min, max)
#  beamGeneratorConfiguration: #the phi angle (rotations around the beam axis) is taken with uniform probability from the range (0, 360) degrees 
#    xMin: -4
#    xMax: 4
#    yMin: -4
#    yMax: 4
#    z: 0
#    energy: 167
#    thetaMin: 0
#    thetaMax: 1e-10  

  #  beamGenerator: MuonGunBeamProfile
  #  beamGeneratorConfiguration:
  #    beamProfile: beamProfileTR

  beamGenerator: ParticleGunBeamProfile
  beamGeneratorConfiguration:
    pdg: -13
    mass: 0.1056583715
    beamProfile: beamProfile_narrow

  forceInteraction: true #set to false to produce minbias
  vertexStationIndex: 1 #the vertex will be produced in the target of this station (as defined in the detector model, but remember the stations are sorted by their Z position, so it's an actual nth station, not the station defined as nth in the model)
  vertexGenerator: Mesmer
  vertexGeneratorConfiguration:
    mode: weighted
    ord: alpha2 # alternative: alpha
    nwarmup: 100000 # do not lower this, or in alternative comment this line and give in input wnorm if calculated with higher precision in advance
    Qmu: +1 #to match beam generators
    Eemin: 0.2
  
  #vertexGenerator: LOSimple
  #vertexGeneratorConfiguration:
  #  constantEnergy: true #recalculates the cross-section histogram for variable energy if set to false (generation with this generator and variable energy is veeeeery slow)
  #  crossSectionElectronEnergyCut: 1 #this should be set to 1 to get sensible results
  #  electronMomentumCut: 1 #only events with electron momentum above this value will be generated



runDigitization: true
digitizationConfiguration:

  tracker:

    saveStripDigis: false #save container of fired strips
    saveStubs: true #save container of stubs
    maxNumberOfLinkedTracks: -1 #negative value to include all tracks with deposits

  calorimeter:

    saveDeposit: false #save calorimeter deposit
  
runReconstruction: false
reconstructionConfiguration:

  isMC: true #does additional stuff for MC events, like track linking
  verbose: false

  useSeedingSensorOnly: false #set to true if you don't want to use bend info in reco

  reassignHitsAfterKinematicFit: false

  runAdaptiveVertexFitter: true

  adaptiveFitterSeedWindow: 0.2
  adaptiveFitterSeedTrackWindow: 0.3
  adaptiveFitterMaxNumberOfIterations: 20
  adaptiveFitterMaxTrackChi2: 25
  adaptiveFitterConvergenceMaxDeltaZ: 0.01 

  weightLinkedTracksByDepositedCharge: false
  allowTrivialIncomingTracks: false #allows for tracks without stereo hits; implemented for testrun

  # xyHitAssignmentWindow: 0.2 #cm
  # uvHitAssignmentWindow: 0.3 #cm

  maxNumberOfSharedHits: 2 
  maxOutlierChi2: -1


runEventFilter: false 
eventFilterConfiguration:

  generatedEvents:


  digitizedEvents:


  reconstructedEvents:
