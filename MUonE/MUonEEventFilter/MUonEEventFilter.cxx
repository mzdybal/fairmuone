#include "MUonEEventFilter.h"

#include "FairRootManager.h"
#include "FairMCApplication.h"

MUonEEventFilter::MUonEEventFilter()
	: FairTask("MUonEEventFilter", 0)
	{
		resetDefaultConfiguration();
	}


MUonEEventFilter::~MUonEEventFilter()
{
}

Bool_t MUonEEventFilter::setConfiguration(MUonEEventFilterConfiguration const& config) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;
	m_configuredCorrectly = config.configuredCorrectly();
	
	if(config.hasGeneratedEventsFilter()) {
		if(!m_generated.setConfiguration(config.generated()))
			m_configuredCorrectly = false;
	}

	if(config.hasDigitizedEventsFilter()) {
		if(!m_digitized.setConfiguration(config.digitized()))
			m_configuredCorrectly = false;
	}

	if(config.hasReconstructedEventsFilter()) {
		if(!m_reconstructed.setConfiguration(config.reconstructed()))
			m_configuredCorrectly = false;
	}	

    if(!m_configuredCorrectly)
        LOG(info) << "eventFilterConfiguration section contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}

void MUonEEventFilter::logCurrentConfiguration() {

	LOG(info) << "";
	m_generated.logCurrentConfiguration();
	m_digitized.logCurrentConfiguration();
	m_reconstructed.logCurrentConfiguration();	
}

void MUonEEventFilter::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_generated.resetDefaultConfiguration();
	m_digitized.resetDefaultConfiguration();
	m_reconstructed.resetDefaultConfiguration();
}


InitStatus MUonEEventFilter::ReInit()
{
	return kSUCCESS;
}

InitStatus MUonEEventFilter::Init()
{
	LOG(info) << "Initializing event filter.";

	FairRootManager* ioman = FairRootManager::Instance();

	if(!ioman) 
	{ 
		LOG(fatal) << "No FairRootManager"; 
		return kERROR;
	} 

	m_generated.init(ioman);
	m_digitized.init(ioman);
	m_reconstructed.init(ioman);

	return kSUCCESS;
}

void MUonEEventFilter::Exec(Option_t* option) 
{
    ++m_allEventsCounter;

	if(!m_generated.saveEvent()) {

    	FairMCApplication::Instance()->SetSaveCurrentEvent(false);
		return;
	}

	if(!m_digitized.saveEvent()) {

    	FairMCApplication::Instance()->SetSaveCurrentEvent(false);
		return;
	}

	if(!m_reconstructed.saveEvent()) {

    	FairMCApplication::Instance()->SetSaveCurrentEvent(false);
		return;
	}

    FairMCApplication::Instance()->SetSaveCurrentEvent(true);
    ++m_passedEventsCounter;
}

void MUonEEventFilter::Finish() 
{
	m_generated.finish();
	m_digitized.finish();
	m_reconstructed.finish();
    LOG(info) << "Combined results of event filters: " << m_passedEventsCounter << " events out of " << m_allEventsCounter << " passed. Filter efficiency: " << efficiency();
}

ClassImp(MUonEEventFilter)


