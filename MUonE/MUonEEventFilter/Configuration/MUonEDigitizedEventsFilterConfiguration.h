#ifndef MUONEDIGITIZEDEVENTSFILTERCONFIGURATION_H
#define MUONEDIGITIZEDEVENTSFILTERCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

class MUonEDigitizedEventsFilterConfiguration {

public:

    MUonEDigitizedEventsFilterConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t isActive() const {return m_isActive;}

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Bool_t m_isActive{false};


    ClassDef(MUonEDigitizedEventsFilterConfiguration, 2)
};

#endif //MUONEDIGITIZEDEVENTSFILTERCONFIGURATION_H
