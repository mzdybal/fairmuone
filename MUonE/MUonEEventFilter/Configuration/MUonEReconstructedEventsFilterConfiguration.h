#ifndef MUONERECONSTRUCTEDEVENTSFILTERCONFIGURATION_H
#define MUONERECONSTRUCTEDEVENTSFILTERCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

class MUonEReconstructedEventsFilterConfiguration {

public:

    MUonEReconstructedEventsFilterConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t isActive() const {return m_isActive;}

    Bool_t saveNotReconstructedEventsActive() const {return m_saveNotReconstructedEventsActive;}
    Bool_t saveNotReconstructedEvents() const {return m_saveNotReconstructedEvents;}

    Bool_t maxBestVertexChi2PerNdfActive() const {return m_maxBestVertexChi2PerNdfActive;}
    Double_t maxBestVertexChi2PerNdf() const {return m_maxBestVertexChi2PerNdf;} 

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Bool_t m_isActive{false};

    Bool_t m_saveNotReconstructedEventsActive{false};
    Bool_t m_saveNotReconstructedEvents;

    Bool_t m_maxBestVertexChi2PerNdfActive{false};
    Double_t m_maxBestVertexChi2PerNdf;


    ClassDef(MUonEReconstructedEventsFilterConfiguration, 2)
};


#endif //MUONERECONSTRUCTEDEVENTSFILTERCONFIGURATION_H

