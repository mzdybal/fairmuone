#include "MUonEEventFilterConfiguration.h"

#include <fairlogger/Logger.h>


MUonEEventFilterConfiguration::MUonEEventFilterConfiguration() {

    resetDefaultConfiguration();
}

Bool_t MUonEEventFilterConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;


    if(config["generatedEvents"]) {

        if(!m_generated.readConfiguration(config["generatedEvents"]))
            m_configuredCorrectly = false;
        else
            m_hasGeneratedEventsFilter = true;
    }

    if(config["digitizedEvents"]) {

        if(!m_digitized.readConfiguration(config["digitizedEvents"]))
            m_configuredCorrectly = false;
        else
            m_hasDigitizedEventsFilter = true;
    }

    if(config["reconstructedEvents"]) {

        if(!m_reconstructed.readConfiguration(config["reconstructedEvents"]))
            m_configuredCorrectly = false;
        else
            m_hasReconstructedEventsFilter = true;
    }


    if(!m_configuredCorrectly)
        LOG(info) << "Event filter configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEEventFilterConfiguration::logCurrentConfiguration() const {
    
    m_generated.logCurrentConfiguration();
    m_digitized.logCurrentConfiguration();
    m_reconstructed.logCurrentConfiguration();
}

void MUonEEventFilterConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;
 
    m_hasGeneratedEventsFilter = false;
    m_generated.resetDefaultConfiguration();

    m_hasDigitizedEventsFilter = false;
    m_digitized.resetDefaultConfiguration();

    m_hasReconstructedEventsFilter = false;
    m_reconstructed.resetDefaultConfiguration(); 
}

ClassImp(MUonEEventFilterConfiguration)