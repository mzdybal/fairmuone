#ifndef MUONEGENERATEDEVENTSFILTERCONFIGURATION_H
#define MUONEGENERATEDEVENTSFILTERCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

class MUonEGeneratedEventsFilterConfiguration {

public:

    MUonEGeneratedEventsFilterConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t isActive() const {return m_isActive;}


    Bool_t minNumberOfMonteCarloTracksActive() const {return m_minNumberOfMonteCarloTracksActive;}
    Int_t minNumberOfMonteCarloTracks() const {return m_minNumberOfMonteCarloTracks;}

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Bool_t m_isActive{false};

    Bool_t m_minNumberOfMonteCarloTracksActive{false};
    Int_t m_minNumberOfMonteCarloTracks{0};


    ClassDef(MUonEGeneratedEventsFilterConfiguration, 2)
};

#endif //MUONEGENERATEDEVENTSFILTERCONFIGURATION_H

