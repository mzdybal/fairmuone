#ifndef MUONERECONSTRUCTEDEVENTSFILTER_H
#define MUONERECONSTRUCTEDEVENTSFILTER_H

#include "MUonEReconstructedEventsFilterConfiguration.h"
#include "MUonERecoOutput.h"
#include "FairRootManager.h"

class MUonEReconstructedEventsFilter {

public:

    MUonEReconstructedEventsFilter();

	Bool_t setConfiguration(MUonEReconstructedEventsFilterConfiguration const& config);

    Bool_t isActive() const {return m_isActive;}
	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t reconstructionOutputFound() const {return m_reconstructionOutputFound;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

    Bool_t init(FairRootManager* ioman);
    Bool_t saveEvent();

    void finish();

    Double_t efficiency() const {return 0 == m_allEventsCounter ? 1 : static_cast<Double_t>(m_passedEventsCounter) / m_allEventsCounter;}


private:

    Bool_t m_isActive{false};
    Bool_t m_configuredCorrectly{false};

    MUonEReconstructedEventsFilterConfiguration m_configuration;

    Bool_t m_reconstructionOutputFound{false};
    const MUonERecoOutput* m_reconstructionOutput{nullptr};

    Int_t m_allEventsCounter{0};
    Int_t m_passedEventsCounter{0};

    ClassDef(MUonEReconstructedEventsFilter,2)


};

#endif //MUONERECONSTRUCTEDEVENTSFILTER_H

