#include "MUonEReconstructedEventsFilter.h"

#include <iostream>
#include "FairRootManager.h"
#include <fairlogger/Logger.h>

MUonEReconstructedEventsFilter::MUonEReconstructedEventsFilter() 
{
    resetDefaultConfiguration();
}


Bool_t MUonEReconstructedEventsFilter::setConfiguration(MUonEReconstructedEventsFilterConfiguration const& config) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;

	m_configuration = config;
    m_configuredCorrectly = m_configuration.configuredCorrectly();
    m_isActive = config.isActive();

    if(!m_configuredCorrectly)
        LOG(info) << "eventFilterConfiguration/reconstructedEvents section contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}

void MUonEReconstructedEventsFilter::logCurrentConfiguration() {

	m_configuration.logCurrentConfiguration();
}

void MUonEReconstructedEventsFilter::resetDefaultConfiguration() {

	m_configuredCorrectly = false;
    m_isActive = false;

	m_configuration.resetDefaultConfiguration();
}


Bool_t MUonEReconstructedEventsFilter::init(FairRootManager* ioman)
{
    if(!m_isActive)
        return false;

	m_reconstructionOutput = ioman->InitObjectAs<const MUonERecoOutput*>("ReconstructionOutput");

    if(nullptr != m_reconstructionOutput) {
        m_reconstructionOutputFound = true;
    }
    else {
        m_reconstructionOutputFound = false;
        LOG(info) << "ReconstructionOutput not found in the ntuple. Reconstructed events filter will be ommited.";
    }

    if(!m_reconstructionOutputFound) {

        LOG(warning) << "Reconstructed events filter defined in job configuration, but reconstruction output not present in the input. Filter will be deactivated.";
        m_isActive = false;
        return false;
    }

    return true;
}

Bool_t MUonEReconstructedEventsFilter::saveEvent() 
{

    ++m_allEventsCounter;

    if(!m_isActive)
        return true;


    if(m_reconstructionOutputFound) {

        if(m_configuration.saveNotReconstructedEventsActive()) {

            if(!m_configuration.saveNotReconstructedEvents() && !m_reconstructionOutput->isReconstructed())
                return false;
        }

        if(m_configuration.maxBestVertexChi2PerNdfActive()) {

            if(m_reconstructionOutput->isReconstructed() && m_reconstructionOutput->bestVertex().chi2perDegreeOfFreedom() > m_configuration.maxBestVertexChi2PerNdf())
                return false;
        }
    }


    ++m_passedEventsCounter;
    return true;
}

void MUonEReconstructedEventsFilter::finish() 
{
    if(m_isActive)
        LOG(info) << "Reconstructed events filter: " << m_passedEventsCounter << " events out of " << m_allEventsCounter << " passed. Filter efficiency: " << efficiency();
}

ClassImp(MUonEReconstructedEventsFilter)