#ifndef MUONEGENERATEDEVENTSFILTER_H
#define MUONEGENERATEDEVENTSFILTER_H

#include "MUonEGeneratedEventsFilterConfiguration.h"
#include "MUonETrackerPoint.h"
#include "MUonECalorimeterPoint.h"
#include "TClonesArray.h"
#include "FairRootManager.h"

class MUonEGeneratedEventsFilter {

public:

    MUonEGeneratedEventsFilter();

	Bool_t setConfiguration(MUonEGeneratedEventsFilterConfiguration const& config);

    Bool_t isActive() const {return m_isActive;}
	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t mcTracksFound() const {return m_mcTracksFound;}
    Bool_t trackerPointsFound() const {return m_trackerPointsFound;}
    Bool_t calorimeterPointsFound() const {return m_calorimeterPointsFound;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

    Bool_t init(FairRootManager* ioman);
    Bool_t saveEvent();

    void finish();

    Double_t efficiency() const {return 0 == m_allEventsCounter ? 1 : static_cast<Double_t>(m_passedEventsCounter) / m_allEventsCounter;}


private:

    Bool_t m_isActive{false};
    Bool_t m_configuredCorrectly{false};

    MUonEGeneratedEventsFilterConfiguration m_configuration;

    Bool_t m_mcTracksFound{false};
    TClonesArray* m_mcTracks{nullptr};

    Bool_t m_trackerPointsFound{false};
    TClonesArray* m_trackerPoints{nullptr};

    Bool_t m_calorimeterPointsFound{false};
    TClonesArray* m_calorimeterPoints{nullptr};

    Int_t m_allEventsCounter{0};
    Int_t m_passedEventsCounter{0};

    ClassDef(MUonEGeneratedEventsFilter,2)


};

#endif //MUONEGENERATEDEVENTSFILTER_H
