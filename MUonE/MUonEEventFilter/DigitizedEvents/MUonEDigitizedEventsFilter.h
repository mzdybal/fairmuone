#ifndef MUONEDIGITIZEDEVENTSFILTER_H
#define MUONEDIGITIZEDEVENTSFILTER_H

#include "MUonEDigitizedEventsFilterConfiguration.h"
#include "MUonECalorimeterDigiDeposit.h"
#include "MUonETrackerStub.h"
#include "TClonesArray.h"
#include "FairRootManager.h"

class MUonEDigitizedEventsFilter {

public:

    MUonEDigitizedEventsFilter();

	Bool_t setConfiguration(MUonEDigitizedEventsFilterConfiguration const& config);

    Bool_t isActive() const {return m_isActive;}
	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t stubsFound() const {return m_stubsFound;}
    Bool_t calorimeterDepositFound() const {return m_calorimeterDepositFound;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

    Bool_t init(FairRootManager* ioman);
    Bool_t saveEvent();

    void finish();

    Double_t efficiency() const {return 0 == m_allEventsCounter ? 1 : static_cast<Double_t>(m_passedEventsCounter) / m_allEventsCounter;}


private:

    Bool_t m_isActive{false};
    Bool_t m_configuredCorrectly{false};

    MUonEDigitizedEventsFilterConfiguration m_configuration;

    Bool_t m_stubsFound{false};
    TClonesArray* m_stubs{nullptr};

    Bool_t m_calorimeterDepositFound{false};
    const MUonECalorimeterDigiDeposit* m_calorimeterDeposit{nullptr};

    Int_t m_allEventsCounter{0};
    Int_t m_passedEventsCounter{0};

    ClassDef(MUonEDigitizedEventsFilter,2)


};

#endif //MUONEDIGITIZEDEVENTSFILTER_H

