#ifndef MUONEEVENTFILTER_H
#define MUONEEVENTFILTER_H


#include "FairTask.h"
#include "Rtypes.h"

#include "MUonEEventFilterConfiguration.h"
#include "MUonEGeneratedEventsFilter.h"
#include "MUonEDigitizedEventsFilter.h"
#include "MUonEReconstructedEventsFilter.h"


class MUonEEventFilter : public FairTask {

public:

    MUonEEventFilter();
    virtual ~MUonEEventFilter();

	Bool_t setConfiguration(MUonEEventFilterConfiguration const& config);

	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

    virtual InitStatus ReInit();
    virtual InitStatus Init();
    virtual void Exec(Option_t* option);
    virtual void Finish();

    Double_t efficiency() const {return 0 == m_allEventsCounter ? 0 : static_cast<Double_t>(m_passedEventsCounter) / m_allEventsCounter;}


private:

    Bool_t m_configuredCorrectly{false};

    MUonEGeneratedEventsFilter m_generated;
    MUonEDigitizedEventsFilter m_digitized;
    MUonEReconstructedEventsFilter m_reconstructed;
    
    Int_t m_allEventsCounter{0};
    Int_t m_passedEventsCounter{0};

    ClassDef(MUonEEventFilter,2)


};


#endif //MUONEEVENTFILTER_H



