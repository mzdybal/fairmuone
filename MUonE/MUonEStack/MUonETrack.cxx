
#include "MUonETrack.h"

#include "TDatabasePDG.h"               // for TDatabasePDG
#include "TParticlePDG.h"               // for TParticlePDG



MUonETrack::MUonETrack()
  : TObject(),
    PdgCode(0), MotherID(0), InteractionID(0), Mass(0),
    Px(0), Py(0), Pz(0), StartX(0), StartY(0), StartZ(0), StationIndex(-1), StopAtTarget(false), SignalParticle(false), PileupParticle(false)
{
}


MUonETrack::MUonETrack(Int_t pdg, Int_t motherid, Int_t interactionid, Double_t px, Double_t py,
  Double_t pz, Double_t x, Double_t y, Double_t z, Int_t stationindex, Bool_t stopattarget, Bool_t signalparticle, Bool_t pileupparticle)
  : TObject(),
    PdgCode(pdg), MotherID(motherid), InteractionID(interactionid), Mass(0),
    Px(px), Py(py), Pz(pz), StartX(x), StartY(y), StartZ(z), StationIndex(stationindex), StopAtTarget(stopattarget), SignalParticle(signalparticle), PileupParticle(pileupparticle)
{
  if (TDatabasePDG::Instance()) {
    TParticlePDG* particle = TDatabasePDG::Instance()->GetParticle(PdgCode);
    if (particle)
      Mass = particle->Mass();
  }
}



MUonETrack::MUonETrack(TParticle* part, Int_t stationindex, Bool_t stopattarget, Bool_t signalparticle, Bool_t pileupparticle) 
  : TObject(),
    PdgCode(part->GetPdgCode()), MotherID(part->GetMother(0)), InteractionID(part->GetUniqueID()), Mass(part->GetMass()),
    Px(part->Px()), Py(part->Py()), Pz(part->Pz()), StartX(part->Vx()), StartY(part->Vy()), StartZ(part->Vz()), StationIndex(stationindex), StopAtTarget(stopattarget), SignalParticle(signalparticle), PileupParticle(pileupparticle)
{
}


ClassImp(MUonETrack)

