
#ifndef MUONESTACK_H
#define MUONESTACK_H

/*
  MCTrack container used during simulation. Mostly copied from examples.
  Some methods were added to mark track to be stopped at target and
  to assign signal tracks to a station ID.

  I've also modified the variable names to make them a bit more desriptive.
*/

#include "FairGenericStack.h"           // for FairGenericStack

#include <fairlogger/Logger.h>

#include "Rtypes.h"                     // for Int_t, Double_t, Bool_t, etc
#include "TMCProcess.h"                 // for TMCProcess

#include <map>                          // for map, map<>::iterator
#include <unordered_map>
#include <stack>                        // for stack
#include <utility>                      // for pair

class TClonesArray;
class TParticle;
class TRefArray;

class MUonEStack : public FairGenericStack
{

  public:

    MUonEStack(Int_t size = 100, Bool_t saveMCTracks = true, Bool_t saveSignalTracks = true, Bool_t savePileupTracks = true);

    virtual ~MUonEStack();


    /** Add a TParticle to the stack.
     ** Declared in TVirtualMCStack
     *@param toBeDone  Flag for tracking
     *@param parentID  Index of mother particle
     *@param pdgCode   Particle type (PDG encoding)
     *@param px,py,pz  Momentum components at start vertex [GeV]
     *@param e         Total energy at start vertex [GeV]
     *@param vx,vy,vz  Coordinates of start vertex [cm]
     *@param time      Start time of track [s]
     *@param polx,poly,polz Polarisation vector
     *@param proc      Production mechanism (VMC encoding)
     *@param ntr       Track number (filled by the stack)
     *@param weight    Particle weight
     *@param is        Generation status code (whatever that means)
     **/
    virtual void PushTrack(Int_t toBeDone, Int_t parentID, Int_t pdgCode,
                           Double_t px, Double_t py, Double_t pz,
                           Double_t e, Double_t vx, Double_t vy,
                           Double_t vz, Double_t time, Double_t polx,
                           Double_t poly, Double_t polz, TMCProcess proc,
                           Int_t& ntr, Double_t weight, Int_t is);

    virtual void PushTrack(Int_t toBeDone, Int_t parentID, Int_t pdgCode,
                           Double_t px, Double_t py, Double_t pz,
                           Double_t e, Double_t vx, Double_t vy,
                           Double_t vz, Double_t time, Double_t polx,
                           Double_t poly, Double_t polz, TMCProcess proc,
                           Int_t& ntr, Double_t weight, Int_t is,Int_t secondParentId);



    //set station index and stop decision for the last track added to stack
    void addStationIndexForLastTrack(Int_t index);
    void addStopAtTargetDecisionForLastTrack(Bool_t kill);
    void setSignalParticleFlagForLastTrack(Bool_t flag);
    void setPileupParticleFlagForLastTrack(Bool_t flag);

    //required because simulation works on TParticle rather than MUonETrack
    //used to stop tracks at target
    Int_t getCurrentTrackStationIndex() const;
    Bool_t getCurrentTrackStopAtTargetDecision() const;
    Bool_t getCurrentTrackSignalParticleFlag() const;
    Bool_t getCurrentTrackPileupParticleFlag() const;


    /** Get next particle for tracking from the stack.
     ** Declared in TVirtualMCStack
     *@param  iTrack  index of popped track (return)
     *@return Pointer to the TParticle of the track
     **/
    virtual TParticle* PopNextTrack(Int_t& iTrack);


    /** Get primary particle by index for tracking from stack
     ** Declared in TVirtualMCStack
     *@param  iPrim   index of primary particle
     *@return Pointer to the TParticle of the track
     **/
    virtual TParticle* PopPrimaryForTracking(Int_t iPrim);


    /** Set the current track number
     ** Declared in TVirtualMCStack
     *@param iTrack  track number
     **/
    virtual void SetCurrentTrack(Int_t iTrack)   {m_currentTrackIndex = iTrack;}


    /** Get total number of tracks
     ** Declared in TVirtualMCStack
     **/
    virtual Int_t GetNtrack() const {return m_numberOfParticles;}


    /** Get number of primary tracks
     ** Declared in TVirtualMCStack
     **/
    virtual Int_t GetNprimary() const {return m_numberOfPrimaryParticles;}


    /** Get the current track's particle
     ** Declared in TVirtualMCStack
     **/
    virtual TParticle* GetCurrentTrack() const;


    /** Get the number of the current track
     ** Declared in TVirtualMCStack
     **/
    virtual Int_t GetCurrentTrackNumber() const {return m_currentTrackIndex;}


    /** Get the track number of the parent of the current track
     ** Declared in TVirtualMCStack
     **/
    virtual Int_t GetCurrentParentTrackNumber() const;


    /** Add a TParticle to the fParticles array **/
    virtual void AddParticle(TParticle* part);


    /** Fill the MCTrack output array, applying filter criteria **/
    virtual void FillTrackArray();


    /** Update the track index in the MCTracks and MCPoints **/
    virtual void UpdateTrackIndex(TRefArray* detArray=0);


    /** Resets arrays and stack and deletes particles and tracks **/
    virtual void Reset();


    /** Register the MCTrack array to the Root Manager  **/
    virtual void Register();


    /** Output to screen
     **@param iVerbose: 0=events summary, 1=track info
     **/
    virtual void Print(Option_t*) const {}


    /** Accessors **/
    TParticle* GetParticle(Int_t trackId) const;
    TClonesArray* GetListOfParticles() {return m_allParticles;}

    /** Clone this object (used in MT mode only) */
    virtual FairGenericStack* CloneStack() const {return new MUonEStack();}

  private:
  
    std::stack<TParticle*>  m_particlesToProcess; //in tracking 
    TClonesArray* m_allParticles{nullptr};

    TClonesArray* m_tracks{nullptr};
    TClonesArray* m_signalTracks{nullptr};
    TClonesArray* m_pileupTracks{nullptr};

    std::unordered_map<Int_t, Bool_t>           m_storageFlagMap; //stores decision of whether to save a track or not
    std::unordered_map<Int_t, Int_t>            m_particleTrackMap; //maps particle index to the corresponding track

  //this could alternatively be handled with a user bit in TObject, but I don't think it's worth it
    std::unordered_map<Int_t, Bool_t>           m_particleSignalParticleMap; //maps trackID to whether the particle is a signal particle from generator
    std::unordered_map<Int_t, Bool_t>           m_particlePileupParticleMap; //maps trackID to whether the particle is a pileup particle from generator
    std::unordered_map<Int_t, Int_t>            m_particleStationIndexMap; //maps trackID to station ID for signal particles
    std::unordered_map<Int_t, Bool_t>           m_particleStopAtTargetMap; //maps trackID to the decision on whether particle should be stopped when entering target of linked station


    Bool_t m_saveMCTracks{true};
    Bool_t m_saveSignalTracks{true};
    Bool_t m_savePileupTracks{true};

    Int_t m_currentTrackIndex; 
    Int_t m_numberOfPrimaryParticles;
    Int_t m_numberOfParticles;
    Int_t m_numberOfTracks;
    Int_t m_numberOfSignalTracks;
    Int_t m_numberOfPileupTracks;    
    Int_t m_mergingIndex; 

    /** Mark tracks for output using selection criteria  **/
    void SelectTracks();

    MUonEStack(const MUonEStack&);
    MUonEStack& operator=(const MUonEStack&);

    ClassDef(MUonEStack,2)


};






#endif //MUONESTACK_H


