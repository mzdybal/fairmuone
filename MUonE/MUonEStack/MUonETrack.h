
#ifndef MUONETRACK_H
#define MUONETRACK_H

#include "TObject.h"                    // for TObject

#include "Rtypes.h"                     // for Double_t, Int_t, Double32_t, etc
#include "TMath.h"                      // for Sqrt
#include "TParticle.h"

class TParticle;

class MUonETrack : public TObject
{

  public:

    MUonETrack();

    MUonETrack(Int_t pdg, Int_t motherid, Int_t interactionid, Double_t px, Double_t py,
      Double_t pz, Double_t x, Double_t y, Double_t z, Int_t stationindex = -1, Bool_t stopattarget = false, Bool_t signalparticle = false, Bool_t pileupparticle = false);

    MUonETrack(TParticle* part, Int_t stationindex = -1, Bool_t stopattarget = false, Bool_t signalparticle = false, Bool_t pileupparticle = false);

  
    Int_t     pdgCode()  const {return PdgCode;}
    Int_t     motherID() const {return MotherID;}
    Int_t     interactionID() const {return InteractionID;}

    Double_t  px()       const {return Px;}
    Double_t  py()       const {return Py;}
    Double_t  pz()       const {return Pz;}
    Double_t  startX()   const {return StartX;}
    Double_t  startY()   const {return StartY;}
    Double_t  startZ()   const {return StartZ;}
    Double_t  ax()       const {return Px/Pz;}
    Double_t  bx()       const {return StartX - ax()*StartZ;}
    Double_t  ay()       const {return Py/Pz;}
    Double_t  by()       const {return StartY - ay()*StartZ;}    
    Double_t  mass()     const {return Mass;}
    Double_t  energy()   const {return TMath::Sqrt(Mass*Mass + Px*Px + Py*Py + Pz*Pz);}
    Double_t  pt()       const {return TMath::Sqrt(Px*Px+Py*Py);}
    Double_t  p() const {return TMath::Sqrt(Px*Px+Py*Py+Pz*Pz);}

    Int_t     stationIndex()   const {return StationIndex;}
    Bool_t    stopAtTarget() const {return StopAtTarget;}

    Bool_t signalParticle() const {return SignalParticle;}
    Bool_t pileupParticle() const {return PileupParticle;}

    /**  Modifiers  **/
    void SetMotherID(Int_t id) {MotherID = id;}
    void setStationIndex(Int_t index) {StationIndex = index;}
    void setStopAtTarget(Bool_t stop) {StopAtTarget = stop;}

  private:

    Int_t PdgCode{0};
    Int_t MotherID{-2}; //-1 for primary particles
    Int_t InteractionID{-2};

    Bool_t SignalParticle{false};
    Bool_t PileupParticle{false};

    Double32_t Mass{0};

    Double32_t Px{0};
    Double32_t Py{0};
    Double32_t Pz{0};

    Double32_t StartX{0};
    Double32_t StartY{0};
    Double32_t StartZ{0};


    Int_t StationIndex{-1}; //set for signal tracks
    Bool_t StopAtTarget{false}; //used to stop incoming tracks during signal generation


    ClassDef(MUonETrack,2);

};


#endif //MUONETRACK_H




