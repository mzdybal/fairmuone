
#include "MUonEStack.h"

#include "FairDetector.h"               // for FairDetector
#include "FairLink.h"                   // for FairLink
#include "MUonEDetectorPoint.h" 
#include "MUonETrack.h"                
#include "FairRootManager.h"            // for FairRootManager
#include <fairlogger/Logger.h>                 // for FairLogger, MESSAGE_ORIGIN

#include <iosfwd>                       // for ostream
#include "TClonesArray.h"               // for TClonesArray
#include "TGeoManager.h"                // for gGeoManager
#include "TIterator.h"                  // for TIterator
#include "TLorentzVector.h"             // for TLorentzVector
#include "TParticle.h"                  // for TParticle
#include "TRefArray.h"                  // for TRefArray
#include "TVirtualMC.h"                 // for gMC

#include <stddef.h>                     // for NULL
#include <iostream>                     // for operator<<, etc

#include "MUonEDetectorPoint.h"

using std::pair;


// -----   Default constructor   -------------------------------------------
MUonEStack::MUonEStack(Int_t size, Bool_t saveMCTracks, Bool_t saveSignalTracks, Bool_t savePileupTracks)
  : FairGenericStack(),
    m_particlesToProcess(),
    m_allParticles(new TClonesArray("TParticle", size)),
    m_tracks(new TClonesArray("MUonETrack", size)),
    m_signalTracks(new TClonesArray("MUonETrack", size)),    
    m_pileupTracks(new TClonesArray("MUonETrack", size)),    
    m_saveMCTracks(saveMCTracks),
    m_saveSignalTracks(saveSignalTracks),
    m_savePileupTracks(savePileupTracks),
    m_storageFlagMap(),
    m_particleTrackMap(),
    m_currentTrackIndex(-1),
    m_numberOfPrimaryParticles(0),
    m_numberOfParticles(0),
    m_numberOfTracks(0),
    m_mergingIndex(0)
{
}


MUonEStack::~MUonEStack()
{
  if (m_allParticles) {
    m_allParticles->Delete();
    delete m_allParticles;
  }
  if (m_tracks) {
    m_tracks->Delete();
    delete m_tracks;
  }
  if(m_signalTracks) {
    m_signalTracks->Delete();
    delete m_signalTracks;
  }

  if(m_pileupTracks) {
    m_pileupTracks->Delete();
    delete m_pileupTracks;
  }  
}

void MUonEStack::PushTrack(Int_t toBeDone, Int_t parentId, Int_t pdgCode,
                          Double_t px, Double_t py, Double_t pz,
                          Double_t e, Double_t vx, Double_t vy, Double_t vz,
                          Double_t time, Double_t polx, Double_t poly,
                          Double_t polz, TMCProcess proc, Int_t& ntr,
                          Double_t weight, Int_t is)
{
  PushTrack( toBeDone, parentId, pdgCode,
             px,  py,  pz,
             e,  vx,  vy,  vz,
             time,  polx,  poly,
             polz, proc, ntr,
             weight, is, -1);
}


void MUonEStack::PushTrack(Int_t toBeDone, Int_t parentId, Int_t pdgCode,
                          Double_t px, Double_t py, Double_t pz,
                          Double_t e, Double_t vx, Double_t vy, Double_t vz,
                          Double_t time, Double_t polx, Double_t poly,
                          Double_t polz, TMCProcess proc, Int_t& ntr,
                          Double_t weight, Int_t /*is*/, Int_t /*secondparentID*/)
{

  TClonesArray& partArray = *m_allParticles;

  Int_t trackId = m_numberOfParticles;
  Int_t nPoints = 0;
  Int_t daughter1Id = -1;
  Int_t daughter2Id = -1;

  TParticle* particle = new(partArray[m_numberOfParticles++]) TParticle(pdgCode, trackId, parentId,
        nPoints, daughter1Id,
        daughter2Id, px, py, pz, e,
        vx, vy, vz, time);

  particle->SetPolarisation(polx, poly, polz);
  particle->SetWeight(weight);
  particle->SetUniqueID(proc);

  if (parentId < 0)
    ++m_numberOfPrimaryParticles;
  
  ntr = trackId;

  if (toBeDone == 1) {m_particlesToProcess.push(particle);}
}

void MUonEStack::addStationIndexForLastTrack(Int_t index) {

  m_particleStationIndexMap[m_numberOfParticles - 1] = index;

}

void MUonEStack::addStopAtTargetDecisionForLastTrack(Bool_t stop) {

  m_particleStopAtTargetMap[m_numberOfParticles - 1] = stop;
}

void MUonEStack::setSignalParticleFlagForLastTrack(Bool_t flag) {

  m_particleSignalParticleMap[m_numberOfParticles - 1] = flag;
}

void MUonEStack::setPileupParticleFlagForLastTrack(Bool_t flag) {

  m_particlePileupParticleMap[m_numberOfParticles - 1] = flag;
}


TParticle* MUonEStack::PopNextTrack(Int_t& iTrack)
{

  if (m_particlesToProcess.empty()) {
    iTrack = -1;
    return NULL;
  }

  TParticle* thisParticle = m_particlesToProcess.top();
  m_particlesToProcess.pop();

  if (!thisParticle) {
    iTrack = 0;
    return NULL;
  }

  m_currentTrackIndex = thisParticle->GetStatusCode();
  iTrack = m_currentTrackIndex;

  return thisParticle;
}


TParticle* MUonEStack::PopPrimaryForTracking(Int_t iPrim)
{
  if (iPrim < 0 || iPrim >= m_numberOfPrimaryParticles) {
    LOG(fatal) << "Primary index out of range!" << iPrim;
  }

  TParticle* part = static_cast<TParticle*>(m_allParticles->At(iPrim));
  if (!(part->GetMother(0) < 0)) {
    LOG(fatal) << "Not a primary track!" << iPrim;
  }

  return part;
}



TParticle* MUonEStack::GetCurrentTrack() const
{
  TParticle* currentPart = GetParticle(m_currentTrackIndex);
  if (!currentPart) {
    LOG(warn) << "Current track not found in stack!" ;
  }
  return currentPart;
}

Int_t MUonEStack::getCurrentTrackStationIndex() const {
  //status code is used to hold track ID, see PushTrack method
  TParticle* currentPart = GetParticle(m_currentTrackIndex);
  auto stationIndex = m_particleStationIndexMap.find(currentPart->GetStatusCode());

  return stationIndex == m_particleStationIndexMap.end() ? -1 : stationIndex->second;
}

Bool_t MUonEStack::getCurrentTrackStopAtTargetDecision() const {
  //status code is used to hold track ID, see PushTrack method
  TParticle* currentPart = GetParticle(m_currentTrackIndex);
  auto stopAtTargetDecision = m_particleStopAtTargetMap.find(currentPart->GetStatusCode());  

  return stopAtTargetDecision == m_particleStopAtTargetMap.end() ? false : stopAtTargetDecision->second;
}

Bool_t MUonEStack::getCurrentTrackSignalParticleFlag() const {
  //status code is used to hold track ID, see PushTrack method
  TParticle* currentPart = GetParticle(m_currentTrackIndex);
  auto flag = m_particleSignalParticleMap.find(currentPart->GetStatusCode());  

  return flag == m_particleSignalParticleMap.end() ? false : flag->second;
}

Bool_t MUonEStack::getCurrentTrackPileupParticleFlag() const {
  //status code is used to hold track ID, see PushTrack method
  TParticle* currentPart = GetParticle(m_currentTrackIndex);
  auto flag = m_particlePileupParticleMap.find(currentPart->GetStatusCode());  

  return flag == m_particlePileupParticleMap.end() ? false : flag->second;
}

void MUonEStack::AddParticle(TParticle* oldPart)
{
  TClonesArray& array = *m_allParticles;
  TParticle* newPart = new(array[m_mergingIndex]) TParticle(*oldPart);
  newPart->SetWeight(oldPart->GetWeight());
  newPart->SetUniqueID(oldPart->GetUniqueID());
  ++m_mergingIndex;
}


void MUonEStack::FillTrackArray()
{

  m_particleTrackMap.clear();
  m_numberOfTracks = 0;
  m_numberOfSignalTracks = 0;
  m_numberOfPileupTracks = 0;

  SelectTracks();

  for (Int_t iPart = 0; iPart < m_numberOfParticles; ++iPart) {

    Bool_t store = kFALSE;

    auto storageFlagIterator = m_storageFlagMap.find(iPart);

    if(storageFlagIterator == m_storageFlagMap.end()) {
      LOG(fatal) << "Particle " << iPart << " not found in storage map! ";
    } else {
      store = (*storageFlagIterator).second;
    }

    if (store) {
      //status code is used to hold track ID, see PushTrack method
      auto part_temp = GetParticle(iPart);
      auto stationIndex = m_particleStationIndexMap.find(part_temp->GetStatusCode());
      auto stopAtTargetDecision = m_particleStopAtTargetMap.find(part_temp->GetStatusCode());
      auto isSignal = m_particleSignalParticleMap.find(part_temp->GetStatusCode());
      auto isPileup = m_particlePileupParticleMap.find(part_temp->GetStatusCode());

      MUonETrack* track = new((*m_tracks)[m_numberOfTracks]) MUonETrack(part_temp, 
        stationIndex == m_particleStationIndexMap.end() ? -1 : stationIndex->second,
        stopAtTargetDecision == m_particleStopAtTargetMap.end() ? false : stopAtTargetDecision->second,
        isSignal == m_particleSignalParticleMap.end() ? false : isSignal->second,
        isPileup == m_particlePileupParticleMap.end() ? false : isPileup->second        
      );

      if(isSignal != m_particleSignalParticleMap.end() && isSignal->second) {

        MUonETrack* signal_track = new((*m_signalTracks)[m_numberOfSignalTracks]) MUonETrack(part_temp, 
          stationIndex == m_particleStationIndexMap.end() ? -1 : stationIndex->second,
          stopAtTargetDecision == m_particleStopAtTargetMap.end() ? false : stopAtTargetDecision->second,
          isSignal == m_particleSignalParticleMap.end() ? false : isSignal->second,
          isPileup == m_particlePileupParticleMap.end() ? false : isPileup->second
        );     

        ++m_numberOfSignalTracks;   
      }

      if(isPileup != m_particlePileupParticleMap.end() && isPileup->second) {

        MUonETrack* pileup_track = new((*m_pileupTracks)[m_numberOfPileupTracks]) MUonETrack(part_temp, 
          stationIndex == m_particleStationIndexMap.end() ? -1 : stationIndex->second,
          stopAtTargetDecision == m_particleStopAtTargetMap.end() ? false : stopAtTargetDecision->second,
          isSignal == m_particleSignalParticleMap.end() ? false : isSignal->second,
          isPileup == m_particlePileupParticleMap.end() ? false : isPileup->second
        );   

        ++m_numberOfPileupTracks;     
      }      

      m_particleTrackMap[iPart] = m_numberOfTracks;

      ++m_numberOfTracks;
      

    } else 
      m_particleTrackMap[iPart] = -2; 
    
  }

  //map index for primary mothers
  m_particleTrackMap[-1] = -1;

}


void MUonEStack::UpdateTrackIndex(TRefArray* detList)
{

  Int_t nColl = 0;

  //update mother ID in MCTracks
  for(Int_t i = 0; i < m_numberOfTracks; ++i) {

    MUonETrack* track = static_cast<MUonETrack*>(m_tracks->At(i));

    Int_t iMotherOld = track->motherID();

    auto indexTrackIterator = m_particleTrackMap.find(iMotherOld);
    if (indexTrackIterator == m_particleTrackMap.end()) {
      LOG(fatal) << "Particle index " << iMotherOld << " not found in index map!";
    } else {
      track->SetMotherID((*indexTrackIterator).second);
    }
  }


  if(fDetList==0) {
    // Now iterate through all active detectors
    fDetIter = detList->MakeIterator();
    fDetIter->Reset();
  } else {
    fDetIter->Reset();
  }

  FairDetector* det = NULL;
  while((det = static_cast<FairDetector*>(fDetIter->Next()))) {


    // --> Get hit collections from detector
    Int_t iColl = 0;
    TClonesArray* hitArray;

    while ((hitArray = det->GetCollection(iColl++))) {

      ++nColl;
      Int_t nPoints = hitArray->GetEntriesFast();

      // --> Update track index for all MCPoints in the collection
      for (Int_t iPoint = nPoints - 1; iPoint >= 0; --iPoint) {

        MUonEDetectorPoint* point = static_cast<MUonEDetectorPoint*>(hitArray->At(iPoint));
        Int_t iTrack = point->trackID();

        auto fastSimIterator = fFSTrackMap.find(iTrack);      // check if point created by FastSimulation
        if ( fastSimIterator != fFSTrackMap.end() ) {    // indeed the point has been created by the FastSimulation mechanism
            iTrack = fastSimIterator->second;
            point->setTrackID(iTrack);                // set proper TrackID
        }

        auto indexTrackIterator = m_particleTrackMap.find(iTrack);
        if (indexTrackIterator == m_particleTrackMap.end()) {
          LOG(fatal) << "Particle index " << iTrack << " not found in index map!";
        } else {
          point->setTrackID((*indexTrackIterator).second);
	      }
      }

    }   // Collections of this detector
  }     // List of active detectors
}


void MUonEStack::Reset()
{
  m_mergingIndex = 0;
  m_currentTrackIndex = -1;
  m_numberOfPrimaryParticles = m_numberOfParticles = m_numberOfTracks = 0;
  m_numberOfSignalTracks = m_numberOfPileupTracks = 0;

  while (!m_particlesToProcess.empty()) {m_particlesToProcess.pop();}

  m_allParticles->Clear();
  m_tracks->Clear();
  m_signalTracks->Clear();
  m_pileupTracks->Clear();
  fFSTrackMap.clear();

  m_storageFlagMap.clear();
  m_particleTrackMap.clear();
  m_particleSignalParticleMap.clear();
  m_particlePileupParticleMap.clear();
  m_particleStationIndexMap.clear();
  m_particleStopAtTargetMap.clear();
}


void MUonEStack::Register()
{
  if ( gMC && ( ! gMC->IsMT() ) ) {
    FairRootManager::Instance()->Register("MCTrack", "Stack", m_tracks, m_saveMCTracks ? kTRUE : kFALSE);
    FairRootManager::Instance()->Register("SignalTracks", "Stack", m_signalTracks, m_saveSignalTracks ? kTRUE : kFALSE);
    FairRootManager::Instance()->Register("PileupTracks", "Stack", m_pileupTracks, m_savePileupTracks ? kTRUE : kFALSE);
    
  } else {
    FairRootManager::Instance()->RegisterAny("MCTrack", m_tracks, m_saveMCTracks ? kTRUE : kFALSE);
    FairRootManager::Instance()->RegisterAny("SignalTracks", m_signalTracks, m_saveSignalTracks ? kTRUE : kFALSE);
    FairRootManager::Instance()->RegisterAny("PileupTracks", m_pileupTracks, m_savePileupTracks ? kTRUE : kFALSE);
  }
}



Int_t MUonEStack::GetCurrentParentTrackNumber() const
{
  TParticle* currentPart = GetCurrentTrack();
  if (currentPart) {return currentPart->GetFirstMother();}
  else {return -1;}
}


TParticle* MUonEStack::GetParticle(Int_t trackID) const
{
  if (trackID < 0 || trackID >= m_numberOfParticles) {
    LOG(fatal) << "Particle index " << trackID << " out of range.";
  }
  return static_cast<TParticle*>(m_allParticles->At(trackID));
}



// -----   Private method SelectTracks   -----------------------------------
void MUonEStack::SelectTracks()
{

  m_storageFlagMap.clear();

  // loop over tracks created by FastSim to order the tracks
  for (auto fastSimIterator = fFSTrackMap.begin(); fastSimIterator != fFSTrackMap.end(); ++fastSimIterator) {
      for (Int_t i=0; i < m_numberOfParticles; i++) {
          TParticle* thisPart = GetParticle(i);
          if (thisPart->GetMother(0) == fastSimIterator->first)
            thisPart->SetMother(0, fastSimIterator->second);
      }
  }

  // --> Check particles in the fParticle array
  for (Int_t i = 0; i < m_numberOfParticles; ++i) {

    TParticle* thisPart = GetParticle(i);
    Bool_t store = kTRUE;

    // --> Get track parameters
    Int_t iMother   = thisPart->GetMother(0);
    TLorentzVector p;
    thisPart->Momentum(p);

    // check if the track was created by the FastSim mechanism. Do not store such tracks
    Bool_t fastSimTrack = kFALSE;
    auto fastSimIterator = fFSTrackMap.find(i);
    if ( fastSimIterator != fFSTrackMap.end() )
        fastSimTrack = kTRUE;

    // --> Check for cuts (store primaries in any case)
    if (iMother < 0) { store = kTRUE; }
    else {
      if (fastSimTrack)      { store = kFALSE; }
    }

    // --> Set storage flag
    m_storageFlagMap[i] = store;
  }
}


ClassImp(MUonEStack)
