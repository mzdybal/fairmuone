#include "MUonEProductionJob.h"
#include <fairlogger/Logger.h>
#include "TMath.h"


#include <filesystem>

MUonEProductionJob::MUonEProductionJob()
{
    resetDefaultConfiguration();
}

Bool_t MUonEProductionJob::readConfiguration(TString config_name) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    LOG(info) << "";
    LOG(info) << "Reading job configuration: " << config_name.Data();

    YAML::Node job;

    if(!loadConfigFile(config_name, job))
        return false;


    if(job["outputFile"])
        m_outputFile = job["outputFile"].as<std::string>();
    else {
        LOG(error) << "Variable 'outputFile' not set.";
        m_configuredCorrectly = false;
    }

    if(job["saveParametersFile"])
        m_saveParametersFile = job["saveParametersFile"].as<Bool_t>();
    else {
        LOG(error) << "Variable 'saveParametersFile' not set. The file will be saved by default";
        m_saveParametersFile = true;
    }

    if(job["saveGeoTracks"])
        m_saveGeoTracks = job["saveGeoTracks"].as<Bool_t>();
    else {
        LOG(error) << "Variable 'saveGeoTracks' not set. They will not be saved by default";
        m_saveGeoTracks = true;
    }


    if(job["detectorConfiguration"])
        m_detectorConfiguration = job["detectorConfiguration"].as<std::string>();
    else {
        LOG(error) << "Variable 'detectorConfiguration' not set.";
        m_configuredCorrectly = false;
    }        


    if(job["runGeneration"])
        m_runGeneration = job["runGeneration"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'runGeneration' not set. Generation phase will be skipped.";
        m_runGeneration = false;            
    }

    if(job["runDigitization"])
        m_runDigitization = job["runDigitization"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'runDigitization' not set. Digitization phase will be skipped.";
        m_runDigitization = false;            
    }

    if(job["runReconstruction"])
        m_runReconstruction = job["runReconstruction"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'runReconstruction' not set. Reconstruction phase will be skipped.";
        m_runReconstruction = false;            
    }

    if(job["runEventFilter"])
        m_runEventFilter = job["runEventFilter"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'runEventFilter' not set. All events will be saved.";
        m_runEventFilter = false;            
    }

    if(m_runGeneration && m_runReconstruction && !m_runDigitization) {
        LOG(error) << "Variables 'runGeneration' and 'runReconstruction' set to true, but 'runDigitization' set to false.";
        m_configuredCorrectly = false;            
    }


    if(job["numberOfEvents"]) {
        m_numberOfEvents = job["numberOfEvents"].as<Int_t>();
        m_numberOfEventsSet = true;
    }
    else {
        m_numberOfEventsSet = false;
    }

    if(!m_runGeneration) {

        if(job["inputDirectory"]) {
            m_inputDirectory = job["inputDirectory"].as<std::string>();
            if(!m_inputDirectory.empty()) {

                if(m_inputDirectory[m_inputDirectory.size() - 1] != '/' && m_inputDirectory[m_inputDirectory.size() - 1] != '\\')
                    m_inputDirectory.append("/");

                m_inputDirectorySet = true;

            } else
                m_inputDirectorySet = false;
        } else
            m_inputDirectorySet = false;

        if(job["inputFiles"] && job["inputFiles"].size() > 0) {

            if(job["inputFiles"].size() == 1 && 0 == job["inputFiles"][0].as<std::string>().compare("*")) {

                if(m_inputDirectorySet) {

                    for (auto &p : std::filesystem::directory_iterator(m_inputDirectory))
                    {
                        if (p.path().extension() == ".root" || p.path().extension() == ".ROOT")
                            m_inputFiles.emplace_back(p.path().filename().string());
                    }

                } else {
                    LOG(error) << "Processing all available root files ('*') requires explicitely set inputDirectory";
                    m_configuredCorrectly = false;
                }

            } else {

                for(auto const& f : job["inputFiles"]) {
                    
                    std::string tmpf = f.as<std::string>();
                    if(tmpf.size() > 5) {
                        auto tmpfsbst = tmpf.substr(tmpf.size() - 5);
                        if(0 != tmpfsbst.compare(".root") && 0 != tmpfsbst.compare(".ROOT"))
                            tmpf.append(".root");
                    } else 
                        tmpf.append(".root");

                    m_inputFiles.emplace_back(tmpf);
                }
            }

        }
        else {
            LOG(error) << "Variable 'runGeneration' set to false, but no input files provided. Please set 'inputFiles' variable in the job config.";
            m_configuredCorrectly = false;
        }        

    } else {

        if(!m_numberOfEventsSet) {
            LOG(error) << "Variable 'runGeneration' set to true, but 'numberOfEvents' not set.";
            m_configuredCorrectly = false;
        }

        if(job["generationConfiguration"]) {

            if(!m_generationConfiguration.readConfiguration(job["generationConfiguration"]))
                m_configuredCorrectly = false;
        }
        else {
            LOG(error) << "Variable 'runGeneration' set to true, but 'generationConfiguration' not provided.";
            m_configuredCorrectly = false;                
        }
           
    }

    if(m_runDigitization) {

        if(job["digitizationConfiguration"]) {

            if(!m_digitizationConfiguration.readConfiguration(job["digitizationConfiguration"]))
                m_configuredCorrectly = false;
        }
        else {
            LOG(error) << "Variable 'runDigitization' set to true, but 'digitizationConfiguration' not provided.";
            m_configuredCorrectly = false;                
        }                
    }
        

    if(m_runGeneration || m_runDigitization) {

        if(job["seed"])
            m_seed = TMath::Abs(job["seed"].as<Int_t>());
        else {
            LOG(info) << "Variable 'seed' not set. Defaulting to 42.";
            m_seed = 42;
        }            
    }


    if(m_runReconstruction) {

        if(job["reconstructionConfiguration"]) {
                
            if(!m_reconstructionConfiguration.readConfiguration(job["reconstructionConfiguration"]))
                m_configuredCorrectly = false;
        }
        else {
            LOG(error) << "Variable 'runReconstruction' set to true, but 'reconstructionConfiguration' not provided.";
            m_configuredCorrectly = false;                
        }                
    }


    if(m_runEventFilter) {

        if(job["eventFilterConfiguration"]) {
                
            if(!m_eventFilterConfiguration.readConfiguration(job["eventFilterConfiguration"]))
                m_configuredCorrectly = false;
        }
        else {
            LOG(error) << "Variable 'runEventFilter' set to true, but 'eventFilterConfiguration' not provided.";
            m_configuredCorrectly = false;                
        }                
    }


    if(m_configuredCorrectly)
        LOG(info) << "Job configuration read successfully.";
    else
        LOG(info) << "Job configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}


void MUonEProductionJob::logCurrentConfiguration() const {

    LOG(info) << "";
    LOG(info) << "";
    LOG(info) << "Summary of job configuration: ";
    LOG(info) << "Output file: " << m_outputFile;
    if(m_saveParametersFile)
        LOG(info) << "Parameters file will be saved.";
    if(m_saveGeoTracks)
        LOG(info) << "GeoTracks will be saved";

    LOG(info) << "Detector configuration: " << m_detectorConfiguration;

    if(m_runGeneration && m_runDigitization && m_runReconstruction) {
        LOG(info) << "The job will run generation, digitization and reconstruction, using seed " << m_seed << ".";           
    }
    else if(m_runGeneration && m_runDigitization) {
        LOG(info) << "The job will run generation and digitization of, using seed " << m_seed << ".";
    }
    else if(m_runDigitization && m_runReconstruction) {
        LOG(info) << "The job will run digitization and reconstruction of events, using seed " << m_seed << ".";
        if(m_inputDirectorySet)
            LOG(info) << "The input files will be taken from the input directory: " << m_inputDirectory;

        LOG(info) << "Input files:";
        for(auto const& f : m_inputFiles)
            LOG(info) << f;
    }
    else if(m_runGeneration) {
        LOG(info) << "The job will run generation of, using seed " << m_seed << ".";
    }
    else if(m_runDigitization) {
        LOG(info) << "The job will run digitization of events, using seed " << m_seed << ".";
        
        if(m_inputDirectorySet)
            LOG(info) << "The input files will be taken from the input directory: " << m_inputDirectory;

        LOG(info) << "Input files:";
        for(auto const& f : m_inputFiles)
            LOG(info) << f;
    
    }
    else if(m_runReconstruction) {
        LOG(info) << "The job will run reconstruction of events.";
    
        if(m_inputDirectorySet)
            LOG(info) << "The input files will be taken from the input directory: " << m_inputDirectory;

        LOG(info) << "Input files:";
        for(auto const& f : m_inputFiles)
            LOG(info) << f;
    }
    else {
        LOG(info) << "All three of the 'runGeneration', 'runDigitization' and 'runReconstruction' flags are set to false. The job will do nothing.";
    }

    if(m_runGeneration && m_numberOfEventsSet)
        LOG(info) << m_numberOfEvents << " events will be produced.";
    else if(m_numberOfEventsSet)
        LOG(info) << "First " << m_numberOfEvents << " events will be processed.";
    else
        LOG(info) << "All input events will be processed.";

    

}


void MUonEProductionJob::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_outputFile = "";
    m_saveParametersFile = true;
    m_saveGeoTracks = false;

    m_inputDirectorySet = false;
    m_inputDirectory = "";
    m_inputFiles.clear();

    m_seed = 42;
    m_numberOfEvents = 0;
    m_numberOfEventsSet = false;

    m_detectorConfiguration = "";

    //generation
    m_runGeneration = false;
    m_generationConfiguration.resetDefaultConfiguration();

    //digitization
    m_runDigitization = false;
    m_digitizationConfiguration.resetDefaultConfiguration();

    //reconstruction
    m_runReconstruction = false;
    m_reconstructionConfiguration.resetDefaultConfiguration();    
}

Bool_t MUonEProductionJob::loadConfigFile(TString config_name, YAML::Node& config) {

    if(!config_name.EndsWith(".yaml") && !config_name.EndsWith(".YAML"))
        config_name.Append(".yaml");

    try {

        config = YAML::LoadFile(config_name.Data());
        LOG(info) << "Configuration file found in the path provided.";

    } catch(...) {

        TString jobs_dir = getenv("JOBSPATH");
        jobs_dir.ReplaceAll("//", "/"); //taken from FR examples, not sure if necessary
        if(!jobs_dir.EndsWith("/"))
            jobs_dir.Append("/");  

        TString full_path = jobs_dir + config_name;

        LOG(info) << "Configuration file in the given path not found or failed to open. Trying in JOBSPATH directory (" << full_path << ").";

        try {

            config = YAML::LoadFile(full_path.Data());
            LOG(info) << "File found.";

        } catch(...) {

            LOG(fatal) << "Failed to load job configuration.";
            return false;
        }
    }   

    return true; 
}

ClassImp(MUonEProductionJob)