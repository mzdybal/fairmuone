#ifndef MUONEPRODUCTIONJOB_H
#define MUONEPRODUCTIONJOB_H

#include <yaml-cpp/yaml.h>
#include "TString.h"
#include <string>
#include <vector>

#include "Rtypes.h"

#include "MUonEGenerationConfiguration.h"
#include "MUonEDigitizationConfiguration.h"
#include "MUonEReconstructionConfiguration.h"
#include "MUonEEventFilterConfiguration.h"

class MUonEProductionJob {


public:

    MUonEProductionJob();

    Bool_t readConfiguration(TString config_name);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    std::string outputFile() const {return m_outputFile;}
    Bool_t saveParametersFile() const {return m_saveParametersFile;}
    Bool_t saveGeoTracks() const {return m_saveGeoTracks;}


    Bool_t inputDirectorySet() const {return m_inputDirectorySet;}
    std::string inputDirectory() const {return m_inputDirectory;}
    std::vector<std::string> const& inputFiles() const {return m_inputFiles;}

    Int_t seed() const {return m_seed;}

    Bool_t numberOfEventsSet() const {return m_numberOfEventsSet;}
    Int_t numberOfEvents() const {return m_numberOfEvents;}

    std::string detectorConfiguration() const {return m_detectorConfiguration;}

    Bool_t runGeneration() const {return m_runGeneration;}
    MUonEGenerationConfiguration const& generationConfiguration() const {
        if(m_runGeneration)
            return m_generationConfiguration;
        else
            throw std::logic_error("Trying to access generationConfiguration, but runGeneration is set to false.");
    }

    Bool_t runDigitization() const {return m_runDigitization;}
    MUonEDigitizationConfiguration const& digitizationConfiguration() const {
        if(m_runDigitization)
            return m_digitizationConfiguration;
        else
            throw std::logic_error("Trying to access digitizationConfiguration, but runDigitization is set to false.");
    }

    Bool_t runReconstruction() const {return m_runReconstruction;}
    MUonEReconstructionConfiguration const& reconstructionConfiguration() const {
        if(m_runReconstruction)
            return m_reconstructionConfiguration;
        else
            throw std::logic_error("Trying to access reconstructionConfiguration, but runReconstruction is set to false.");
    }

    Bool_t runEventFilter() const {return m_runEventFilter;}
    MUonEEventFilterConfiguration const& eventFilterConfiguration() const {
        if(m_runEventFilter)
            return m_eventFilterConfiguration;
        else
            throw std::logic_error("Trying to access eventFilterConfiguration, but runEventFilter is set to false.");
    }    

    void resetDefaultConfiguration();

private:

    Bool_t loadConfigFile(TString config_name, YAML::Node& config);

    Bool_t m_configuredCorrectly{false};

    std::string m_outputFile;
    Bool_t m_saveParametersFile{true};
    Bool_t m_saveGeoTracks{false};

    Bool_t m_inputDirectorySet{false};
    std::string m_inputDirectory;
    std::vector<std::string> m_inputFiles;

    Int_t m_seed{42};

    Bool_t m_numberOfEventsSet{false};
    Int_t m_numberOfEvents{0};

    std::string m_detectorConfiguration;

    //generation
    Bool_t m_runGeneration{false};
    MUonEGenerationConfiguration m_generationConfiguration;

    //digitization
    Bool_t m_runDigitization{false};
    MUonEDigitizationConfiguration m_digitizationConfiguration;

    //reconstruction
    Bool_t m_runReconstruction{false};
    MUonEReconstructionConfiguration m_reconstructionConfiguration;

    //event filter
    Bool_t m_runEventFilter{false};
    MUonEEventFilterConfiguration m_eventFilterConfiguration;


    ClassDef(MUonEProductionJob, 2)
};


#endif //MUONEPRODUCTIONJOB_H

