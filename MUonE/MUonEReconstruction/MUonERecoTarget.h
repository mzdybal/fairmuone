#ifndef MUONERECOTARGET_H
#define MUONERECOTARGET_H

#include "Rtypes.h"
#include <fairlogger/Logger.h>

#include "FairGeoLoader.h"
#include "FairGeoMedia.h"
#include "FairGeoInterface.h"
#include "FairGeoMedium.h"

#include "MUonEDetectorTargetsConfiguration.h"
/*
    Class used to store information on targets in a unified way for all input types.
    Note that most of the geometry info can be easily read from the detector configuration
    and is the same for all targets. We're only storing what we actually need for the reconstruction.
*/

class MUonERecoTarget {

public:

    MUonERecoTarget() = default;

    MUonERecoTarget(Int_t index, Int_t station, Double_t z, MUonEDetectorTargetsConfiguration const& config)
        : m_index(index), m_station(station), m_z(z), 
        m_zMin(z - 0.5 * config.thickness()), m_zMax(z + 0.5 * config.thickness()),
        m_xMin(-0.5 * config.width()), m_xMax(0.5 * config.width()),
        m_yMin(-0.5 * config.height()), m_yMax(0.5 * config.height())
        {

			//get radiation length from G4 material
			static FairGeoLoader* geoLoad = FairGeoLoader::Instance();

			if(!geoLoad) //may happen if generation step isn't run
				geoLoad = new FairGeoLoader("TGeo", "Geo Loader");

			if(!geoLoad)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoLoader to get radiation length";

			static FairGeoInterface* geoFace = geoLoad->getGeoInterface();

			if(!geoFace)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoInterface to get radiation length";

			static FairGeoMedia* media = geoFace->getMedia();

			if(!media)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoMedia to get radiation length";

			FairGeoMedium* medium = media->getMedium(config.targetMaterial().c_str());		
			
			if(!medium) {//may happen if generation step isn't run

				//the name has to be hardcoded here for now, because FairRunAna does not allow to set it from run macro
				//copied from FairRunSim::SetMaterials
				    TString Mat = "";
				    TString work = getenv("GEOMPATH");
				    work.ReplaceAll("//", "/");
				    if (work.IsNull()) {
					work = getenv("VMCWORKDIR");
					Mat = work + "/geometry/";
					Mat.ReplaceAll("//", "/");
				    } else {
					Mat = work;
					if (!Mat.EndsWith("/")) {
					    Mat += "/";
					}
				    }
				    TString MatFname = Mat + "media.geo"; //media.geo hardcoded

				geoFace->setMediaFile(MatFname.Data());
				geoFace->readMedia();

				media = geoFace->getMedia();
				medium = media->getMedium(config.targetMaterial().c_str());
			}

			if(!medium)
				LOG(fatal) << "Reconstruction: Failed to retrieve target medium to get radiation length";
		
			m_radiationLength = medium->getRadiationLength();
			LOG(info) << "Reconstruction: Initializing target " << index << " with radiation length " << m_radiationLength;

        }


    Int_t index() const {return m_index;}
    Int_t station() const {return m_station;}

	Double_t z() const {return m_z;}
	Double_t zMin() const {return m_zMin;}
	Double_t zMax() const {return m_zMax;}    
	Double_t xMin() const {return m_xMin;}
	Double_t xMax() const {return m_xMax;}    
	Double_t yMin() const {return m_yMin;}
	Double_t yMax() const {return m_yMax;}    

	Double_t radiationLength() const {return m_radiationLength;}

private:

    Int_t m_index{-1}; //index in m_targets vector
    Int_t m_station{-1};

	Double_t m_z{0};
    Double_t m_zMin{0};
    Double_t m_zMax{0};
    Double_t m_xMin{0};
    Double_t m_xMax{0};
    Double_t m_yMin{0};
    Double_t m_yMax{0};

    Double_t m_radiationLength{0};

    ClassDef(MUonERecoTarget, 2)
};

#endif//MUONERECOTARGET_H

