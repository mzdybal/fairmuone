#ifndef MUONERECONSTRUCTIONCONFIGURATION_H
#define MUONERECONSTRUCTIONCONFIGURATION_H


#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

class MUonEReconstructionConfiguration {

public:

    MUonEReconstructionConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t isMC() const {return m_isMC;}
    Bool_t verbose() const {return m_verbose;}

    Int_t skipEventsWithMoreThanNHits() const {return m_skipEventsWithMoreThanNHits;}
    Double_t maxVertexChi2PerDegreeOfFreedom() const {return m_maxVertexChi2PerDegreeOfFreedom;}
    Double_t maxGenericVertexChi2PerDegreeOfFreedom() const {return m_maxGenericVertexChi2PerDegreeOfFreedom;}

    Bool_t useSeedingSensorOnly() const {return m_useSeedingSensorOnly;}

    Bool_t runKinematicFit() const {return m_runKinematicFit;}

    Double_t MSCorrectionAssumedBeamEnergy() const {return m_MSCorrectionAssumedBeamEnergy;}

    Bool_t refitElectronTrackWithMSCorrection() const {return m_refitElectronTrackWithMSCorrection;}    
    Bool_t refitMuonTrackWithMSCorrection() const {return m_refitMuonTrackWithMSCorrection;}    
    Bool_t refitIncomingTrackWithMSCorrection() const {return m_refitIncomingTrackWithMSCorrection;}    

    Bool_t addBaselineMSCorrectionToAllTracks() const {return m_addBaselineMSCorrectionToAllTracks;}

    Double_t vertexSplittingAngleThreshold() const {return m_vertexSplittingAngleThreshold;}


    Int_t maxNumberOfSharedHits() const {return m_maxNumberOfSharedHits;}
    Int_t restrictHitSharingToFirstNModulesInSector() const {return m_restrictHitSharingToFirstNModulesInSector;}

    Double_t maxOutlierChi2() const {return m_maxOutlierChi2;}

    Double_t xyHitAssignmentWindow() const {return m_xyHitAssignmentWindow;}
    Double_t uvHitAssignmentWindow() const {return m_uvHitAssignmentWindow;}

    Bool_t alignmentFileSet() const {return m_alignmentFileSet;}
    std::string const& alignmentFile() const {return m_alignmentFile;}

    Bool_t runAdaptiveVertexFitter() const {return m_runAdaptiveVertexFitter;}
    Bool_t disableAdaptiveFitterSeeding() const {return m_disableAdaptiveFitterSeeding;}

	Double_t adaptiveFitterSeedWindow() const {return m_adaptiveFitterSeedWindow;}
	Double_t adaptiveFitterSeedTrackWindow() const {return m_adaptiveFitterSeedTrackWindow;}
	Int_t adaptiveFitterMaxNumberOfIterations() const {return m_adaptiveFitterMaxNumberOfIterations;}
	Double_t adaptiveFitterMaxTrackChi2() const {return m_adaptiveFitterMaxTrackChi2;}
	Double_t adaptiveFitterConvergenceMaxDeltaZ() const {return m_adaptiveFitterConvergenceMaxDeltaZ;}

    Bool_t useFittedZPositionInKinematicFit() const {return m_useFittedZPositionInKinematicFit;}
    Bool_t useFittedPositionInKinematicFit() const {return m_useFittedPositionInKinematicFit;}
    Bool_t restrictVertexPositionToTarget() const {return m_restrictVertexPositionToTarget;}

    Bool_t useFittedZPositionInGenericVertexKinematicFit() const {return m_useFittedZPositionInGenericVertexKinematicFit;}
    Bool_t useFittedPositionInGenericVertexKinematicFit() const {return m_useFittedPositionInGenericVertexKinematicFit;}
    Bool_t restrictGenericVertexPositionToTarget() const {return m_restrictGenericVertexPositionToTarget;}

    Int_t maxGenericVertexOutgoingTrackMultiplicity() const {return m_maxGenericVertexOutgoingTrackMultiplicity;}

    Bool_t ingoreLastStationInVertexing() const {return m_ingoreLastStationInVertexing;}


    Bool_t weightLinkedTracksByDepositedCharge() const {return m_weightLinkedTracksByDepositedCharge;}
    Bool_t allowTrivialIncomingTracks() const {return m_allowTrivialIncomingTracks;}

    Bool_t reassignHitsAfterKinematicFit() const {return m_reassignHitsAfterKinematicFit;}

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Bool_t m_isMC{true};
    Bool_t m_verbose{false};

    Int_t m_skipEventsWithMoreThanNHits{-1};
    Double_t m_maxVertexChi2PerDegreeOfFreedom{-1};
    Double_t m_maxGenericVertexChi2PerDegreeOfFreedom{-1};

    Bool_t m_useSeedingSensorOnly{false};

    Bool_t m_runKinematicFit{true};

    Double_t m_MSCorrectionAssumedBeamEnergy{160};
    Bool_t m_refitElectronTrackWithMSCorrection{true};
    Bool_t m_refitMuonTrackWithMSCorrection{false};
    Bool_t m_refitIncomingTrackWithMSCorrection{false};

    Bool_t m_addBaselineMSCorrectionToAllTracks{false};

    Double_t m_vertexSplittingAngleThreshold{-1};

    Int_t m_maxNumberOfSharedHits{1};
    Int_t m_restrictHitSharingToFirstNModulesInSector{2};

    Double_t m_maxOutlierChi2{50};

    Double_t m_xyHitAssignmentWindow{0.2};
    Double_t m_uvHitAssignmentWindow{0.3};

    Bool_t m_alignmentFileSet{false};
    std::string m_alignmentFile;

    Bool_t m_runAdaptiveVertexFitter{true};
    Bool_t m_disableAdaptiveFitterSeeding{true};

	Double_t m_adaptiveFitterSeedWindow{0.2};
	Double_t m_adaptiveFitterSeedTrackWindow{0.3};
	Int_t m_adaptiveFitterMaxNumberOfIterations{20};
	Double_t m_adaptiveFitterMaxTrackChi2{25};
	Double_t m_adaptiveFitterConvergenceMaxDeltaZ{0.01};

    Bool_t m_useFittedZPositionInKinematicFit{false};
    Bool_t m_useFittedPositionInKinematicFit{false};
    Bool_t m_restrictVertexPositionToTarget{true};


    Bool_t m_useFittedZPositionInGenericVertexKinematicFit{false};
    Bool_t m_useFittedPositionInGenericVertexKinematicFit{false};
    Bool_t m_restrictGenericVertexPositionToTarget{true};

    Int_t m_maxGenericVertexOutgoingTrackMultiplicity{0};

    Bool_t m_ingoreLastStationInVertexing{false};


    Bool_t m_weightLinkedTracksByDepositedCharge{false};

    Bool_t m_allowTrivialIncomingTracks{false};

    Bool_t m_reassignHitsAfterKinematicFit{false};

    ClassDef(MUonEReconstructionConfiguration, 2)
};


#endif //MUONERECONSTRUCTIONCONFIGURATION_H

