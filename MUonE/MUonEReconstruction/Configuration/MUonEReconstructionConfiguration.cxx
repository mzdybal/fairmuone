#include "MUonEReconstructionConfiguration.h"


#include <fairlogger/Logger.h>


MUonEReconstructionConfiguration::MUonEReconstructionConfiguration() {

    resetDefaultConfiguration();
}

Bool_t MUonEReconstructionConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    if(config["isMC"])
        m_isMC = config["isMC"].as<Bool_t>();
    else {
        LOG(error) << "Variable 'isMC' not set.";
        m_configuredCorrectly = false;
    }

    if(config["verbose"])
        m_verbose = config["verbose"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'verbose' not set. Defaulting to false.";
    }


    if(config["skipEventsWithMoreThanNHits"])
        m_skipEventsWithMoreThanNHits = config["skipEventsWithMoreThanNHits"].as<Int_t>();
    else {
        LOG(info) << "Variable 'skipEventsWithMoreThanNHits' not set. Defaulting to disabled.";
    }

    if(config["maxVertexChi2PerDegreeOfFreedom"])
        m_maxVertexChi2PerDegreeOfFreedom = config["maxVertexChi2PerDegreeOfFreedom"].as<Double_t>();
    else {
        LOG(info) << "Variable 'maxVertexChi2PerDegreeOfFreedom' not set. Defaulting to disabled.";
    }

    if(config["maxGenericVertexChi2PerDegreeOfFreedom"])
        m_maxGenericVertexChi2PerDegreeOfFreedom = config["maxGenericVertexChi2PerDegreeOfFreedom"].as<Double_t>();
    else {
        LOG(info) << "Variable 'maxGenericVertexChi2PerDegreeOfFreedom' not set. Defaulting to disabled.";
    }


    if(config["useSeedingSensorOnly"])
        m_useSeedingSensorOnly = config["useSeedingSensorOnly"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'useSeedingSensorOnly' not set. Defaulting to false.";
    }    


    if(config["runKinematicFit"])
        m_runKinematicFit = config["runKinematicFit"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'runKinematicFit' not set. Defaulting to true.";
    }  

    if(config["MSCorrectionAssumedBeamEnergy"])
        m_MSCorrectionAssumedBeamEnergy = config["MSCorrectionAssumedBeamEnergy"].as<Double_t>();
    else {
        LOG(info) << "Variable 'MSCorrectionAssumedBeamEnergy' not set. Defaulting to 160.";
    }      


    if(config["refitElectronTrackWithMSCorrection"])
        m_refitElectronTrackWithMSCorrection = config["refitElectronTrackWithMSCorrection"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'refitElectronTrackWithMSCorrection' not set. Defaulting to true.";
    }    

    if(config["refitMuonTrackWithMSCorrection"])
        m_refitMuonTrackWithMSCorrection = config["refitMuonTrackWithMSCorrection"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'refitMuonTrackWithMSCorrection' not set. Defaulting to false.";
    }    

    if(config["refitIncomingTrackWithMSCorrection"])
        m_refitIncomingTrackWithMSCorrection = config["refitIncomingTrackWithMSCorrection"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'refitIncomingTrackWithMSCorrection' not set. Defaulting to false.";
    }   

    if(config["addBaselineMSCorrectionToAllTracks"])
        m_addBaselineMSCorrectionToAllTracks = config["addBaselineMSCorrectionToAllTracks"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'addBaselineMSCorrectionToAllTracks' not set. Defaulting to false.";
    }     

    if(config["vertexSplittingAngleThreshold"])
        m_vertexSplittingAngleThreshold = config["vertexSplittingAngleThreshold"].as<Double_t>();
    else {
        LOG(info) << "Variable 'vertexSplittingAngleThreshold' not set. Defaulting to -1.";
    }        

    if(config["maxNumberOfSharedHits"])
        m_maxNumberOfSharedHits = config["maxNumberOfSharedHits"].as<Int_t>();
    else {
        LOG(info) << "Variable 'maxNumberOfSharedHits' not set. Defaulting to 1.";
    }

    if(config["restrictHitSharingToFirstNModulesInSector"])
        m_restrictHitSharingToFirstNModulesInSector = config["restrictHitSharingToFirstNModulesInSector"].as<Int_t>();
    else {
        LOG(info) << "Variable 'restrictHitSharingToFirstNModulesInSector' not set. Defaulting to 2.";
    }    

    if(config["maxOutlierChi2"])
        m_maxOutlierChi2 = config["maxOutlierChi2"].as<Double_t>();
    else {
        LOG(info) << "Variable 'maxOutlierChi2' not set. Defaulting to 25.";
    }

    if(config["xyHitAssignmentWindow"])
        m_xyHitAssignmentWindow = config["xyHitAssignmentWindow"].as<Double_t>();
    else {
        LOG(info) << "Variable 'xyHitAssignmentWindow' not set. Defaulting to 0.2 cm.";
    }

    if(config["uvHitAssignmentWindow"])
        m_uvHitAssignmentWindow = config["uvHitAssignmentWindow"].as<Double_t>();
    else {
        LOG(info) << "Variable 'uvHitAssignmentWindow' not set. Defaulting to 0.3 cm.";
    }

    if(config["alignmentFile"]) {
        m_alignmentFile = config["alignmentFile"].as<std::string>();
        m_alignmentFileSet = true;
    }
    else {
        LOG(info) << "Variable 'alignmentFile' not set.";
        m_alignmentFileSet = false;
    }

    if(config["runAdaptiveVertexFitter"])
        m_runAdaptiveVertexFitter = config["runAdaptiveVertexFitter"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'runAdaptiveVertexFitter' not set. Defaulting to true.";
    }

    if(config["disableAdaptiveFitterSeeding"])
        m_disableAdaptiveFitterSeeding = config["disableAdaptiveFitterSeeding"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'disableAdaptiveFitterSeeding' not set. Defaulting to true.";
    }

    if(config["adaptiveFitterSeedWindow"])
        m_adaptiveFitterSeedWindow = config["adaptiveFitterSeedWindow"].as<Double_t>();
    else {
        LOG(info) << "Variable 'adaptiveFitterSeedWindow' not set. Defaulting to 0.2.";
    }

    if(config["adaptiveFitterSeedTrackWindow"])
        m_adaptiveFitterSeedTrackWindow = config["adaptiveFitterSeedTrackWindow"].as<Double_t>();
    else {
        LOG(info) << "Variable 'adaptiveFitterSeedTrackWindow' not set. Defaulting to 0.3.";
    }

    if(config["adaptiveFitterMaxNumberOfIterations"])
        m_adaptiveFitterMaxNumberOfIterations = config["adaptiveFitterMaxNumberOfIterations"].as<Int_t>();
    else {
        LOG(info) << "Variable 'adaptiveFitterMaxNumberOfIterations' not set. Defaulting to 20.";
    }

    if(config["adaptiveFitterMaxTrackChi2"])
        m_adaptiveFitterMaxTrackChi2 = config["adaptiveFitterMaxTrackChi2"].as<Double_t>();
    else {
        LOG(info) << "Variable 'adaptiveFitterMaxTrackChi2' not set. Defaulting to 25.";
    }

    if(config["adaptiveFitterConvergenceMaxDeltaZ"])
        m_adaptiveFitterConvergenceMaxDeltaZ = config["adaptiveFitterConvergenceMaxDeltaZ"].as<Double_t>();
    else {
        LOG(info) << "Variable 'adaptiveFitterConvergenceMaxDeltaZ' not set. Defaulting to 0.01.";
    }

    if(config["useFittedZPositionInKinematicFit"])
        m_useFittedZPositionInKinematicFit = config["useFittedZPositionInKinematicFit"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'useFittedZPositionInKinematicFit' not set. Defaulting to false.";
    }

    if(config["useFittedPositionInKinematicFit"])
        m_useFittedPositionInKinematicFit = config["useFittedPositionInKinematicFit"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'useFittedPositionInKinematicFit' not set. Defaulting to false.";
    }

    if(config["restrictVertexPositionToTarget"])
        m_restrictVertexPositionToTarget = config["restrictVertexPositionToTarget"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'restrictVertexPositionToTarget' not set. Defaulting to true.";
    }


    if(config["useFittedZPositionInGenericVertexKinematicFit"])
        m_useFittedZPositionInGenericVertexKinematicFit = config["useFittedZPositionInGenericVertexKinematicFit"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'useFittedZPositionInGenericVertexKinematicFit' not set. Defaulting to false.";
    }

    if(config["useFittedPositionInGenericVertexKinematicFit"])
        m_useFittedPositionInGenericVertexKinematicFit = config["useFittedPositionInGenericVertexKinematicFit"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'useFittedPositionInGenericVertexKinematicFit' not set. Defaulting to false.";
    }

    if(config["restrictGenericVertexPositionToTarget"])
        m_restrictGenericVertexPositionToTarget = config["restrictGenericVertexPositionToTarget"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'restrictGenericVertexPositionToTarget' not set. Defaulting to true.";
    }

    if(config["maxGenericVertexOutgoingTrackMultiplicity"])
        m_maxGenericVertexOutgoingTrackMultiplicity = config["maxGenericVertexOutgoingTrackMultiplicity"].as<Int_t>();
    else {
        LOG(info) << "Variable 'maxGenericVertexOutgoingTrackMultiplicity' not set. Defaulting to 0 (disabled).";
    }


    if(config["ingoreLastStationInVertexing"])
        m_ingoreLastStationInVertexing = config["ingoreLastStationInVertexing"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'ingoreLastStationInVertexing' not set. Default is false.";
    }


    if(config["weightLinkedTracksByDepositedCharge"])
        m_weightLinkedTracksByDepositedCharge = config["weightLinkedTracksByDepositedCharge"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'weightLinkedTracksByDepositedCharge' not set. The tracks will be considered equal by default.";
    }

    if(config["allowTrivialIncomingTracks"])
        m_allowTrivialIncomingTracks = config["allowTrivialIncomingTracks"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'm_allowTrivialIncomingTracks' not set. Only non-trivial tracks will be allowed.";
    }

    if(config["reassignHitsAfterKinematicFit"])
        m_reassignHitsAfterKinematicFit = config["reassignHitsAfterKinematicFit"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'm_reassignHitsAfterKinematicFit' not set. Default is false.";
    }    

    if(!m_configuredCorrectly)
        LOG(info) << "Generation configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEReconstructionConfiguration::logCurrentConfiguration() const {
    
	LOG(info) << "";
    if(m_isMC)
        LOG(info) << "Reconstruction will run for MonteCarlo events.";
    else
        LOG(info) << "Reconstruction will run for data events.";

    LOG(info) << "verbose: " << m_verbose;

    LOG(info) << "skipEventsWithMoreThanNHits: " << m_skipEventsWithMoreThanNHits;
    LOG(info) << "maxVertexChi2PerDegreeOfFreedom: " << m_maxVertexChi2PerDegreeOfFreedom;
    LOG(info) << "maxGenericVertexChi2PerDegreeOfFreedom: " << m_maxGenericVertexChi2PerDegreeOfFreedom;

    LOG(info) << "useSeedingSensorOnly: " << m_useSeedingSensorOnly;
    LOG(info) << "runKinematicFit: " << m_runKinematicFit;
    LOG(info) << "MSCorrectionAssumedBeamEnergy: " << m_MSCorrectionAssumedBeamEnergy;
    LOG(info) << "refitElectronTrackWithMSCorrection: " << m_refitElectronTrackWithMSCorrection;
    LOG(info) << "refitMuonTrackWithMSCorrection: " << m_refitMuonTrackWithMSCorrection;
    LOG(info) << "refitIncomingTrackWithMSCorrection: " << m_refitIncomingTrackWithMSCorrection;
    LOG(info) << "addBaselineMSCorrectionToAllTracks: " << m_addBaselineMSCorrectionToAllTracks;   
    LOG(info) << "vertexSplittingAngleThreshold: " << m_vertexSplittingAngleThreshold;

    LOG(info) << "maxNumberOfSharedHits: " << m_maxNumberOfSharedHits;
    LOG(info) << "refitElectronTrackWithMSCorrection: " << m_refitElectronTrackWithMSCorrection;
    LOG(info) << "maxOutlierChi2: " << m_maxOutlierChi2;
    LOG(info) << "xyHitAssignmentWindow: " << m_xyHitAssignmentWindow;
    LOG(info) << "uvHitAssignmentWindow: " << m_uvHitAssignmentWindow;


    if(m_weightLinkedTracksByDepositedCharge)
        LOG(info) << "Linked tracks will be weighted by 1/position in vector sorted by deposited charge.";
    else
        LOG(info) << "Linked tracks will be considered equal.";

    if(m_allowTrivialIncomingTracks)
        LOG(info) << "Trivial incoming tracks are allowed.";
    else
        LOG(info) << "Only non-trivial incoming tracks are allowed.";

    if(m_alignmentFileSet)
        LOG(info) << "Alignment file used: " << m_alignmentFile;

    LOG(info) << "m_runAdaptiveVertexFitter: " << m_runAdaptiveVertexFitter;
    if(m_runAdaptiveVertexFitter) {

        LOG(info) << "disableAdaptiveFitterSeeding: " << m_disableAdaptiveFitterSeeding;
        LOG(info) << "adaptiveFitterSeedWindow: " << m_adaptiveFitterSeedWindow;
        LOG(info) << "adaptiveFitterSeedTrackWindow: " << m_adaptiveFitterSeedTrackWindow;
        LOG(info) << "adaptiveFitterMaxNumberOfIterations: " << m_adaptiveFitterMaxNumberOfIterations;
        LOG(info) << "adaptiveFitterMaxTrackChi2: " << m_adaptiveFitterMaxTrackChi2;
        LOG(info) << "adaptiveFitterConvergenceMaxDeltaZ: " << m_adaptiveFitterConvergenceMaxDeltaZ;
    }

    LOG(info) << "useFittedZPositionInKinematicFit: " << m_useFittedZPositionInKinematicFit; 
    LOG(info) << "useFittedPositionInKinematicFit: " << m_useFittedPositionInKinematicFit; 
    LOG(info) << "restrictVertexPositionToTarget: " << m_restrictVertexPositionToTarget; 

    LOG(info) << "maxGenericVertexOutgoingTrackMultiplicity: " << m_maxGenericVertexOutgoingTrackMultiplicity; 

    LOG(info) << "ingoreLastStationInVertexing: " << m_ingoreLastStationInVertexing; 

    LOG(info) << "useFittedZPositionInGenericVertexKinematicFit: " << m_useFittedZPositionInGenericVertexKinematicFit; 
    LOG(info) << "useFittedPositionInGenericVertexKinematicFit: " << m_useFittedPositionInGenericVertexKinematicFit; 
    LOG(info) << "restrictGenericVertexPositionToTarget: " << m_restrictGenericVertexPositionToTarget; 

    LOG(info) << "reassignHitsAfterKinematicFit: " << m_reassignHitsAfterKinematicFit;
}

void MUonEReconstructionConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_isMC = true;
    m_verbose = false;

    m_skipEventsWithMoreThanNHits = -1;
    m_maxVertexChi2PerDegreeOfFreedom = -1;
    m_maxGenericVertexChi2PerDegreeOfFreedom = -1;


    m_useSeedingSensorOnly = false;
    m_runKinematicFit = true;
    m_MSCorrectionAssumedBeamEnergy = 160;
    m_refitElectronTrackWithMSCorrection = true;
    m_refitMuonTrackWithMSCorrection = false;
    m_refitIncomingTrackWithMSCorrection = false;
    m_addBaselineMSCorrectionToAllTracks = false;
    m_vertexSplittingAngleThreshold = -1;

    m_maxNumberOfSharedHits = 1;
    m_restrictHitSharingToFirstNModulesInSector = 2;

    m_maxOutlierChi2 = 50;
    m_xyHitAssignmentWindow = 0.2;
    m_uvHitAssignmentWindow = 0.3;

    m_alignmentFileSet = false;
    m_alignmentFile.clear();


    m_runAdaptiveVertexFitter = true;
    m_disableAdaptiveFitterSeeding = true;
    m_adaptiveFitterSeedWindow = 0.2;
    m_adaptiveFitterSeedTrackWindow = 0.3;
    m_adaptiveFitterMaxNumberOfIterations = 20;
    m_adaptiveFitterMaxTrackChi2 = 25;
    m_adaptiveFitterConvergenceMaxDeltaZ = 0.01;

    m_useFittedZPositionInKinematicFit = false;
    m_useFittedPositionInKinematicFit = false;
    m_restrictVertexPositionToTarget = true;

    m_maxGenericVertexOutgoingTrackMultiplicity = 0;
    m_useFittedZPositionInGenericVertexKinematicFit = false;
    m_useFittedPositionInGenericVertexKinematicFit = false;
    m_restrictGenericVertexPositionToTarget = false;

    m_ingoreLastStationInVertexing = false;

    m_weightLinkedTracksByDepositedCharge = false;
    m_allowTrivialIncomingTracks = false;

    m_reassignHitsAfterKinematicFit = false;

}

ClassImp(MUonEReconstructionConfiguration)