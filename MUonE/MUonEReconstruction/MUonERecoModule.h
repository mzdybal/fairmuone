#ifndef MUONERECOMODULE_H
#define MUONERECOMODULE_H

#include "Rtypes.h"
#include "TMath.h"
#include "TVector3.h"
#include <fairlogger/Logger.h>

#include "FairGeoLoader.h"
#include "FairGeoMedia.h"
#include "FairGeoInterface.h"
#include "FairGeoMedium.h"


#include "MUonEDetectorModuleGeometryConfiguration.h"
#include "MUonEDetectorStationModuleConfiguration.h"
#include <cmath>

/*
	Class used to store info on a single module of the tracking system in a unified way for all input types.
*/

class MUonERecoModule {

public:

	MUonERecoModule() = default;


	MUonERecoModule(Int_t index, Int_t sector, Int_t stationID, Int_t moduleID, Double_t z, Bool_t isMC, MUonEDetectorStationModuleConfiguration const& moduleConfig)
		: m_index(index), m_sector(sector), m_stationID(stationID), m_moduleID(moduleID),
		m_twoSensorsPerModule(moduleConfig.configuration().geometry().twoSensorsPerModule()),
		m_projection(moduleConfig.projection().name()), m_isStereo('u' == moduleConfig.projection().name() || 'v' == moduleConfig.projection().name()),
		m_z(z), m_sensorThickness(moduleConfig.configuration().geometry().sensorThickness()), m_hitResolution(moduleConfig.hitResolution()),
		m_negateHitPosition(isMC ? false : moduleConfig.projection().negateDataHitPosition()),
		m_tilt(moduleConfig.projection().tilt()),
		m_tiltSine(TMath::Sin(moduleConfig.projection().tilt() * TMath::DegToRad())),
		m_tiltCosine(TMath::Cos(moduleConfig.projection().tilt() * TMath::DegToRad())),
		m_angle(moduleConfig.projection().angle()),
		m_angleSine(TMath::Sin((moduleConfig.projection().angle()) * TMath::DegToRad())),
		m_angleCosine(TMath::Cos((moduleConfig.projection().angle()) * TMath::DegToRad())),
		m_numberOfStrips(moduleConfig.configuration().geometry().numberOfStrips()), m_sensorSize(moduleConfig.configuration().geometry().sensorSizeMeasurementDirection()),
		m_distanceBetweenSensors(moduleConfig.configuration().geometry().distanceBetweenSensorCenters()),
		m_seedSensor(moduleConfig.configuration().digitization().seedSensor())
		{

			//get radiation length from G4 material
			static FairGeoLoader* geoLoad = FairGeoLoader::Instance();

			if(!geoLoad) //may happen if generation step isn't run
				geoLoad = new FairGeoLoader("TGeo", "Geo Loader");

			if(!geoLoad)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoLoader to get radiation length";

			static FairGeoInterface* geoFace = geoLoad->getGeoInterface();

			if(!geoFace)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoInterface to get radiation length";

			static FairGeoMedia* media = geoFace->getMedia();

			if(!media)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoMedia to get radiation length";

			FairGeoMedium* medium = media->getMedium(moduleConfig.configuration().geometry().sensorMaterial().c_str());		
			
			if(!medium) {//may happen if generation step isn't run

				//the name has to be hardcoded here for now, because FairRunAna does not allow to set it from run macro
				//copied from FairRunSim::SetMaterials
				    TString Mat = "";
				    TString work = getenv("GEOMPATH");
				    work.ReplaceAll("//", "/");
				    if (work.IsNull()) {
					work = getenv("VMCWORKDIR");
					Mat = work + "/geometry/";
					Mat.ReplaceAll("//", "/");
				    } else {
					Mat = work;
					if (!Mat.EndsWith("/")) {
					    Mat += "/";
					}
				    }
				    TString MatFname = Mat + "media.geo"; //media.geo hardcoded

				geoFace->setMediaFile(MatFname.Data());
				geoFace->readMedia();

				media = geoFace->getMedia();
				medium = media->getMedium(moduleConfig.configuration().geometry().sensorMaterial().c_str());
			}

			if(!medium)
				LOG(fatal) << "Reconstruction: Failed to retrieve module medium to get radiation length";
		
			m_radiationLength = medium->getRadiationLength();
			LOG(info) << "Reconstruction: Initializing module " << index << " with radiation length " << m_radiationLength;

		}


	Int_t index() const {return m_index;}
	Int_t sector() const {return m_sector;}

	Int_t stationID() const {return m_stationID;}
	Int_t moduleID() const {return m_moduleID;}

	Bool_t twoSensorsPerModule() const {return m_twoSensorsPerModule;}

	char projection() const {return m_projection;}

	Double_t z() const {return m_z + m_zOffset;}
	Double_t sensorThickness() const {return m_sensorThickness;}
	
	Double_t hitResolution() const {return m_hitResolution;}

	Double_t tiltSine() const {return m_tiltSine;}
	Double_t tiltCosine() const {return m_tiltCosine;}

	Double_t angleSine() const {return m_angleSine;}
	Double_t angleCosine() const {return m_angleCosine;}

	Double_t xOffset() const {return m_xOffset;}
	Double_t yOffset() const {return m_yOffset;}
	Double_t zOffset() const {return m_zOffset;}
	Double_t angleOffset() const {return m_angleOffset;}
	Double_t tiltOffset() const {return m_tiltOffset;}

	Int_t numberOfStrips() const {return m_numberOfStrips;}
	Double_t sensorSize() const {return m_sensorSize;}
	Double_t distanceBetweenSensors() const {return m_distanceBetweenSensors;}
	Int_t seedSensor() const {return m_seedSensor;}

	Double_t radiationLength() const {return m_radiationLength;}

	void setAlignmentParameters(Double_t xOffset, Double_t yOffset, Double_t zOffset, Double_t angleOffset, Double_t tiltOffset) {

		m_xOffset = xOffset;
		m_yOffset = yOffset;
		m_zOffset = zOffset;
		m_angleOffset = angleOffset;
		m_tiltOffset = tiltOffset;

		m_angleSine = TMath::Sin((m_angle + angleOffset) * TMath::DegToRad());
		m_angleCosine = TMath::Cos((m_angle + angleOffset) * TMath::DegToRad());

		m_tiltSine = TMath::Sin((m_tilt + tiltOffset) * TMath::DegToRad());
		m_tiltCosine = TMath::Cos((m_tilt + tiltOffset) * TMath::DegToRad());			
	}


	//convert stub information to position in cm)
	Double_t stubToPosition(Double_t seeding_center_strip, Double_t bend) const {

		if(m_negateHitPosition)
			return -1 * ((seeding_center_strip + 0.5 + 0.5 * bend) * m_sensorSize / m_numberOfStrips - 0.5 * m_sensorSize);
		else
			return (seeding_center_strip + 0.5 + 0.5 * bend) * m_sensorSize / m_numberOfStrips - 0.5 * m_sensorSize;
	}


	//calculate position in plane perpendicular to the Z axis (without tilt)
	//tilt is accounted for by the hit position (see MUonERecoHit.h)
	//this method is used to calculate local position of point on the 3D track
	Double_t localCoordinatePerpendicular(Double_t x, Double_t y) const {
		return (x - m_xOffset)*m_angleCosine + (y - m_yOffset)*m_angleSine;
	}

	Bool_t sameModuleAs(MUonERecoModule const& module) const {return sameSectorAs(module) && (m_index == module.index());}
	Bool_t sameSectorAs(MUonERecoModule const& module) const {return m_sector == module.sector();}
	Bool_t sameProjectionAs(MUonERecoModule const& module) const {return m_projection == module.projection();}
	Bool_t isStereo() const {return m_isStereo;}

private:

	Int_t m_index{-1}; //index in m_modulesPerSector vector
	Int_t m_sector{-1}; //0-before 1st target, 1 after first, 2 after 2nd and so on

	//wrt. detector configuration
	Int_t m_stationID{-1};
	Int_t m_moduleID{-1};

	Bool_t m_twoSensorsPerModule{true};

	char m_projection{'x'};
	Bool_t m_isStereo{false};


	Double_t m_sensorThickness{0}; //for easy access in MS calculation

	Double_t m_hitResolution{0}; //for simplified geometry

	Bool_t m_negateHitPosition{false}; //can be turned on only when real data is processed


	//the actual values of tilt, angle and offsets are hidden
	//only sines and cosines are used outside

	Double_t m_tiltSine{0};
	Double_t m_tiltCosine{0};

	//angle around the Z axis
	Double_t m_angleSine{0}; //includes offset from alignment
	Double_t m_angleCosine{0};

	//additional geometry info
	Int_t m_numberOfStrips{0};
	Double_t m_sensorSize{0};
	Double_t m_distanceBetweenSensors{0};
	Int_t m_seedSensor{-1};

	Double_t m_radiationLength{0};

    ClassDef(MUonERecoModule, 2)


protected: //for alignment module class

	Double_t m_z{0}; //z position of the module midpoint; only stored internally, returned value contains alignment to avoid confusion
	Double_t m_tilt{0}; //stored only internally to avoid confuson with alignment; 
	Double_t m_angle{0}; //stored only internally to avoid confuson with alignment; 

	Double_t m_xOffset{0};
	Double_t m_yOffset{0};
	Double_t m_zOffset{0};
	Double_t m_angleOffset{0};
	Double_t m_tiltOffset{0};
};


#endif //MUONERECOMODULE_H
