#ifndef MUONERECOHIT_H
#define MUONERECOHIT_H

#include "Rtypes.h"
#include "TMath.h"
#include "MUonETrackerStub.h"
#include "MUonERecoModule.h"

#include <iostream>

/*
	Class containing hit information. Used to unify all possible event input types.

	To work around the tilt, we instead consider modules perpendicular to the beam axis and placed at z = z_hit.
	To do that we need to modify the hit position based on tilt value. These modified values have 'perpendicular' in their names.
	Also note that if any tilt is present then, with an exception of (0,0), hit_z =/= module_z.

	For stubs, z position is assumed to be the same as module_z (i.e. exactly in the middle between two sensors) and the position
	is assumed to be the mean position between seeding and correlation sensor (i.e. seeding_position + 0.5*bend).
	
	Linked tracks are sorted based on their deposited energy contribution to both stub clusters. The highest goes first.
	
*/

class MUonERecoHit {

public:

	MUonERecoHit() = default;

	MUonERecoHit(const MUonETrackerStub& stub, Int_t moduleSector, Int_t moduleIndex, const std::vector<std::vector<MUonERecoModule>>* modulesPerSector, Int_t index, Bool_t useSeedingSensorOnly, UShort_t bx = 0)
		: m_moduleSector(moduleSector), m_moduleIndex(moduleIndex), m_modulesPerSector(modulesPerSector),
		m_cicID(stub.sensorHalf()),
		m_position(module().stubToPosition(stub.seedClusterCenterStrip(), useSeedingSensorOnly ? 0 : stub.bend())), 
		m_positionError(module().hitResolution()), 
		m_positionErrorMeasurement(module().hitResolution()),
		m_seedingClusterLinkedTrackIds(stub.seedingClusterLinkedTrackIds()),
		m_seedingClusterLinkedTrackDeposits(stub.seedingClusterLinkedTrackDeposits()),
		m_correlationClusterLinkedTrackIds(stub.correlationClusterLinkedTrackIds()),
		m_correlationClusterLinkedTrackDeposits(stub.correlationClusterLinkedTrackDeposits()),
		m_seedClusterWidth(stub.seedClusterWidth()),
		m_correlationClusterWidth(stub.correlationClusterWidth()),
		m_index(index),
		m_bx(bx)
		{
			if(useSeedingSensorOnly) {

				Double_t sensor_shift = 0.5 * module().distanceBetweenSensors();
				if(0 == module().seedSensor())
					sensor_shift *= -1;

				//additional terms, because both sensors are rotated wrt. module's midpoint
				m_z = module().z() + sensor_shift * module().tiltCosine() - position() * module().tiltSine();
				m_positionPerpendicular = sensor_shift * module().tiltSine() + position() * module().tiltCosine();

			} else {

				m_z = module().z() - position() * module().tiltSine();
				m_positionPerpendicular = position() * module().tiltCosine();
			}
		}

	MUonERecoHit(Float_t strip, Int_t cicID, Float_t bend, Int_t moduleSector, Int_t moduleIndex, const std::vector<std::vector<MUonERecoModule>>* modulesPerSector, Int_t index, Bool_t useSeedingSensorOnly, UShort_t bx = 0, UInt_t superID = 0)
		: m_moduleSector(moduleSector), m_moduleIndex(moduleIndex), m_modulesPerSector(modulesPerSector),
		m_cicID(cicID),
		m_position(module().stubToPosition(strip, useSeedingSensorOnly ? 0 : bend)), 
		m_positionError(module().hitResolution()),
		m_positionErrorMeasurement(module().hitResolution()),
		m_index(index),
		m_bx(bx),
		m_superID(superID)
		{
			if(useSeedingSensorOnly) {

				Double_t sensor_shift = 0.5 * module().distanceBetweenSensors();
				if(0 == module().seedSensor())
					sensor_shift *= -1;

				//additional terms, because both sensors are rotated wrt. module's midpoint
				m_z = module().z() + sensor_shift * module().tiltCosine() - position() * module().tiltSine();
				m_positionPerpendicular = sensor_shift * module().tiltSine() + position() * module().tiltCosine();

			} else {

				m_z = module().z() - position() * module().tiltSine();
				m_positionPerpendicular = position() * module().tiltCosine();
			}
		}			


	Int_t moduleIndex() const {return m_moduleIndex;}
	Int_t moduleSector() const {return m_moduleSector;}
	const MUonERecoModule& module() const {return (*m_modulesPerSector)[m_moduleSector][m_moduleIndex];}

	Int_t cicID() const {return m_cicID;}

	Double_t z() const {return m_z;}

	Double_t position() const {return m_position;}
	Double_t positionError() const {return m_positionError;}
	Double_t positionErrorMeasurement() const {return m_positionErrorMeasurement;}
	Double_t positionErrorMultipleScattering() const {return m_positionErrorMultipleScattering;}

	Double_t positionPerpendicular() const {return m_positionPerpendicular;}

	std::vector<Int_t> const& seedingClusterLinkedTrackIds() const {return m_seedingClusterLinkedTrackIds;}
	std::vector<Double_t> const& seedingClusterLinkedTrackDeposits() const {return m_seedingClusterLinkedTrackDeposits;}
    std::vector<Int_t> const& correlationClusterLinkedTrackIds() const {return m_correlationClusterLinkedTrackIds;}
    std::vector<Double_t> const& correlationClusterLinkedTrackDeposits() const {return m_correlationClusterLinkedTrackDeposits;}

    Double_t seedClusterWidth() const {return m_seedClusterWidth;}
    Double_t correlationClusterWidth() const {return m_correlationClusterWidth;}

	Int_t index() const {return m_index;}
	UShort_t bx() const {return m_bx;}
	UInt_t superID() const {return m_superID;}

	void setMultipleScatteringError(Double_t error) {

		m_positionErrorMultipleScattering = error;
		m_positionError = TMath::Sqrt(m_positionErrorMeasurement*m_positionErrorMeasurement + m_positionErrorMultipleScattering*m_positionErrorMultipleScattering);
	}

private:

	Int_t m_moduleIndex{-1}; 	
    Int_t m_moduleSector{-1};
									//position of hit's module in the modules container
	const std::vector<std::vector<MUonERecoModule>>* m_modulesPerSector {nullptr}; 	//an access point to modules container

	Int_t m_cicID{-1};

	Double_t m_z{0};

	Double_t m_position{0};			//position in module's coordinates
	Double_t m_positionError{0}; 	//equivalent to modules's hit resolution

	Double_t m_positionErrorMeasurement{0};
	Double_t m_positionErrorMultipleScattering{0};

	Double_t m_positionPerpendicular{0}; 		//position in plane perpendicular to beam axis (if the module is not tilted, it's equivalent to m_position)
												//note that it doesn't depend on the angle around beam axis, only on the amount of tilt

	//valid only for MonteCarlo
	std::vector<Int_t> m_seedingClusterLinkedTrackIds;
	std::vector<Double_t> m_seedingClusterLinkedTrackDeposits;
    std::vector<Int_t> m_correlationClusterLinkedTrackIds;
    std::vector<Double_t> m_correlationClusterLinkedTrackDeposits;

    Double_t m_seedClusterWidth{0};
    Double_t m_correlationClusterWidth{0};

	Int_t m_index{-1};  //in the input from digi/data
	UShort_t m_bx{0};
	UInt_t m_superID{0};

public:

	//used to check whether two hit objects describe the same hit
	//it's enough to check module's indices instead of z position
    friend inline bool operator==(MUonERecoHit const& lhs, MUonERecoHit const& rhs){ 
        return lhs.module().sameModuleAs(rhs.module()) && fabs(lhs.cicID() - rhs.cicID()) < 1e-7 && fabs(lhs.position() - rhs.position()) < 1e-7;
    }
    friend inline bool operator!=(MUonERecoHit const& lhs, MUonERecoHit const& rhs){return !operator==(lhs,rhs);}

    ClassDef(MUonERecoHit,2)

};



#endif //MUONERECOHIT_H


