#ifndef MUONERECOTRACK2D_H
#define MUONERECOTRACK2D_H

#include "Rtypes.h"
#include "TMath.h"

#include <vector>
#include <utility>

#include "MUonERecoHit.h"

/*
	Used to store 2D tracks in x and y projections with their assigned hits.
	x/y = m_slope*(z - m_z0) + m_x0. Z0 can be chosen arbitrarily and is not a parameter of the fit
	we choose it to be at the z of interaction
*/

class MUonERecoTrack2D {


public:

	MUonERecoTrack2D() = default;

	//creates track form a pair of hits from different modules
	//parameters are calculated from their positions
	MUonERecoTrack2D(MUonERecoHit const& hit1, MUonERecoHit const& hit2)
		: m_hits({hit1, hit2}),
		m_slope((hit2.positionPerpendicular() - hit1.positionPerpendicular()) / (hit2.z() - hit1.z())),
		m_x0(hit1.positionPerpendicular() - m_slope*hit1.z())
		{}


	//for direct access to hits container see setHits() below
	std::vector<MUonERecoHit> const& hits() const {return m_hits;}
	std::vector<MUonERecoHit> hitsCopy() const {return m_hits;}

	Int_t numberOfHits() const {return m_hits.size();}

	Double_t z0() const {return m_z0;}

	Double_t slope() const {return m_slope;}
	Double_t slopeError() const {return m_slopeError;}

	Double_t x0() const {return m_x0;}
	Double_t x0Error() const {return m_x0Error;}

	//defined for clarity when working with y-projection
	Double_t y0() const {return m_x0;}
	Double_t y0Error() const {return m_x0Error;}	

	Double_t chi2() const {return m_chi2;}
	Int_t degreesOfFreedom() const {return (numberOfHits() - 2);} //x0 and slope are fit parameters
	Double_t chi2PerDegreeOfFreedom() const {return m_chi2/degreesOfFreedom();}



	Double_t valueAtZ(Double_t z) const {return m_slope*(z - m_z0) + m_x0;}
	Double_t errorAtZ(Double_t z) const {return TMath::Sqrt(((z - m_z0)*m_slopeError)*((z - m_z0)*m_slopeError) + m_x0Error*m_x0Error);}

	//both versions defined for clarity in both projections
	Double_t x(Double_t z) const {return valueAtZ(z);}
	Double_t y(Double_t z) const {return valueAtZ(z);}

	//absolute distance from track to hit in the plane perpendicular to the Z axis
	//note that 2D tracks are defined in a given projection, so there's no need for coordinate transformations
	Double_t distanceToHit(Double_t hit_z, Double_t hit_position_perpendicular) const {return fabs(valueAtZ(hit_z) - hit_position_perpendicular);}
	Double_t distanceToHit(const MUonERecoHit& hit) const {return distanceToHit(hit.z(), hit.positionPerpendicular());}


	//methods for track creation and filtering

	//add closest hit within distance threshold from the module
	//returns true if hit was found and added
	Int_t findClosestHit(const std::vector<MUonERecoHit>& hits_from_module, Double_t threshold) {

		Int_t closest_hit_index = -1;
		Double_t closest_hit_distance = 9999999999999;

		for(Int_t hit_index = 0; hit_index < hits_from_module.size(); ++hit_index) {

			auto const& hit = hits_from_module[hit_index];
			Double_t distance_from_track = distanceToHit(hit);

			if(distance_from_track < threshold && distance_from_track < closest_hit_distance) {

				closest_hit_distance = distance_from_track;
				closest_hit_index = hit_index;
			}
		}

		return closest_hit_index;
	}

	Bool_t addClosestHit(const std::vector<MUonERecoHit>& hits_from_module, Double_t threshold) {

		Int_t closest_hit_index = findClosestHit(hits_from_module, threshold);

		if(-1 != closest_hit_index) {

			m_hits.emplace_back(hits_from_module[closest_hit_index]);
			return true;
		}

		return false;
	}

	//used for hit reassignment after the kinematic fit
	Bool_t reassignClosestHit(const std::vector<MUonERecoHit>& hitsInModule, Int_t hitIndex, Double_t threshold) {

		Int_t closest_hit_index = findClosestHit(hitsInModule, threshold);

		if(-1 != closest_hit_index && (m_hits[hitIndex] != hitsInModule[closest_hit_index])) {

			m_hits[hitIndex] = hitsInModule[closest_hit_index];
			return true;
		}

		return false;
	}

	//remove hits shared with given track from m_hits collection
	//returns true if track is left with only one hit, so it can be removed
	Bool_t removeHitsSharedWith(MUonERecoTrack2D const& track) {

		auto const& track_hits = track.hits();

		m_hits.erase(std::remove_if(m_hits.begin(), m_hits.end(), [&track_hits](MUonERecoHit const& hit){

			for(auto const& track_hit : track_hits)
				if(hit == track_hit)	//see MUonERecoHit.h for equality definition
					return true;

			return false;
		}), m_hits.end());

		return m_hits.size() <= 1;
	}

	std::pair<Int_t, Int_t> numberOfHitsSharedWith(MUonERecoTrack2D const& track, Int_t restrictHitSharingToFirstNModulesInSector = -1) {

		Int_t below_count = 0;
		Int_t above_count = 0;

		auto const& track_hits = track.hits();

		for(auto const& hit : m_hits) {

			for(auto const& track_hit : track_hits) {

				if(hit == track_hit) {

					hit.moduleIndex() < restrictHitSharingToFirstNModulesInSector ? ++below_count : ++above_count;
					break;
				}
			}
		}

		return std::make_pair(below_count, above_count); 
	}



	//methods for fitter

	//call this after new track parameters have been set or hits vector has been modified
	Double_t recalculateChi2() {

		m_chi2 = 0;

		for(auto const& hit : m_hits){
			Double_t chi2_sqrt = distanceToHit(hit) / hit.positionError();
			m_chi2 += chi2_sqrt * chi2_sqrt;
		}

		return m_chi2;
	}	



	//chosen arbitrarily, not a fit parameter, no error needed
	void setZ0(Double_t z0) {m_z0 = z0;}

	void setSlope(Double_t slope, Double_t error) {

		m_slope = slope;
		m_slopeError = error;		
	}

	void setX0(Double_t x0, Double_t error) {

		m_x0 = x0;
		m_x0Error = error;
	}

	//defined for clarity with y-projection tracks
	void setY0(Double_t y0, Double_t error) {setX0(y0, error);}

	//used by fitter if any outliers were removed
	void setHits(std::vector<MUonERecoHit>& hits) {m_hits = hits;}


private:
	
	std::vector<MUonERecoHit> m_hits;

	Double_t m_z0{0};

	Double_t m_slope{0};
	Double_t m_slopeError{0};

	Double_t m_x0{0};
	Double_t m_x0Error{0};

	Double_t m_chi2{0};


	//used for checking whether two tracks are equivalent, that is
	//have the same number of hits and share a given percentage of them
	//for now it's 100%
    friend inline bool operator==(MUonERecoTrack2D const& lhs, MUonERecoTrack2D const& rhs){ 
        
		//tracks are always different if they have a different number of hits
		if(lhs.numberOfHits() != rhs.numberOfHits())
			return false;

		//tracks are the same if they share at least a set number of hits
		Int_t max_shared = lhs.numberOfHits(); //this could possibly be tweaked for speed
		Int_t ctr = 0;

		for(auto const& lh : lhs.hits())
			for(auto const& rh : rhs.hits())
				if(lh == rh && ++ctr >= max_shared) //for hit comparison, see MUonERecoHit.h
					return true;

		return false;
    }
    friend inline bool operator!=(MUonERecoTrack2D const& lhs, MUonERecoTrack2D const& rhs){return !operator==(lhs,rhs);}

	//if sorting is required, prefer tracks with higher number of hits and lower chi2
	//note that for the same number of hits, it's enough to check for chi2 and not chi2 per degree of freedom
	//worse track is considered to be of lower value, that is worse < better is true
    friend inline bool operator< (MUonERecoTrack2D const& lhs, MUonERecoTrack2D const& rhs) {

		if(lhs.numberOfHits() != rhs.numberOfHits())
			return lhs.numberOfHits() < rhs.numberOfHits();
		else 		
        	return lhs.chi2() > rhs.chi2();
    }
    friend inline bool operator> (MUonERecoTrack2D const& lhs, MUonERecoTrack2D const& rhs) {return  operator< (rhs,lhs);}
    friend inline bool operator<=(MUonERecoTrack2D const& lhs, MUonERecoTrack2D const& rhs) {return !operator> (lhs,rhs);}
    friend inline bool operator>=(MUonERecoTrack2D const& lhs, MUonERecoTrack2D const& rhs) {return !operator< (lhs,rhs);}


    ClassDef(MUonERecoTrack2D,2)

};

#endif//MUONERECOTRACK2D_H

