#ifndef MUONERECOVERTEX_H
#define MUONERECOVERTEX_H

#include "MUonERecoTarget.h"
#include "MUonERecoTrack3D.h"

#include "TVector3.h"
#include "Math/SMatrix.h"

#include <cmath>

/*
	Used to store a vertex of a scattering event.
*/

class MUonERecoVertex {

public:

	MUonERecoVertex() = default;

	//constructs vertex from a single incoming and two outgoing tracks
	//initial x and y coordinates are calculated as arithmetic means of the incoming tracks position and the arithmetic mean of outgoing tracks positions
	//initial z is set to target's z
	MUonERecoVertex(MUonERecoTrack3D const& incomingTrack, MUonERecoTrack3D const& firstOutgoingTrack, MUonERecoTrack3D const& secondOutgoingTrack, MUonERecoTarget const& target, Bool_t alternativePIDHypothesis = false)
		: m_incomingTrack(incomingTrack), m_firstOutgoingTrack(firstOutgoingTrack), m_secondOutgoingTrack(secondOutgoingTrack),
		m_stationIndex(target.station()),
		m_chi2(incomingTrack.chi2() + firstOutgoingTrack.chi2() + secondOutgoingTrack.chi2()),
		m_alternativePIDHypothesis(alternativePIDHypothesis)
		{

			//calculate theta angles
			recalculateAngles();
	
		}	


	MUonERecoTrack3D const& incomingTrack() const {return m_incomingTrack;}
	MUonERecoTrack3D const& firstOutgoingTrack() const {return m_firstOutgoingTrack;}
	MUonERecoTrack3D const& secondOutgoingTrack() const {return m_secondOutgoingTrack;}

	//used to add MS and refit
	//methods above should be prefered if modifications are not needed
	MUonERecoTrack3D& incomingTrackRef() {return m_incomingTrack;}
	MUonERecoTrack3D& firstOutgoingTrackRef() {return m_firstOutgoingTrack;}
	MUonERecoTrack3D& secondOutgoingTrackRef() {return m_secondOutgoingTrack;}


	Int_t stationIndex() const {return m_stationIndex;}

	Bool_t alternativePIDHypothesis() const {return m_alternativePIDHypothesis;}

	Double_t firstTheta() const {return m_firstTheta;}
	Double_t firstThetaError() const {return m_firstThetaError;}

	Double_t secondTheta() const {return m_secondTheta;}
	Double_t secondThetaError() const {return m_secondThetaError;}

	Double_t xKinematicFit() const {return m_xKinematicFit;}
	Double_t xKinematicFitError() const {return m_xKinematicFitError;}

	Double_t yKinematicFit() const {return m_yKinematicFit;}
	Double_t yKinematicFitError() const {return m_yKinematicFitError;}

	Double_t zKinematicFit() const {return m_zKinematicFit;}
	Double_t zKinematicFitError() const {return m_zKinematicFitError;}

	Double_t xPositionFit() const {return m_xPositionFit;}
	Double_t xPositionFitError() const {return m_xPositionFitError;}

	Double_t yPositionFit() const {return m_yPositionFit;}
	Double_t yPositionFitError() const {return m_yPositionFitError;}

	Double_t zPositionFit() const {return m_zPositionFit;}
	Double_t zPositionFitError() const {return m_zPositionFitError;}

	Bool_t positionFitSuccessful() const {return m_positionFitSuccessful;}

	ROOT::Math::SMatrix<Double_t, 3> const& positionFitCovarianceMatrix() const {return m_positionFitCovarianceMatrix;}


	Double_t chi2() const {return m_chi2;}

	//only tracks DoF are used for a fit quality variable
	Int_t degreesOfFreedom() const {return m_incomingTrack.degreesOfFreedom() + m_firstOutgoingTrack.degreesOfFreedom() + m_secondOutgoingTrack.degreesOfFreedom();}

    Double_t chi2PerDegreeOfFreedom() const {return m_chi2/degreesOfFreedom();}

	Bool_t hitsReassigned() const {return m_hitsReassigned;}

	//methods for fitter
	Bool_t reassignHits(std::vector<std::vector<MUonERecoHit>> const& hitsPerModule, Int_t maxNumberOfSharedHits, Double_t xyThreshold, Double_t uvThreshsold) {

		m_hitsReassigned = false;

		if(maxNumberOfSharedHits < 1) return false;

		for(int i = 0; i < maxNumberOfSharedHits && i < hitsPerModule.size(); ++i) {

			auto hits_in_module = hitsPerModule[i];
			if(hits_in_module.empty()) continue;

			auto mod = hits_in_module.front().module();

			auto hits_out1 = mod.isStereo() ? m_firstOutgoingTrack.stereoHits() : ('x' == mod.projection() ? m_firstOutgoingTrack.xTrack().hits() : m_firstOutgoingTrack.yTrack().hits());
			auto hits_out2 = mod.isStereo() ? m_secondOutgoingTrack.stereoHits() : ('x' == mod.projection() ? m_secondOutgoingTrack.xTrack().hits() : m_secondOutgoingTrack.yTrack().hits());

			//find hits in this module from both tracks
			auto first_track_hit = std::find_if(hits_out1.begin(), hits_out1.end(), [&mod](MUonERecoHit const& hit){return hit.module().sameModuleAs(mod);});
			auto second_track_hit = std::find_if(hits_out2.begin(), hits_out2.end(), [&mod](MUonERecoHit const& hit){return hit.module().sameModuleAs(mod);});

			if(first_track_hit == hits_out1.end() || second_track_hit == hits_out2.end()) continue;

			//they are shared between tracks
			if((*first_track_hit == *second_track_hit)) {

				//if electron track got pulled away by KF, the hit should be closer to the muon track
				auto hit_tack1_distance = m_firstOutgoingTrack.distanceToHit(*first_track_hit);
				auto hit_tack2_distance = m_secondOutgoingTrack.distanceToHit(*first_track_hit);
				
				if(hit_tack1_distance < hit_tack2_distance) {
					//hit is closer to track1
					//try to assign a better hit to track 2
					//if not found, allow sharing
					m_hitsReassigned = m_hitsReassigned | m_secondOutgoingTrack.reassignHit(hits_in_module, mod, second_track_hit - hits_out2.begin(), xyThreshold, uvThreshsold);
				} else {

					m_hitsReassigned = m_hitsReassigned | m_firstOutgoingTrack.reassignHit(hits_in_module, mod, first_track_hit - hits_out1.begin(), xyThreshold, uvThreshsold);
				}
			}
		}	

		return m_hitsReassigned;	
	}


	//call this after new track parameters have been set or hits vectors have been modified
	Double_t recalculateChi2() {

		//only tracks' chi2 are used as a fit quality variable
		m_chi2 = m_incomingTrack.recalculateChi2() + m_firstOutgoingTrack.recalculateChi2() + m_secondOutgoingTrack.recalculateChi2();
		return m_chi2;
	}


	//call this after hit vectors have been modified or track slopes changed
	void recalculateAngles() {

		auto calculateTheta = [](MUonERecoTrack3D const& track1, MUonERecoTrack3D const& track2, Double_t& theta, Double_t& error) {

			TVector3 v1 = track1.directionVector();
			TVector3 v2 = track2.directionVector();

			theta = v1.Angle(v2);

			Double_t v1Mag = v1.Mag();
			Double_t v2Mag = v2.Mag();

			Double_t C1 = v1Mag * v2Mag;
			Double_t C1_inv = 1/C1;
			Double_t C2 = v1.Dot(v2);
			Double_t C3 = C2 * C1_inv * C1_inv * C1_inv;
			Double_t C4 = 1 - C2 * C2 * C1_inv * C1_inv;

			Double_t v1AxErrSq = (track2.xTrack().slope() * C1_inv - track1.xTrack().slope() * v2Mag * C3) * track1.xTrack().slopeError();
			v1AxErrSq *= v1AxErrSq;

			Double_t v1AyErrSq = (track2.yTrack().slope() * C1_inv - track1.yTrack().slope() * v2Mag * C3) * track1.yTrack().slopeError();
			v1AyErrSq *= v1AyErrSq;			

			Double_t v2AxErrSq = (track1.xTrack().slope() * C1_inv - track2.xTrack().slope() * v1Mag * C3) * track2.xTrack().slopeError();
			v2AxErrSq *= v2AxErrSq;

			Double_t v2AyErrSq = (track1.yTrack().slope() * C1_inv - track2.yTrack().slope() * v1Mag * C3) * track2.yTrack().slopeError();
			v2AyErrSq *= v2AyErrSq;

			error = 1/sqrt(C4) * sqrt(v1AxErrSq + v1AyErrSq + v2AxErrSq + v2AyErrSq);		
		};

		calculateTheta(m_incomingTrack, m_firstOutgoingTrack, m_firstTheta, m_firstThetaError);
		calculateTheta(m_incomingTrack, m_secondOutgoingTrack, m_secondTheta, m_secondThetaError);
	}

    void setPositionKinematicFit(Double_t x, Double_t xError, Double_t y, Double_t yError, Double_t z, Double_t zError = 0) {

	    m_xKinematicFit = x;
	    m_xKinematicFitError = xError;

	    m_yKinematicFit = y;
	    m_yKinematicFitError = yError;

		m_zKinematicFit = z;
		m_zKinematicFitError = zError;
    }

    void setPositionPositionFit(Double_t x, Double_t xError, Double_t y, Double_t yError, Double_t z, Double_t zError = 0) {

	    m_xPositionFit = x;
	    m_xPositionFitError = xError;

	    m_yPositionFit = y;
	    m_yPositionFitError = yError;

		m_zPositionFit = z;
		m_zPositionFitError = zError;
    }

	void setPositionFitCovarianceMatrix(ROOT::Math::SMatrix<Double_t, 3> const& m) {

		m_positionFitCovarianceMatrix = m;
	}

	void setPositionFitStatus(Bool_t s) {

		m_positionFitSuccessful = s;
	}

	void setIncomingTrackParameters(Double_t x_slope, Double_t x_slope_error, Double_t x0, Double_t x0_error,
									Double_t y_slope, Double_t y_slope_error, Double_t y0, Double_t y0_error) {

		m_incomingTrack.setXTrackParameters(x_slope, x_slope_error, x0, x0_error);
		m_incomingTrack.setYTrackParameters(y_slope, y_slope_error, y0, y0_error);
	}

	void setIncomingTrackZ0(Double_t z0) {

		m_incomingTrack.setZ0(z0);
	}

	void setFirstOutgoingTrackParameters(Double_t x_slope, Double_t x_slope_error, Double_t x0, Double_t x0_error,
										Double_t y_slope, Double_t y_slope_error, Double_t y0, Double_t y0_error) {

		m_firstOutgoingTrack.setXTrackParameters(x_slope, x_slope_error, x0, x0_error);
		m_firstOutgoingTrack.setYTrackParameters(y_slope, y_slope_error, y0, y0_error);
	}

	void setFirstOutgoingTrackZ0(Double_t z0) {

		m_firstOutgoingTrack.setZ0(z0);
	}

	void setSecondOutgoingTrackParameters(Double_t x_slope, Double_t x_slope_error, Double_t x0, Double_t x0_error,
										Double_t y_slope, Double_t y_slope_error, Double_t y0, Double_t y0_error) {

		m_secondOutgoingTrack.setXTrackParameters(x_slope, x_slope_error, x0, x0_error);
		m_secondOutgoingTrack.setYTrackParameters(y_slope, y_slope_error, y0, y0_error);
	}

	void setSecondOutgoingTrackZ0(Double_t z0) {

		m_secondOutgoingTrack.setZ0(z0);
	}

	void setLinearFitDerivativeMatrix(ROOT::Math::SMatrix<Double_t, 8> const& m) {

		 Double_t inc[16] = {
			 m(0,0), m(0,1), m(0,2), m(0,3),
			 m(1,0), m(1,1), m(1,2), m(1,3),
			 m(2,0), m(2,1), m(2,2), m(2,3),
			 m(3,0), m(3,1), m(3,2), m(3,3)
		 };
		 m_incomingTrack.setLinearFitDerivativeMatrix(ROOT::Math::SMatrix<Double_t, 4>(inc, 16));

		 Double_t out1[16] = {
			 m(0,0), m(0,1), m(0,4), m(0,5),
			 m(1,0), m(1,1), m(1,4), m(1,5),
			 m(4,0), m(4,1), m(4,4), m(4,5),
			 m(5,0), m(5,1), m(5,4), m(5,5)
		 };
		 m_firstOutgoingTrack.setLinearFitDerivativeMatrix(ROOT::Math::SMatrix<Double_t, 4>(out1, 16));

		 Double_t out2[16] = {
			 m(0,0), m(0,1), m(0,6), m(0,7),
			 m(1,0), m(1,1), m(1,6), m(1,7),
			 m(6,0), m(6,1), m(6,6), m(6,7),
			 m(7,0), m(7,1), m(7,6), m(7,7)
		 };
		m_secondOutgoingTrack.setLinearFitDerivativeMatrix(ROOT::Math::SMatrix<Double_t, 4>(out2, 16));
	}

	void setLinearFitCovarianceMatrix(ROOT::Math::SMatrix<Double_t, 8> const& m) {

		 Double_t inc[16] = {
			 m(0,0), m(0,1), m(0,2), m(0,3),
			 m(1,0), m(1,1), m(1,2), m(1,3),
			 m(2,0), m(2,1), m(2,2), m(2,3),
			 m(3,0), m(3,1), m(3,2), m(3,3)
		 };
		 m_incomingTrack.setLinearFitCovarianceMatrix(ROOT::Math::SMatrix<Double_t, 4>(inc, 16));

		 Double_t out1[16] = {
			 m(0,0), m(0,1), m(0,4), m(0,5),
			 m(1,0), m(1,1), m(1,4), m(1,5),
			 m(4,0), m(4,1), m(4,4), m(4,5),
			 m(5,0), m(5,1), m(5,4), m(5,5)
		 };
		 m_firstOutgoingTrack.setLinearFitCovarianceMatrix(ROOT::Math::SMatrix<Double_t, 4>(out1, 16));

		 Double_t out2[16] = {
			 m(0,0), m(0,1), m(0,6), m(0,7),
			 m(1,0), m(1,1), m(1,6), m(1,7),
			 m(6,0), m(6,1), m(6,6), m(6,7),
			 m(7,0), m(7,1), m(7,6), m(7,7)
		 };
		m_secondOutgoingTrack.setLinearFitCovarianceMatrix(ROOT::Math::SMatrix<Double_t, 4>(out2, 16));

	}

private:

	MUonERecoTrack3D m_incomingTrack;
	MUonERecoTrack3D m_firstOutgoingTrack;
	MUonERecoTrack3D m_secondOutgoingTrack;


	Int_t m_stationIndex{-1};

	Bool_t m_alternativePIDHypothesis{false};

	Double_t m_firstTheta{0};
	Double_t m_firstThetaError{0};

	Double_t m_secondTheta{0};
	Double_t m_secondThetaError{0};

	//vertex position from KF
	Double_t m_xKinematicFit{0};
	Double_t m_xKinematicFitError{0};

	Double_t m_yKinematicFit{0};
	Double_t m_yKinematicFitError{0};

	Double_t m_zKinematicFit{0}; 
	Double_t m_zKinematicFitError{0};

	//vertex position from position fitter
	Double_t m_xPositionFit{0};
	Double_t m_xPositionFitError{0};

	Double_t m_yPositionFit{0};
	Double_t m_yPositionFitError{0};

	Double_t m_zPositionFit{0}; 
	Double_t m_zPositionFitError{0};

	Bool_t m_positionFitSuccessful{false};

	ROOT::Math::SMatrix<Double_t, 3> m_positionFitCovarianceMatrix;

	Double_t m_chi2{0};

	Bool_t m_hitsReassigned{false};


	//if sorting is required, prefer vertices with lower chi2 per degree of freedom
	//worse vertex is considered to be of lower value, that is worse < better is true	
    friend inline bool operator< (MUonERecoVertex const& lhs, MUonERecoVertex const& rhs) {
        
        return lhs.chi2PerDegreeOfFreedom() > rhs.chi2PerDegreeOfFreedom();
    }
    friend inline bool operator> (MUonERecoVertex const& lhs, MUonERecoVertex const& rhs) {return  operator< (rhs,lhs);}
    friend inline bool operator<=(MUonERecoVertex const& lhs, MUonERecoVertex const& rhs) {return !operator> (lhs,rhs);}
    friend inline bool operator>=(MUonERecoVertex const& lhs, MUonERecoVertex const& rhs) {return !operator< (lhs,rhs);}

    ClassDef(MUonERecoVertex,2)
};



/*
Generic N-vertex class.
*/

class MUonERecoGenericVertex {

public:

	MUonERecoGenericVertex() = default;

	MUonERecoGenericVertex(MUonERecoTrack3D const& incoming, std::vector<MUonERecoTrack3D> const& outgoing, MUonERecoTarget const& target)
		: m_incomingTrack(incoming), m_outgoingTracks(outgoing),
		m_stationIndex(target.station())
		{
			recalculateChi2();
		}

	MUonERecoTrack3D const& incomingTrack() const {return m_incomingTrack;}
	std::vector<MUonERecoTrack3D> const& outgoingTracks() const {return m_outgoingTracks;}

	MUonERecoTrack3D& incomingTrackRef() {return m_incomingTrack;}
	std::vector<MUonERecoTrack3D>& outgoingTracksRef() {return m_outgoingTracks;}

	
	Int_t multiplicity() const {return m_outgoingTracks.size();}
	Int_t stationIndex() const {return m_stationIndex;}
	
	Double_t xKinematicFit() const {return m_xKinematicFit;}
	Double_t xKinematicFitError() const {return m_xKinematicFitError;}

	Double_t yKinematicFit() const {return m_yKinematicFit;}
	Double_t yKinematicFitError() const {return m_yKinematicFitError;}

	Double_t zKinematicFit() const {return m_zKinematicFit;}
	Double_t zKinematicFitError() const {return m_zKinematicFitError;}

	Double_t xPositionFit() const {return m_xPositionFit;}
	Double_t xPositionFitError() const {return m_xPositionFitError;}

	Double_t yPositionFit() const {return m_yPositionFit;}
	Double_t yPositionFitError() const {return m_yPositionFitError;}

	Double_t zPositionFit() const {return m_zPositionFit;}
	Double_t zPositionFitError() const {return m_zPositionFitError;}

	Bool_t positionFitSuccessful() const {return m_positionFitSuccessful;}

	ROOT::Math::SMatrix<Double_t, 3> const& positionFitCovarianceMatrix() const {return m_positionFitCovarianceMatrix;}


	Double_t chi2() const {return m_chi2;}


	//only tracks DoF are used for a fit quality variable
	Int_t degreesOfFreedom() const {

		Int_t ndf = m_incomingTrack.degreesOfFreedom();

		for(auto& track : m_outgoingTracks) 
			ndf += track.degreesOfFreedom();

		return ndf;
	}


    Double_t chi2PerDegreeOfFreedom() const {return m_chi2/degreesOfFreedom();}
	Bool_t hitsReassigned() const {return m_hitsReassigned;}


	//call this after new track parameters have been set or hits vectors have been modified
	Double_t recalculateChi2() {

		//only tracks' chi2 are used as a fit quality variable
		m_chi2 = m_incomingTrack.recalculateChi2();

		for(auto& track : m_outgoingTracks) 
			m_chi2 += track.recalculateChi2();

		return m_chi2;
	}

    void setPositionKinematicFit(Double_t x, Double_t xError, Double_t y, Double_t yError, Double_t z, Double_t zError = 0) {

	    m_xKinematicFit = x;
	    m_xKinematicFitError = xError;

	    m_yKinematicFit = y;
	    m_yKinematicFitError = yError;

		m_zKinematicFit = z;
		m_zKinematicFitError = zError;
    }

    void setPositionPositionFit(Double_t x, Double_t xError, Double_t y, Double_t yError, Double_t z, Double_t zError = 0) {

	    m_xPositionFit = x;
	    m_xPositionFitError = xError;

	    m_yPositionFit = y;
	    m_yPositionFitError = yError;

		m_zPositionFit = z;
		m_zPositionFitError = zError;
    }

	void setPositionFitCovarianceMatrix(ROOT::Math::SMatrix<Double_t, 3> const& m) {

		m_positionFitCovarianceMatrix = m;
	}

	void setPositionFitStatus(Bool_t s) {

		m_positionFitSuccessful = s;
	}

private:

	MUonERecoTrack3D m_incomingTrack;
	std::vector<MUonERecoTrack3D> m_outgoingTracks;

	Int_t m_stationIndex{-1};

	//vertex position from KF
	Double_t m_xKinematicFit{0};
	Double_t m_xKinematicFitError{0};

	Double_t m_yKinematicFit{0};
	Double_t m_yKinematicFitError{0};

	Double_t m_zKinematicFit{0}; 
	Double_t m_zKinematicFitError{0};

	//vertex position from position fitter
	Double_t m_xPositionFit{0};
	Double_t m_xPositionFitError{0};

	Double_t m_yPositionFit{0};
	Double_t m_yPositionFitError{0};

	Double_t m_zPositionFit{0}; 
	Double_t m_zPositionFitError{0};

	Bool_t m_positionFitSuccessful{false};

	ROOT::Math::SMatrix<Double_t, 3> m_positionFitCovarianceMatrix;


	Double_t m_chi2{0};

	Bool_t m_hitsReassigned{false};


	//if sorting is required, prefer vertices with lower chi2 per degree of freedom
	//worse vertex is considered to be of lower value, that is worse < better is true	
    friend inline bool operator< (MUonERecoGenericVertex const& lhs, MUonERecoGenericVertex const& rhs) {
        
        return lhs.chi2PerDegreeOfFreedom() > rhs.chi2PerDegreeOfFreedom();
    }
    friend inline bool operator> (MUonERecoGenericVertex const& lhs, MUonERecoGenericVertex const& rhs) {return  operator< (rhs,lhs);}
    friend inline bool operator<=(MUonERecoGenericVertex const& lhs, MUonERecoGenericVertex const& rhs) {return !operator> (lhs,rhs);}
    friend inline bool operator>=(MUonERecoGenericVertex const& lhs, MUonERecoGenericVertex const& rhs) {return !operator< (lhs,rhs);}


    ClassDef(MUonERecoGenericVertex,2)
};

#endif//MUONERECOVERTEX_H

