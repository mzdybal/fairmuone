#include "MUonERecoFitter.h"

#include "MUonERecoModule.h"

#include <cmath>

#include "TH1F.h"
#include "TMath.h"
#include "TMatrixD.h"
#include "TVectorD.h"

#include "Math/SMatrix.h"

#include "TGraphErrors.h"
#include "TFitResult.h"

#include <fairlogger/Logger.h>
#include <algorithm>

#include <boost/container/static_vector.hpp>

#include "ElasticState.h"

MUonERecoFitter::MUonERecoFitter() {}


void MUonERecoFitter::addFitInfo(MUonERecoTrack2D& track) const {

	//no hits added, slope and x0 are the same as calculated on initialization
	if(2 == track.numberOfHits()) {
	
		track.recalculateChi2();
		return;
	} else if (track.numberOfHits() < 2)
		LOG(error) << "Error: trying to fit 2D track with less than 2 hits!";
	//note, if this happens something went very wrong at earlier stages, so I'm leaving an error here that will crash production

	/*
		For details on how the fit works refer to the description in function below for 3D tracks.
		I'm only pasting the equations here. Same as there, 2s are ignored as they disappear anyway.

		Chi2 of a single hit = (u - (a * z + b - offset))**2 / sigma_i**2, where (z, u) is hit position and (a, b) are slope and intercept of the track

		Hessian:
		⎡   2     ⎤
		⎢2⋅z   2⋅z⎥
		⎢────  ───⎥
		⎢  2     2⎥
		⎢σᵢ    σᵢ ⎥
		⎢         ⎥
		⎢2⋅z    2 ⎥
		⎢───   ───⎥
		⎢  2     2⎥
		⎣σᵢ    σᵢ ⎦

		Gradient:
		a:
			     2                          
			2⋅a⋅z    2⋅b⋅z   2⋅off⋅z   2⋅u⋅z
			────── + ───── - ─────── - ─────
			   2        2        2        2 
			 σᵢ       σᵢ       σᵢ       σᵢ  

		b:
			2⋅a⋅z   2⋅b   2⋅off   2⋅u
			───── + ─── - ───── - ───
			   2      2      2      2
			 σᵢ     σᵢ     σᵢ     σᵢ 
	*/

	auto hits = track.hits();


	Double_t tmpA[3] = {
							0,  
							0, 0 
						};

	for(auto const& hit : hits) {

		const Double_t z = hit.z();
		const Double_t du2_inverse = 1. / (hit.positionError() * hit.positionError());

		tmpA[0] += z*z * du2_inverse;
		tmpA[1] += z * du2_inverse;
		tmpA[2] += du2_inverse;
	}

	ROOT::Math::SMatrix<Double_t, 2, 2, ROOT::Math::MatRepSym<Double_t,2>> A(tmpA, 3);

	//this should never happen here, unless track is seriously broken (e.g. no hits), so I'm leaving an error that will crash processing
	if(!A.Invert()) //A is now a covariance matrix
		LOG(error) << "Error: failed to invert matrix in a 2D fit!"; //failed to invert


	ROOT::Math::SVector<Double_t, 2> B;
	for(auto const& hit : hits) {
		const Double_t du2_inverse =  1 / (hit.positionError()*hit.positionError());

		const Double_t z = hit.z();
		const Double_t ui = hit.positionPerpendicular();
		const Double_t offset = hit.module().projection() == 'x'? hit.module().xOffset() : hit.module().yOffset();

		//a
		B(0) += du2_inverse * z * (offset + ui);
		//b
		B(1) += du2_inverse * (offset + ui);
	}	

	ROOT::Math::SVector<Double_t, 2> c_svd = A*B;


	track.setSlope(c_svd(0), TMath::Sqrt(A(0,0)));
	track.setX0(c_svd(1), TMath::Sqrt(A(1,1)));
	track.recalculateChi2();

}

Int_t MUonERecoFitter::addFitInfo(MUonERecoTrack3D& track, Bool_t allowTrivial, Double_t maxOutlierChi2, Bool_t enableOutlierRemoval) const {

	auto hits = track.hitsCopy();

	Bool_t fit_converged = false;
	Int_t number_of_xHits = track.xTrack().numberOfHits();
	Int_t number_of_yHits = track.yTrack().numberOfHits();
	Int_t number_of_stereo_hits = track.numberOfStereoHits();

	Double_t z0 = 0; //track z0, fixed for now, important for where the covariance matrix is calculated


	while(!fit_converged) {

		//track becomes trivial/non-defined
		if(0 == number_of_stereo_hits) {

			if(allowTrivial) {

				if(number_of_xHits < 2 || number_of_yHits < 2)
					return 1;

			} else {

				if(number_of_xHits < 3 || number_of_yHits < 3)
					return 1;
			}

		} else {

			if(number_of_xHits < 2 || number_of_yHits < 2)
				return 1;			
		}

		/*

		fit minimizes chi2:

                                                                               2
(-uᵢ + (ax⋅(-z₀ + zᵢ) + bx - xOff)⋅cos(a) + (ay⋅(-z₀ + zᵢ) + by - yOff)⋅sin(a)) 
────────────────────────────────────────────────────────────────────────────────
                                        2                                       
                                      σᵢ                                        

  		where u_i is hit position, recalculated to include tilt and tilt alignment (hit position perpendicular)
		z_i is hit z recalculated to include tilt and tilt alignment
		a is the angle of the module, including alignment
		xOff and yOff are shifts from the alignment
		and 3D track consists of 2 2D tracks:
		ax*z + bx
		ay*z + by

		z0 is track's z0 at which the covariance matrix is calculated

		sigma is the hit position error derived from gaussian fits after alignment; it already includes the influence of tilt 

		hessian matrix for track chi2 from a single hit
		calculated below as A
		2s in each term cancel out later, so they're ignored
		order of fit parameters is: x intercept, y intercept, x slope, y slope

		this part of the fit is sensitive only to alignment in alpha, which is already included in the sines and cosines returned from the modules

		
⎡             2                                                             2                                    ⎤           
⎢        2⋅cos (a)                2⋅sin(a)⋅cos(a)           2⋅(-z₀ + zᵢ)⋅cos (a)      2⋅(-z₀ + zᵢ)⋅sin(a)⋅cos(a) ⎥
⎢        ─────────                ───────────────           ────────────────────      ────────────────────────── ⎥
⎢             2                           2                           2                            2             ⎥
⎢           σᵢ                          σᵢ                          σᵢ                           σᵢ              ⎥
⎢                                                                                                                ⎥
⎢                                         2                                                              2       ⎥
⎢     2⋅sin(a)⋅cos(a)                2⋅sin (a)           2⋅(-z₀ + zᵢ)⋅sin(a)⋅cos(a)      2⋅(-z₀ + zᵢ)⋅sin (a)    ⎥
⎢     ───────────────                ─────────           ──────────────────────────      ────────────────────    ⎥
⎢             2                           2                           2                            2             ⎥
⎢           σᵢ                          σᵢ                          σᵢ                           σᵢ              ⎥
⎢                                                                                                                ⎥
⎢                   2                                                   2    2                    2              ⎥
⎢   2⋅(-z₀ + zᵢ)⋅cos (a)     2⋅(-z₀ + zᵢ)⋅sin(a)⋅cos(a)     2⋅(-z₀ + zᵢ) ⋅cos (a)     2⋅(-z₀ + zᵢ) ⋅sin(a)⋅cos(a)⎥
⎢   ────────────────────     ──────────────────────────     ─────────────────────     ───────────────────────────⎥
⎢             2                           2                            2                            2            ⎥
⎢           σᵢ                          σᵢ                           σᵢ                           σᵢ             ⎥
⎢                                                                                                                ⎥
⎢                                               2                    2                               2    2      ⎥
⎢2⋅(-z₀ + zᵢ)⋅sin(a)⋅cos(a)     2⋅(-z₀ + zᵢ)⋅sin (a)     2⋅(-z₀ + zᵢ) ⋅sin(a)⋅cos(a)     2⋅(-z₀ + zᵢ) ⋅sin (a)   ⎥
⎢──────────────────────────     ────────────────────     ───────────────────────────     ─────────────────────   ⎥
⎢             2                           2                            2                            2            ⎥
⎣           σᵢ                          σᵢ                           σᵢ                           σᵢ             ⎦	
		*/

		auto lsum = [&hits, &z0](Int_t cosine_term_power, Int_t sine_term_power, Int_t z_term_power) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t hit_contribution = 1;
				
				for(Int_t i = 0; i < cosine_term_power; ++i) hit_contribution *= hit.module().angleCosine();
				for(Int_t i = 0; i < sine_term_power; ++i) hit_contribution *= hit.module().angleSine();
				for(Int_t i = 0; i < z_term_power; ++i) hit_contribution *= (hit.z() - z0); //z0 fixed above for now
				
				Double_t du2 = hit.positionError()*hit.positionError();
				
				sum += hit_contribution/du2;				
			}

			return sum;
		};


		Double_t tmpA[10] = {
								lsum(2,0,0),  
								lsum(1,1,0), lsum(0,2,0), 
								lsum(2,0,1), lsum(1,1,1), lsum(2,0,2), 
								lsum(1,1,1), lsum(0,2,1), lsum(1,1,2), lsum(0,2,2)
							};

		//note MatRepSym, hence not all terms are needed to define the matrix (it's symmetric)
		ROOT::Math::SMatrix<Double_t, 4, 4, ROOT::Math::MatRepSym<Double_t,4>> A(tmpA, 10);
		track.setLinearFitDerivativeMatrix(A);

		if(!A.Invert()) //A is now a covariance matrix
			return 1; //failed to invert

/*
gradient equations for each parameter
the 2s cancel out, so they're not included in the calculations
terms without fit parameters go into the B vector negated
terms near fit parameters are consistent with the hessian matrix above (bearing in mind the different ordering below)

hence for minimum A * (bx, by, ax, ay) = B

this part is also sensitive to offsets in x and y from the alignment 

second row contains only terms without fit parameters, which enter B vector


bx

   ⎛          2              2   ⎞                                                            2                        
   ⎜  2⋅z₀⋅cos (a)   2⋅zᵢ⋅cos (a)⎟      ⎛  2⋅z₀⋅sin(a)⋅cos(a)   2⋅zᵢ⋅sin(a)⋅cos(a)⎞   2⋅bx⋅cos (a)   2⋅by⋅sin(a)⋅cos(a)
ax⋅⎜- ──────────── + ────────────⎟ + ay⋅⎜- ────────────────── + ──────────────────⎟ + ──────────── + ──────────────────
   ⎜        2              2     ⎟      ⎜           2                    2        ⎟         2                 2        
   ⎝      σᵢ             σᵢ      ⎠      ⎝         σᵢ                   σᵢ         ⎠       σᵢ                σᵢ         


                          2                          
  2⋅uᵢ⋅cos(a)   2⋅xOff⋅cos (a)   2⋅yOff⋅sin(a)⋅cos(a)
- ─────────── - ────────────── - ────────────────────
        2              2                   2         
      σᵢ             σᵢ                  σᵢ          




by

                                                    ⎛          2              2   ⎞                                2   
   ⎛  2⋅z₀⋅sin(a)⋅cos(a)   2⋅zᵢ⋅sin(a)⋅cos(a)⎞      ⎜  2⋅z₀⋅sin (a)   2⋅zᵢ⋅sin (a)⎟   2⋅bx⋅sin(a)⋅cos(a)   2⋅by⋅sin (a)
ax⋅⎜- ────────────────── + ──────────────────⎟ + ay⋅⎜- ──────────── + ────────────⎟ + ────────────────── + ────────────
   ⎜           2                    2        ⎟      ⎜        2              2     ⎟            2                 2     
   ⎝         σᵢ                   σᵢ         ⎠      ⎝      σᵢ             σᵢ      ⎠          σᵢ                σᵢ      

                                                  2   
   2⋅uᵢ⋅sin(a)   2⋅xOff⋅sin(a)⋅cos(a)   2⋅yOff⋅sin (a)
 - ─────────── - ──────────────────── - ──────────────
         2                 2                   2      
       σᵢ                σᵢ                  σᵢ       



ax

   ⎛    2    2                 2          2    2   ⎞      ⎛    2                                             2              ⎞      ⎛          2              2   ⎞                                                 
   ⎜2⋅z₀ ⋅cos (a)   4⋅z₀⋅zᵢ⋅cos (a)   2⋅zᵢ ⋅cos (a)⎟      ⎜2⋅z₀ ⋅sin(a)⋅cos(a)   4⋅z₀⋅zᵢ⋅sin(a)⋅cos(a)   2⋅zᵢ ⋅sin(a)⋅cos(a)⎟      ⎜  2⋅z₀⋅cos (a)   2⋅zᵢ⋅cos (a)⎟      ⎛  2⋅z₀⋅sin(a)⋅cos(a)   2⋅zᵢ⋅sin(a)⋅cos(a)⎞
ax⋅⎜───────────── - ─────────────── + ─────────────⎟ + ay⋅⎜─────────────────── - ───────────────────── + ───────────────────⎟ + bx⋅⎜- ──────────── + ────────────⎟ + by⋅⎜- ────────────────── + ──────────────────⎟
   ⎜       2                2                2     ⎟      ⎜          2                      2                      2        ⎟      ⎜        2              2     ⎟      ⎜           2                    2        ⎟
   ⎝     σᵢ               σᵢ               σᵢ      ⎠      ⎝        σᵢ                     σᵢ                     σᵢ         ⎠      ⎝      σᵢ             σᵢ      ⎠      ⎝         σᵢ                   σᵢ         ⎠


                                                 2                   2                                                       
      ⎛2⋅z₀⋅cos(a)   2⋅zᵢ⋅cos(a)⎞   2⋅xOff⋅z₀⋅cos (a)   2⋅xOff⋅zᵢ⋅cos (a)   2⋅yOff⋅z₀⋅sin(a)⋅cos(a)   2⋅yOff⋅zᵢ⋅sin(a)⋅cos(a)
 + uᵢ⋅⎜─────────── - ───────────⎟ + ───────────────── - ───────────────── + ─────────────────────── - ───────────────────────
      ⎜      2             2    ⎟            2                   2                      2                         2          
      ⎝    σᵢ            σᵢ     ⎠          σᵢ                  σᵢ                     σᵢ                        σᵢ           




ay

   ⎛    2                                             2              ⎞      ⎛    2    2                 2          2    2   ⎞                                                       ⎛          2              2   ⎞
   ⎜2⋅z₀ ⋅sin(a)⋅cos(a)   4⋅z₀⋅zᵢ⋅sin(a)⋅cos(a)   2⋅zᵢ ⋅sin(a)⋅cos(a)⎟      ⎜2⋅z₀ ⋅sin (a)   4⋅z₀⋅zᵢ⋅sin (a)   2⋅zᵢ ⋅sin (a)⎟      ⎛  2⋅z₀⋅sin(a)⋅cos(a)   2⋅zᵢ⋅sin(a)⋅cos(a)⎞      ⎜  2⋅z₀⋅sin (a)   2⋅zᵢ⋅sin (a)⎟
ax⋅⎜─────────────────── - ───────────────────── + ───────────────────⎟ + ay⋅⎜───────────── - ─────────────── + ─────────────⎟ + bx⋅⎜- ────────────────── + ──────────────────⎟ + by⋅⎜- ──────────── + ────────────⎟
   ⎜          2                      2                      2        ⎟      ⎜       2                2                2     ⎟      ⎜           2                    2        ⎟      ⎜        2              2     ⎟
   ⎝        σᵢ                     σᵢ                     σᵢ         ⎠      ⎝     σᵢ               σᵢ               σᵢ      ⎠      ⎝         σᵢ                   σᵢ         ⎠      ⎝      σᵢ             σᵢ      ⎠


                                                                                                     2                   2   
      ⎛2⋅z₀⋅sin(a)   2⋅zᵢ⋅sin(a)⎞   2⋅xOff⋅z₀⋅sin(a)⋅cos(a)   2⋅xOff⋅zᵢ⋅sin(a)⋅cos(a)   2⋅yOff⋅z₀⋅sin (a)   2⋅yOff⋅zᵢ⋅sin (a)
 + uᵢ⋅⎜─────────── - ───────────⎟ + ─────────────────────── - ─────────────────────── + ───────────────── - ─────────────────
      ⎜      2             2    ⎟               2                         2                      2                   2       
      ⎝    σᵢ            σᵢ     ⎠             σᵢ                        σᵢ                     σᵢ                  σᵢ        



*/


		ROOT::Math::SVector<Double_t, 4> B;
		for(auto const& hit : hits) {

			const Double_t du2_inverse =  1 / (hit.positionError()*hit.positionError());
			const Double_t cosa = hit.module().angleCosine();
			const Double_t sina = hit.module().angleSine();
			const Double_t dz = hit.z() - z0;
			const Double_t ui = hit.positionPerpendicular();
			const Double_t xOff = hit.module().xOffset();
			const Double_t yOff = hit.module().yOffset();

			//bx
			B(0) += du2_inverse * (ui*cosa + xOff*cosa*cosa + yOff*sina*cosa);
			//by
			B(1) += du2_inverse * (ui*sina + xOff*sina*cosa + yOff*sina*sina);
			//ax
			B(2) += du2_inverse * (ui*dz*cosa + xOff*dz*cosa*cosa + yOff*dz*sina*cosa);
			//ay
			B(3) += du2_inverse * (ui*dz*sina + xOff*dz*sina*cosa + yOff*dz*sina*sina);			
		}	

		ROOT::Math::SVector<Double_t, 4> c_svd = A*B;

									//slope, slope error, x0/y0, x0/y0 error
		track.setXTrackParameters(c_svd(2), TMath::Sqrt(A(2,2)), c_svd(0), TMath::Sqrt(A(0,0)));
		track.setYTrackParameters(c_svd(3), TMath::Sqrt(A(3,3)), c_svd(1), TMath::Sqrt(A(1,1)));
		track.setLinearFitCovarianceMatrix(A);

		if(enableOutlierRemoval && maxOutlierChi2 > 0) {

			//find maximum outlier
			Double_t max_chi2 = 0;
			auto outlier_iter = hits.end();

			for(auto it = hits.begin(); it != hits.end(); ++it) {

				Double_t chi2_sqrt = track.distanceToHit(*it) / it->positionError();
				Double_t chi2 = chi2_sqrt * chi2_sqrt;

				if(chi2 > max_chi2) {
					
					outlier_iter = it;
					max_chi2 = chi2;
				}
			}


			if(max_chi2 < maxOutlierChi2)
				fit_converged = true;
			else {

				hits.erase(outlier_iter); //remove outlier from hit collection

				if('x' == outlier_iter->module().projection())
					--number_of_xHits;
				else if('y' == outlier_iter->module().projection())
					--number_of_yHits;
				else if(outlier_iter->module().isStereo())
					--number_of_stereo_hits;
			}			
		} else
			fit_converged = true;


	} //!while(!fit_converged)

	//if any of the hits were removed from the copy
	if(hits.size() < track.numberOfHits())
		track.setHits(hits);

	track.recalculateChi2();

	return 0;
}


Int_t MUonERecoFitter::addFitInfo(MUonERecoTarget const& target, std::vector<std::vector<MUonERecoModule>> const& modulesPerSector, MUonERecoVertex& vertex, MUonEReconstructionConfiguration const& config) const {

	if(config.refitElectronTrackWithMSCorrection() || config.refitMuonTrackWithMSCorrection() || config.refitIncomingTrackWithMSCorrection()) {

		//PID based on initial theta angles
		auto first_theta = vertex.firstOutgoingTrack().directionVector().Angle(vertex.incomingTrack().directionVector());
		auto second_theta = vertex.secondOutgoingTrack().directionVector().Angle(vertex.incomingTrack().directionVector());


		auto& incoming_ref = vertex.incomingTrackRef();
		auto& electron_ref = vertex.alternativePIDHypothesis() ? (first_theta < second_theta ? vertex.firstOutgoingTrackRef() : vertex.secondOutgoingTrackRef()) : (first_theta < second_theta ? vertex.secondOutgoingTrackRef() : vertex.firstOutgoingTrackRef());
		auto& muon_ref = vertex.alternativePIDHypothesis() ? (first_theta < second_theta ? vertex.secondOutgoingTrackRef() : vertex.firstOutgoingTrackRef()) : (first_theta < second_theta ? vertex.firstOutgoingTrackRef() : vertex.secondOutgoingTrackRef());

		auto& theta_e = vertex.alternativePIDHypothesis() ? (first_theta < second_theta ? first_theta : second_theta) : (first_theta < second_theta ? second_theta : first_theta);

		const Double_t mass_e  = 0.51099895*1e-3; // GeV
  		const Double_t mass_mu = 105.6583755*1e-3; // GeV
  
  		MuE::ElasticState mue(config.MSCorrectionAssumedBeamEnergy(), mass_mu, mass_e, theta_e*1e3);


		if(config.refitElectronTrackWithMSCorrection()) {

			auto momentum = mue.GetEnergy_e();

			addMS(electron_ref, momentum, modulesPerSector, 0.5*(target.zMax() - target.zMin()), target);
			addFitInfo(electron_ref, true, -1, false);
			addFirstStateInfo(electron_ref, true, momentum);			
		}

		if(config.refitMuonTrackWithMSCorrection()) {

			auto momentum = mue.GetEnergy_mu();

			addMS(muon_ref, momentum, modulesPerSector, 0.5*(target.zMax() - target.zMin()), target);
			addFitInfo(muon_ref, true, -1, false);
			addFirstStateInfo(muon_ref, true, momentum);	
		}

		if(config.refitIncomingTrackWithMSCorrection()) {

			auto momentum = config.MSCorrectionAssumedBeamEnergy();

			addMS(incoming_ref, momentum, modulesPerSector, 0.5*(target.zMax() - target.zMin()), target, true);
			addFitInfo(incoming_ref, true, -1, false);
			addFirstStateInfo(incoming_ref, true, momentum);	
		}
	}

	if(vertex.hitsReassigned()) {
		//refit all tracks for position fit, otherwise it's the first iteration of a KF
		auto& inc_ref = vertex.incomingTrackRef();
		auto& out1_ref = vertex.firstOutgoingTrackRef();
		auto& out2_ref = vertex.secondOutgoingTrackRef();

		addFitInfo(inc_ref, true, -1, false);
		addFitInfo(out1_ref, true, -1, false);
		addFitInfo(out2_ref, true, -1, false);
	}

	TVector3 position_positionfit; 
	TVector3 positionError_positionfit; 
	ROOT::Math::SMatrix<Double_t, 3> covarianceMatrix_positionfit;
	Bool_t position_fit_status = fitVertexPosition({vertex.incomingTrack(), vertex.firstOutgoingTrack(), vertex.secondOutgoingTrack()}, position_positionfit, positionError_positionfit, covarianceMatrix_positionfit);

	vertex.setPositionFitStatus(position_fit_status);

	if(position_fit_status) {

		vertex.setPositionPositionFit(position_positionfit[0], positionError_positionfit[0], position_positionfit[1], positionError_positionfit[1], position_positionfit[2], positionError_positionfit[2]);
		vertex.setPositionFitCovarianceMatrix(covarianceMatrix_positionfit);
	
	} else {

		LOG(info) << "Position fit failed for a vertex. If it was to be used for the KF, middle of the target will be used instead";
	}

	auto vertex_z_position = target.z();
	Double_t vertex_x_position = -999;
	Double_t vertex_y_position = -999;

	if(!config.runKinematicFit()) {

		vertex.recalculateChi2();
		vertex.recalculateAngles();
		return 0;
	}

	if(config.useFittedZPositionInKinematicFit() || config.useFittedPositionInKinematicFit()) {

		if(position_fit_status) {

			vertex_x_position = position_positionfit[0];
			vertex_y_position = position_positionfit[1];
			vertex_z_position = position_positionfit[2];
			
			if(config.restrictVertexPositionToTarget()) {

				if(vertex_z_position < target.zMin())
					vertex_z_position = target.zMin();
				else if(vertex_z_position > target.zMax())
					vertex_z_position = target.zMax();

				if(vertex_x_position < target.xMin())
					vertex_x_position = target.xMin();
				else if(vertex_x_position > target.xMax())
					vertex_x_position = target.xMax();		

				if(vertex_y_position < target.yMin())
					vertex_y_position = target.yMin();
				else if(vertex_y_position > target.yMax())
					vertex_y_position = target.yMax();				
			}
		}
	}


	auto const& incoming_hits = vertex.incomingTrack().hitsCopy();
	auto const& first_outgoing_hits = vertex.firstOutgoingTrack().hitsCopy();
	auto const& second_outgoing_hits = vertex.secondOutgoingTrack().hitsCopy();

	if(config.useFittedPositionInKinematicFit() && position_fit_status) {

		auto aSum = [&vertex_z_position](std::vector<MUonERecoHit> const& hits, Int_t ica, Int_t isa, Int_t iz) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t pca = 1.0; 
				Double_t psa = 1.0; 
				Double_t pz  = 1.0; 

				if (ica > 0)  pca = hit.module().angleCosine();
				if (ica == 2) pca *= hit.module().angleCosine();

				if (isa > 0)  psa = hit.module().angleSine();
				if (isa == 2) psa *= hit.module().angleSine();

				if (iz > 0)  pz = (hit.z() - vertex_z_position);
				if (iz == 2) pz *= (hit.z() - vertex_z_position);

				Double_t du = hit.positionError() * hit.positionError();
				
				sum += pca * psa * pz / du;
			}
			return sum;
		};


		auto bxSum = [&vertex_z_position, &vertex_x_position, &vertex_y_position](std::vector<MUonERecoHit> const& hits) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t dz  = (hit.z() - vertex_z_position);
				Double_t du = hit.positionError() * hit.positionError();

				sum += ((hit.positionPerpendicular() + hit.module().xOffset()*hit.module().angleCosine() + hit.module().yOffset()*hit.module().angleSine()) * hit.module().angleCosine() - vertex_x_position * hit.module().angleCosine() * hit.module().angleCosine() - vertex_y_position * hit.module().angleSine() * hit.module().angleCosine()) * dz / du;			
			}

			return sum;
		};	


		auto bySum = [&vertex_z_position, &vertex_x_position, &vertex_y_position](std::vector<MUonERecoHit> const& hits) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t dz  = (hit.z() - vertex_z_position);
				Double_t du = hit.positionError() * hit.positionError();

				sum += ((hit.positionPerpendicular() + hit.module().xOffset()*hit.module().angleCosine() + hit.module().yOffset()*hit.module().angleSine()) * hit.module().angleSine() - vertex_y_position * hit.module().angleSine() * hit.module().angleSine() - vertex_x_position * hit.module().angleSine() * hit.module().angleCosine()) * dz / du;			
			}

			return sum;
		};			

		Double_t tmpA[21] = { 

				aSum(incoming_hits, 2, 0, 2),		
				aSum(incoming_hits, 1, 1, 2), aSum(incoming_hits, 0, 2, 2),
							0,								0,				aSum(first_outgoing_hits, 2, 0, 2),
							0,								0,				aSum(first_outgoing_hits, 1, 1, 2), aSum(first_outgoing_hits, 0, 2, 2),
							0,								0,								0,								0,						aSum(second_outgoing_hits, 2, 0, 2),
							0,								0,								0,								0,						aSum(second_outgoing_hits, 1, 1, 2), aSum(second_outgoing_hits, 0, 2, 2)
			};

		ROOT::Math::SMatrix<Double_t, 6, 6, ROOT::Math::MatRepSym<Double_t,6>> A(tmpA, 21);
		//vertex.setLinearFitDerivativeMatrix(A);


		Double_t tmpB[6] = {
				bxSum(incoming_hits),
				bySum(incoming_hits),
				bxSum(first_outgoing_hits),
				bySum(first_outgoing_hits),
				bxSum(second_outgoing_hits),
				bySum(second_outgoing_hits)			
			};

		//1-in, 2-- out1, 3-- out2
		ROOT::Math::SVector<Double_t, 6> B(tmpB, 6);

		if(!A.Invert()) //A is now a covariance matrix
			return 1; //failed to invert

		auto c_svd = A * B;		

		//x, x error, y, y error;
		vertex.setPositionKinematicFit(vertex_x_position, positionError_positionfit[0], vertex_y_position, positionError_positionfit[1], vertex_z_position, positionError_positionfit[2]);

		//x0 and y0 set to vertex position
		//x slope, x slope error, x0, x0 error, y slope, y slope error, y0, y0 error
		vertex.setIncomingTrackParameters(c_svd(0), TMath::Sqrt(A(0,0)), vertex_x_position, positionError_positionfit[0], c_svd(1), TMath::Sqrt(A(1,1)), vertex_y_position, positionError_positionfit[1]);
		vertex.setFirstOutgoingTrackParameters(c_svd(2), TMath::Sqrt(A(2,2)), vertex_x_position, positionError_positionfit[0], c_svd(3), TMath::Sqrt(A(3,3)), vertex_y_position, positionError_positionfit[1]);
		vertex.setSecondOutgoingTrackParameters(c_svd(4), TMath::Sqrt(A(4,4)), vertex_x_position, positionError_positionfit[0], c_svd(5), TMath::Sqrt(A(5,5)), vertex_y_position, positionError_positionfit[1]);

		//set z0 of all tracks to target's z
		vertex.setIncomingTrackZ0(vertex_z_position);
		vertex.setFirstOutgoingTrackZ0(vertex_z_position);
		vertex.setSecondOutgoingTrackZ0(vertex_z_position);

		//vertex.setLinearFitCovarianceMatrix(A);

		vertex.recalculateChi2();
		vertex.recalculateAngles();


	} else {

		//use KF to estimate XY position

		auto aSum = [&vertex_z_position](std::vector<MUonERecoHit> const& hits, Int_t ica, Int_t isa, Int_t iz) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t pca = 1.0; 
				Double_t psa = 1.0; 
				Double_t pz  = 1.0; 

				if (ica > 0)  pca = hit.module().angleCosine();
				if (ica == 2) pca *= hit.module().angleCosine();

				if (isa > 0)  psa = hit.module().angleSine();
				if (isa == 2) psa *= hit.module().angleSine();

				if (iz > 0)  pz = (hit.z() - vertex_z_position);
				if (iz == 2) pz *= (hit.z() - vertex_z_position);

				Double_t du = hit.positionError() * hit.positionError();
				
				sum += pca * psa * pz / du;
			}
			return sum;
		};


		auto bSum = [&vertex_z_position](std::vector<MUonERecoHit> const& hits, Int_t ica, Int_t isa, Int_t iz) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t pca = 1.0; 
				Double_t psa = 1.0; 
				Double_t pz  = 1.0;

				if (ica > 0)  pca = hit.module().angleCosine();
				if (isa > 0)  psa = hit.module().angleSine();
				if (iz > 0)  pz = (hit.z() - vertex_z_position);

				Double_t du = hit.positionError() * hit.positionError();

				sum += (hit.positionPerpendicular() + hit.module().xOffset()*hit.module().angleCosine() + hit.module().yOffset()*hit.module().angleSine()) * pca * psa * pz / du;			
			}

			return sum;
		};

		Double_t tmpA[36] = { 
				aSum(incoming_hits, 2,0,0) + aSum(first_outgoing_hits, 2,0,0) + aSum(second_outgoing_hits, 2,0,0),
				aSum(incoming_hits, 1,1,0) + aSum(first_outgoing_hits, 1,1,0) + aSum(second_outgoing_hits, 1,1,0),  aSum(incoming_hits, 0,2,0) + aSum(first_outgoing_hits, 0,2,0) + aSum(second_outgoing_hits, 0,2,0),		
				aSum(incoming_hits, 2,0,1), 																		aSum(incoming_hits, 1,1,1), 																		aSum(incoming_hits, 2,0,2),						
				aSum(incoming_hits, 1,1,1), 																		aSum(incoming_hits, 0,2,1), 																		aSum(incoming_hits, 1,1,2), aSum(incoming_hits, 0,2,2),			
				aSum(first_outgoing_hits, 2,0,1), 																	aSum(first_outgoing_hits, 1,1,1), 																	0.0, 						0.0, 						aSum(first_outgoing_hits, 2,0,2),
				aSum(first_outgoing_hits, 1,1,1), 																	aSum(first_outgoing_hits, 0,2,1), 																	0.0, 						0.0, 						aSum(first_outgoing_hits, 1,1,2), 	aSum(first_outgoing_hits, 0,2,2),
				aSum(second_outgoing_hits, 2,0,1), 																	aSum(second_outgoing_hits, 1,1,1), 																	0.0, 						0.0, 						0.0, 								0.0, 								aSum(second_outgoing_hits, 2,0,2),
				aSum(second_outgoing_hits, 1,1,1), 																	aSum(second_outgoing_hits, 0,2,1), 																	0.0, 						0.0, 						0.0, 								0.0, 								aSum(second_outgoing_hits, 1,1,2), aSum(second_outgoing_hits, 0,2,2)			
			};

		ROOT::Math::SMatrix<Double_t, 8, 8, ROOT::Math::MatRepSym<Double_t,8>> A(tmpA, 36);
		vertex.setLinearFitDerivativeMatrix(A);

		Double_t tmpB[8] = {
			bSum(incoming_hits, 1,0,0) + bSum(first_outgoing_hits, 1,0,0) + bSum(second_outgoing_hits, 1,0,0),
			bSum(incoming_hits, 0,1,0) + bSum(first_outgoing_hits, 0,1,0) + bSum(second_outgoing_hits, 0,1,0),
			bSum(incoming_hits, 1,0,1),
			bSum(incoming_hits, 0,1,1),
			bSum(first_outgoing_hits, 1,0,1),
			bSum(first_outgoing_hits, 0,1,1),
			bSum(second_outgoing_hits, 1,0,1),
			bSum(second_outgoing_hits, 0,1,1),		
			};

		//1-in, 2-- out1, 3-- out2
		ROOT::Math::SVector<Double_t, 8> B(tmpB, 8);

		if(!A.Invert()) //A is now a covariance matrix
			return 1; //failed to invert

		auto c_svd = A * B;
		
		//x, x error, y, y error;
		vertex.setPositionKinematicFit(c_svd(0), TMath::Sqrt(A(0,0)), c_svd(1), TMath::Sqrt(A(1,1)), vertex_z_position, config.useFittedZPositionInKinematicFit() && position_fit_status? positionError_positionfit[2] : 0);

		//x0 and y0 set to vertex position
		//x slope, x slope error, x0, x0 error, y slope, y slope error, y0, y0 error
		vertex.setIncomingTrackParameters(c_svd(2), TMath::Sqrt(A(2,2)), c_svd(0), TMath::Sqrt(A(0,0)), c_svd(3), TMath::Sqrt(A(3,3)), c_svd(1), TMath::Sqrt(A(1,1)));
		vertex.setFirstOutgoingTrackParameters(c_svd(4), TMath::Sqrt(A(4,4)), c_svd(0), TMath::Sqrt(A(0,0)), c_svd(5), TMath::Sqrt(A(5,5)), c_svd(1), TMath::Sqrt(A(1,1)));
		vertex.setSecondOutgoingTrackParameters(c_svd(6), TMath::Sqrt(A(6,6)), c_svd(0), TMath::Sqrt(A(0,0)), c_svd(7), TMath::Sqrt(A(7,7)), c_svd(1), TMath::Sqrt(A(1,1)));

		//set z0 of all tracks to target's z
		vertex.setIncomingTrackZ0(vertex_z_position);
		vertex.setFirstOutgoingTrackZ0(vertex_z_position);
		vertex.setSecondOutgoingTrackZ0(vertex_z_position);

		vertex.setLinearFitCovarianceMatrix(A);

		vertex.recalculateChi2();
		vertex.recalculateAngles();
	}
	return 0;
}

template<Int_t N> Int_t MUonERecoFitter::addFitInfo(MUonERecoTarget const& target, std::vector<std::vector<MUonERecoModule>> const& modulesPerSector, MUonERecoGenericVertex& vertex, MUonEReconstructionConfiguration const& config) const {

	TVector3 position_positionfit; 
	TVector3 positionError_positionfit; 
	ROOT::Math::SMatrix<Double_t, 3> covarianceMatrix_positionfit;

	auto ot = vertex.outgoingTracks();
	ot.emplace_back(vertex.incomingTrack());

	Bool_t position_fit_status = fitVertexPosition(ot, position_positionfit, positionError_positionfit, covarianceMatrix_positionfit);

	vertex.setPositionFitStatus(position_fit_status);

	if(position_fit_status) {

		vertex.setPositionPositionFit(position_positionfit[0], positionError_positionfit[0], position_positionfit[1], positionError_positionfit[1], position_positionfit[2], positionError_positionfit[2]);
		vertex.setPositionFitCovarianceMatrix(covarianceMatrix_positionfit);
	
	} else {

		LOG(info) << "Position fit failed for a generic vertex. If it was to be used for the KF, middle of the target will be used instead";
	}

	auto vertex_z_position = target.z();
	Double_t vertex_x_position = -999;
	Double_t vertex_y_position = -999;

	if(!config.runKinematicFit()) {

		vertex.recalculateChi2();
		return 0;
	}

	if(config.useFittedZPositionInGenericVertexKinematicFit() || config.useFittedPositionInGenericVertexKinematicFit()) {

		if(position_fit_status) {

			vertex_x_position = position_positionfit[0];
			vertex_y_position = position_positionfit[1];
			vertex_z_position = position_positionfit[2];
			
			if(config.restrictGenericVertexPositionToTarget()) {

				if(vertex_z_position < target.zMin())
					vertex_z_position = target.zMin();
				else if(vertex_z_position > target.zMax())
					vertex_z_position = target.zMax();

				if(vertex_x_position < target.xMin())
					vertex_x_position = target.xMin();
				else if(vertex_x_position > target.xMax())
					vertex_x_position = target.xMax();		

				if(vertex_y_position < target.yMin())
					vertex_y_position = target.yMin();
				else if(vertex_y_position > target.yMax())
					vertex_y_position = target.yMax();				
			}
		}
	}

	auto const& incoming_hits = vertex.incomingTrack().hitsCopy();
	auto const& outgoing_tracks = vertex.outgoingTracks();


	if(config.useFittedPositionInGenericVertexKinematicFit() && position_fit_status) {

		auto aSum = [&vertex_z_position](std::vector<MUonERecoHit> const& hits, Int_t ica, Int_t isa, Int_t iz) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t pca = 1.0; 
				Double_t psa = 1.0; 
				Double_t pz  = 1.0; 

				if (ica > 0)  pca = hit.module().angleCosine();
				if (ica == 2) pca *= hit.module().angleCosine();

				if (isa > 0)  psa = hit.module().angleSine();
				if (isa == 2) psa *= hit.module().angleSine();

				if (iz > 0)  pz = (hit.z() - vertex_z_position);
				if (iz == 2) pz *= (hit.z() - vertex_z_position);

				Double_t du = hit.positionError() * hit.positionError();
				
				sum += pca * psa * pz / du;
			}
			return sum;
		};


		auto bxSum = [&vertex_z_position, &vertex_x_position, &vertex_y_position](std::vector<MUonERecoHit> const& hits) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t dz  = (hit.z() - vertex_z_position);
				Double_t du = hit.positionError() * hit.positionError();

				sum += ((hit.positionPerpendicular() + hit.module().xOffset()*hit.module().angleCosine() + hit.module().yOffset()*hit.module().angleSine()) * hit.module().angleCosine() - vertex_x_position * hit.module().angleCosine() * hit.module().angleCosine() - vertex_y_position * hit.module().angleSine() * hit.module().angleCosine()) * dz / du;			
			}

			return sum;
		};	


		auto bySum = [&vertex_z_position, &vertex_x_position, &vertex_y_position](std::vector<MUonERecoHit> const& hits) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t dz  = (hit.z() - vertex_z_position);
				Double_t du = hit.positionError() * hit.positionError();

				sum += ((hit.positionPerpendicular() + hit.module().xOffset()*hit.module().angleCosine() + hit.module().yOffset()*hit.module().angleSine()) * hit.module().angleSine() - vertex_y_position * hit.module().angleSine() * hit.module().angleSine() - vertex_x_position * hit.module().angleSine() * hit.module().angleCosine()) * dz / du;			
			}

			return sum;
		};			
		//symmetric representation requires N*(N+1)/2 terms
		const Int_t A_size = (2*N + 2)*((2*N + 2) +1)/2;
		Double_t tmpA[A_size] = {0};/* = { 

				aSum(incoming_hits, 2, 0, 2),		
				aSum(incoming_hits, 1, 1, 2), aSum(incoming_hits, 0, 2, 2),
							0,								0,				aSum(first_outgoing_hits, 2, 0, 2),
							0,								0,				aSum(first_outgoing_hits, 1, 1, 2), aSum(first_outgoing_hits, 0, 2, 2),
							0,								0,								0,								0,						aSum(second_outgoing_hits, 2, 0, 2),
							0,								0,								0,								0,						aSum(second_outgoing_hits, 1, 1, 2), aSum(second_outgoing_hits, 0, 2, 2)
			};*/
		tmpA[0] = aSum(incoming_hits, 2, 0, 2);
		tmpA[1] = aSum(incoming_hits, 1, 1, 2);
		tmpA[2] = aSum(incoming_hits, 0, 2, 2);

		Double_t tmpB[2 * (N+1)] = {0};/* = {
				bxSum(incoming_hits),
				bySum(incoming_hits),
				bxSum(first_outgoing_hits),
				bySum(first_outgoing_hits),
				bxSum(second_outgoing_hits),
				bySum(second_outgoing_hits)			
			};*/
		tmpB[0] = bxSum(incoming_hits);
		tmpB[1] = bySum(incoming_hits);

		//filling A and B together to avoid accessing hits copy twice

		for(Int_t ot_index = 0; ot_index < outgoing_tracks.size(); ++ot_index) {

			auto hits = outgoing_tracks[ot_index].hitsCopy();

			tmpA[3/*incoming*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 2 * (ot_index + 1)/*current outgoing zeros*/] = aSum(hits, 2, 0, 2);
			tmpA[3/*incoming*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 4 * (ot_index + 1)/*current outgoing zeros*/ + 1/*current outgoing term*/] = aSum(hits, 1, 1, 2);
			tmpA[3/*incoming*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 4 * (ot_index + 1)/*current outgoing zeros*/ + 2/*current outgoing term*/] = aSum(hits, 0, 2, 2);

			tmpB[2 * (ot_index + 1)] = bxSum(hits);
			tmpB[2 * (ot_index + 1) + 1] = bySum(hits);
		}

		ROOT::Math::SMatrix<Double_t, 2 * N + 2, 2 * N + 2, ROOT::Math::MatRepSym<Double_t, 2 * N + 2>> A(tmpA, A_size);
		//vertex.setLinearFitDerivativeMatrix(A);


		//1-in, 2-- out1, 3-- out2, etc.
		ROOT::Math::SVector<Double_t, 2 * (N+1)> B(tmpB, 2 * (N+1));

		if(!A.Invert()) //A is now a covariance matrix
			return 1; //failed to invert

		auto c_svd = A * B;		

		//x, x error, y, y error; z already set to target's z
		vertex.setPositionKinematicFit(vertex_x_position, positionError_positionfit[0], vertex_y_position, positionError_positionfit[1], vertex_z_position, positionError_positionfit[2]);

		auto& inc = vertex.incomingTrackRef();
		auto& out = vertex.outgoingTracksRef();

		inc.setXTrackParameters(c_svd(0), TMath::Sqrt(A(0,0)), vertex_x_position, positionError_positionfit[0]);
		inc.setYTrackParameters(c_svd(1), TMath::Sqrt(A(1,1)), vertex_y_position, positionError_positionfit[1]);
		inc.setZ0(vertex_z_position);

		for(Int_t ot_index = 0; ot_index < outgoing_tracks.size(); ++ot_index) {


			out[ot_index].setXTrackParameters(c_svd(2 * (ot_index + 1)), TMath::Sqrt(A(2 * (ot_index + 1),2 * (ot_index + 1))), vertex_x_position, positionError_positionfit[0]);
			out[ot_index].setYTrackParameters(c_svd(2 * (ot_index + 1) + 1), TMath::Sqrt(A(2 * (ot_index + 1) + 1,2 * (ot_index + 1) + 1)), vertex_y_position, positionError_positionfit[1]);
			out[ot_index].setZ0(vertex_z_position);
		}

		vertex.recalculateChi2();

	} else {

		//use KF to estimate XY position

		auto aSum = [&vertex_z_position](std::vector<MUonERecoHit> const& hits, Int_t ica, Int_t isa, Int_t iz) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t pca = 1.0; 
				Double_t psa = 1.0; 
				Double_t pz  = 1.0; 

				if (ica > 0)  pca = hit.module().angleCosine();
				if (ica == 2) pca *= hit.module().angleCosine();

				if (isa > 0)  psa = hit.module().angleSine();
				if (isa == 2) psa *= hit.module().angleSine();

				if (iz > 0)  pz = (hit.z() - vertex_z_position);
				if (iz == 2) pz *= (hit.z() - vertex_z_position);

				Double_t du = hit.positionError() * hit.positionError();
				
				sum += pca * psa * pz / du;
			}
			return sum;
		};


		auto bSum = [&vertex_z_position](std::vector<MUonERecoHit> const& hits, Int_t ica, Int_t isa, Int_t iz) {

			Double_t sum = 0;

			for(auto const& hit : hits) {

				Double_t pca = 1.0; 
				Double_t psa = 1.0; 
				Double_t pz  = 1.0;

				if (ica > 0)  pca = hit.module().angleCosine();
				if (isa > 0)  psa = hit.module().angleSine();
				if (iz > 0)  pz = (hit.z() - vertex_z_position);

				Double_t du = hit.positionError() * hit.positionError();

				sum += (hit.positionPerpendicular() + hit.module().xOffset()*hit.module().angleCosine() + hit.module().yOffset()*hit.module().angleSine()) * pca * psa * pz / du;			
			}

			return sum;
		};

		//symmetric representation requires N*(N+1)/2 terms
		const Int_t A_size = (2*N + 4)*((2*N + 4) +1)/2;
		Double_t tmpA[A_size] = {0};/*{ 
				aSum(incoming_hits, 2,0,0) + aSum(first_outgoing_hits, 2,0,0) + aSum(second_outgoing_hits, 2,0,0),
				aSum(incoming_hits, 1,1,0) + aSum(first_outgoing_hits, 1,1,0) + aSum(second_outgoing_hits, 1,1,0),  aSum(incoming_hits, 0,2,0) + aSum(first_outgoing_hits, 0,2,0) + aSum(second_outgoing_hits, 0,2,0),		
				aSum(incoming_hits, 2,0,1), 																		aSum(incoming_hits, 1,1,1), 																		aSum(incoming_hits, 2,0,2),						
				aSum(incoming_hits, 1,1,1), 																		aSum(incoming_hits, 0,2,1), 																		aSum(incoming_hits, 1,1,2), aSum(incoming_hits, 0,2,2),			
				aSum(first_outgoing_hits, 2,0,1), 																	aSum(first_outgoing_hits, 1,1,1), 																	0.0, 						0.0, 						aSum(first_outgoing_hits, 2,0,2),
				aSum(first_outgoing_hits, 1,1,1), 																	aSum(first_outgoing_hits, 0,2,1), 																	0.0, 						0.0, 						aSum(first_outgoing_hits, 1,1,2), 	aSum(first_outgoing_hits, 0,2,2),
				aSum(second_outgoing_hits, 2,0,1), 																	aSum(second_outgoing_hits, 1,1,1), 																	0.0, 						0.0, 						0.0, 								0.0, 								aSum(second_outgoing_hits, 2,0,2),
				aSum(second_outgoing_hits, 1,1,1), 																	aSum(second_outgoing_hits, 0,2,1), 																	0.0, 						0.0, 						0.0, 								0.0, 								aSum(second_outgoing_hits, 1,1,2), aSum(second_outgoing_hits, 0,2,2)			
			};*/

		tmpA[0] = aSum(incoming_hits, 2,0,0);

		tmpA[1] = aSum(incoming_hits, 1,1,0);
		tmpA[2] = aSum(incoming_hits, 0,2,0);

		tmpA[3] = aSum(incoming_hits, 2,0,1);
		tmpA[4] = aSum(incoming_hits, 1,1,1);
		tmpA[5] = aSum(incoming_hits, 2,0,2);

		tmpA[6] = aSum(incoming_hits, 1,1,1);
		tmpA[7] = aSum(incoming_hits, 0,2,1);
		tmpA[8] = aSum(incoming_hits, 1,1,2);
		tmpA[9] = aSum(incoming_hits, 0,2,2);


		Double_t tmpB[2 * (N+2)] = {0};/*{
			bSum(incoming_hits, 1,0,0) + bSum(first_outgoing_hits, 1,0,0) + bSum(second_outgoing_hits, 1,0,0),
			bSum(incoming_hits, 0,1,0) + bSum(first_outgoing_hits, 0,1,0) + bSum(second_outgoing_hits, 0,1,0),
			bSum(incoming_hits, 1,0,1),
			bSum(incoming_hits, 0,1,1),
			bSum(first_outgoing_hits, 1,0,1),
			bSum(first_outgoing_hits, 0,1,1),
			bSum(second_outgoing_hits, 1,0,1),
			bSum(second_outgoing_hits, 0,1,1),		
			};*/

		tmpB[0] = bSum(incoming_hits, 1,0,0);
		tmpB[1] = bSum(incoming_hits, 0,1,0);
		tmpB[2] = bSum(incoming_hits, 1,0,1);
		tmpB[3] = bSum(incoming_hits, 0,1,1);


		//filling A and B together to avoid accessing hits copy twice

		for(Int_t ot_index = 0; ot_index < outgoing_tracks.size(); ++ot_index) {

			auto hits = outgoing_tracks[ot_index].hitsCopy();

			tmpA[0] += aSum(hits, 2,0,0);
			tmpA[1] += aSum(hits, 1,1,0);
			tmpA[2] += aSum(hits, 0,2,0);

			tmpA[10/*vertex+incoming*/ + 4 * ot_index/*previous vertex terms*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/] = aSum(hits, 2,0,1);
			tmpA[10/*vertex+incoming*/ + 4 * ot_index/*previous vertex terms*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 1/*current outgoing term*/] = aSum(hits, 1,1,1);
			tmpA[10/*vertex+incoming*/ + 4 * ot_index/*previous vertex terms*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 1/*current outgoing term*/ + 2 * (ot_index + 1)/*current outgoing zeros*/ + 1/*current outgoing term*/] = aSum(hits, 2,0,2);

			tmpA[10/*vertex+incoming*/ + 4 * ot_index/*previous vertex terms*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 1/*current outgoing term*/ + 2 * (ot_index + 1)/*current outgoing zeros*/ + 2/*current outgoing term*/] = aSum(hits, 1,1,1);
			tmpA[10/*vertex+incoming*/ + 4 * ot_index/*previous vertex terms*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 1/*current outgoing term*/ + 2 * (ot_index + 1)/*current outgoing zeros*/ + 3/*current outgoing term*/] = aSum(hits, 0,2,1);
			tmpA[10/*vertex+incoming*/ + 4 * ot_index/*previous vertex terms*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 1/*current outgoing term*/ + 2 * (ot_index + 1)/*current outgoing zeros*/ + 3/*current outgoing term*/ + 2 * (ot_index + 1)/*current outgoing zeros*/ + 1/*current outgoing term*/] = aSum(hits, 1,1,2);
			tmpA[10/*vertex+incoming*/ + 4 * ot_index/*previous vertex terms*/ + 4 * ot_index/*previous outgoing zeros*/ + 3 * ot_index/*previous outgoing terms*/ + 1/*current outgoing term*/ + 2 * (ot_index + 1)/*current outgoing zeros*/ + 3/*current outgoing term*/ + 2 * (ot_index + 1)/*current outgoing zeros*/ + 2/*current outgoing term*/] = aSum(hits, 0,2,2);


			tmpB[0] += bSum(hits, 1,0,0);
			tmpB[1] += bSum(hits, 0,1,0);

			tmpB[2 * (ot_index + 2)] = bSum(hits, 1,0,1);
			tmpB[2 * (ot_index + 2) + 1] = bSum(hits, 0,1,1);			
		}

		ROOT::Math::SMatrix<Double_t, 2 * N + 4, 2 * N + 4, ROOT::Math::MatRepSym<Double_t, 2 * N + 4>> A(tmpA, A_size);
		//vertex.setLinearFitDerivativeMatrix(A);

		//1-in, 2-- out1, 3-- out2
		ROOT::Math::SVector<Double_t, 2 * (N+2)> B(tmpB, 2 * (N+2));

		if(!A.Invert()) //A is now a covariance matrix
			return 1; //failed to invert

		auto c_svd = A * B;
		
		//x, x error, y, y error;
		vertex.setPositionKinematicFit(c_svd(0), TMath::Sqrt(A(0,0)), c_svd(1), TMath::Sqrt(A(1,1)), vertex_z_position, config.useFittedZPositionInGenericVertexKinematicFit() && position_fit_status? positionError_positionfit[2] : 0);


		auto& inc = vertex.incomingTrackRef();
		auto& out = vertex.outgoingTracksRef();

		inc.setXTrackParameters(c_svd(2), TMath::Sqrt(A(2,2)), c_svd(0), TMath::Sqrt(A(0,0)));
		inc.setYTrackParameters(c_svd(3), TMath::Sqrt(A(3,3)), c_svd(1), TMath::Sqrt(A(1,1)));
		inc.setZ0(vertex_z_position);

		for(Int_t ot_index = 0; ot_index < outgoing_tracks.size(); ++ot_index) {


			out[ot_index].setXTrackParameters(c_svd(2 * (ot_index + 2)), TMath::Sqrt(A(2 * (ot_index + 2),2 * (ot_index + 2))), c_svd(0), TMath::Sqrt(A(0,0)));
			out[ot_index].setYTrackParameters(c_svd(2 * (ot_index + 2) + 1), TMath::Sqrt(A(2 * (ot_index + 2) + 1,2 * (ot_index + 2) + 1)), c_svd(1), TMath::Sqrt(A(1,1)));
			out[ot_index].setZ0(vertex_z_position);
		}

		vertex.recalculateChi2();
	}
	return 0;

}


void MUonERecoFitter::addFirstStateInfo(MUonERecoTrack3D& track, Bool_t enableMSCorrection, Double_t momentum) const {

	auto hits = track.hitsCopy();

	std::sort(hits.begin(), hits.end(), [](MUonERecoHit const& hl, MUonERecoHit const& hr){return hl.z() < hr.z();});

	//matrix creation

	/*  1, 0, dz, 0,
        0, 1, 0, dz,
        0, 0, 1, 0,
        0, 0, 0, 1;
	*/
	ROOT::Math::SMatrix<Double_t, 4> A = ROOT::Math::SMatrixIdentity();
	auto fillA = [&A](Double_t dz) {
		A[0][2] = A[1][3] = dz;
	};


	/*
        1, 0, 0, 0,
        0, 1, 0, 0;
	*/
	ROOT::Math::SMatrix<Double_t, 2, 4> C;
	C[0][0] = C[1][1] = 1;


	/*
    	hitResolution^2, 0,
        0, hitResolution^2;
	*/
	ROOT::Math::SMatrix<Double_t, 2, 2, ROOT::Math::MatRepSym<Double_t, 2>> R;
	auto fillR = [&R](Double_t hitResolution) {
		R[0][0] = R[1][1] = hitResolution * hitResolution;
	};


	/*
    	MS*MS*dz*dz, 0, MS*MS*dz, 0,
        0, MS*MS*dz*dz, 0, MS*MS*dz,
        MS*MS*dz, 0, MS*MS, 0,
        0, MS*MS*dz, 0, MS*MS;
	*/
	ROOT::Math::SMatrix<Double_t, 4, 4, ROOT::Math::MatRepSym<Double_t, 4>> Q;
	auto fillQ = [&Q, &momentum](Double_t dz, MUonERecoHit const& hit) {

		auto const mod = hit.module();

		Double_t x0 = mod.sensorThickness() / mod.radiationLength();
		Double_t MS = 13.6 * TMath::Sqrt(x0) * (1.+0.038*TMath::Log(x0))/(1000 * momentum);

		Double_t MS_squared = 0;
		if(mod.twoSensorsPerModule())
			MS_squared = 2 * MS * MS;
		else
			MS_squared = MS * MS;

		Double_t ds = MS_squared * dz;
		Double_t dd = ds * dz;

		Double_t a[10] = {
							dd,
							0, dd,
							ds, 0, MS_squared,
							0, ds, 0, MS_squared
						};
		Q.SetElements(a,a+10);
	};


	const ROOT::Math::SMatrix<Double_t, 4, 4, ROOT::Math::MatRepSym<Double_t, 4>> I = ROOT::Math::SMatrixIdentity();

	//start with slopes from linear fit
	auto x_slope = track.xTrack().slope();
	auto y_slope = track.yTrack().slope();

	//states vector, contains for each hit (x position, y position, x slope, y slope) 
	std::vector<ROOT::Math::SVector<Double_t, 4>> states;
	states.resize(hits.size());

	//extrapolate missing first hit position
	auto const& initial_hit = hits.front();
	if('x' == initial_hit.module().projection()) {

		auto const& first_y_hit = *std::find_if(hits.begin(), hits.end(), [](MUonERecoHit const& h){return 'y' == h.module().projection();});
		Double_t tmp[4] = {initial_hit.positionPerpendicular(), 
							first_y_hit.positionPerpendicular() - y_slope * (first_y_hit.z() - initial_hit.z()), 
							x_slope, y_slope};
		states[0].SetElements(tmp, tmp+4);

	} else if('y' == initial_hit.module().projection()) {

		auto const& first_x_hit = *std::find_if(hits.begin(), hits.end(), [](MUonERecoHit const& h){return 'x' == h.module().projection();});
		Double_t tmp[4] = {first_x_hit.positionPerpendicular() - x_slope * (first_x_hit.z() - initial_hit.z()),
							initial_hit.positionPerpendicular(),
							x_slope, y_slope};
		states[0].SetElements(tmp, tmp+4);
	} else {
		ROOT::Math::SMatrix<Double_t, 4> tmp;
		track.setFirstState(false, 0, 0, 0, 0, 0, tmp);
		return;
	}

	ROOT::Math::SVector<Double_t, 2> Y;
	ROOT::Math::SMatrix<Double_t, 4> P;
	P[0][0] = P[1][1] = 10*initial_hit.module().hitResolution();
	P[2][2] = 100*track.xTrack().slopeError();
	P[3][3] = 100*track.yTrack().slopeError();

	for(Int_t fixed_hit_index = 0; fixed_hit_index < hits.size() - 1; ++fixed_hit_index) {

		//forward loop
		for(Int_t current_hit_index = 0; current_hit_index <= fixed_hit_index; ++current_hit_index) {

			auto const& next_hit = hits[current_hit_index + 1];
			auto const& current_hit = hits[current_hit_index];

			auto dz = next_hit.z() - current_hit.z();

			fillA(dz);
			if(enableMSCorrection)
				fillQ(dz, next_hit);
			fillR(next_hit.positionError());

			states[current_hit_index + 1] = A * states[current_hit_index];

			auto const& next_hit_module = next_hit.module();
			auto const& angle_sine = next_hit_module.angleSine();
			auto const& angle_cosine = next_hit_module.angleCosine();
			auto const& next_state = states[current_hit_index + 1];


			switch(next_hit_module.projection()) {

				case 'x':
					Y[0] = next_hit.positionPerpendicular();
					Y[1] = next_state[1];
					break;

				case 'y':
					Y[0] = next_state[0];
					Y[1] = next_hit.positionPerpendicular();
					break;

				case 'u':
					Y[0] = (next_hit.positionPerpendicular() - next_state[1] * angle_sine) / angle_cosine;
					Y[1] = next_state[1];
					break;

				case 'v':
					Y[0] = next_state[0];
					Y[1] = (next_hit.positionPerpendicular() - next_state[0] * angle_cosine) / angle_sine;
					break;
			}
	
			//SMatrices operate on templated expressions, so it's fine to keep them in single equation instead of creating a matrix and changing it in-place

			P = A * P * ROOT::Math::Transpose(A);// + Q;
			if(enableMSCorrection)
				P += Q;


			//we need to evaluate the expression to get a matrix to invert
			ROOT::Math::SMatrix<Double_t, 2> tmp_kprim = C * P * ROOT::Math::Transpose(C) + R;
			tmp_kprim.Invert();

			ROOT::Math::SMatrix<Double_t, 4, 2> K = P * ROOT::Math::Transpose(C) * tmp_kprim;

			states[current_hit_index + 1] += K * (Y - C * next_state);

			P = (I - K * C) * P;



		} //end of forward loop

		//backwards loop
		for(Int_t current_hit_index = fixed_hit_index + 1; current_hit_index > 0; --current_hit_index) {

			auto const& previous_hit = hits[current_hit_index - 1];
			auto const& current_hit = hits[current_hit_index];

			auto dz = previous_hit.z() - current_hit.z();

			fillA(dz);
			if(enableMSCorrection)
				fillQ(dz, previous_hit);	

			fillR(previous_hit.positionError());
			states[current_hit_index - 1] = A * states[current_hit_index];

			auto const& previous_hit_module = previous_hit.module();
			auto const& angle_sine = previous_hit_module.angleSine();
			auto const& angle_cosine = previous_hit_module.angleCosine();
			auto const& previous_state = states[current_hit_index - 1];


			switch(previous_hit_module.projection()) {

				case 'x':
					Y[0] = previous_hit.positionPerpendicular();
					Y[1] = previous_state[1];
					break;

				case 'y':
					Y[0] = previous_state[0];
					Y[1] = previous_hit.positionPerpendicular();
					break;

				case 'u':
					Y[0] = previous_state[0];
					Y[1] = (previous_hit.positionPerpendicular() - previous_state[0] * angle_cosine) / angle_sine;
					break;

				case 'v':
					Y[0] = (previous_hit.positionPerpendicular() - previous_state[1] * angle_sine) / angle_cosine;
					Y[1] = previous_state[1];
					break;
			}	

			//SMatrices operate on templated expressions, so it's fine to keep them in single equation instead of creating a matrix and changing it in-place

			P = A * P * ROOT::Math::Transpose(A);// + Q;
			if(enableMSCorrection)
				P += Q;

			//we need to evaluate the expression to get a matrix to invert
			ROOT::Math::SMatrix<Double_t, 2> tmp_kprim = C * P * ROOT::Math::Transpose(C) + R;
			tmp_kprim.Invert();

			ROOT::Math::SMatrix<Double_t, 4, 2> K = P * ROOT::Math::Transpose(C) * tmp_kprim;
			
			states[current_hit_index - 1] += K * (Y - C * previous_state);
			P = (I - K * C) * P;

		} //end of backwards loop

	} //end of loop on fixed hits

	//save slopes from first states
	track.setFirstState(
		true,
		hits[0].z(),
		states[0][0],
		states[0][1],
		(states[1][0] - states[0][0]) / (hits[1].z() - hits[0].z()),
		(states[1][1] - states[0][1]) / (hits[1].z() - hits[0].z()),
		P
	);
}

Bool_t MUonERecoFitter::runAdaptiveFitter(MUonERecoTarget const& target, std::vector<MUonERecoTrack3D> const& tracks, std::vector<MUonERecoAdaptiveFitterVertex>& reconstrucedAFVertices, MUonEReconstructionConfiguration const& config, Bool_t runSeeding) const {

	std::vector<AFTrack> aftracks;
	aftracks.reserve(tracks.size());

	
	for(Int_t i = 0; i < tracks.size(); ++i) {

		auto const track = tracks[i];

		if(track.kalmanFitSuccessful()) {

			auto const slope_x = track.firstStateXSlope();
			auto const slope_y = track.firstStateYSlope();
			auto const x = track.firstStateX();
			auto const y = track.firstStateY();
			auto const z = track.firstStateZ();

			const Double_t dz = target.z() - z;

			aftracks.emplace_back(track, dz, i);
		}
	}

	//seeding
	std::vector<AFSeed> seeds;
	seeds.reserve(10);

	if(!runSeeding && aftracks.size() > 1) {

		seeds.emplace_back(target.z(), aftracks.begin(), aftracks.end());

	} else if (runSeeding) {

		std::vector<AFTrack>::iterator search_starting_point = aftracks.begin();
		
		do {

			//find two tracks with closest distance in xy plane, within reasonable limits
			Double_t lowest_distance = 999999;
			std::vector<AFTrack>::iterator first_track;
			std::vector<AFTrack>::iterator second_track;
			Bool_t seed_found = false;

			for(std::vector<AFTrack>::iterator first_track_it = search_starting_point; first_track_it < aftracks.end(); ++first_track_it) {
				for(std::vector<AFTrack>::iterator second_track_it = first_track_it + 1; second_track_it != aftracks.end(); ++second_track_it) {

					Double_t distance = first_track_it->distance(*second_track_it);

					if(distance < lowest_distance && distance < config.adaptiveFitterSeedWindow()) {

						seed_found = true;
						first_track = first_track_it;
						second_track = second_track_it;
						lowest_distance = distance;
					}
				}
			}

			//if seed is not found, no more seeds can be found anyway
			//so this can be an ending condition
			if(!seed_found) 
				break;

			//set both seed tracks as used
			first_track->markUsed();
			second_track->markUsed();

			//calculate midpoint between two tracks as reference for assigning tracks
			Double_t seed_x = 0.5 * (first_track->position()(0) + second_track->position()(0));
			Double_t seed_y = 0.5 * (first_track->position()(1) + second_track->position()(1));

			//seed tracks will start here
			std::vector<AFTrack>::iterator seed_starting_point = search_starting_point;


			//move seed tracks to the beginning of the range
			//starting point now points to the element after last used track, i.e. the two tracks in the seed
			search_starting_point = std::partition(search_starting_point, aftracks.end(), [](AFTrack const& track){return track.isUsed();});

			//search for other tracks that can be assigned to the seed among non-used tracks
			for(std::vector<AFTrack>::iterator it = search_starting_point; it != aftracks.end(); ++it) {

				if(it->distance(seed_x, seed_y) < config.adaptiveFitterSeedTrackWindow())
					it->markUsed();
			}


			//move assigned tracks to the beginning of the range, all seed tracks will now be contained in 
			//between seed_starting_point and the output of std::partition
			//at this point we're also guaranteed at least 2 tracks in seed

			//starting point for next seed search
			search_starting_point = std::partition(search_starting_point, aftracks.end(), [](AFTrack const& track){return track.isUsed();});
			
			seeds.emplace_back(target.z(), seed_starting_point, search_starting_point);

		} while(true);		
	}


	for(auto const& seed : seeds) {

		MUonERecoAdaptiveFitterVertex vertex;
		auto status = fitVertexAdaptive(seed, target, vertex, config);
		if(status)
			reconstrucedAFVertices.emplace_back(std::move(vertex));
	}

	return !reconstrucedAFVertices.empty();
}

Bool_t MUonERecoFitter::fitVertexAdaptive(const AFSeed& seed, MUonERecoTarget const& target, MUonERecoAdaptiveFitterVertex& reconstrucedAFVertex, MUonEReconstructionConfiguration const& config) const {

	std::vector<AFTrackInVertex> tracks(seed.begin(), seed.end());
	Bool_t converged = false;

	ROOT::Math::XYZPoint vertex_position(seed.x(), seed.y(), target.z());
	ROOT::Math::SMatrix<Double_t, 3> vertex_cov;

	Double_t chi2_total{0};
	Long_t selected_tracks_number{0};

	for(Long_t i = 0; i < config.adaptiveFitterMaxNumberOfIterations() && !converged; ++i) {

		ROOT::Math::SMatrix<Double_t, 3> halfD2_chi2_DX2;
		ROOT::Math::SVector<Double_t, 3> halfD_chi2_DX;

		chi2_total = 0;
		selected_tracks_number = 0;

		ROOT::Math::SVector<Double_t, 2> vertex_position2D{vertex_position.x(), vertex_position.y()};

		for(auto& track : tracks) {

			//chi2
			const Double_t dz = vertex_position.z() - track.z();

			ROOT::Math::SVector<Double_t, 2> res = vertex_position2D - (track.position() + dz * track.slopes());
			Double_t chi2 = ROOT::Math::Similarity(res, track.W());

			track.setWeight(0);
			if(chi2 < config.adaptiveFitterMaxTrackChi2()) {

				++selected_tracks_number;

				//Tukey's weight
				track.setWeight((1 - chi2 / config.adaptiveFitterMaxTrackChi2()) * (1 - chi2 / config.adaptiveFitterMaxTrackChi2()));
				halfD2_chi2_DX2 += track.weight() * track.HWH();
				halfD_chi2_DX += track.weight() * track.HW() * res;

				chi2_total += track.weight() * chi2;
			}
		}

		if(selected_tracks_number >= 2) {

			//compute new covariance
			vertex_cov = halfD2_chi2_DX2;
			vertex_cov.Invert();

			//diff wrt reference
			ROOT::Math::SVector<Double_t, 3> delta = -1 * vertex_cov * halfD_chi2_DX;

			chi2_total += ROOT::Math::Dot(delta, halfD_chi2_DX);

			//update position
			vertex_position.SetX(vertex_position.x() + delta(0));
			vertex_position.SetY(vertex_position.y() + delta(1));
			vertex_position.SetZ(vertex_position.z() + delta(2));

			converged = std::abs(delta(2)) < config.adaptiveFitterConvergenceMaxDeltaZ();

		} else
			break;

	} //end of iterations

	if(!converged)
		return false;

	std::vector<Int_t> track_ID;
	std::vector<Double_t> track_weights;
	track_ID.reserve(tracks.size());
	track_weights.reserve(tracks.size());

	for(auto const& track : tracks) {

		if(track.weight() > 0) {

			track_ID.push_back(track.trackIndex());
			track_weights.push_back(track.weight());
		}
	}

	if(track_ID.size() >= 2) {

		reconstrucedAFVertex = MUonERecoAdaptiveFitterVertex(vertex_position, vertex_cov, track_ID, track_weights, chi2_total, target.station(), target.index());
		return true;
	}	

	return false;
}


Bool_t MUonERecoFitter::fitVertexPosition(std::vector<MUonERecoTrack3D> const& tracks, TVector3& position, TVector3& positionError, ROOT::Math::SMatrix<Double_t, 3>& covarianceMatrix) const {

    /*
    point-track distance

    p0 = (x0, y0, z0)
    p = (x, y, z)
    n = (ax, ay, 1) normalized = (n1, n2, n3)

    d = |(p-p0) - ((p-p0).n)n|

    d = |(x-x0, y-y0, z-z0) - (n1*(x-x0) + n2*(y-y0) + n3*(z-z0)) * (n1, n2, n3)|

        d1 = x-x0 - (n1*(x-x0) + n2*(y-y0) + n3*(z-z0)) * n1
        d2 = y-y0 - (n1*(x-x0) + n2*(y-y0) + n3*(z-z0)) * n2
        d3 = z-z0 - (n1*(x-x0) + n2*(y-y0) + n3*(z-z0)) * n3

    chi^2 = d1^2 + d2^2 + d3^2
    */

   Double_t tmpA[6] = {0};
   Double_t tmpB[3] = {0};

   for(auto const& t : tracks) {

        auto x0 = t.xTrack().x0();
        auto y0 = t.yTrack().y0();
        auto z0 = t.xTrack().z0();

        auto n = t.directionVector();
        auto n1 = n.x();
        auto n2 = n.y();
        auto n3 = n.z();

        auto n1sq = n1 * n1;
        auto n2sq = n2 * n2;
        auto n3sq = n3 * n3;

        tmpA[0] += 2 * n1sq * n2sq + 2*n1sq*n3sq + (1-n1sq)*(2 - 2*n1sq);
        tmpA[1] += 2 * n1*n2*n3sq - 2*n1*n2*(1-n2sq) - n1*n2*(2-2*n1sq);
        tmpA[2] += 2*n1sq*n2sq + 2*n2sq*n3sq + (1-n2sq)*(2-2*n2sq);
        tmpA[3] += 2*n1*n2sq*n3 - 2*n1*n3*(1-n3sq) - n1*n3*(2-2*n1sq);
        tmpA[4] += 2*n1sq*n2*n3 - 2*n2*n3*(1-n3sq) - n2*n3*(2-2*n2sq);
        tmpA[5] += 2*n1sq*n3sq + 2*n2sq*n3sq + (1-n3sq)*(2-2*n3sq);

        tmpB[0] -= x0*(-2*n1sq*n1sq - 2*n1sq*n2sq - 2*n1sq*n3sq + 4*n1sq - 2) + y0*(-2*n1sq*n1*n2 - 2*n1*n2sq*n2 - 2*n1*n2*n3sq + 4*n1*n2) + z0*(-2*n1sq*n1*n3 - 2*n1*n2sq*n3 - 2*n1*n3sq*n3 + 4*n1*n3);
        tmpB[1] -= x0*(-2*n1sq*n1*n2 - 2*n1*n2sq*n2 - 2*n1*n2*n3sq + 4*n1*n2) + y0*(-2*n1sq*n2sq - 2*n2sq*n2sq - 2*n2sq*n3sq + 4*n2sq - 2) + z0*(-2*n1sq*n2*n3 - 2*n2sq*n2*n3 - 2*n2*n3sq*n3 + 4*n2*n3);
        tmpB[2] -= x0*(-2*n1sq*n1*n3 - 2*n1*n2sq*n3 - 2*n1*n3sq*n3 + 4*n1*n3) + y0*(-2*n1sq*n2*n3 - 2*n2sq*n2*n3 - 2*n2*n3sq*n3 + 4*n2*n3) + z0*(-2*n1sq*n3sq - 2*n2sq*n3sq - 2*n3sq*n3sq + 4*n3sq - 2);
   }

    ROOT::Math::SMatrix<Double_t, 3, 3, ROOT::Math::MatRepSym<Double_t, 3>> A(tmpA, 6);
    ROOT::Math::SVector<Double_t, 3> B(tmpB, 3);

	if(!A.Invert()) //A is now a covariance matrix
		return false; //failed to invert

    covarianceMatrix = A;

    auto c_svd = A * B;  

    position = TVector3(c_svd(0), c_svd(1), c_svd(2));  
    positionError = TVector3(TMath::Sqrt(A(0,0)), TMath::Sqrt(A(1,1)), TMath::Sqrt(A(2,2)));

    return true;

};



void MUonERecoFitter::addMS(MUonERecoTrack3D& track, Double_t p, std::vector<std::vector<MUonERecoModule>> const& modulesPerSector, Double_t thicknessFromTarget, MUonERecoTarget const& target, Bool_t reverseWalk) const {


	auto hits = track.hitsCopy();
	auto modules = modulesPerSector[hits.front().moduleSector()];

	Double_t x0_target = 0;
	Double_t target_error_nodz = 0;

	if(thicknessFromTarget > 0) {

		x0_target = thicknessFromTarget / target.radiationLength();
		target_error_nodz = 13.6 * TMath::Sqrt(x0_target) * (1.+0.038*TMath::Log(x0_target))/(1000 * p);		
	}



	auto moduleContributionNoDz = [&p](MUonERecoModule const& mod) -> Double_t {

		Double_t x0 = mod.sensorThickness() / mod.radiationLength();
		if(mod.twoSensorsPerModule()) x0 *= 2;

		return 13.6 * TMath::Sqrt(x0) * (1.+0.038*TMath::Log(x0))/(1000 * p);
	};


	if(reverseWalk) { //add up errors from modules between the target and the hit, walking upstream

		for(auto& hit : hits) {

			Double_t total_error = (target.z()-hit.z())*target_error_nodz;
			total_error = total_error * total_error;

			for(Int_t i = modules.size() - 1; i >= 0; --i) {

				auto const& mod = modules[i];

				//modules are sorted by their Z position
				//after this point we're guaranteed to not encounter one that's not between the target and hit
				if(mod.index() <= hit.moduleIndex()) break; 

				Double_t sensor_error = (mod.z()-hit.z()) * moduleContributionNoDz(mod);
				total_error += sensor_error*sensor_error;
			}

			hit.setMultipleScatteringError(TMath::Sqrt(total_error));
		}	

	} else { //add up errors from modules between the target and the hit, walking downstream

		for(auto& hit : hits) {

			Double_t total_error = (hit.z()-target.z())*target_error_nodz;
			total_error = total_error * total_error;

			for(auto const& mod : modules) {

				//modules are sorted by their Z position
				//after this point we're guaranteed to not encounter one that's not between the target and hit
				if(mod.index() >= hit.moduleIndex()) break; 

				Double_t sensor_error = (hit.z()-mod.z()) * moduleContributionNoDz(mod);
				total_error += sensor_error*sensor_error;
			}

			hit.setMultipleScatteringError(TMath::Sqrt(total_error));
		}		
	}



	track.setHits(hits);
}


template Int_t MUonERecoFitter::addFitInfo<3>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<4>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<5>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<6>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<7>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<8>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<9>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<10>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;



ClassImp(MUonERecoFitter)


