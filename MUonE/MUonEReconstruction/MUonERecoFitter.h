#ifndef MUONERECOFITTER_H
#define MUONERECOFITTER_H

#include "MUonERecoTarget.h"
#include "MUonERecoTrack2D.h"
#include "MUonERecoTrack3D.h"
#include "MUonERecoVertex.h"
#include "MUonERecoModule.h"
#include "MUonERecoAdaptiveFitterVertex.h"
#include "MUonEReconstructionConfiguration.h"

#include "Math/SMatrix.h"

/*
	Class performing all reconstruction fits. 2D tracks are fitted using TGraphErrors from ROOT.
	3D tracks and vertices are fitted with custom linear fitters.
*/
class AFTrack;
class AFSeed;

class MUonERecoFitter {

public:

    MUonERecoFitter();

	void addFitInfo(MUonERecoTrack2D& track) const;
	//returns 1 if less than 3 hits in x or y proj. left after outliers removal
	//0 if everything went fine
	Int_t addFitInfo(MUonERecoTrack3D& track, Bool_t allowTrivial, Double_t maxOutlierChi2, Bool_t enableOutlierRemoval = true) const; 

	Int_t addFitInfo(MUonERecoTarget const& target, std::vector<std::vector<MUonERecoModule>> const& modulesPerSector, MUonERecoVertex& vertex, MUonEReconstructionConfiguration const& config) const;

	void addFirstStateInfo(MUonERecoTrack3D& track, Bool_t enableMSCorrection = false, Double_t momentum = 0) const;

	//adapted from LHCb adaptive fitter
	Bool_t runAdaptiveFitter(MUonERecoTarget const& target, std::vector<MUonERecoTrack3D> const& tracks, std::vector<MUonERecoAdaptiveFitterVertex>& reconstrucedAFVertices, MUonEReconstructionConfiguration const& config, Bool_t runSeeding = true) const;
	Bool_t fitVertexAdaptive(const AFSeed& seed, MUonERecoTarget const& target, MUonERecoAdaptiveFitterVertex& reconstrucedAFVertex, MUonEReconstructionConfiguration const& config) const;


	//generic N-vertices, templated because S-matrices require size known at compile time
	template<Int_t N> Int_t addFitInfo(MUonERecoTarget const& target, std::vector<std::vector<MUonERecoModule>> const& modulesPerSector, MUonERecoGenericVertex& vertex, MUonEReconstructionConfiguration const& config) const;

	//doca based position fit
	Bool_t fitVertexPosition(std::vector<MUonERecoTrack3D> const& tracks, TVector3& position, TVector3& positionError, ROOT::Math::SMatrix<Double_t, 3>& covarianceMatrix) const;

	void addMS(MUonERecoTrack3D& track, Double_t p, std::vector<std::vector<MUonERecoModule>> const& modulesPerSector, Double_t thicknessFromTarget = -1, MUonERecoTarget const& target = MUonERecoTarget(), Bool_t reverseWalk = false) const;


    ClassDef(MUonERecoFitter,2)
};

//structures for adaptive fitter
class AFTrack {

public:

	AFTrack() = default;

	AFTrack(MUonERecoTrack3D const& track, Double_t dz, Int_t trackIndex)
		: m_z(track.firstStateZ() + dz),
		  m_position{track.firstStateX() + dz * track.firstStateXSlope(), track.firstStateY() + dz * track.firstStateYSlope()},
		  m_slopes{track.firstStateXSlope(), track.firstStateYSlope()},
		  m_trackIndex(trackIndex) {

			auto const V = track.firstStateCovarianceMatrix();

			auto dz2 = dz * dz;

			m_W(0,0) = V(0,0) + 2 * dz * V(2,0) + dz2 * V(2,2);
			m_W(1,0) = V(1,0) + dz * (V(3,0) + V(2,1)) + dz2 * V(3,2);
			m_W(1,1) = V(1,1) + 2 * dz * V(3,1) + dz2 * V(3,3);

			m_W.Invert();			  
		  }

	
	Double_t z() const {return m_z;}
	ROOT::Math::SVector<Double_t, 2> const& position() const {return m_position;}
	ROOT::Math::SVector<Double_t, 2> const& slopes() const {return m_slopes;}

	ROOT::Math::SMatrix<Double_t, 2> const& W() const {return m_W;}

	Int_t trackIndex() const {return m_trackIndex;}

	Bool_t isUsed() const {return m_isUsed;}
	void markUsed() {m_isUsed = true;}

	Double_t distance(AFTrack const& track) {

		return TMath::Sqrt(
			(track.position()(0) - m_position(0)) * (track.position()(0) - m_position(0))
			+
			(track.position()(1) - m_position(1)) * (track.position()(1) - m_position(1))
		);
	}

	Double_t distance(Double_t x, Double_t y) {

		return TMath::Sqrt(
			(x - m_position(0)) * (x - m_position(0))
			+
			(y - m_position(1)) * (y - m_position(1))
		);
	}



private:

	Double_t m_z{0};
	ROOT::Math::SVector<Double_t, 2> m_position;
	ROOT::Math::SVector<Double_t, 2> m_slopes;

	ROOT::Math::SMatrix<Double_t, 2> m_W;

	Int_t m_trackIndex{-1};

	Bool_t m_isUsed{false};

	ClassDef(AFTrack, 2)
};

class AFSeed {

public:

	AFSeed(Double_t z, std::vector<AFTrack>::iterator begin, std::vector<AFTrack>::iterator end)
		: m_z(z), m_begin(begin), m_end(end)
		{
			Double_t norm = 1.0/std::distance(begin, end);

			for(std::vector<AFTrack>::iterator it = begin; it != end; ++it) {

				m_x += it->position()(0);
				m_y += it->position()(1);
			}

			m_x *= norm;
			m_y *= norm;
		}

	Double_t z() const {return m_z;}
	Double_t x() const {return m_x;}
	Double_t y() const {return m_y;}

	std::vector<AFTrack>::iterator begin() const {return m_begin;}
	std::vector<AFTrack>::iterator end() const {return m_end;}

private:

	Double_t m_z{0};
	Double_t m_x{0};
	Double_t m_y{0};

	std::vector<AFTrack>::iterator m_begin;
	std::vector<AFTrack>::iterator m_end;

	ClassDef(AFSeed, 2)
};

class AFTrackInVertex : public AFTrack {

public:

	AFTrackInVertex(const AFTrack& track)
		: AFTrack{track}
		{
			ROOT::Math::SMatrix<Double_t, 3, 2> H;
			H(0,0) = H(1, 1) = 1;
			H(2,0) = -track.slopes()(0);
			H(2,1) = -track.slopes()(1);
			m_HW = H * W();
			m_HWH = m_HW * ROOT::Math::Transpose(H);

		}

	ROOT::Math::SMatrix<Double_t, 3, 2> const& HW() const {return m_HW;}
	ROOT::Math::SMatrix<Double_t, 3> const& HWH() const {return m_HWH;}

	Double_t weight() const {return m_weight;}	

	void setWeight(Double_t w) {m_weight = w;}

private:

	ROOT::Math::SMatrix<Double_t, 3, 2> m_HW;
	ROOT::Math::SMatrix<Double_t, 3> m_HWH;

	Double_t m_weight{1};

	ClassDef(AFTrackInVertex, 2)
};

#endif//MUONERECOFITTER_H


