#ifndef MUONERECONSTRUCTION_H
#define MUONERECONSTRUCTION_H

#include "FairTask.h"

#include "TVector3.h"
#include "Rtypes.h"
#include "TClonesArray.h"


#include <vector>
#include <cmath>
#include <unordered_map>
#include <utility>

#include "MUonETrack.h"
#include "MUonETrackerStub.h"
#include "MUonECalorimeterDigiDeposit.h"

#include "MUonEDetectorConfiguration.h"
#include "MUonEReconstructionConfiguration.h"

#include "MUonERecoModule.h"
#include "MUonERecoTarget.h"
#include "MUonERecoHit.h"
#include "MUonERecoTrack2D.h"
#include "MUonERecoTrack3D.h"
#include "MUonERecoVertex.h"
#include "MUonERecoAdaptiveFitterVertex.h"
#include "MUonERecoOutput.h"
#include "MUonERecoFitter.h"



/*
	Perform hit-based reconstruction with simplified geometry. Works with multiple targets, but the stations and modules
	have to be sorted by their Z position. This is already taken care of when using MUonEDetectorConfiguration class.
*/

class MUonEReconstruction : public FairTask
{

public:

	MUonEReconstruction();
	virtual ~MUonEReconstruction();

	Bool_t setConfiguration(MUonEDetectorConfiguration const& detectorConfig, MUonEReconstructionConfiguration const& reconstructionConfig);

	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();


    virtual InitStatus ReInit();
    virtual InitStatus Init();
    virtual void SetParContainers();

    virtual void Exec(Option_t* option);

    virtual void Finish();

	void Register();

private:

	Bool_t m_configuredCorrectly{false};

    MUonEDetectorConfiguration m_detectorConfiguration;
    MUonEReconstructionConfiguration m_reconstructionConfiguration;

	std::vector<std::vector<MUonERecoModule>> m_modulesPerSector;
	std::vector<MUonERecoTarget> m_targets;

	MUonERecoFitter m_fitter;

	//mc only
	const TClonesArray* m_mcTracks{nullptr};
	const TClonesArray* m_signalTracks{nullptr};	

	const TClonesArray* m_stubs{nullptr};
	const MUonECalorimeterDigiDeposit* m_calorimeterDeposit{nullptr};

	//real data only

	const std::vector<unsigned short>* m_dataBx{nullptr};
	const std::vector<unsigned short>* m_dataLink{nullptr};
	const std::vector<float>* m_dataStrip{nullptr};
	const std::vector<float>* m_dataCicID{nullptr};
	const std::vector<float>* m_dataBend{nullptr};
	const std::vector<unsigned int>* m_dataSuperID{nullptr};

	MUonERecoOutput* m_output{nullptr};

	Int_t m_eventNumber{0};
	
	//maps hit's station and module number to position in modulesPerSector and hitsPerModulePerSector vectors
	std::vector<std::vector<std::pair<Int_t, Int_t>>> m_hitModuleMap;

	//if we only have 2 x/y modules in sector there's no need to look for additional hits once the track is created
	//we can also do that for stereo modules to avoid unnecessary looping
	std::vector<Int_t> m_numberOfXModulesInSector;
	std::vector<Int_t> m_numberOfYModulesInSector;
	std::vector<Int_t> m_numberOfStereoModulesInSector;

	//used for loading in real data events
	std::vector<std::pair<Int_t, Int_t>> m_linkIDstationIDmoduleIDMap;


	Int_t fillMCData(std::vector<std::vector<std::vector<MUonERecoHit>>>& hitsPerModulePerSector, Double_t& eventEnergy) const;
	Int_t fillData(std::vector<std::vector<std::vector<MUonERecoHit>>>& hitsPerModulePerSector, Double_t& eventEnergy) const;

	Bool_t reconstruct2DTracks(std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, Int_t sector, std::vector<MUonERecoTrack2D>& reconstructed2DTracksInXProjection, std::vector<MUonERecoTrack2D>& reconstructed2DTracksInYProjection) const;
	Bool_t reconstruct3DTracks(std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, std::vector<std::vector<MUonERecoTrack3D>>& reconstructedTracksPerSector) const;
	Bool_t reconstructVertices(std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector, std::vector<MUonERecoVertex>& reconstrucedVertices, std::vector<MUonERecoAdaptiveFitterVertex>& reconstrucedAFVertices, std::vector<MUonERecoGenericVertex>& reconstrucedGenericVertices) const;

	ClassDef(MUonEReconstruction,2)

};


#endif //MUONERECONSTRUCTION_H


