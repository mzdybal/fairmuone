#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonERecoFitter+;
#pragma link C++ class AFTrack+;
#pragma link C++ class AFSeed+;
#pragma link C++ class AFTrackInVertex+;
#pragma link C++ class MUonERecoHit+;
#pragma link C++ class MUonERecoModule+;
#pragma link C++ class MUonEReconstruction+;
#pragma link C++ class MUonERecoOutput+;
#pragma link C++ class MUonERecoOutputHit+;
#pragma link C++ class MUonERecoOutputTrack+;
#pragma link C++ class MUonERecoOutputVertex+;
#pragma link C++ class MUonERecoOutputGenericVertex+;
#pragma link C++ class MUonERecoOutputAdaptiveFitterVertex+;
#pragma link C++ class MUonERecoTarget+;
#pragma link C++ class MUonERecoTrack2D+;
#pragma link C++ class MUonERecoTrack3D+;
#pragma link C++ class MUonERecoVertex+;
#pragma link C++ class MUonERecoGenericVertex+;
#pragma link C++ class MUonERecoAdaptiveFitterVertex+;
#pragma link C++ class MUonERealDataStub+;
#pragma link C++ class MUonERealDataEvent+;



#endif