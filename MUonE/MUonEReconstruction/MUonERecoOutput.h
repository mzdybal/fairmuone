#ifndef MUONERECOOUTPUT_H
#define MUONERECOOUTPUT_H

#include "TClonesArray.h"
#include "TVector3.h"
#include "TMath.h"
#include "Rtypes.h"
#include "Math/SMatrix.h"

#include <cmath>
#include <vector>
#include <algorithm>
#include <unordered_map>

#include "MUonETrack.h"
#include "MUonERecoHit.h"
#include "MUonERecoTrack3D.h"
#include "MUonERecoVertex.h"
#include "MUonERecoAdaptiveFitterVertex.h"

#include "MUonEReconstructionConfiguration.h"

/*
    Classes used to store track information in the output.
*/

class MUonERecoOutputHit {

public:

    MUonERecoOutputHit() = default;

    MUonERecoOutputHit(MUonERecoHit const& hit, Double_t res)
        : Projection(hit.module().projection()),
        Z(hit.z()), PositionPerpendicular(hit.positionPerpendicular()),
        PositionError(hit.positionError()), 
        PositionErrorMeasurement(hit.positionErrorMeasurement()),
        PositionErrorMultipleScattering(hit.positionErrorMultipleScattering()),
        PerpendicularResiduum(res),
        Position(hit.position()), StationID(hit.module().stationID()),
        ModuleID(hit.module().moduleID()),
        SeedingClusterLinkedTrackIds(hit.seedingClusterLinkedTrackIds()),
        SeedingClusterLinkedTrackDeposits(hit.seedingClusterLinkedTrackDeposits()),
        CorrelationClusterLinkedTrackIds(hit.correlationClusterLinkedTrackIds()),
        CorrelationClusterLinkedTrackDeposits(hit.correlationClusterLinkedTrackDeposits()),
        SeedClusterWidth(hit.seedClusterWidth()),
        CorrelationClusterWidth(hit.correlationClusterWidth()),
        Index(hit.index()),
        Bx(hit.bx()),
        SuperID(hit.superID())
        {}

    char projection() const {return Projection;}

    Double_t z() const {return Z;}

    Double_t positionPerpendicular() const {return PositionPerpendicular;}
    Double_t positionError() const {return PositionError;}
    Double_t positionErrorMeasurement() const {return PositionErrorMeasurement;}
    Double_t positionErrorMultipleScattering() const {return PositionErrorMultipleScattering;}
    Double_t perpendicularResiduum() const {return PerpendicularResiduum;}

	Double_t position() const {return Position;}

    Int_t stationID() const {return StationID;}
    Int_t moduleID() const {return ModuleID;}

	std::vector<Int_t> const& seedingClusterLinkedTrackIds() const {return SeedingClusterLinkedTrackIds;}
	std::vector<Double_t> const& seedingClusterLinkedTrackDeposits() const {return SeedingClusterLinkedTrackDeposits;}
    std::vector<Int_t> const& correlationClusterLinkedTrackIds() const {return CorrelationClusterLinkedTrackIds;}
    std::vector<Double_t> const& correlationClusterLinkedTrackDeposits() const {return CorrelationClusterLinkedTrackDeposits;}

    Double_t seedClusterWidth() const {return SeedClusterWidth;}
    Double_t correlationClusterWidth() const {return CorrelationClusterWidth;}

    Int_t index() const {return Index;}
    UShort_t bx() const {return Bx;}
    UInt_t superID() const {return SuperID;}

private:

    char Projection{'n'};

    Double_t Z{0}; //module().z() + position() * module().tiltSine();
	//position in plane perpendicular to beam axis (if the module is not tilted, it's equivalent to m_position)
	//note that it doesn't depend on the angle around beam axis, only on the amount of tilt
    //position() * module().tiltCosine();
    Double_t PositionPerpendicular{0};
	Double_t PositionError{0}; 	//equivalent to modules's hit resolution
    Double_t PositionErrorMeasurement{0};
    Double_t PositionErrorMultipleScattering{0};
    Double_t PerpendicularResiduum{0};

	Double_t Position{0};			//position in module's coordinates

    Int_t StationID{-1};
    Int_t ModuleID{-1};

	std::vector<Int_t> SeedingClusterLinkedTrackIds;
	std::vector<Double_t> SeedingClusterLinkedTrackDeposits;
    std::vector<Int_t> CorrelationClusterLinkedTrackIds;
    std::vector<Double_t> CorrelationClusterLinkedTrackDeposits;

    Double_t SeedClusterWidth{0};
    Double_t CorrelationClusterWidth{0};

    Int_t Index{-1}; //in the input from digi/data
    UShort_t Bx{0};
    UInt_t SuperID{0};

    ClassDef(MUonERecoOutputHit,2)
};

class MUonERecoOutputTrack {

public:

    MUonERecoOutputTrack() = default;

    MUonERecoOutputTrack(MUonERecoTrack3D const& track)
    : Sector(track.sector()), Index(track.index()),
    Z0(track.xTrack().z0()),
    XSlope(track.xTrack().slope()), XSlopeError(track.xTrack().slopeError()),
    X0(track.xTrack().x0()), X0Error(track.xTrack().x0Error()), 
    YSlope(track.yTrack().slope()), YSlopeError(track.yTrack().slopeError()),
    Y0(track.yTrack().y0()), Y0Error(track.yTrack().y0Error()), 
    LinearFitDerivativeMatrix(track.linearFitDerivativeMatrix()),
    LinearFitCovarianceMatrix(track.linearFitCovarianceMatrix()),
    NumberOfXProjectionHits(track.xTrack().numberOfHits()), NumberOfYProjectionHits(track.yTrack().numberOfHits()), NumberOfStereoHits(track.numberOfStereoHits()),
    Chi2(track.chi2()),DegreesOfFreedom(track.degreesOfFreedom()), Chi2perDegreeOfFreedom(track.chi2PerDegreeOfFreedom()),
    KalmanFitSuccessful(track.kalmanFitSuccessful()),
	FirstStateZ(track.firstStateZ()), FirstStateX(track.firstStateX()), FirstStateY(track.firstStateY()),
	FirstStateXSlope(track.firstStateXSlope()), FirstStateYSlope(track.firstStateYSlope()),
    LinkedTrackID(track.linkedTrackID()), 
    FractionOfHitsSharedWithLinkedTrack(track.fractionOfHitsSharedWithLinkedTrack()),
    ProcessIDofLinkedTrack(track.processIDofLinkedTrack()),
    HitsReassignedDuringKinematicFit(track.hitsReassigned())
    {

        if(track.kalmanFitSuccessful()) {

            auto const& cov = track.firstStateCovarianceMatrix();

            FirstStateXError = TMath::Sqrt(cov(0,0));
            FirstStateYError = TMath::Sqrt(cov(1,1));
            FirstStateXSlopeError = TMath::Sqrt(cov(2,2));
            FirstStateYSlopeError = TMath::Sqrt(cov(3,3));            
        }

        for(auto& h : track.hitsCopy()) {

            Hits.emplace_back(h, track.distanceToHitSigned(h));
        }
    }    

    Int_t sector() const {return Sector;}
    Int_t index() const {return Index;}

    Double_t z0() const {return Z0;}

    Double_t xSlope() const {return XSlope;}
    Double_t xSlopeError() const {return XSlopeError;}

    Double_t x0() const {return X0;}
    Double_t x0Error() const {return X0Error;}

    Double_t ySlope() const {return YSlope;}
    Double_t ySlopeError() const {return YSlopeError;}

    Double_t y0() const {return Y0;}
    Double_t y0Error() const {return Y0Error;}

    std::vector<MUonERecoOutputHit> const& hits() const {return Hits;}
    ROOT::Math::SMatrix<Double_t, 4> const& linearFitDerivativeMatrix() const {return LinearFitDerivativeMatrix;}
    ROOT::Math::SMatrix<Double_t, 4> const& linearFitCovarianceMatrix() const {return LinearFitCovarianceMatrix;}
    

    Int_t numberOfXProjectionHits() const {return NumberOfXProjectionHits;}
    Int_t numberOfYProjectionHits() const {return NumberOfYProjectionHits;}
    Int_t numberOfStereoHits() const {return NumberOfStereoHits;}

    Double_t chi2() const {return Chi2;}
    Int_t degreesOfFreedom() const {return DegreesOfFreedom;}
    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}

    Bool_t kalmanFitSuccessful() const {return KalmanFitSuccessful;}

	Double_t firstStateZ() const {return FirstStateZ;}
	Double_t firstStateX() const {return FirstStateX;}
	Double_t firstStateXError() const {return FirstStateXError;}
	Double_t firstStateY() const {return FirstStateY;}
	Double_t firstStateYError() const {return FirstStateYError;}

	Double_t firstStateXSlope() const {return FirstStateXSlope;}
	Double_t firstStateXSlopeError() const {return FirstStateXSlopeError;}
	Double_t firstStateYSlope() const {return FirstStateYSlope;}
	Double_t firstStateYSlopeError() const {return FirstStateYSlopeError;}

    Int_t linkedTrackID() const {return LinkedTrackID;}
    Double_t fractionOfHitsSharedWithLinkedTrack() const {return FractionOfHitsSharedWithLinkedTrack;}
    Int_t processIDofLinkedTrack() const {return ProcessIDofLinkedTrack;}

    Bool_t hitsReassignedDuringKinematicFit() const {return HitsReassignedDuringKinematicFit;}

private:

    Int_t Sector{-1};
    Int_t Index{-1};

    Double_t Z0{0};

    Double_t XSlope{0};
    Double_t XSlopeError{0};

    Double_t X0{0};
    Double_t X0Error{0};

    Double_t YSlope{0};
    Double_t YSlopeError{0};

    Double_t Y0{0};
    Double_t Y0Error{0};

    std::vector<MUonERecoOutputHit> Hits;

    ROOT::Math::SMatrix<Double_t, 4> LinearFitDerivativeMatrix;
    ROOT::Math::SMatrix<Double_t, 4> LinearFitCovarianceMatrix;

    Int_t NumberOfXProjectionHits{0};
    Int_t NumberOfYProjectionHits{0};
    Int_t NumberOfStereoHits{0};

    Double_t Chi2{0};
    Int_t DegreesOfFreedom{0};
    Double_t Chi2perDegreeOfFreedom{0};

    Bool_t KalmanFitSuccessful{false};

	Double_t FirstStateZ{0};
	Double_t FirstStateX{0};
    Double_t FirstStateXError{0};
	Double_t FirstStateY{0};
    Double_t FirstStateYError{0};

	Double_t FirstStateXSlope{0};
	Double_t FirstStateXSlopeError{0};
    
	Double_t FirstStateYSlope{0};
	Double_t FirstStateYSlopeError{0};

    Int_t LinkedTrackID{-1};
    Double_t FractionOfHitsSharedWithLinkedTrack{0};
    Int_t ProcessIDofLinkedTrack{-1};

    Bool_t HitsReassignedDuringKinematicFit{false};

    ClassDef(MUonERecoOutputTrack,2)
};

class MUonERecoOutputVertex {

public:

    MUonERecoOutputVertex() = default;

    MUonERecoOutputVertex(MUonERecoVertex const& vertex)
    : IncomingMuon(vertex.incomingTrack()),
    StationIndex(vertex.stationIndex()),
    AlternativePIDHypothesis(vertex.alternativePIDHypothesis()),
    XKinematicFit(vertex.xKinematicFit()), XKinematicFitError(vertex.xKinematicFitError()), 
    YKinematicFit(vertex.yKinematicFit()), YKinematicFitError(vertex.yKinematicFitError()), 
    ZKinematicFit(vertex.zKinematicFit()), ZKinematicFitError(vertex.zKinematicFitError()),
    XPositionFit(vertex.xPositionFit()), XPositionFitError(vertex.xPositionFitError()), 
    YPositionFit(vertex.yPositionFit()), YPositionFitError(vertex.yPositionFitError()), 
    ZPositionFit(vertex.zPositionFit()), ZPositionFitError(vertex.zPositionFitError()),
    PositionFitSuccessful(vertex.positionFitSuccessful()), PositionFitCovarianceMatrix(vertex.positionFitCovarianceMatrix()),
    Chi2perDegreeOfFreedom(vertex.chi2PerDegreeOfFreedom()),
    HitsReassignedDuringKinematicFit(vertex.hitsReassigned())
    {

        OutgoingElectron = vertex.alternativePIDHypothesis() ? (vertex.firstTheta() < vertex.secondTheta() ? MUonERecoOutputTrack(vertex.firstOutgoingTrack()) : MUonERecoOutputTrack(vertex.secondOutgoingTrack())) : (vertex.firstTheta() < vertex.secondTheta() ? MUonERecoOutputTrack(vertex.secondOutgoingTrack()) : MUonERecoOutputTrack(vertex.firstOutgoingTrack()));
        ElectronTheta = vertex.alternativePIDHypothesis() ? (vertex.firstTheta() < vertex.secondTheta() ? vertex.firstTheta() : vertex.secondTheta()) : (vertex.firstTheta() < vertex.secondTheta() ? vertex.secondTheta() : vertex.firstTheta());
        ElectronThetaError = vertex.alternativePIDHypothesis() ? (vertex.firstTheta() < vertex.secondTheta() ? vertex.firstThetaError() : vertex.secondThetaError()) : (vertex.firstTheta() < vertex.secondTheta() ? vertex.secondThetaError() : vertex.firstThetaError());

        OutgoingMuon = vertex.alternativePIDHypothesis() ? (vertex.firstTheta() < vertex.secondTheta() ? MUonERecoOutputTrack(vertex.secondOutgoingTrack()) : MUonERecoOutputTrack(vertex.firstOutgoingTrack())) : (vertex.firstTheta() < vertex.secondTheta() ? MUonERecoOutputTrack(vertex.firstOutgoingTrack()) : MUonERecoOutputTrack(vertex.secondOutgoingTrack()));
        MuonTheta = vertex.alternativePIDHypothesis() ? (vertex.firstTheta() < vertex.secondTheta() ? vertex.secondTheta() : vertex.firstTheta()) : (vertex.firstTheta() < vertex.secondTheta() ? vertex.firstTheta() : vertex.secondTheta());
        MuonThetaError = vertex.alternativePIDHypothesis() ? (vertex.firstTheta() < vertex.secondTheta() ? vertex.secondThetaError() : vertex.firstThetaError()) : (vertex.firstTheta() < vertex.secondTheta() ? vertex.firstThetaError() : vertex.secondThetaError());


        auto i = vertex.incomingTrack().directionVector();
        auto o1 = vertex.firstOutgoingTrack().directionVector();
        auto o2 = vertex.secondOutgoingTrack().directionVector();

        TripleProduct = i.Dot(o1.Cross(o2));
        Acoplanarity = M_PI_2 - i.Angle(o1.Cross(o2));
        ModifiedAcoplanarity = M_PI - (i.Cross(o1)).Angle(i.Cross(o2)); 
        if(TripleProduct < 0) ModifiedAcoplanarity *= -1;

    }

    MUonERecoOutputTrack incomingMuon() const {return IncomingMuon;}
    MUonERecoOutputTrack outgoingMuon() const {return OutgoingMuon;}
    MUonERecoOutputTrack outgoingElectron() const {return OutgoingElectron;}

    Int_t stationIndex() const {return StationIndex;}

    Bool_t alternativePIDHypothesis() const {return AlternativePIDHypothesis;}

    Double_t muonTheta() const {return MuonTheta;}
    Double_t muonThetaError() const {return MuonThetaError;}

    Double_t electronTheta() const {return ElectronTheta;}
    Double_t electronThetaError() const {return ElectronThetaError;}

    Double_t tripleProduct() const {return TripleProduct;}
    Double_t acoplanarity() const {return Acoplanarity;}
    Double_t modifiedAcoplanarity() const {return ModifiedAcoplanarity;}

    Double_t xKinematicFit() const {return XKinematicFit;}
    Double_t xKinematicFitError() const {return XKinematicFitError;}

    Double_t yKinematicFit() const {return YKinematicFit;}
    Double_t yKinematicFitError() const {return YKinematicFitError;}

    Double_t zKinematicFit() const {return ZKinematicFit;}
    Double_t zKinematicFitError() const {return ZKinematicFitError;}

    Double_t xPositionFit() const {return XPositionFit;}
    Double_t xPositionFitError() const {return XPositionFitError;}

    Double_t yPositionFit() const {return YPositionFit;}
    Double_t yPositionFitError() const {return YPositionFitError;}

    Double_t zPositionFit() const {return ZPositionFit;}
    Double_t zPositionFitError() const {return ZPositionFitError;}

    Bool_t positionFitSuccessful() const {return PositionFitSuccessful;}
    ROOT::Math::SMatrix<Double_t, 3> const& positionFitCovarianceMatrix() const {return PositionFitCovarianceMatrix;}    

    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}

    Bool_t hitsReassignedDuringKinematicFit() const {return HitsReassignedDuringKinematicFit;}

private:

    MUonERecoOutputTrack IncomingMuon;
    MUonERecoOutputTrack OutgoingMuon;
    MUonERecoOutputTrack OutgoingElectron;

    Int_t StationIndex{-1};

    Bool_t AlternativePIDHypothesis{false};

    Double_t MuonTheta{0};
    Double_t MuonThetaError{0};

    Double_t ElectronTheta{0};
    Double_t ElectronThetaError{0};

    Double_t TripleProduct{0};
    Double_t Acoplanarity{0};
    Double_t ModifiedAcoplanarity{0};

    Double_t XKinematicFit{0};
    Double_t XKinematicFitError{0};

    Double_t YKinematicFit{0};
    Double_t YKinematicFitError{0};

    Double_t ZKinematicFit{0};
    Double_t ZKinematicFitError{0};

    Double_t XPositionFit{0};
    Double_t XPositionFitError{0};

    Double_t YPositionFit{0};
    Double_t YPositionFitError{0};

    Double_t ZPositionFit{0};
    Double_t ZPositionFitError{0};

    Bool_t PositionFitSuccessful{false};
    ROOT::Math::SMatrix<Double_t, 3> PositionFitCovarianceMatrix;

    Double_t Chi2perDegreeOfFreedom{0};

    Bool_t HitsReassignedDuringKinematicFit{false};

    ClassDef(MUonERecoOutputVertex,2)
};


class MUonERecoOutputGenericVertex {

public:

    MUonERecoOutputGenericVertex() = default;

    MUonERecoOutputGenericVertex(MUonERecoGenericVertex const& vertex)
    : IncomingTrack(vertex.incomingTrack()),
    Multiplicity(vertex.multiplicity()),
    StationIndex(vertex.stationIndex()),
    XKinematicFit(vertex.xKinematicFit()), XKinematicFitError(vertex.xKinematicFitError()), 
    YKinematicFit(vertex.yKinematicFit()), YKinematicFitError(vertex.yKinematicFitError()), 
    ZKinematicFit(vertex.zKinematicFit()), ZKinematicFitError(vertex.zKinematicFitError()),
    XPositionFit(vertex.xPositionFit()), XPositionFitError(vertex.xPositionFitError()), 
    YPositionFit(vertex.yPositionFit()), YPositionFitError(vertex.yPositionFitError()), 
    ZPositionFit(vertex.zPositionFit()), ZPositionFitError(vertex.zPositionFitError()),
    PositionFitSuccessful(vertex.positionFitSuccessful()), PositionFitCovarianceMatrix(vertex.positionFitCovarianceMatrix()),
    Chi2perDegreeOfFreedom(vertex.chi2PerDegreeOfFreedom()),
    HitsReassignedDuringKinematicFit(vertex.hitsReassigned())
    {
        OutgoingTracks.reserve(vertex.multiplicity());
        for(auto const& track : vertex.outgoingTracks())
            OutgoingTracks.emplace_back(track);
    }

    MUonERecoOutputTrack incomingTrack() const {return IncomingTrack;}
    std::vector<MUonERecoOutputTrack> outgoingTracks() const {return OutgoingTracks;}


    Int_t multiplicity() const {return Multiplicity;}
    Int_t stationIndex() const {return StationIndex;}

    Double_t xKinematicFit() const {return XKinematicFit;}
    Double_t xKinematicFitError() const {return XKinematicFitError;}

    Double_t yKinematicFit() const {return YKinematicFit;}
    Double_t yKinematicFitError() const {return YKinematicFitError;}

    Double_t zKinematicFit() const {return ZKinematicFit;}
    Double_t zKinematicFitError() const {return ZKinematicFitError;}

    Double_t xPositionFit() const {return XPositionFit;}
    Double_t xPositionFitError() const {return XPositionFitError;}

    Double_t yPositionFit() const {return YPositionFit;}
    Double_t yPositionFitError() const {return YPositionFitError;}

    Double_t zPositionFit() const {return ZPositionFit;}
    Double_t zPositionFitError() const {return ZPositionFitError;}

    Bool_t positionFitSuccessful() const {return PositionFitSuccessful;}
    ROOT::Math::SMatrix<Double_t, 3> const& positionFitCovarianceMatrix() const {return PositionFitCovarianceMatrix;}    


    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}

    Bool_t hitsReassignedDuringKinematicFit() const {return HitsReassignedDuringKinematicFit;}


private:

    MUonERecoOutputTrack IncomingTrack;
    std::vector<MUonERecoOutputTrack> OutgoingTracks;

    Int_t Multiplicity{0};

    Int_t StationIndex{-1};

    Double_t XKinematicFit{0};
    Double_t XKinematicFitError{0};

    Double_t YKinematicFit{0};
    Double_t YKinematicFitError{0};

    Double_t ZKinematicFit{0};
    Double_t ZKinematicFitError{0};

    Double_t XPositionFit{0};
    Double_t XPositionFitError{0};

    Double_t YPositionFit{0};
    Double_t YPositionFitError{0};

    Double_t ZPositionFit{0};
    Double_t ZPositionFitError{0};

    Bool_t PositionFitSuccessful{false};
    ROOT::Math::SMatrix<Double_t, 3> PositionFitCovarianceMatrix;


    Double_t Chi2perDegreeOfFreedom{0};

    Bool_t HitsReassignedDuringKinematicFit{false};


    ClassDef(MUonERecoOutputGenericVertex,2)
};


class MUonERecoOutputAdaptiveFitterVertex {

public:

    MUonERecoOutputAdaptiveFitterVertex() = default;

    MUonERecoOutputAdaptiveFitterVertex(MUonERecoAdaptiveFitterVertex const& vertex, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector) 
        : StationIndex(vertex.stationIndex()), TrackWeights(vertex.trackWeights()),
        X(vertex.position().x()), XError(TMath::Sqrt(vertex.positionCovariance()(0,0))),
        Y(vertex.position().y()), YError(TMath::Sqrt(vertex.positionCovariance()(1,1))),
        Z(vertex.position().z()), ZError(TMath::Sqrt(vertex.positionCovariance()(2,2))),
        Chi2(vertex.chi2()), PositionCovariance(vertex.positionCovariance())
    {
        auto const& tracks = reconstructedTracksPerSector[vertex.targetIndex() + 1]; //tracks after the target in which the vertex was reconstructed
        auto const& track_id = vertex.trackID();

        for(auto const& id : track_id) {

            Tracks.emplace_back(tracks[id]);
        }
    }

    Int_t stationIndex() const {return StationIndex;}

    std::vector<MUonERecoOutputTrack> const& tracks() const {return Tracks;}
    std::vector<Double_t> const& trackWeights() const {return TrackWeights;}    

    Double_t x() const {return X;}
    Double_t xError() const {return XError;}

    Double_t y() const {return Y;}
    Double_t yError() const {return YError;}

    Double_t z() const {return Z;}
    Double_t zError() const {return ZError;}

    Double_t chi2() const {return Chi2;}

    ROOT::Math::SMatrix<Double_t, 3> const& positionCovariance() const {return PositionCovariance;}    

private:

    Int_t StationIndex{-1};

    std::vector<MUonERecoOutputTrack> Tracks;
    std::vector<Double_t> TrackWeights;    

    Double_t X{0};
    Double_t XError{0};

    Double_t Y{0};
    Double_t YError{0};

    Double_t Z{0};
    Double_t ZError{0};

    Double_t Chi2{0};

    ROOT::Math::SMatrix<Double_t, 3> PositionCovariance;
    
    ClassDef(MUonERecoOutputAdaptiveFitterVertex,2)
};


/*
    Class used to store reconstruction output.
*/

class MUonERecoOutput {

public:

    MUonERecoOutput() = default;

    //used for events that failed to reconstruct
    MUonERecoOutput(Int_t eventNumber, Double_t eventEnergy, std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector, std::vector<MUonERecoAdaptiveFitterVertex> afVertices, MUonEReconstructionConfiguration const& recoConfig, Bool_t event_skipped)
        : IsReconstructed(false), SourceEventNumber(eventNumber), IsMC(recoConfig.isMC()), TotalEventEnergy(eventEnergy), EventSkipped(event_skipped)
        {

            ReconstructedHits.reserve(50);
            ReconstructedHitsMultiplicity = 0;

            for(auto const& sectors : hitsPerModulePerSector) {
                for(auto const& modules : sectors) {
                    for(auto const& hit : modules) {

                        ReconstructedHits.emplace_back(hit, 0);
                        ++ReconstructedHitsMultiplicity;
                    }
                }
            }
                

            ReconstructedTracks.reserve(50);
            ReconstructedTracksMultiplicity = 0;


            for(auto const& sectors : reconstructedTracksPerSector) {
                for(auto const& track : sectors) {

                    ReconstructedTracks.emplace_back(track);
                    ++ReconstructedTracksMultiplicity;
                }
            }
            

            if(!afVertices.empty()) {

                AdaptiveFitterVerticesMultiplicity = afVertices.size();
                AdaptiveFitterVertices.reserve(afVertices.size());

                for(auto& afv : afVertices) {

                    AdaptiveFitterVertices.emplace_back(afv, reconstructedTracksPerSector);
                }
            }            

        }


    //used for correctly reconstructed event
    MUonERecoOutput(Int_t eventNumber, Double_t eventEnergy, std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector, std::vector<MUonERecoVertex> const& vertices, std::vector<MUonERecoAdaptiveFitterVertex> afVertices, std::vector<MUonERecoGenericVertex> genericVertices, MUonEReconstructionConfiguration const& recoConfig)
        : IsReconstructed(true), SourceEventNumber(eventNumber), IsMC(recoConfig.isMC()), TotalEventEnergy(eventEnergy), BestVertex(vertices[0])
        {

            ReconstructedHits.reserve(50);
            ReconstructedHitsMultiplicity = 0;

            for(auto const& sectors : hitsPerModulePerSector) {
                for(auto const& modules : sectors) {
                    for(auto const& hit : modules) {

                        ReconstructedHits.emplace_back(hit, 0);
                        ++ReconstructedHitsMultiplicity;
                    }
                }
            }

            ReconstructedTracks.reserve(50);
            ReconstructedTracksMultiplicity = 0;


            for(auto const& sectors : reconstructedTracksPerSector) {
                for(auto const& track : sectors) {

                    ReconstructedTracks.emplace_back(track);
                    ++ReconstructedTracksMultiplicity;
                }
            }
            

            ReconstructedVertices.reserve(vertices.size());


            ReconstructedVerticesMultiplicity = vertices.size();
            for(auto const& vertex : vertices)
                ReconstructedVertices.emplace_back(vertex);                    
            

            if(!afVertices.empty()) {

                AdaptiveFitterVerticesMultiplicity = afVertices.size();
                AdaptiveFitterVertices.reserve(afVertices.size());

                for(auto& afv : afVertices) {

                    AdaptiveFitterVertices.emplace_back(afv, reconstructedTracksPerSector);
                }
            }

            ReconstructedGenericVerticesMultiplicity = genericVertices.size();
            ReconstructedGenericVertices.reserve(genericVertices.size());
            for(auto const& vertex : genericVertices)
                ReconstructedGenericVertices.emplace_back(vertex);
        }




    Bool_t isReconstructed() const {return IsReconstructed;}

    Int_t sourceEventNumber() const {return SourceEventNumber;}

    Bool_t isMC() const {return IsMC;}
    Bool_t eventSkipped() const {return EventSkipped;}

    Double_t totalEventEnergy() const {return TotalEventEnergy;}

    MUonERecoOutputVertex const& bestVertex() const {return BestVertex;}

    Int_t reconstructedHitsMultiplicity() const {return ReconstructedHitsMultiplicity;}
    std::vector<MUonERecoOutputHit> const& reconstructedHits() const {return ReconstructedHits;}

    Int_t reconstructedTracksMultiplicity() const {return ReconstructedTracksMultiplicity;}
    std::vector<MUonERecoOutputTrack> const& reconstructedTracks() const {return ReconstructedTracks;}

    Int_t reconstructedVerticesMultiplicity() const {return ReconstructedVerticesMultiplicity;}
    std::vector<MUonERecoOutputVertex> const& reconstructedVertices() const {return ReconstructedVertices;}

    Int_t adaptiveFitterVerticesMultiplicity() const {return AdaptiveFitterVerticesMultiplicity;}
    std::vector<MUonERecoOutputAdaptiveFitterVertex> const& adaptiveFitterVertices() const {return AdaptiveFitterVertices;}

    Int_t reconstructedGenericVerticesMultiplicity() const {return ReconstructedGenericVerticesMultiplicity;}
    std::vector<MUonERecoOutputGenericVertex> const& reconstructedGenericVertices() const {return ReconstructedGenericVertices;}

private:

    Bool_t IsReconstructed{false};

    Int_t SourceEventNumber{-1};

    Bool_t IsMC{true};
    Bool_t EventSkipped{false};

    Double_t TotalEventEnergy{0};


    MUonERecoOutputVertex BestVertex;

    Int_t ReconstructedHitsMultiplicity{0};
    std::vector<MUonERecoOutputHit> ReconstructedHits;

    Int_t ReconstructedTracksMultiplicity{0};
    std::vector<MUonERecoOutputTrack> ReconstructedTracks;

    Int_t ReconstructedVerticesMultiplicity{0};
    std::vector<MUonERecoOutputVertex> ReconstructedVertices;

    Int_t AdaptiveFitterVerticesMultiplicity{0};
    std::vector<MUonERecoOutputAdaptiveFitterVertex> AdaptiveFitterVertices;

    Int_t ReconstructedGenericVerticesMultiplicity{0};
    std::vector<MUonERecoOutputGenericVertex> ReconstructedGenericVertices;


    ClassDef(MUonERecoOutput,2)
};

#endif//MUONERECOOUTPUT_H 

