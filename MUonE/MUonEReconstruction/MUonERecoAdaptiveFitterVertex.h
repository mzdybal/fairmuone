#ifndef MUONERECOADAPTIVEFITTERVERTEX_H
#define MUONERECOADAPTIVEFITTERVERTEX_H

#include "Math/Point3D.h"
#include "Math/SMatrix.h"
#include "Rtypes.h"

#include <vector>
#include <utility>



class MUonERecoAdaptiveFitterVertex {

public:

    MUonERecoAdaptiveFitterVertex() = default;

    MUonERecoAdaptiveFitterVertex(ROOT::Math::XYZPoint& position, ROOT::Math::SMatrix<Double_t, 3> const& positionCovariance,
        std::vector<Int_t>& trackID, std::vector<Double_t>& trackWeights, Double_t chi2, Int_t stationIndex, Int_t targetIndex)
        : m_position(position), m_positionCovariance(positionCovariance), 
        m_trackID(trackID), m_trackWeights(trackWeights), m_chi2(chi2), 
        m_stationIndex(stationIndex), m_targetIndex(targetIndex)
        {}

    ROOT::Math::XYZPoint const& position() const {return m_position;}
    ROOT::Math::SMatrix<Double_t, 3> const& positionCovariance() const {return m_positionCovariance;}

    std::vector<Int_t> const& trackID() const {return m_trackID;}
    std::vector<Double_t> const& trackWeights() const {return m_trackWeights;}

    Double_t chi2() const {return m_chi2;}

    Int_t stationIndex() const {return m_stationIndex;}
    Int_t targetIndex() const {return m_targetIndex;}

private:

    ROOT::Math::XYZPoint m_position;
    ROOT::Math::SMatrix<Double_t, 3> m_positionCovariance;

    std::vector<Int_t> m_trackID;
    std::vector<Double_t> m_trackWeights;

    Double_t m_chi2{0};

    Int_t m_stationIndex{-1};
    Int_t m_targetIndex{-1}; //to correctly map track IDs to 3DTracks for output


	//if sorting is required, prefer vertices with lower chi2
	//worse vertex is considered to be of lower value, that is worse < better is true	
    friend inline bool operator< (MUonERecoAdaptiveFitterVertex const& lhs, MUonERecoAdaptiveFitterVertex const& rhs) {
        
        return lhs.chi2() > rhs.chi2();
    }
    friend inline bool operator> (MUonERecoAdaptiveFitterVertex const& lhs, MUonERecoAdaptiveFitterVertex const& rhs) {return  operator< (rhs,lhs);}
    friend inline bool operator<=(MUonERecoAdaptiveFitterVertex const& lhs, MUonERecoAdaptiveFitterVertex const& rhs) {return !operator> (lhs,rhs);}
    friend inline bool operator>=(MUonERecoAdaptiveFitterVertex const& lhs, MUonERecoAdaptiveFitterVertex const& rhs) {return !operator< (lhs,rhs);}


    ClassDef(MUonERecoAdaptiveFitterVertex,2)
};


#endif //MUONERECOADAPTIVEFITTERVERTEX_H


