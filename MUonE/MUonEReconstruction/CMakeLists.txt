Add_Subdirectory(Configuration)

set(target MUonEReconstruction)

set(sources 
MUonEReconstruction.cxx
MUonERecoFitter.cxx
)

fair_change_extensions_if_exists(.cxx .h FILES "${sources}" OUTVAR headers)

list(APPEND headers
MUonERecoHit.h
MUonERecoModule.h
MUonERecoTarget.h
MUonERecoTrack2D.h
MUonERecoTrack3D.h
MUonERecoVertex.h
MUonERecoAdaptiveFitterVertex.h
MUonERecoOutput.h
)


add_library(${target} SHARED ${sources} ${headers})
add_library(MUonE::${target} ALIAS ${target})
set_target_properties(${target} PROPERTIES ${PROJECT_LIBRARY_PROPERTIES})



target_include_directories(${target} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>

    # TODO: DELETE ME ONCE USING root targets
    ${ROOT_INCLUDE_DIR}
)

target_link_directories(${target} PUBLIC
    ${ROOT_LIBRARY_DIR}
)

target_link_libraries(${target} PUBLIC
    MUonE::MUonEDetectorConfiguration
    MUonE::MUonEStack
    MUonE::MUonEDigitizationTracker
    MUonE::MUonEDigitizationCalorimeter
    MUonE::MUonEReconstructionConfiguration
    MUonE::MUonEAlignmentContainer
    MUonE::Mesmer
    Core
    Physics # TLorentzVector, TVector3
    MathCore # TMath
    FairRoot::Base
)

add_dependencies(${target} Mesmer)

fairroot_target_root_dictionary(${target}
    HEADERS ${headers}
    LINKDEF MUonEReconstructionLinkDef.h
)

install(TARGETS ${target} LIBRARY DESTINATION ${PROJECT_INSTALL_LIBDIR})
install(FILES ${headers} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
