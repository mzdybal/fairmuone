#include "MUonEAnalysisBasicPlotsConfiguration.h"

#include <fairlogger/Logger.h>


MUonEAnalysisBasicPlotsConfiguration::MUonEAnalysisBasicPlotsConfiguration()
{
    resetDefaultConfiguration();
}

Bool_t MUonEAnalysisBasicPlotsConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    if(config["plotBestVertexChi2PerNdf"]) {
        m_plotBestVertexChi2PerNdf = config["plotBestVertexChi2PerNdf"].as<Bool_t>();
        m_isActive = true;
    }

    if(m_plotBestVertexChi2PerNdf) {

        if(config["bestVertexChi2PerNdfRange"]) {
            
            auto range = config["bestVertexChi2PerNdfRange"];

            if(range["min"])
                m_bestVertexChi2PerNdfRangeMin = range["min"].as<Double_t>();
            else
                LOG(info) << "bestVertexChi2PerNdfRange/min not set. Default minimum will be used.";

            if(range["max"])
                m_bestVertexChi2PerNdfRangeMax = range["max"].as<Double_t>();
            else
                LOG(info) << "bestVertexChi2PerNdfRange/max not set. Default maximum will be used.";

        } else
            LOG(info) << "Variable 'bestVertexChi2PerNdfRange' not set. Default ranges will be used.";
        
        if(config["bestVertexChi2PerNdfBins"])
            m_bestVertexChi2PerNdfBins = config["bestVertexChi2PerNdfBins"].as<Int_t>();
        else
            LOG(info) << "Variable 'bestVertexChi2PerNdfBins' not set. Default number of bins will be used.";

    }


    if(config["plotAngleCorrelation"]) {
        m_plotAngleCorrelation = config["plotAngleCorrelation"].as<Bool_t>();
        m_isActive = true;
    }

    if(m_plotAngleCorrelation) {

        if(config["angleCorrelationMuThetaRange"]) {
            
            auto range = config["angleCorrelationMuThetaRange"];

            if(range["min"])
                m_angleCorrelationMuThetaRangeMin = range["min"].as<Double_t>();
            else
                LOG(info) << "angleCorrelationMuThetaRange/min not set. Default minimum will be used.";

            if(range["max"])
                m_angleCorrelationMuThetaRangeMax = range["max"].as<Double_t>();
            else
                LOG(info) << "angleCorrelationMuThetaRange/max not set. Default maximum will be used.";

        } else
            LOG(info) << "Variable 'angleCorrelationMuThetaRange' not set. Default ranges will be used.";
        
        if(config["angleCorrelationMuThetaBins"])
            m_angleCorrelationMuThetaBins = config["angleCorrelationMuThetaBins"].as<Int_t>();
        else
            LOG(info) << "Variable 'angleCorrelationMuThetaBins' not set. Default number of bins will be used.";


        if(config["angleCorrelationEThetaRange"]) {
            
            auto range = config["angleCorrelationEThetaRange"];

            if(range["min"])
                m_angleCorrelationEThetaRangeMin = range["min"].as<Double_t>();
            else
                LOG(info) << "angleCorrelationEThetaRange/min not set. Default minimum will be used.";

            if(range["max"])
                m_angleCorrelationEThetaRangeMax = range["max"].as<Double_t>();
            else
                LOG(info) << "angleCorrelationEThetaRange/max not set. Default maximum will be used.";

        } else
            LOG(info) << "Variable 'angleCorrelationEThetaRange' not set. Default ranges will be used.";
        
        if(config["angleCorrelationEThetaBins"])
            m_angleCorrelationEThetaBins = config["angleCorrelationEThetaBins"].as<Int_t>();
        else
            LOG(info) << "Variable 'angleCorrelationEThetaBins' not set. Default number of bins will be used.";


        if(config["angleCorrelationAdditionalChi2PerNdfCuts"]) {

            auto cuts = config["angleCorrelationAdditionalChi2PerNdfCuts"];

            for(auto c : cuts)
                m_angleCorrelationAdditionalChi2PerNdfCuts.push_back(c.as<Double_t>());
        }
    }

    if(!m_configuredCorrectly)
        LOG(info) << "BasicPlots configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}


void MUonEAnalysisBasicPlotsConfiguration::logCurrentConfiguration() const {

    LOG(info) << "";
    if(m_isActive)
        LOG(info) << "BasicPlots task is active";
    else
        LOG(info) << "BasicPlots task is not active";

    if(m_plotBestVertexChi2PerNdf) {
        LOG(info) << "Best vertex Chi2/ndf will be plotted with following settings:";

        LOG(info) << "plotBestVertexChi2PerNdf: " << m_plotBestVertexChi2PerNdf;
        LOG(info) << "bestVertexChi2PerNdfRangeMin: " << m_bestVertexChi2PerNdfRangeMin;
        LOG(info) << "bestVertexChi2PerNdfRangeMax: " << m_bestVertexChi2PerNdfRangeMax;
        LOG(info) << "bestVertexChi2PerNdfBins: " << m_bestVertexChi2PerNdfBins;    

    }


    if(m_plotAngleCorrelation) {
        LOG(info) << "";
        LOG(info) << "Angle correlation will be plotted with following settings:";

        LOG(info) << "angleCorrelationMuThetaRangeMin: " << m_angleCorrelationMuThetaRangeMin;
        LOG(info) << "angleCorrelationMuThetaRangeMax: " << m_angleCorrelationMuThetaRangeMax;
        LOG(info) << "angleCorrelationMuThetaBins: " << m_angleCorrelationMuThetaBins;
        LOG(info) << "angleCorrelationEThetaRangeMin: " << m_angleCorrelationEThetaRangeMin;
        LOG(info) << "angleCorrelationEThetaRangeMax: " << m_angleCorrelationEThetaRangeMax;
        LOG(info) << "angleCorrelationEThetaBins: " << m_angleCorrelationEThetaBins;  

        if(!m_angleCorrelationAdditionalChi2PerNdfCuts.empty()) {

            std::string tmp = std::to_string(m_angleCorrelationAdditionalChi2PerNdfCuts.front());
            if(m_angleCorrelationAdditionalChi2PerNdfCuts.size() > 1) {

                for(Int_t indx = 1; indx < m_angleCorrelationAdditionalChi2PerNdfCuts.size(); ++ indx)
                    tmp += ", " + std::to_string(m_angleCorrelationAdditionalChi2PerNdfCuts[indx]);
            }
            LOG(info) << "angleCorrelationAdditionalChi2PerNdfCuts: " << tmp;
        }      
    }
}


void MUonEAnalysisBasicPlotsConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_isActive = false;

    m_plotBestVertexChi2PerNdf = false;
    m_bestVertexChi2PerNdfRangeMin = 0;
    m_bestVertexChi2PerNdfRangeMax = 100;
    m_bestVertexChi2PerNdfBins = 100;    

    m_plotAngleCorrelation = false;
    m_angleCorrelationMuThetaRangeMin = 0;
    m_angleCorrelationMuThetaRangeMax = 5;
    m_angleCorrelationMuThetaBins = 100;
    m_angleCorrelationEThetaRangeMin = 0;
    m_angleCorrelationEThetaRangeMax = 50;
    m_angleCorrelationEThetaBins = 100;
    m_angleCorrelationAdditionalChi2PerNdfCuts.clear();    
}


ClassImp(MUonEAnalysisBasicPlotsConfiguration)