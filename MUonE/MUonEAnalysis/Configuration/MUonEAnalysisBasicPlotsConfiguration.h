#ifndef MUONEANALYSISBASICPLOTSCONFIGURATION_H
#define MUONEANALYSISBASICPLOTSCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>
#include <vector>

class MUonEAnalysisBasicPlotsConfiguration {

public:

    MUonEAnalysisBasicPlotsConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t isActive() const {return m_isActive;}

    Bool_t plotBestVertexChi2PerNdf() const {return m_plotBestVertexChi2PerNdf;}
    Double_t bestVertexChi2PerNdfRangeMin() const {return m_bestVertexChi2PerNdfRangeMin;}
    Double_t bestVertexChi2PerNdfRangeMax() const {return m_bestVertexChi2PerNdfRangeMax;}
    Int_t bestVertexChi2PerNdfBins() const {return m_bestVertexChi2PerNdfBins;}


    Bool_t plotAngleCorrelation() const {return m_plotAngleCorrelation;}
    Double_t angleCorrelationMuThetaRangeMin() const {return m_angleCorrelationMuThetaRangeMin;}
    Double_t angleCorrelationMuThetaRangeMax() const {return m_angleCorrelationMuThetaRangeMax;}
    Int_t angleCorrelationMuThetaBins() const {return m_angleCorrelationMuThetaBins;}
    Double_t angleCorrelationEThetaRangeMin() const {return m_angleCorrelationEThetaRangeMin;}
    Double_t angleCorrelationEThetaRangeMax() const {return m_angleCorrelationEThetaRangeMax;}
    Int_t angleCorrelationEThetaBins() const {return m_angleCorrelationEThetaBins;}
    std::vector<Double_t> const& angleCorrelationAdditionalChi2PerNdfCuts() const {return m_angleCorrelationAdditionalChi2PerNdfCuts;}
    

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Bool_t m_isActive{false};

    Bool_t m_plotBestVertexChi2PerNdf{false};
    Double_t m_bestVertexChi2PerNdfRangeMin{0};
    Double_t m_bestVertexChi2PerNdfRangeMax{100};
    Int_t m_bestVertexChi2PerNdfBins{100};


    Bool_t m_plotAngleCorrelation{false};
    Double_t m_angleCorrelationMuThetaRangeMin{0};
    Double_t m_angleCorrelationMuThetaRangeMax{5};
    Int_t m_angleCorrelationMuThetaBins{100};
    Double_t m_angleCorrelationEThetaRangeMin{0};
    Double_t m_angleCorrelationEThetaRangeMax{50};
    Int_t m_angleCorrelationEThetaBins{100};
    std::vector<Double_t> m_angleCorrelationAdditionalChi2PerNdfCuts;
  

    ClassDef(MUonEAnalysisBasicPlotsConfiguration, 2)
};


#endif //MUONEANALYSISBASICPLOTSCONFIGURATION_H

