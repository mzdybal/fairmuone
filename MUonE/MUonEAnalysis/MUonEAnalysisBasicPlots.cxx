#include "MUonEAnalysisBasicPlots.h"

#include "FairRootManager.h"

#include "MUonERecoOutput.h"

MUonEAnalysisBasicPlots::MUonEAnalysisBasicPlots()
	: FairTask("MUonEAnalysisBasicPlots", 0)
	{
		resetDefaultConfiguration();
	}


MUonEAnalysisBasicPlots::~MUonEAnalysisBasicPlots()
{
    if(h_bestVertexChi2PerNdf) {
        delete h_bestVertexChi2PerNdf;
        h_bestVertexChi2PerNdf = nullptr;
    }

    if(h_angleCorrelation) {
        delete h_angleCorrelation;
        h_angleCorrelation = nullptr;
    }

    for(auto& p : h_angleCorrelationAdditionalChi2PerNdfCuts) {
        if(p) {
            delete p;
            p = nullptr;
        }
    }
}

Bool_t MUonEAnalysisBasicPlots::setConfiguration(MUonEAnalysisBasicPlotsConfiguration const& config) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;

	m_configuration = config;
    m_configuredCorrectly = m_configuration.configuredCorrectly();

    if(!m_configuredCorrectly)
        LOG(info) << "basicPlotsConfiguration section contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}

void MUonEAnalysisBasicPlots::logCurrentConfiguration() {

	LOG(info) << "";
	m_configuration.logCurrentConfiguration();
}

void MUonEAnalysisBasicPlots::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_configuration.resetDefaultConfiguration();

    for(auto& p : h_angleCorrelationAdditionalChi2PerNdfCuts) {
        if(p) {
            delete p;
            p = nullptr;
        }
    }

    h_angleCorrelationAdditionalChi2PerNdfCuts.clear();
}


InitStatus MUonEAnalysisBasicPlots::ReInit()
{
	return kSUCCESS;
}

InitStatus MUonEAnalysisBasicPlots::Init()
{
    if(!m_configuration.isActive())
        return kSUCCESS;

	LOG(info) << "Initializing BasicPlots task.";

	FairRootManager* ioman = FairRootManager::Instance();

	if(!ioman) 
	{ 
		LOG(fatal) << "No FairRootManager"; 
		return kERROR;
	} 

	m_trackerPoints = static_cast<TClonesArray*>(ioman->GetObject("TrackerPoints"));

    if(nullptr != m_trackerPoints) {
        m_trackerPointsFound = true;
    }
    else {
        m_trackerPointsFound = false;
        LOG(info) << "TrackerPoints not found in the ntuple. Plots dependent on them will be ommited.";
    }

	m_calorimeterPoints = static_cast<TClonesArray*>(ioman->GetObject("CalorimeterPoints"));

    if(nullptr != m_trackerPoints) {
        m_calorimeterPointsFound = true;
    }
    else {
        m_calorimeterPointsFound = false;
        LOG(info) << "CalorimeterPoints not found in the ntuple. Plots dependent on them will be ommited.";
    }

	m_stubs = static_cast<TClonesArray*>(ioman->GetObject("TrackerStubs"));

    if(nullptr != m_stubs) {
        m_stubsFound = true;
    }
    else {
        m_stubsFound = false;
        LOG(info) << "Stubs not found in the ntuple. Plots dependent on them will be ommited.";
    }

	m_calorimeterDeposit = ioman->InitObjectAs<const MUonECalorimeterDigiDeposit*>("CalorimeterDigiDeposit");

    if(nullptr != m_stubs) {
        m_calorimeterDepositFound = true;
    }
    else {
        m_calorimeterDepositFound = false;
        LOG(info) << "CalorimeterDigiDeposit not found in the ntuple. Plots dependent on it will be ommited.";
    }

    m_reconstructionOutput = ioman->InitObjectAs<const MUonERecoOutput*>("ReconstructionOutput");

    if(nullptr != m_reconstructionOutput) {
        m_reconstructionOutputFound = true;
    }
    else {
        m_reconstructionOutputFound = false;
        LOG(info) << "ReconstructionOutput not found in the ntuple. Plots dependent on it will be ommited.";
    }

    setupHistograms();

	return kSUCCESS;
}

void MUonEAnalysisBasicPlots::Exec(Option_t* option) 
{

    if(!m_configuration.isActive())
        return;

    if(m_reconstructionOutputFound && m_reconstructionOutput->isReconstructed()) {

        h_bestVertexChi2PerNdf->Fill(m_reconstructionOutput->bestVertex().chi2perDegreeOfFreedom());
        h_angleCorrelation->Fill(m_reconstructionOutput->bestVertex().electronTheta()*1e3, m_reconstructionOutput->bestVertex().muonTheta()*1e3);

        auto cuts = m_configuration.angleCorrelationAdditionalChi2PerNdfCuts();
        for(Int_t indx = 0; indx < cuts.size(); ++indx) {

            if(m_reconstructionOutput->bestVertex().chi2perDegreeOfFreedom() < cuts[indx]) {
                h_angleCorrelationAdditionalChi2PerNdfCuts[indx]->Fill(m_reconstructionOutput->bestVertex().electronTheta()*1e3, m_reconstructionOutput->bestVertex().muonTheta()*1e3);
            }
        }
    }

}

void MUonEAnalysisBasicPlots::Finish() 
{
    saveHistograms();
}


void MUonEAnalysisBasicPlots::setupHistograms() {

    if(m_configuration.plotBestVertexChi2PerNdf()) {

        h_bestVertexChi2PerNdf = new TH1D("h_bestVertexChi2PerNdf", "best vertex #chi^{2}/ndf", 
            m_configuration.bestVertexChi2PerNdfBins(),
            m_configuration.bestVertexChi2PerNdfRangeMin(),
            m_configuration.bestVertexChi2PerNdfRangeMax()
        );
        h_bestVertexChi2PerNdf->SetXTitle("#chi^{2}/ndf");
    }

    if(m_configuration.plotAngleCorrelation()) {

        h_angleCorrelation = new TH2D("h_angleCorrelation", "angle correlation",
            m_configuration.angleCorrelationEThetaBins(),
            m_configuration.angleCorrelationEThetaRangeMin(),
            m_configuration.angleCorrelationEThetaRangeMax(),
            m_configuration.angleCorrelationMuThetaBins(),
            m_configuration.angleCorrelationMuThetaRangeMin(),
            m_configuration.angleCorrelationMuThetaRangeMax()
        );
        h_angleCorrelation->SetXTitle("#theta_{e} [mrad]");
        h_angleCorrelation->SetYTitle("#theta_{#mu} [mrad]");

        auto cuts = m_configuration.angleCorrelationAdditionalChi2PerNdfCuts();
        if(!cuts.empty()) {

            for(auto c : cuts) {

                h_angleCorrelationAdditionalChi2PerNdfCuts.push_back(
                    new TH2D(
                        ("h_angleCorrelation" + std::to_string(c)).c_str(), ("angle correlation, #chi^{2}/ndf < " + std::to_string(c)).c_str(),
                        m_configuration.angleCorrelationEThetaBins(),
                        m_configuration.angleCorrelationEThetaRangeMin(),
                        m_configuration.angleCorrelationEThetaRangeMax(),
                        m_configuration.angleCorrelationMuThetaBins(),
                        m_configuration.angleCorrelationMuThetaRangeMin(),
                        m_configuration.angleCorrelationMuThetaRangeMax()
                    )
                );
                h_angleCorrelationAdditionalChi2PerNdfCuts.back()->SetXTitle("#theta_{e} [mrad]");
                h_angleCorrelationAdditionalChi2PerNdfCuts.back()->SetYTitle("#theta_{#mu} [mrad]");                
            }
        }
    }
}

void MUonEAnalysisBasicPlots::saveHistograms() {

    if(!m_configuration.isActive())
        return;

	FairRootManager* ioman = FairRootManager::Instance();

	if(!ioman) 
	{ 
		LOG(fatal) << "No FairRootManager"; 
		return;
	} 

    auto sink = ioman->GetSink();

    sink->WriteObject(h_bestVertexChi2PerNdf, "bestVertexChi2PerNdf");
    sink->WriteObject(h_angleCorrelation, "angleCorrelation");
    for(auto h : h_angleCorrelationAdditionalChi2PerNdfCuts)
        sink->WriteObject(h, h->GetTitle());

}

ClassImp(MUonEAnalysisBasicPlots)
