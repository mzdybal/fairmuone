#ifndef MUONEANALYSISBASICPLOTS_H
#define MUONEANALYSISBASICPLOTS_H

#include "TClonesArray.h"
#include "FairTask.h"
#include "Rtypes.h"

#include <vector>

#include "TH1D.h"
#include "TH2D.h"

#include "MUonEAnalysisBasicPlotsConfiguration.h"

#include "MUonECalorimeterPoint.h"
#include "MUonETrackerPoint.h"
#include "MUonECalorimeterDigiDeposit.h"
#include "MUonETrackerStub.h"
#include "MUonERecoOutput.h"

class MUonEAnalysisBasicPlots : public FairTask {

public:

    MUonEAnalysisBasicPlots();
    virtual ~MUonEAnalysisBasicPlots();

	Bool_t setConfiguration(MUonEAnalysisBasicPlotsConfiguration const& config);

	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

    virtual InitStatus ReInit();
    virtual InitStatus Init();
    virtual void Exec(Option_t* option);
    virtual void Finish();


private:

    void setupHistograms();
    void saveHistograms();

    Bool_t m_configuredCorrectly{false};

    MUonEAnalysisBasicPlotsConfiguration m_configuration;

    Bool_t m_trackerPointsFound{false};
    const TClonesArray* m_trackerPoints{nullptr};

    Bool_t m_calorimeterPointsFound{false};
    const TClonesArray* m_calorimeterPoints{nullptr};

    Bool_t m_stubsFound{false};
    const TClonesArray* m_stubs{nullptr};

    Bool_t m_calorimeterDepositFound{false};
    const MUonECalorimeterDigiDeposit* m_calorimeterDeposit{nullptr};

    Bool_t m_reconstructionOutputFound{false};
    const MUonERecoOutput* m_reconstructionOutput{nullptr};


    TH1D* h_bestVertexChi2PerNdf{nullptr};
    TH2D* h_angleCorrelation{nullptr};
    std::vector<TH2D*> h_angleCorrelationAdditionalChi2PerNdfCuts;




    ClassDef(MUonEAnalysisBasicPlots,2)


};


#endif //MUONEANALYSISBASICPLOTS_H


