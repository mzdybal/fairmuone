#ifndef MUONEALIGNMENTCONTAINER_H
#define MUONEALIGNMENTCONTAINER_H

#include "Rtypes.h"
#include <vector>
#include <yaml-cpp/yaml.h>
#include "TString.h"

class MUonEModuleAlignmentParameters {

public:

    MUonEModuleAlignmentParameters() = default;
    MUonEModuleAlignmentParameters(Double_t xOffset, Double_t yOffset, Double_t zOffset, Double_t angleOffset, Double_t tiltOffset)
        : m_xOffset(xOffset), m_yOffset(yOffset), m_zOffset(zOffset), m_angleOffset(angleOffset), m_tiltOffset(tiltOffset)
        {}

    Double_t xOffset() const {return m_xOffset;}
    Double_t yOffset() const {return m_yOffset;}
    Double_t zOffset() const {return m_zOffset;}
    Double_t angleOffset() const {return m_angleOffset;}
    Double_t tiltOffset() const {return m_tiltOffset;}

    void setXOffset(Double_t offset) {m_xOffset = offset;}
    void setYOffset(Double_t offset) {m_yOffset = offset;}
    void setZOffset(Double_t offset) {m_zOffset = offset;}
    void setAngleOffset(Double_t offset) {m_angleOffset = offset;}
    void setTiltOffset(Double_t offset) {m_tiltOffset = offset;}

private:

    Double_t m_xOffset{0};
    Double_t m_yOffset{0};
    Double_t m_zOffset{0};
    Double_t m_angleOffset{0};
    Double_t m_tiltOffset{0};

    ClassDef(MUonEModuleAlignmentParameters, 2)

};


class MUonEAlignmentContainer {

public:

    MUonEAlignmentContainer() = default;
    MUonEAlignmentContainer(std::vector<std::vector<MUonEModuleAlignmentParameters>> const& stations)
        : m_stations(stations)
        {}

    std::vector<std::vector<MUonEModuleAlignmentParameters>> const& stations() const {return m_stations;}
    MUonEModuleAlignmentParameters const& moduleAlignment(Int_t station, Int_t module) const {return m_stations[station][module];}

    void setAligment(std::vector<std::vector<MUonEModuleAlignmentParameters>> const& stations) {m_stations = stations;}

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t readConfiguration(TString config_name);

    void clearConfiguration() {m_stations.clear();};

    void logCurrentConfiguration() const;
    void saveCurrentConfiguration(std::string file) const;

private:

    std::vector<std::vector<MUonEModuleAlignmentParameters>> m_stations;

    Bool_t m_configuredCorrectly{false};


    Bool_t loadConfigFile(TString config_name, YAML::Node& config);

    ClassDef(MUonEAlignmentContainer, 2)

};


#endif //MUONEALIGNMENTCONTAINER_H

