#include <fairlogger/Logger.h>

#include "MUonEAlignmentContainer.h"


#include <fstream>

Bool_t MUonEAlignmentContainer::readConfiguration(TString config_name) {

    clearConfiguration();

    m_configuredCorrectly = true;

    LOG(info) << "";
    LOG(info) << "Reading alignment data: " << config_name.Data();

    YAML::Node config;

    if(!loadConfigFile(config_name, config))
        return false;

    m_stations.resize(config.size());

    for(Int_t si = 0; si < config.size(); ++si) {

        auto const& station = config[si];

        m_stations[si].reserve(station.size());

        for(auto const& mod : station) {

            MUonEModuleAlignmentParameters tmp;

            if(mod["xOffset"]) tmp.setXOffset(mod["xOffset"].as<Double_t>());
            if(mod["yOffset"]) tmp.setYOffset(mod["yOffset"].as<Double_t>());
            if(mod["zOffset"]) tmp.setZOffset(mod["zOffset"].as<Double_t>());
            if(mod["angleOffset"]) tmp.setAngleOffset(mod["angleOffset"].as<Double_t>());
            if(mod["tiltOffset"]) tmp.setTiltOffset(mod["tiltOffset"].as<Double_t>());
            
            m_stations[si].emplace_back(tmp);
        }
    }

    if(m_configuredCorrectly)
        LOG(info) << "Alignment data read successfully.";
    else
        LOG(info) << "Alignment data contains errors. Please fix them and try again.";

    return m_configuredCorrectly;        
}

void MUonEAlignmentContainer::logCurrentConfiguration() const {


    LOG(info) << "";
    LOG(info) << "";
    LOG(info) << "Alignment configuration: ";
    LOG(info) << "";
    LOG(info) << "";
        
    for(Int_t si = 0; si < m_stations.size(); ++si) {

        auto const& st = m_stations[si];

        LOG(info) << "STATION " << si;

        for(Int_t mi = 0; mi < st.size(); ++mi) {

            auto const& mo = st[mi];

            LOG(info) << "MODULE " << mi;

            LOG(info) << "xOffset " << mo.xOffset();
            LOG(info) << "yOffset " << mo.yOffset();
            LOG(info) << "zOffset " << mo.zOffset();
            LOG(info) << "angleOffset " << mo.angleOffset();
            LOG(info) << "tiltOffset " << mo.tiltOffset();
        }
    }
}

void MUonEAlignmentContainer::saveCurrentConfiguration(std::string file) const {


	YAML::Node main_node;


    for(auto const& s : m_stations) {

        YAML::Node station_node;

        for(auto const& m : s) {

            YAML::Node module_node;
			module_node["xOffset"] = m.xOffset();
			module_node["yOffset"] = m.yOffset();
            module_node["zOffset"] = m.zOffset();
			module_node["angleOffset"] = m.angleOffset();                
			module_node["tiltOffset"] = m.tiltOffset();                
            
            station_node.push_back(module_node);
        }

        main_node.push_back(station_node);
    }

    std::ofstream fout(file);
    YAML::Emitter out;
    out << main_node;
    fout << out.c_str();
    fout.close();        
}


Bool_t MUonEAlignmentContainer::loadConfigFile(TString config_name, YAML::Node& config) {

    if(!config_name.EndsWith(".yaml") && !config_name.EndsWith(".YAML"))
        config_name.Append(".yaml");

    try {

        config = YAML::LoadFile(config_name.Data());
        LOG(info) << "Alignment file found in the path provided.";

    } catch(...) {

        TString jobs_dir = getenv("ALIGNMENTPATH");
        jobs_dir.ReplaceAll("//", "/"); //taken from FR examples, not sure if necessary
        if(!jobs_dir.EndsWith("/"))
            jobs_dir.Append("/");  

        TString full_path = jobs_dir + config_name;

        LOG(info) << "Alignment file in the given path not found or failed to open. Trying in ALIGNMENTPATH directory (" << full_path << ").";

        try {

            config = YAML::LoadFile(full_path.Data());
            LOG(info) << "File found.";

        } catch(...) {

            LOG(fatal) << "Failed to load alignment data.";
            return false;
        }
    }   

    return true; 
}


ClassImp(MUonEModuleAlignmentParameters)
ClassImp(MUonEAlignmentContainer)
