#include "MUonEAlignmentConfiguration.h"

#include <fairlogger/Logger.h>


MUonEAlignmentConfiguration::MUonEAlignmentConfiguration()
{
    resetDefaultConfiguration();
}

Bool_t MUonEAlignmentConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    LOG(info) << "config found";


    if(config["simplified"])
        m_simplified = config["simplified"].as<Bool_t>();
    else
        LOG(info) << "Variable 'simplified' not set. Simplified alignment will be used by default.";

    if(config["maxTrackChi2PerNdf"])
        m_maxTrackChi2PerNdf = config["maxTrackChi2PerNdf"].as<Double_t>();
    else
        LOG(info) << "Variable 'maxTrackChi2PerNdf' not set, default is -1 (no cut).";

    if(config["minNumberOfStereoHits"])
        m_minNumberOfStereoHits = config["minNumberOfStereoHits"].as<Int_t>();
    else
        LOG(info) << "Variable 'minNumberOfStereoHits' not set, default is 2.";

    if(config["useSeedingSensorOnly"])
        m_useSeedingSensorOnly = config["useSeedingSensorOnly"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'useSeedingSensorOnly' not set. Defaulting to false.";
    }    


    if(config["alignXY"])
        m_alignXY = config["alignXY"].as<Bool_t>();
    else
        LOG(info) << "Variable 'alignXY' not set, default is true.";        

    if(config["alignZ"])
        m_alignZ = config["alignZ"].as<Bool_t>();
    else
        LOG(info) << "Variable 'alignZ' not set, default is false.";    

    if(config["alignAngle"])
        m_alignAngle = config["alignAngle"].as<Bool_t>();
    else
        LOG(info) << "Variable 'alignAngle' not set, default is true.";        

    if(config["alignTilt"])
        m_alignTilt = config["alignTilt"].as<Bool_t>();
    else
        LOG(info) << "Variable 'alignTilt' not set, default is false.";  

    if(config["freezePerpendicularOffsetForXYModules"])
        m_freezePerpendicularOffsetForXYModules = config["freezePerpendicularOffsetForXYModules"].as<Bool_t>();
    else
        LOG(info) << "Variable 'freezePerpendicularOffsetForXYModules' not set, default is true.";       


    if(config["alignFullStation"])
        m_alignFullStation = config["alignFullStation"].as<Int_t>();
    else
        LOG(info) << "Variable 'alignFullStation' not set, default is -1 (disabled, perform normal alignment instead).";


    if(config["minimizerMaxFunctionCalls"])
        m_minimizerMaxFunctionCalls = config["minimizerMaxFunctionCalls"].as<Int_t>();
    else
        LOG(info) << "Variable 'minimizerMaxFunctionCalls' not set, default is 1000000.";       

    if(config["minimizerTolerance"])
        m_minimizerTolerance = config["minimizerTolerance"].as<Double_t>();
    else
        LOG(info) << "Variable 'minimizerTolerance' not set, default is 0.01.";       

    if(config["minimizerPrintLevel"])
        m_minimizerPrintLevel = config["minimizerPrintLevel"].as<Int_t>();
    else
        LOG(info) << "Variable 'minimizerPrintLevel' not set, default is 1.";       




    if(config["initialAlignmentFile"]) {

        m_initialAlignmentFile = config["initialAlignmentFile"].as<std::string>();
        m_initialAlignmentSet = true;
    }
    else {
        m_initialAlignmentSet = false;
    }                    

    if(!m_configuredCorrectly)
        LOG(info) << "Alignment configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}


void MUonEAlignmentConfiguration::logCurrentConfiguration() const {

    if(m_simplified)
        LOG(info) << "Running simplified alignment in XY plane.";
    else
        LOG(info) << "Running full alignment.";

    if(m_maxTrackChi2PerNdf > 0)
        LOG(info) << "Only tracks with chi2/ndf < " << m_maxTrackChi2PerNdf << " will be used.";

    LOG(info) << "minNumberOfStereoHits: " << m_minNumberOfStereoHits;

    LOG(info) << "useSeedingSensorOnly: " << m_useSeedingSensorOnly;


    LOG(info) << "alignXY: " << m_alignXY;

    LOG(info) << "freezePerpendicularOffsetForXYModules: " << m_freezePerpendicularOffsetForXYModules;

    LOG(info) << "alignFullStation: " << m_alignFullStation;


    LOG(info) << "minimizerMaxFunctionCalls: " << m_minimizerMaxFunctionCalls;
    LOG(info) << "minimizerTolerance: " << m_minimizerTolerance;
    LOG(info) << "minimizerPrintLevel: " << m_minimizerPrintLevel;


    if(m_initialAlignmentSet)
        LOG(info) << "Alignment file " << m_initialAlignmentFile << " will be used as base.";


}


void MUonEAlignmentConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false; 

    m_simplified = true;
    m_maxTrackChi2PerNdf = -1;
    m_minNumberOfStereoHits = 2;

    m_useSeedingSensorOnly = false;

    m_alignXY = true;
    m_alignZ = false;
    m_alignAngle = true;
    m_alignTilt = false;

    m_freezePerpendicularOffsetForXYModules = true;

    m_alignFullStation = -1;


    m_minimizerMaxFunctionCalls = 1000000;
    m_minimizerTolerance = 0.01;
    m_minimizerPrintLevel = 1;


    m_initialAlignmentSet = false;
    m_initialAlignmentFile.clear();
}


ClassImp(MUonEAlignmentConfiguration)