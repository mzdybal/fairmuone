#ifndef MUONEALIGNMENTCONFIGURATION_H
#define MUONEALIGNMENTCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>
#include <vector>

class MUonEAlignmentConfiguration {

public:

    MUonEAlignmentConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t simplified() const {return m_simplified;}
    Double_t maxTrackChi2PerNdf() const {return m_maxTrackChi2PerNdf;}
    Int_t minNumberOfStereoHits() const {return m_minNumberOfStereoHits;}

    Bool_t useSeedingSensorOnly() const {return m_useSeedingSensorOnly;}


    Bool_t alignXY() const {return m_alignXY;}
    Bool_t alignZ() const {return m_alignZ;}
    Bool_t alignAngle() const {return m_alignAngle;}
    Bool_t alignTilt() const {return m_alignTilt;}

    Bool_t freezePerpendicularOffsetForXYModules() const {return m_freezePerpendicularOffsetForXYModules;}

    Int_t alignFullStation() const {return m_alignFullStation;}

    Int_t minimizerMaxFunctionCalls() const {return m_minimizerMaxFunctionCalls;}
    Double_t minimizerTolerance() const {return m_minimizerTolerance;}
    Int_t minimizerPrintLevel() const {return m_minimizerPrintLevel;}

    Bool_t initialAlignmentSet() const {return m_initialAlignmentSet;}
    std::string const& initialAlignmentFile() const {return m_initialAlignmentFile;}

    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};


    Bool_t m_simplified{true};
    Double_t m_maxTrackChi2PerNdf{-1};
    Int_t m_minNumberOfStereoHits{2};

    
    Bool_t m_useSeedingSensorOnly{false};

    //relevant only to simplified alignment

    Bool_t m_alignXY{true};
    Bool_t m_alignZ{false};
    Bool_t m_alignAngle{true};
    Bool_t m_alignTilt{false};

    Bool_t m_freezePerpendicularOffsetForXYModules{true};


    //full station alignment
    Int_t m_alignFullStation{-1};

    Int_t m_minimizerMaxFunctionCalls{1000000};
    Double_t m_minimizerTolerance{0.01};
    Int_t m_minimizerPrintLevel{1};
    ////////////////////////////////////////

    //relevant only to global alignment
    

    ////////////////////////////////////////

    Bool_t m_initialAlignmentSet{false};
    std::string m_initialAlignmentFile;



    ClassDef(MUonEAlignmentConfiguration, 2)
};


#endif //MUONEALIGNMENTCONFIGURATION_H

