#ifndef MUONEALIGNMENT_H
#define MUONEALIGNMENT_H

#include "TClonesArray.h"
#include "FairTask.h"
#include "Rtypes.h"

#include <vector>
#include <string>
#include <unordered_map>

#include "TH1D.h"
#include "TH2D.h"

#include "MUonEDetectorConfiguration.h"
#include "MUonEAlignmentConfiguration.h"
#include "MUonERecoOutput.h"
#include "MUonEAlignmentModule.h"

class MUonEAlignment : public FairTask {

public:

    MUonEAlignment();
    virtual ~MUonEAlignment();

	Bool_t setConfiguration(MUonEDetectorConfiguration const& detectorConfig, MUonEAlignmentConfiguration const& alignmentConfig);

	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

    virtual InitStatus ReInit();
    virtual InitStatus Init();
    virtual void Exec(Option_t* option);
    virtual void Finish();

    void setOutputName(std::string name) {m_outputName = name;}


private:

    Bool_t m_configuredCorrectly{false};

    MUonEDetectorConfiguration m_detectorConfiguration;
    MUonEAlignmentConfiguration m_alignmentConfiguration;

    const MUonERecoOutput* m_reconstructionOutput{nullptr};

    //indexed by stationID and moduleID
    std::vector<std::vector<MUonEAlignmentModule>> m_modules;
    std::vector<MUonERecoOutputTrack> m_goodTracks;

    //used for full station alignment
    std::vector<std::pair<MUonERecoOutputTrack, MUonERecoOutputTrack>> m_goodTracksStationAlignment;

    std::string m_outputName;


    void saveAlignmentToFile();

    ClassDef(MUonEAlignment,2)

};


#endif //MUONEALIGNMENT_H


