#ifndef MUONEALIGNMENTMODULE_H
#define MUONEALIGNMENTMODULE_H

#include "MUonERecoModule.h"
#include <vector>
#include <utility>

//most of the needed stuff is already implemented in the module class used for reconstruction
class MUonEAlignmentModule : public MUonERecoModule {

public:

    MUonEAlignmentModule() = default;

    MUonEAlignmentModule(Int_t stationID, Int_t moduleID, Double_t z, MUonEDetectorStationModuleConfiguration const& moduleConfig)
        : MUonERecoModule(-1 /*index not needed*/, -1 /*sector not needed*/, stationID, moduleID, z, false /*alignment only used on data, isMC = false*/, moduleConfig)
        {}

    std::vector<std::pair<Int_t, Int_t>> const& linkedTracks() const {return m_linkedTracks;}

    void addLinkedTrack(Int_t trackIndex, Int_t hitIndex) {m_linkedTracks.emplace_back(std::make_pair(trackIndex, hitIndex));}


    void setInitialAlignmentParameters(Double_t xOffset, Double_t yOffset, Double_t zOffset, Double_t angleOffset, Double_t tiltOffset) {

        setAlignmentParameters(xOffset, yOffset, zOffset, angleOffset, tiltOffset);

        m_initialXOffset = xOffset;
        m_initialYOffset = yOffset;
        m_initialZOffset = zOffset;
        m_initialAngleOffset = angleOffset;
        m_initialTiltOffset = tiltOffset;
    }

    void updateAlignmentParameters(Double_t xOffset, Double_t yOffset, Double_t zOffset, Double_t angleOffset, Double_t tiltOffset) {

		setAlignmentParameters(m_initialXOffset + xOffset, m_initialYOffset + yOffset, m_initialZOffset + zOffset, m_initialAngleOffset + angleOffset, m_initialTiltOffset + tiltOffset);		
	}

    void resetInitialAlignmentParameters() {

        setAlignmentParameters(m_initialXOffset, m_initialYOffset, m_initialZOffset, m_initialAngleOffset, m_initialTiltOffset);
    }

    void setAlignmentParametersFromFullValues(Double_t x, Double_t y, Double_t z, Double_t angle, Double_t tilt) {

        setAlignmentParameters(x, y, z - m_z, angle - m_angle, tilt - m_tilt);
    }

    void setAlignmentStationPositionOffsets(Double_t x, Double_t y, Double_t z) {

        setAlignmentParameters(x, y, z, m_angleOffset, m_tiltOffset);
    }

    void makeCurrentAlignmentParametersInitial() {

        setInitialAlignmentParameters(m_xOffset, m_yOffset, m_zOffset, m_angleOffset, m_tiltOffset);
    }

private:

    //tracks that have a hit in this module (can be used to align it)
    //holding index in the global "good" tracks container and index of hit in the track
    std::vector<std::pair<Int_t, Int_t>> m_linkedTracks;
    

    //used in simplified alignment to exclude the two first xy modules, which are used as a reference for other positions
    Bool_t m_isReferenceModule{false};

    Double_t m_initialXOffset{0};
    Double_t m_initialYOffset{0};
    Double_t m_initialZOffset{0};
    Double_t m_initialAngleOffset{0};
    Double_t m_initialTiltOffset{0};

};

#endif//MUONEALIGNMENTMODULE_H

