#include "MUonEAlignment.h"
#include "MUonEAlignmentContainer.h"

#include "FairRootManager.h"

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

#include <functional>
#include <fstream>

#include <yaml-cpp/yaml.h>

MUonEAlignment::MUonEAlignment()
	: FairTask("MUonEAlignment", 0)
	{
		resetDefaultConfiguration();
	}


MUonEAlignment::~MUonEAlignment()
{
}

Bool_t MUonEAlignment::setConfiguration(MUonEDetectorConfiguration const& detectorConfig, MUonEAlignmentConfiguration const& alignmentConfig) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;

	LOG(info) << "";
	LOG(info) << "Creating Alignment object.";


	m_detectorConfiguration = detectorConfig;
	m_alignmentConfiguration = alignmentConfig;
	
	m_configuredCorrectly = m_detectorConfiguration.configuredCorrectly() & m_alignmentConfiguration.configuredCorrectly();

    if(m_configuredCorrectly)
        LOG(info) << "Alignment object created successfully.";
    else
        LOG(info) << "Alignment configuration contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}

void MUonEAlignment::logCurrentConfiguration() {

	m_alignmentConfiguration.logCurrentConfiguration();
}

void MUonEAlignment::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_detectorConfiguration.resetDefaultConfiguration();
	m_alignmentConfiguration.resetDefaultConfiguration();

}


InitStatus MUonEAlignment::ReInit()
{
	return kSUCCESS;
}

InitStatus MUonEAlignment::Init()
{
	LOG(info) << "Initializing alignment task.";

	FairRootManager* ioman = FairRootManager::Instance();

	if(!ioman) 
	{ 
		LOG(fatal) << "No FairRootManager"; 
		return kERROR;
	} 


    m_reconstructionOutput = ioman->InitObjectAs<const MUonERecoOutput*>("ReconstructionOutput");

    if(nullptr == m_reconstructionOutput) {

        LOG(error) << "ReconstructionOutput not found in the ntuple.";
        return kERROR;
    }

	MUonEAlignmentContainer initial_alignment;

	if(m_alignmentConfiguration.initialAlignmentSet()) {
			
		if(!initial_alignment.readConfiguration(m_alignmentConfiguration.initialAlignmentFile()))
			return kERROR;
	}

	//load modules from the detector configuration
	//stations and modules are assumed to be sorted by their z position
	auto const& stations = m_detectorConfiguration.stations();
	m_modules.resize(stations.size());

	for(Int_t station_index = 0; station_index < stations.size(); ++station_index) {

		auto const& station = stations[station_index];
		auto const& station_modules = station.modules();

		m_modules[station_index].reserve(station_modules.size());


		for(Int_t module_index = 0; module_index < station_modules.size(); ++module_index) {

			auto const& module = station_modules[module_index];
			Double_t module_position = station.modulePosition(module);

			m_modules[station_index].emplace_back(station_index, module_index, module_position, module);

			if(m_alignmentConfiguration.initialAlignmentSet()) {

				auto const& module_alignment = initial_alignment.moduleAlignment(station_index, module_index);
				m_modules[station_index].back().setInitialAlignmentParameters(module_alignment.xOffset(), module_alignment.yOffset(), module_alignment.zOffset(), module_alignment.angleOffset(), module_alignment.tiltOffset());
			}
		}
	}

	if(m_alignmentConfiguration.alignFullStation() > 0)
		m_goodTracksStationAlignment.reserve(200000);
	else
		m_goodTracks.reserve(200000);

	return kSUCCESS;
}

void MUonEAlignment::Exec(Option_t* option) 
{


	if(m_alignmentConfiguration.alignFullStation() > 0) {


		if(m_reconstructionOutput->reconstructedTracksMultiplicity() < 2)
			return;

		Int_t station_to_align = m_alignmentConfiguration.alignFullStation();

		auto const& reconstructed_tracks = m_reconstructionOutput->reconstructedTracks();
		Int_t intrack = -1;
		Int_t outtrack = -1;

		for(Int_t i = 0; i < reconstructed_tracks.size(); ++i) {

			auto const& t = reconstructed_tracks[i];

			if(t.sector() == station_to_align - 1) {

				if(intrack >= 0) //more than one track in this station
					return;
				else
					intrack = i;
			}
			else if(t.sector() == station_to_align) {

				if(outtrack >= 0) //more than one track in this station
					return;
				else
					outtrack = i;
			}
		}

		if(intrack >= 0 && outtrack >= 0)
			m_goodTracksStationAlignment.emplace_back(std::make_pair(reconstructed_tracks[intrack], reconstructed_tracks[outtrack]));
		

	} else {

		//collect good tracks from all events
		auto const& tracks = m_reconstructionOutput->reconstructedTracks();

		auto trackIsGood = [this](MUonERecoOutputTrack const& track) {

			if(track.degreesOfFreedom() == 0) return false;
			if(track.numberOfStereoHits() < m_alignmentConfiguration.minNumberOfStereoHits()) return false;

			if(m_alignmentConfiguration.maxTrackChi2PerNdf() > 0) {

				return track.chi2perDegreeOfFreedom() < m_alignmentConfiguration.maxTrackChi2PerNdf();
			} else {

				return true;
			}
		};

		for(auto const& track : tracks) {

			if(trackIsGood(track)) {

				auto const& hits = track.hits();

				for(Int_t hit_index = 0; hit_index < hits.size(); ++hit_index) {

					auto const& hit = hits[hit_index];
					m_modules[hit.stationID()][hit.moduleID()].addLinkedTrack(m_goodTracks.size(), hit_index);
				}

				m_goodTracks.emplace_back(track);

				if(0 == m_goodTracks.size() % 10000)
					std::cout << m_goodTracks.size() << " good tracks collected" << std::endl;
			}
		}
	}
}

void MUonEAlignment::Finish() 
{

	//perform fits to all tracks and final alignment steps
	if(m_alignmentConfiguration.simplified() && m_alignmentConfiguration.alignFullStation() < 1) {

		LOG(info) << m_goodTracks.size() << " good tracks collected in total.";

		//initialize minimizer
   		ROOT::Math::Minimizer* minimizer = ROOT::Math::Factory::CreateMinimizer("Minuit2", "");

		//this will have to be tweaked in the future
		minimizer->SetMaxFunctionCalls(m_alignmentConfiguration.minimizerMaxFunctionCalls());
   		minimizer->SetTolerance(m_alignmentConfiguration.minimizerTolerance());
   		minimizer->SetPrintLevel(m_alignmentConfiguration.minimizerPrintLevel());

		//set variables (x offset, y offset, angle offset)
		minimizer->SetVariable(0, "x_offset", 0, 0.001); //index, name, starting value, step
		minimizer->SetVariable(1, "y_offset", 0, 0.001);
		minimizer->SetVariable(2, "z_offset", 0, 0.001);
		minimizer->SetVariable(3, "angle_offset", 0, 0.001);
		minimizer->SetVariable(4, "tilt_offset", 0, 0.001);


		//method to calculate chi2 for a given module, but we need to strip it from all unnecessary arguments, because ROOT...
		std::function<Double_t(const Double_t*, MUonEAlignmentModule&, std::vector<MUonERecoOutputTrack> const&, Bool_t, Bool_t)> functionToMinimizeBase = [](const Double_t* arguments, MUonEAlignmentModule& mod, std::vector<MUonERecoOutputTrack> const& goodTracks, Bool_t initialAlignmentSet, Bool_t useSeedingSensorOnly)->Double_t {

			auto const& linked_tracks = mod.linkedTracks();

			//set current fit parameters in the given module
			//they will be used for all calculations involving this module

			if(initialAlignmentSet)
				mod.updateAlignmentParameters(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
			else
				mod.setAlignmentParameters(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);

			Double_t chi2 = 0;
			//loop on all good tracks that have a hit in this module
			//consider only distance of hits in this module
			for(auto const& index_pair : linked_tracks) {

				auto const& track = goodTracks[index_pair.first];
				auto const& hit = track.hits()[index_pair.second];

				auto const xAtZ = [&track](Double_t z){return track.xSlope()*(z - track.z0()) + track.x0();};
				auto const yAtZ = [&track](Double_t z){return track.ySlope()*(z - track.z0()) + track.y0();};


				//recalculate positions with tilt
				Double_t hit_z = -99999;
				Double_t hit_pos = -99999;

				if(useSeedingSensorOnly) {

					Double_t sensor_shift = 0.5 * mod.distanceBetweenSensors();
					if(0 == mod.seedSensor())
						sensor_shift *= -1;

					//additional terms, because both sensors are rotated wrt. module's midpoint
					hit_z = mod.z() + sensor_shift * mod.tiltCosine() - hit.position() * mod.tiltSine();
					hit_pos = sensor_shift * mod.tiltSine() + hit.position() * mod.tiltCosine();


				} else {

					hit_z = mod.z() - hit.position() * mod.tiltSine();
					hit_pos = hit.position() * mod.tiltCosine();
				}

				auto chi2_sqrt = fabs(mod.localCoordinatePerpendicular(xAtZ(hit_z), yAtZ(hit_z)) - hit_pos) / hit.positionError();
				chi2 += chi2_sqrt * chi2_sqrt;
	
			}

			return chi2;
		};

		//only arguments and module at this point
		auto functionToMinimizeGeneral = std::bind(functionToMinimizeBase, std::placeholders::_1, std::placeholders::_2, m_goodTracks, m_alignmentConfiguration.initialAlignmentSet(), m_alignmentConfiguration.useSeedingSensorOnly());


		for(Int_t station_index = 0; station_index < m_modules.size(); ++station_index) {
			
			auto& modules = m_modules[station_index];
			
			for(Int_t module_index = 0; module_index < modules.size(); ++module_index) {

				auto& mod = modules[module_index];

				LOG(info) << "Performing alignment for module " << module_index << " in station " << station_index << " (" << mod.projection() << ")" << " using " << mod.linkedTracks().size() << " tracks.";

				//only arguments now, module to align is fixed, function is ready to pass to a minimizer
				auto functionToMinimize = std::bind(functionToMinimizeGeneral, std::placeholders::_1, mod);

				//format ROOT likes
				ROOT::Math::Functor f(functionToMinimize,5);

				minimizer->SetFunction(f);

				//release all variables
				minimizer->ReleaseVariable(0);
				minimizer->ReleaseVariable(1);
				minimizer->ReleaseVariable(2);
				minimizer->ReleaseVariable(3);
				minimizer->ReleaseVariable(4);

				//reset values
				minimizer->SetVariableValue(0, 0);
				minimizer->SetVariableValue(1, 0);
				minimizer->SetVariableValue(2, 0);
				minimizer->SetVariableValue(3, 0);
				minimizer->SetVariableValue(4, 0);

				if(!m_alignmentConfiguration.alignXY()) {

					minimizer->FixVariable(0);
					minimizer->FixVariable(1);

				} else if(m_alignmentConfiguration.freezePerpendicularOffsetForXYModules()){

					//fix non-measurable variable for x and y modules
					if('x' == mod.projection())
						minimizer->FixVariable(1);
					else if('y' == mod.projection())
						minimizer->FixVariable(0);
				}

				if(!m_alignmentConfiguration.alignZ())
					minimizer->FixVariable(2);

				if(!m_alignmentConfiguration.alignAngle())
					minimizer->FixVariable(3);

				if(!m_alignmentConfiguration.alignTilt())
					minimizer->FixVariable(4);


				
				if(minimizer->Minimize()) {

					const double *xx = minimizer->X();
					if(m_alignmentConfiguration.initialAlignmentSet())
						mod.updateAlignmentParameters(xx[0], xx[1], xx[2], xx[3], xx[4]);
					else
						mod.setAlignmentParameters(xx[0], xx[1], xx[2], xx[3], xx[4]);
					LOG(info) << "minimum: " << minimizer->MinValue() << ", values: " << xx[0] << ", " << xx[1] << ", " << xx[2] << ", " << xx[3] << ", " << xx[4];	

				} else {

					if(m_alignmentConfiguration.initialAlignmentSet())
						mod.resetInitialAlignmentParameters();
					else
						mod.setAlignmentParameters(0, 0, 0, 0, 0);
					LOG(info) << "Minimization failed. Assuming 0 offsets.";
				}
			}
		}

		saveAlignmentToFile();
	
	
	} else if(m_alignmentConfiguration.simplified() && m_alignmentConfiguration.alignFullStation() > 0) {



		LOG(info) << m_goodTracksStationAlignment.size() << " good tracks collected in total.";


		//minimize angles

		//initialize minimizer
   		ROOT::Math::Minimizer* minimizer = ROOT::Math::Factory::CreateMinimizer("Minuit2", "");

		//this will have to be tweaked in the future
		minimizer->SetMaxFunctionCalls(m_alignmentConfiguration.minimizerMaxFunctionCalls());
   		minimizer->SetTolerance(m_alignmentConfiguration.minimizerTolerance());
   		minimizer->SetPrintLevel(m_alignmentConfiguration.minimizerPrintLevel());


		minimizer->SetVariable(0, "station_alpha_offset", 0, 0.01);
		minimizer->SetVariable(1, "station_beta_offset", 0, 0.01);
		minimizer->SetVariable(2, "station_gamma_offset", 0, 0.01);

		std::function<Double_t(const Double_t*, std::vector<std::vector<MUonEAlignmentModule>>&, std::vector<MUonEAlignmentModule>&, std::vector<std::pair<MUonERecoOutputTrack, MUonERecoOutputTrack>> const&, Double_t, Bool_t)> functionToMinimizeBase = [](const Double_t* arguments, std::vector<std::vector<MUonEAlignmentModule>>& modules, std::vector<MUonEAlignmentModule>& modulesInStation, std::vector<std::pair<MUonERecoOutputTrack, MUonERecoOutputTrack>> const& goodTracks, Double_t targetZ, Bool_t useSeedingSensorOnly)->Double_t {

			//apply full station transformations to each module
			TRotation rotation;
			//rotation.RotateXEulerAngles(arguments[0], arguments[1], arguments[2]);
			rotation.RotateX(arguments[0]);
			rotation.RotateY(arguments[1]);
			rotation.RotateZ(arguments[2]);
		
			//std::cout << "it" << std::endl;
			//std::cout << arguments[0] << ", " << arguments[1] << ", " << arguments[2] << ", " << arguments[3] << ", " << arguments[4] << ", " << arguments[5] << std::endl;

			for(auto& m : modulesInStation) {
				
				//always start from the initial alignment
				m.resetInitialAlignmentParameters();

				//measurement direction vector (rotation by tilt around Y + rotation by alpha around Z of (1,0,0))
				TVector3 measurement_direction_vector = (TVector3(m.angleCosine() * m.tiltCosine(), m.angleSine() * m.tiltCosine(), -m.tiltSine())).Unit();

				//vector from the middle of the target to the middle of the module
				TVector3 module_position_vector(m.xOffset(), m.yOffset(), m.z() - targetZ);

				//vector from the middle of the target, to the measurement direction vector tip
				TVector3 module_measurement_direction_tip = module_position_vector + measurement_direction_vector;

				//rotate both
				module_position_vector.Transform(rotation);
				module_measurement_direction_tip.Transform(rotation);

				TVector3 measurement_direction_vector_new = module_measurement_direction_tip - module_position_vector;
				
				//translate back to local degrees of freedom
				Double_t new_tilt = measurement_direction_vector_new.Angle(TVector3(0,0,1)) - M_PI_2;
				Double_t new_alpha = TVector3(measurement_direction_vector_new.x(), measurement_direction_vector_new.y(), 0).Angle(TVector3(1,0,0));
				if(measurement_direction_vector_new.y() < 0) new_alpha *= -1;

				//std::cout << module_position_vector_new.x() << ", " <<  module_position_vector_new.y() << ", " <<  targetZ + module_position_vector_new.z() << ", " <<  new_alpha << ", " <<  new_tilt << std::endl;
				m.setAlignmentParametersFromFullValues(module_position_vector.x(), module_position_vector.y(), targetZ + module_position_vector.z(), new_alpha*TMath::RadToDeg(), new_tilt*TMath::RadToDeg());
			}


				auto trackuvdist = [](std::vector<std::tuple<Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, char>> const& hits)->std::pair<Double_t, Double_t> {


					Int_t first_x = -1;
					Int_t second_x = -1;
					Int_t first_y = -1;
					Int_t second_y = -1;
					Int_t first_uv = -1;
					Int_t second_uv = -1;		

					for(Int_t idx = 0; idx < hits.size(); ++idx) {

						char pr = std::get<7>(hits[idx]);

						if('x' == pr) {

							if(first_x >= 0)
								second_x = idx;
							else
								first_x = idx;

						} else if('y' == pr) {

							if(first_y >= 0)
								second_y = idx;
							else
								first_y = idx;	

						} else {

							if(first_uv >= 0)
								second_uv = idx;
							else
								first_uv = idx;	
						}
					}	

					Double_t xslope = (std::get<1>(hits[second_x]) - std::get<1>(hits[first_x])) / (std::get<0>(hits[second_x]) - std::get<0>(hits[first_x]));
					Double_t yslope = (std::get<1>(hits[second_y]) - std::get<1>(hits[first_y])) / (std::get<0>(hits[second_y]) - std::get<0>(hits[first_y]));

					Double_t x0 = std::get<1>(hits[second_x]) - xslope * std::get<0>(hits[second_x]);
					Double_t y0 = std::get<1>(hits[second_y]) - yslope * std::get<0>(hits[second_y]);


					Double_t d1 = -1;
					Double_t d2 = -1;

					if(first_uv >= 0) {

						Double_t z = std::get<0>(hits[first_uv]);
						Double_t pos = std::get<1>(hits[first_uv]);
						Double_t acos = std::get<3>(hits[first_uv]);
						Double_t asin = std::get<4>(hits[first_uv]);
						Double_t xoff = std::get<5>(hits[first_uv]);
						Double_t yoff = std::get<6>(hits[first_uv]);


						Double_t x = xslope * z + x0;
						Double_t y = yslope * z + y0;

						Double_t proj = (x - xoff)*acos + (y-yoff)*asin;
						d1 = fabs(proj - pos);						
					}
					
					if(second_uv >= 0) {

						Double_t z = std::get<0>(hits[second_uv]);
						Double_t pos = std::get<1>(hits[second_uv]);
						Double_t acos = std::get<3>(hits[second_uv]);
						Double_t asin = std::get<4>(hits[second_uv]);
						Double_t xoff = std::get<5>(hits[second_uv]);
						Double_t yoff = std::get<6>(hits[second_uv]);


						Double_t x = xslope * z + x0;
						Double_t y = yslope * z + y0;

						Double_t proj = (x - xoff)*acos + (y-yoff)*asin;
						d2 = fabs(proj - pos);							
					}

					return std::make_pair(d1, d2);

				};

			
			Double_t chi2 = 0;
			Int_t reconstructible_ctr = 0;

			//refit second track
			for(auto const& t : goodTracks) {

				auto outgoing_hits_tmp = t.second.hits();

				//recalculate their position
				std::vector<std::tuple<Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, char>> outgoing_hits;
				outgoing_hits.reserve(outgoing_hits_tmp.size());

				auto newHitPos = [&useSeedingSensorOnly](MUonERecoOutputHit const& hit, MUonEAlignmentModule const& mod)->std::tuple<Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, char> {

					Double_t hit_z = -99999;
					Double_t hit_pos = -99999;

					if(useSeedingSensorOnly) {

						Double_t sensor_shift = 0.5 * mod.distanceBetweenSensors();
						if(0 == mod.seedSensor())
							sensor_shift *= -1;

						//additional terms, because both sensors are rotated wrt. module's midpoint
						hit_z = mod.z() + sensor_shift * mod.tiltCosine() - hit.position() * mod.tiltSine();
						hit_pos = sensor_shift * mod.tiltSine() + hit.position() * mod.tiltCosine();


					} else {

						hit_z = mod.z() - hit.position() * mod.tiltSine();
						hit_pos = hit.position() * mod.tiltCosine();
					}

					return std::make_tuple(hit_z, hit_pos, hit.positionError(), mod.angleCosine(), mod.angleSine(), mod.xOffset(), mod.yOffset(), hit.projection());

				};

				for(auto const& h : outgoing_hits_tmp)
					outgoing_hits.emplace_back(newHitPos(h, modulesInStation[h.moduleID()]));
									

					

				//refit outgoing track
				Double_t z0 = 0;

				auto lsum = [&outgoing_hits, &z0](Int_t cosine_term_power, Int_t sine_term_power, Int_t z_term_power) {

					Double_t sum = 0;

					for(auto const& hit : outgoing_hits) {

						Double_t z = std::get<0>(hit);
						Double_t pos = std::get<1>(hit);
						Double_t posErr = std::get<2>(hit);
						Double_t acos = std::get<3>(hit);
						Double_t asin = std::get<4>(hit);

						Double_t hit_contribution = 1;
						
						for(Int_t i = 0; i < cosine_term_power; ++i) hit_contribution *= acos;
						for(Int_t i = 0; i < sine_term_power; ++i) hit_contribution *= asin;
						for(Int_t i = 0; i < z_term_power; ++i) hit_contribution *= (z - z0); //z0 fixed above for now
						
						Double_t du2 = posErr*posErr;
						
						sum += hit_contribution/du2;				
					}

					return sum;
				};


				Double_t tmpA[10] = {
										lsum(2,0,0),  
										lsum(1,1,0), lsum(0,2,0), 
										lsum(2,0,1), lsum(1,1,1), lsum(2,0,2), 
										lsum(1,1,1), lsum(0,2,1), lsum(1,1,2), lsum(0,2,2)
									};

				//note MatRepSym, hence not all terms are needed to define the matrix (it's symmetric)
				ROOT::Math::SMatrix<Double_t, 4, 4, ROOT::Math::MatRepSym<Double_t,4>> A(tmpA, 10);

				if(!A.Invert()) //A is now a covariance matrix
					return 1; //failed to invert


				ROOT::Math::SVector<Double_t, 4> B;
				for(auto const& hit : outgoing_hits) {

					Double_t posErr = std::get<2>(hit);
					const Double_t du2_inverse =  1 / (posErr*posErr);
					const Double_t cosa = std::get<3>(hit);
					const Double_t sina = std::get<4>(hit);
					const Double_t dz = std::get<0>(hit) - z0;
					const Double_t ui = std::get<1>(hit);
					const Double_t xOff = std::get<5>(hit);
					const Double_t yOff = std::get<6>(hit);

					//bx
					B(0) += du2_inverse * (ui*cosa + xOff*cosa*cosa + yOff*sina*cosa);
					//by
					B(1) += du2_inverse * (ui*sina + xOff*sina*cosa + yOff*sina*sina);
					//ax
					B(2) += du2_inverse * (ui*dz*cosa + xOff*dz*cosa*cosa + yOff*dz*sina*cosa);
					//ay
					B(3) += du2_inverse * (ui*dz*sina + xOff*dz*sina*cosa + yOff*dz*sina*sina);			
				}	

				ROOT::Math::SVector<Double_t, 4> c_svd = A*B;					


				TVector3 inctrack_vec(t.first.xSlope(), t.first.ySlope(), 1);
				TVector3 outtrack_vec(c_svd(2), c_svd(3), 1); 

				TVector2 intrack_pos(t.first.xSlope() * targetZ + t.first.x0(), t.first.ySlope() * targetZ + t.first.y0());
				TVector2 outrack_pos(c_svd(2) * targetZ + c_svd(0), c_svd(3) * targetZ + c_svd(1));
				

				
				//chi2 += (intrack_pos - outrack_pos).Mod();

				auto uvdist = trackuvdist(outgoing_hits);

				Double_t thresh = 0.3;

				Bool_t reco = false;

				if( (0 <= uvdist.first && uvdist.first < thresh) || (0 <= uvdist.second && uvdist.second < thresh))
					reco = true;		

				if(reco) {
					++reconstructible_ctr;
					chi2 += inctrack_vec.Angle(outtrack_vec);
				}
			}


			
			std::cout << chi2 / reconstructible_ctr << ", still reconstructible -- " << reconstructible_ctr << " / " << goodTracks.size() << std::endl;
			
			if(reconstructible_ctr != 0)
				return chi2 / reconstructible_ctr;
			else
				return 1e20;
		};

		Double_t tZ = m_detectorConfiguration.stations()[m_alignmentConfiguration.alignFullStation()].targetPosition();

		auto functionToMinimize = std::bind(functionToMinimizeBase, std::placeholders::_1, m_modules, m_modules[m_alignmentConfiguration.alignFullStation()], m_goodTracksStationAlignment, tZ, m_alignmentConfiguration.useSeedingSensorOnly());

		ROOT::Math::Functor f(functionToMinimize,3);

		minimizer->SetFunction(f);

		//release all variables
		minimizer->ReleaseVariable(0);
		minimizer->ReleaseVariable(1);
		minimizer->ReleaseVariable(2);

		//reset values
		minimizer->SetVariableValue(0, 0);
		minimizer->SetVariableValue(1, 0);
		minimizer->SetVariableValue(2, 0);		

		//dont minimize rotation around Z axis
		minimizer->FixVariable(2);

		if(minimizer->Minimize()) {

			const double *arguments = minimizer->X();

			TRotation rotation;
			//rotation.RotateXEulerAngles(arguments[0], arguments[1], arguments[2]);
			rotation.RotateX(arguments[0]);
			rotation.RotateY(arguments[1]);
			rotation.RotateZ(arguments[2]);

			for(auto& m : m_modules[m_alignmentConfiguration.alignFullStation()]) {


				//always start from the initial alignment
				m.resetInitialAlignmentParameters();

				//measurement direction vector (rotation by tilt around Y + rotation by alpha around Z of (1,0,0))
				TVector3 measurement_direction_vector = (TVector3(m.angleCosine() * m.tiltCosine(), m.angleSine() * m.tiltCosine(), -m.tiltSine())).Unit();

				//vector from the middle of the target to the middle of the module
				TVector3 module_position_vector(m.xOffset(), m.yOffset(), m.z() - tZ);

				//vector from the middle of the target, to the measurement direction vector tip
				TVector3 module_measurement_direction_tip = module_position_vector + measurement_direction_vector;

				//rotate both
				module_position_vector.Transform(rotation);
				module_measurement_direction_tip.Transform(rotation);

				TVector3 measurement_direction_vector_new = module_measurement_direction_tip - module_position_vector;

				//translate back to local degrees of freedom
				Double_t new_tilt = measurement_direction_vector_new.Angle(TVector3(0,0,1)) - M_PI_2;
				Double_t new_alpha = TVector3(measurement_direction_vector_new.x(), measurement_direction_vector_new.y(), 0).Angle(TVector3(1,0,0));
				if(measurement_direction_vector_new.y() < 0) new_alpha *= -1;				

				m.setAlignmentParametersFromFullValues(module_position_vector.x(), module_position_vector.y(), tZ + module_position_vector.z(), new_alpha*TMath::RadToDeg(), new_tilt*TMath::RadToDeg());
				m.makeCurrentAlignmentParametersInitial();
			}


			LOG(info) << "minimum: " << minimizer->MinValue() << ", values: " << arguments[0] << " (" << arguments[0]*TMath::RadToDeg() <<"), " << arguments[1] << " (" << arguments[1]*TMath::RadToDeg() <<"), " << arguments[2]  << " (" << arguments[2]*TMath::RadToDeg() <<"), " <<  std::endl;	

		} else {

			for(auto& m : m_modules[m_alignmentConfiguration.alignFullStation()]) {

				if(m_alignmentConfiguration.initialAlignmentSet())
					m.resetInitialAlignmentParameters();
				else
					m.setAlignmentParameters(0, 0, 0, 0, 0);
			}
			LOG(info) << "Minimization failed. Assuming 0 offsets.";
		}



		//align shift

   		ROOT::Math::Minimizer* minimizer2 = ROOT::Math::Factory::CreateMinimizer("Minuit2", "");

		//this will have to be tweaked in the future
		minimizer2->SetMaxFunctionCalls(m_alignmentConfiguration.minimizerMaxFunctionCalls());
   		minimizer2->SetTolerance(m_alignmentConfiguration.minimizerTolerance());
   		minimizer2->SetPrintLevel(m_alignmentConfiguration.minimizerPrintLevel());


		minimizer2->SetVariable(0, "station_x_offset", 0, 0.0001);
		minimizer2->SetVariable(1, "station_y_offset", 0, 0.0001);
		minimizer2->SetVariable(2, "station_z_offset", 0, 0.0001);

		std::function<Double_t(const Double_t*, std::vector<std::vector<MUonEAlignmentModule>>&, std::vector<MUonEAlignmentModule>&, std::vector<std::pair<MUonERecoOutputTrack, MUonERecoOutputTrack>> const&, Double_t, Bool_t)> functionToMinimizeBase_pos = [](const Double_t* arguments, std::vector<std::vector<MUonEAlignmentModule>>& modules, std::vector<MUonEAlignmentModule>& modulesInStation, std::vector<std::pair<MUonERecoOutputTrack, MUonERecoOutputTrack>> const& goodTracks, Double_t targetZ, Bool_t useSeedingSensorOnly)->Double_t {


			for(auto& m : modulesInStation) {
				
				//always start from the initial alignment
				m.updateAlignmentParameters(arguments[0], arguments[1], arguments[2], 0, 0);
			}

				auto trackuvdist = [](std::vector<std::tuple<Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, char>> const& hits)->std::pair<Double_t, Double_t> {


					Int_t first_x = -1;
					Int_t second_x = -1;
					Int_t first_y = -1;
					Int_t second_y = -1;
					Int_t first_uv = -1;
					Int_t second_uv = -1;		

					for(Int_t idx = 0; idx < hits.size(); ++idx) {

						char pr = std::get<7>(hits[idx]);

						if('x' == pr) {

							if(first_x >= 0)
								second_x = idx;
							else
								first_x = idx;

						} else if('y' == pr) {

							if(first_y >= 0)
								second_y = idx;
							else
								first_y = idx;	

						} else {

							if(first_uv >= 0)
								second_uv = idx;
							else
								first_uv = idx;	
						}
					}	

					Double_t xslope = (std::get<1>(hits[second_x]) - std::get<1>(hits[first_x])) / (std::get<0>(hits[second_x]) - std::get<0>(hits[first_x]));
					Double_t yslope = (std::get<1>(hits[second_y]) - std::get<1>(hits[first_y])) / (std::get<0>(hits[second_y]) - std::get<0>(hits[first_y]));

					Double_t x0 = std::get<1>(hits[second_x]) - xslope * std::get<0>(hits[second_x]);
					Double_t y0 = std::get<1>(hits[second_y]) - yslope * std::get<0>(hits[second_y]);


					Double_t d1 = -1;
					Double_t d2 = -1;

					if(first_uv >= 0) {

						Double_t z = std::get<0>(hits[first_uv]);
						Double_t pos = std::get<1>(hits[first_uv]);
						Double_t acos = std::get<3>(hits[first_uv]);
						Double_t asin = std::get<4>(hits[first_uv]);
						Double_t xoff = std::get<5>(hits[first_uv]);
						Double_t yoff = std::get<6>(hits[first_uv]);


						Double_t x = xslope * z + x0;
						Double_t y = yslope * z + y0;

						Double_t proj = (x - xoff)*acos + (y-yoff)*asin;
						d1 = fabs(proj - pos);						
					}
					
					if(second_uv >= 0) {

						Double_t z = std::get<0>(hits[second_uv]);
						Double_t pos = std::get<1>(hits[second_uv]);
						Double_t acos = std::get<3>(hits[second_uv]);
						Double_t asin = std::get<4>(hits[second_uv]);
						Double_t xoff = std::get<5>(hits[second_uv]);
						Double_t yoff = std::get<6>(hits[second_uv]);


						Double_t x = xslope * z + x0;
						Double_t y = yslope * z + y0;

						Double_t proj = (x - xoff)*acos + (y-yoff)*asin;
						d2 = fabs(proj - pos);							
					}

					return std::make_pair(d1, d2);

				};

			Double_t chi2 = 0;
			Int_t reconstructible_ctr = 0;

			//refit second track
			for(auto const& t : goodTracks) {

				auto outgoing_hits_tmp = t.second.hits();

				//recalculate their position
				std::vector<std::tuple<Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, char>> outgoing_hits;
				outgoing_hits.reserve(outgoing_hits_tmp.size());

				auto newHitPos = [&useSeedingSensorOnly](MUonERecoOutputHit const& hit, MUonEAlignmentModule const& mod)->std::tuple<Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, Double_t, char> {

					Double_t hit_z = -99999;
					Double_t hit_pos = -99999;

					if(useSeedingSensorOnly) {

						Double_t sensor_shift = 0.5 * mod.distanceBetweenSensors();
						if(0 == mod.seedSensor())
							sensor_shift *= -1;

						//additional terms, because both sensors are rotated wrt. module's midpoint
						hit_z = mod.z() + sensor_shift * mod.tiltCosine() - hit.position() * mod.tiltSine();
						hit_pos = sensor_shift * mod.tiltSine() + hit.position() * mod.tiltCosine();


					} else {

						hit_z = mod.z() - hit.position() * mod.tiltSine();
						hit_pos = hit.position() * mod.tiltCosine();
					}

					return std::make_tuple(hit_z, hit_pos, hit.positionError(), mod.angleCosine(), mod.angleSine(), mod.xOffset(), mod.yOffset(), hit.projection());

				};

				for(auto const& h : outgoing_hits_tmp)
					outgoing_hits.emplace_back(newHitPos(h, modulesInStation[h.moduleID()]));
									

					

				//refit outgoing track
				Double_t z0 = 0;

				auto lsum = [&outgoing_hits, &z0](Int_t cosine_term_power, Int_t sine_term_power, Int_t z_term_power) {

					Double_t sum = 0;

					for(auto const& hit : outgoing_hits) {

						Double_t z = std::get<0>(hit);
						Double_t pos = std::get<1>(hit);
						Double_t posErr = std::get<2>(hit);
						Double_t acos = std::get<3>(hit);
						Double_t asin = std::get<4>(hit);

						Double_t hit_contribution = 1;
						
						for(Int_t i = 0; i < cosine_term_power; ++i) hit_contribution *= acos;
						for(Int_t i = 0; i < sine_term_power; ++i) hit_contribution *= asin;
						for(Int_t i = 0; i < z_term_power; ++i) hit_contribution *= (z - z0); //z0 fixed above for now
						
						Double_t du2 = posErr*posErr;
						
						sum += hit_contribution/du2;				
					}

					return sum;
				};


				Double_t tmpA[10] = {
										lsum(2,0,0),  
										lsum(1,1,0), lsum(0,2,0), 
										lsum(2,0,1), lsum(1,1,1), lsum(2,0,2), 
										lsum(1,1,1), lsum(0,2,1), lsum(1,1,2), lsum(0,2,2)
									};

				//note MatRepSym, hence not all terms are needed to define the matrix (it's symmetric)
				ROOT::Math::SMatrix<Double_t, 4, 4, ROOT::Math::MatRepSym<Double_t,4>> A(tmpA, 10);

				if(!A.Invert()) //A is now a covariance matrix
					return 1; //failed to invert


				ROOT::Math::SVector<Double_t, 4> B;
				for(auto const& hit : outgoing_hits) {

					Double_t posErr = std::get<2>(hit);
					const Double_t du2_inverse =  1 / (posErr*posErr);
					const Double_t cosa = std::get<3>(hit);
					const Double_t sina = std::get<4>(hit);
					const Double_t dz = std::get<0>(hit) - z0;
					const Double_t ui = std::get<1>(hit);
					const Double_t xOff = std::get<5>(hit);
					const Double_t yOff = std::get<6>(hit);

					//bx
					B(0) += du2_inverse * (ui*cosa + xOff*cosa*cosa + yOff*sina*cosa);
					//by
					B(1) += du2_inverse * (ui*sina + xOff*sina*cosa + yOff*sina*sina);
					//ax
					B(2) += du2_inverse * (ui*dz*cosa + xOff*dz*cosa*cosa + yOff*dz*sina*cosa);
					//ay
					B(3) += du2_inverse * (ui*dz*sina + xOff*dz*sina*cosa + yOff*dz*sina*sina);			
				}	

				ROOT::Math::SVector<Double_t, 4> c_svd = A*B;					


				TVector2 intrack_pos(t.first.xSlope() * targetZ + t.first.x0(), t.first.ySlope() * targetZ + t.first.y0());
				TVector2 outrack_pos(c_svd(2) * targetZ + c_svd(0), c_svd(3) * targetZ + c_svd(1));
				

				//chi2 += inctrack_vec.Angle(outtrack_vec);
				
				auto uvdist = trackuvdist(outgoing_hits);

				Double_t thresh = 0.3;

				Bool_t reco = false;

				if( (0 <= uvdist.first && uvdist.first < thresh) || (0 <= uvdist.second && uvdist.second < thresh))
					reco = true;		

				if(reco) {
					++reconstructible_ctr;
					chi2 += (intrack_pos - outrack_pos).Mod();
				}
			}

			std::cout << chi2 / reconstructible_ctr << ", still reconstructible -- " << reconstructible_ctr << " / " << goodTracks.size() << std::endl;
			
			if(reconstructible_ctr != 0)
				return chi2 / reconstructible_ctr;
			else
				return 1e20;
		};

		auto functionToMinimize_pos = std::bind(functionToMinimizeBase_pos, std::placeholders::_1, m_modules, m_modules[m_alignmentConfiguration.alignFullStation()], m_goodTracksStationAlignment, tZ, m_alignmentConfiguration.useSeedingSensorOnly());

		ROOT::Math::Functor f_pos(functionToMinimize_pos,3);

		minimizer2->SetFunction(f_pos);

		//release all variables
		minimizer2->ReleaseVariable(0);
		minimizer2->ReleaseVariable(1);
		minimizer2->ReleaseVariable(2);

		//reset values
		minimizer2->SetVariableValue(0, 0);
		minimizer2->SetVariableValue(1, 0);
		minimizer2->SetVariableValue(2, 0);	

		//dont minimize z shift
		minimizer2->FixVariable(2);


		if(minimizer2->Minimize()) {

			const double *arguments2 = minimizer2->X();

			for(auto& m : m_modules[m_alignmentConfiguration.alignFullStation()]) {

				//always start from the initial alignment
				m.updateAlignmentParameters(arguments2[0], arguments2[1], arguments2[2], 0, 0);
			
			}

			LOG(info) << "minimum: " << minimizer2->MinValue() << ", values: " << arguments2[0] << ", " << arguments2[1] << ", " << arguments2[2] << std::endl;	

		} else {

			for(auto& m : m_modules[m_alignmentConfiguration.alignFullStation()]) {

				if(m_alignmentConfiguration.initialAlignmentSet())
					m.resetInitialAlignmentParameters();
				else
					m.setAlignmentParameters(0, 0, 0, 0, 0);
			}
			LOG(info) << "Minimization failed. Assuming 0 offsets.";
		}			


		saveAlignmentToFile();
	}

}

void MUonEAlignment::saveAlignmentToFile() {

	std::vector<std::vector<MUonEModuleAlignmentParameters>> tmp;
	tmp.resize(m_modules.size());

	for(Int_t si = 0; si < m_modules.size(); ++si) {

		auto const& station = m_modules[si];

		for(auto const& mod : station) {

			tmp[si].emplace_back(mod.xOffset(), mod.yOffset(), mod.zOffset(), mod.angleOffset(), mod.tiltOffset());
		}
	}

	MUonEAlignmentContainer tmpcnt(tmp);
	tmpcnt.saveCurrentConfiguration(m_outputName.empty() ? "alignment_results.yaml" : m_outputName);
}


ClassImp(MUonEAlignment)
