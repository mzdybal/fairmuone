#ifndef MUONESIGNALGENERATORBASE_H
#define MUONESIGNALGENERATORBASE_H

#include "FairPrimaryGenerator.h"
#include "FairGenerator.h"
#include "Rtypes.h"
#include "TVector3.h"
#include "TLorentzVector.h"


#include <string>
#include <vector>

#include "TVector3.h"

#include "MUonEDetectorConfiguration.h"
#include "MUonEGenerationConfiguration.h"
#include "MUonEBeamGenerator.h"
#include "MUonEVertexGenerator.h"


class MUonEGenerator : public FairGenerator {

public:

	MUonEGenerator();

	~MUonEGenerator();

	//the las two arguments are required for Mesmer
	Bool_t setConfiguration(MUonEDetectorConfiguration const& config, MUonEGenerationConfiguration const& generationConfig, Int_t seed, Int_t numberOfEvents, std::string outputFile);
	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

	Int_t eventsCounter() const {return m_eventsCounter;}
	Int_t verticesCounter() const {return m_verticesCounter;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

	virtual Bool_t Init() override;
	virtual Bool_t ReadEvent(FairPrimaryGenerator* primGen) override;
	virtual void Finish() override;

private:

	Bool_t m_configuredCorrectly{false};
	Int_t m_eventsCounter{0};
	Int_t m_verticesCounter{0};

	//generates interaction position and momentum 3-vectors of the outgoing muon and electron
	//should be called on the output of generateBeamMuonKinematics
	//returns 0 if generated correctly
	//returns 1 if beam missed the target completely
	//returns 2 if beam entered target, but left through X/Y side and the algorithm failed to find interaction position within target
	//this shouldn't happen outside of very rare cases, like beam hitting the very edge of the target  
	Int_t findInteractionPoint(TVector3 const& beamPosition, TVector3 const& beamMomentum, TVector3& vertexPosition);


	MUonEDetectorConfiguration m_detectorConfiguration;
	MUonEGenerationConfiguration m_generationConfiguration;	

	MUonEBeamGenerator*	m_beamGenerator{nullptr};
	MUonEVertexGenerator* m_vertexGenerator{nullptr};

	Int_t m_vertexStationIndex{-1};
	Double_t m_targetMinX{0};
	Double_t m_targetMaxX{0};
	Double_t m_targetMinY{0};
	Double_t m_targetMaxY{0};
	Double_t m_targetMinZ{0};
	Double_t m_targetMaxZ{0};

	ClassDef(MUonEGenerator,2)

};


#endif //MUONESIGNALGENERATORBASE_H

