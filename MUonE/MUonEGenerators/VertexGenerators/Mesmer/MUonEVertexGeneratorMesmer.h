#ifndef MUONEVERTEXGENERATORMESMER_H
#define MUONEVERTEXGENERATORMESMER_H

#include "MUonEVertexGenerator.h"

#include <string>

#include "MUonEVertexGeneratorMesmerConfiguration.h"

#include "MuEtree.h"

class MUonEVertexGeneratorMesmer : public MUonEVertexGenerator {

public:

    MUonEVertexGeneratorMesmer();
    ~MUonEVertexGeneratorMesmer();

    virtual Int_t generateOutgoingParticlesKinematics(Double_t beamEnergy, TVector3 const& beamMomentum, std::vector<Int_t>& outgoingPDG, std::vector<TVector3>& outgoingMomenta) override;

    virtual Bool_t setConfiguration(const MUonEGeneratorConfiguration* config) override;
    void setSeed(Int_t seed) {m_seed = seed;}
    void setNumberOfEvents(Int_t numberOfEvents) {m_numberOfEvents = numberOfEvents;}
    void setNominalBeamEnergy(Double_t energy) {m_nominalBeamEnergy = energy;}
    void setOutputFile(std::string file) {m_outputFile = file;}

    virtual void logCurrentConfiguration() const override;

    virtual void resetDefaultConfiguration() override;

    virtual Bool_t Init();
    virtual Bool_t Finalize(Int_t numberOfEvents = 0, Int_t numberOfVertices = 0);

    virtual void Clear() override;

private:

    MUonEVertexGeneratorMesmerConfiguration m_configuration;
    MuE::MCpara m_parameters;


    //taken from job configuration
    Int_t m_seed{42};
    Int_t m_numberOfEvents{0};
    std::string m_outputFile;

    MuE::Event* m_event = nullptr;

    //taken from beam generator
    Double_t m_nominalBeamEnergy{0};

    ClassDef(MUonEVertexGeneratorMesmer, 2)
};


#endif//MUONEVERTEXGENERATORMESMER_H

