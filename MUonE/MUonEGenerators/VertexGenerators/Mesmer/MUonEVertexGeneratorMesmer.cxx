#include "MUonEVertexGeneratorMesmer.h"

#include "TVirtualMC.h"
#include "TRandom.h"

#include "TTree.h"

#include <iostream>
#include <fstream>

#include <fairlogger/Logger.h>
#include "FairRootManager.h"
/*
extern "C" void generate_event_mesmer(double *pmu, int *nfs, int *mcids, double (*pmat)[4], double *weight,
				    int *itag, long int *ievtnr, double *wnovp, double *wnohad, double *wLO,
				    double *wNLO, double *cwvp, int *ierr);

extern "C" void finalize_mesmer(double *xsw, double *exsw,long int *foohpm, long int *fooh, double *truemax,long int *nabove,
			       long int *nlt0, double *xsbias, double *exsbias, double *xsbiasn,
			       double *exsbiasn, double *sumow, double *sum2ow2,
			       double *sumnow, double *sum2now2);

extern "C" void init_mesmer(char* indatacard);
*/
MUonEVertexGeneratorMesmer::MUonEVertexGeneratorMesmer()
    : MUonEVertexGenerator("Mesmer"), m_event(new MuE::Event())
    {
		resetDefaultConfiguration();
		//temporary fix
		FairRootManager::Instance()->RegisterAny("MesmerEvent", m_event, kTRUE);
	}

MUonEVertexGeneratorMesmer::~MUonEVertexGeneratorMesmer() {

	if(nullptr != m_event) {
		delete m_event;
		m_event = nullptr;
	}
}

Bool_t MUonEVertexGeneratorMesmer::Init() {

	//generate steering file
	std::ofstream steering;
	steering.open(m_outputFile + "_mesmer_card", std::ios::trunc);


	steering << "extmubeam yes" << std::endl;
	steering << "seed " << std::to_string(m_seed) << std::endl;
	steering << "nev " << std::to_string(m_numberOfEvents) << std::endl;
	steering << "Ebeam " << std::to_string(m_nominalBeamEnergy) << std::endl;
	steering << "bspr " << "3.5" << std::endl;
	steering << "path " << m_outputFile + "_mesmer_data" << std::endl;

    if(m_configuration.QmuSet())
        steering << "Qmu " << m_configuration.Qmu() << std::endl;

    if(m_configuration.EeminSet())
        steering << "Eemin " << m_configuration.Eemin() << std::endl;

    if(m_configuration.EemaxSet())
        steering << "Eemax " << m_configuration.Eemax() << std::endl;

    if(m_configuration.theminSet())
        steering << "themin " << m_configuration.themin() << std::endl;

    if(m_configuration.themaxSet())
        steering << "themax " << m_configuration.themax() << std::endl;

    if(m_configuration.thmuminSet())
        steering << "thmumin " << m_configuration.thmumin() << std::endl;

    if(m_configuration.thmumaxSet())
        steering << "thmumax " << m_configuration.thmumax() << std::endl;

    if(m_configuration.acoplcutSet())
        steering << "acoplcut " << m_configuration.acoplcutDecision() << " " << m_configuration.acoplcut() << std::endl;

    if(m_configuration.elastcutSet())
        steering << "elastcut " << m_configuration.elastcutDcision() << " " << m_configuration.elastcut() << std::endl;

    if(m_configuration.EthrSet())
        steering << "Ethr " << m_configuration.Ethr() << std::endl;

    if(m_configuration.ththrSet())
        steering << "ththr " << m_configuration.ththr() << std::endl;

    if(m_configuration.ordSet())
        steering << "ord " << m_configuration.ord() << std::endl;

    if(m_configuration.arunSet())
        steering << "arun " << m_configuration.arun() << std::endl;

    if(m_configuration.hadoffSet())
        steering << "hadoff " << m_configuration.hadoff() << std::endl;

    if(m_configuration.modeSet())
        steering << "mode " << m_configuration.mode() << std::endl;

    if(m_configuration.nwarmupSet())
        steering << "nwarmup " << m_configuration.nwarmup() << std::endl;

    if(m_configuration.nwriteSet())
        steering << "nwrite " << m_configuration.nwrite() << std::endl;

    if(m_configuration.sdmaxSet())
        steering << "sdmax " << m_configuration.sdmax() << std::endl;

    if(m_configuration.wnormSet())
        steering << "wnorm " << m_configuration.wnorm() << std::endl;

	if(m_configuration.syncSet())
		steering << "sync " << m_configuration.sync() << std::endl;

    if(m_configuration.radchsSet())
        steering << "radchs " << m_configuration.radchsDecision1() << " " << m_configuration.radchsDecision2() << std::endl;

    if(m_configuration.epsSet())
        steering << "eps " << m_configuration.eps() << std::endl;

    if(m_configuration.phmassSet())
        steering << "phmass " << m_configuration.phmass() << std::endl;

    if(m_configuration.nphotSet())
        steering << "nphot " << m_configuration.nphot() << std::endl;

    if(m_configuration.ndistrSet())
        steering << "ndistr " << m_configuration.ndistr() << std::endl;

    if(m_configuration.NEmuminSet())
        steering << "NEmumin " << m_configuration.NEmumin() << std::endl;

    if(m_configuration.NQcutSet())
        steering << "NQcut " << m_configuration.NQcut() << std::endl;

    if(m_configuration.NZSet())
        steering << "NZ " << m_configuration.NZ() << std::endl;

    if(m_configuration.NFFSet())
        steering << "NFF " << m_configuration.NFF() << std::endl;

	steering << "run" << std::endl;

	steering.close();
	m_parameters.InitandSetRunParams_mesmer((m_outputFile + "_mesmer_card").data());

	return true;
}

Int_t MUonEVertexGeneratorMesmer::generateOutgoingParticlesKinematics(Double_t beamEnergy, TVector3 const& beamMomentum, std::vector<Int_t>& outgoingPDG, std::vector<TVector3>& outgoingMomenta) {

	double pmu[4] = {beamEnergy, beamMomentum.X(), beamMomentum.Y(), beamMomentum.Z()};

	int ierr = -1;
	do {
	  ierr = m_event->GenerateEvent_mesmer(pmu);
	} while (ierr != 0);

	for(auto const& p : m_event->fspart) {

		outgoingPDG.push_back(p.pdgId);
		outgoingMomenta.emplace_back(TVector3(p.px, p.py, p.pz));
	}

    if(!m_configuration.suppressPerEventPrintout())
	    m_event->Print();

	return 0;
}

Bool_t MUonEVertexGeneratorMesmer::Finalize(Int_t numberOfEvents, Int_t numberOfVertices) {
    
    LOG(info) << "TEST, NUMBER OF EVENTS: " << numberOfEvents << ", NUMBER OF VERTICES GENERATED: " << numberOfVertices;

	MuE::MCstat stats;
	stats.SetEndofRun_mesmer();
	stats.Set_Nevgen(numberOfVertices);
	stats.Print(std::cout, m_parameters.Wnorm);

    MuE::Setup setup_info(m_parameters, stats);

    // write out all the MESMER parameters (temporary solution: only on output file)
    if (1) {
      std::ofstream of("MESMER_setup.txt");
      setup_info.Print(of, m_parameters.Wnorm);

    } else {
      TTree* setup_tree = new TTree("MuEsetup","MuE MC parameters");
      setup_tree->Branch("MuEparams", &setup_info, 64000, 2);
      setup_tree->Fill();

      FairRootManager::Instance()->GetSink()->WriteObject(setup_tree, "MuEsetup");
    }
	return 0;
}

Bool_t MUonEVertexGeneratorMesmer::setConfiguration(const MUonEGeneratorConfiguration* config) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;
	m_configuredCorrectly = config->configuredCorrectly();

	m_configuration = *dynamic_cast<const MUonEVertexGeneratorMesmerConfiguration*>(config);

    if(!m_configuredCorrectly)
        LOG(info) << "Mesmer configuration contains errors. Please fix them and try again.";

	return m_configuredCorrectly;
}

void MUonEVertexGeneratorMesmer::logCurrentConfiguration() const {

	LOG(info) << "name: " << m_name;
	m_configuration.logCurrentConfiguration();
}

void MUonEVertexGeneratorMesmer::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_configuration.resetDefaultConfiguration();

	m_seed = 42;
	m_numberOfEvents = 0;
	m_nominalBeamEnergy = 0;
	m_outputFile = "";
}

void MUonEVertexGeneratorMesmer::Clear() {
  m_event->Clear();
}


ClassImp(MUonEVertexGeneratorMesmer)

