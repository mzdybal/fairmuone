#include "MUonEVertexGeneratorLOSimple.h"

#include "TVirtualMC.h"
#include "TRandom.h"

#include <iostream>

#include <fairlogger/Logger.h>


//fortran code handle
extern "C" double  alpha_running_(double *q2,int *i);


MUonEVertexGeneratorLOSimple::MUonEVertexGeneratorLOSimple()
    : MUonEVertexGenerator("LOSimple")
    {
		resetDefaultConfiguration();
	}

MUonEVertexGeneratorLOSimple::~MUonEVertexGeneratorLOSimple() {

    if(m_crossSection) {
        delete m_crossSection;
        m_crossSection = nullptr;
    }    
}

Int_t MUonEVertexGeneratorLOSimple::generateOutgoingParticlesKinematics(Double_t beamEnergy, TVector3 const& beamMomentum, std::vector<Int_t>& outgoingPDG, std::vector<TVector3>& outgoingMomenta) {

	outgoingPDG.clear();
	outgoingMomenta.clear();

    if(!m_crossSection || !m_configuration.constantEnergy())
        generateCrossSectionData(beamEnergy);

	Bool_t electronMomentumAboveThreshold = false;
	Int_t electronMomentumBelowThresholdCounter = 0;

	Double_t electronP, muonP, electronTheta, muonTheta;

	do {

		if(electronMomentumBelowThresholdCounter++ >= 100) return 3;
			
		//calculate produced particles kinematics
		Double_t electronEnergy = ELECTRON_MASS - m_crossSection->GetRandom() / ELECTRON_MASS / 2;
		Double_t muonEnergy = beamEnergy + ELECTRON_MASS - electronEnergy;

		electronP = TMath::Sqrt(electronEnergy*electronEnergy - ELECTRON_MASS_SQUARED);

		//generation level cut on electron momentum
		if(electronP > m_configuration.electronMomentumCut())
			electronMomentumAboveThreshold = true;
		else
			continue;

		muonP = TMath::Sqrt(muonEnergy*muonEnergy - MUON_MASS_SQUARED);

		//calculate theta angles
		Double_t r = beamMomentum.Mag() / (beamEnergy + ELECTRON_MASS);

		electronTheta = TMath::ACos(TMath::Sqrt((electronEnergy - ELECTRON_MASS) / (electronEnergy + ELECTRON_MASS)) / r);
		muonTheta = TMath::ASin(electronP/muonP * TMath::Sin(electronTheta));

	} while(!electronMomentumAboveThreshold);

	//setup outgoing particles 4-vectors


	//phi angles with respect to the incoming muon (around the incoming beam vector)
	//particles should fly in opposite directions (angle should be different by 180 degrees)
    Double_t electronPhi = gRandom->Uniform(0., 360.);
    Double_t muonPhi = (electronPhi + 180) * TMath::DegToRad();
    electronPhi *= TMath::DegToRad();

	//unit vector along the incoming muon direction
    TVector3 inc_frame(beamMomentum);
    inc_frame *= 1/inc_frame.Mag(); 	

    //vectors with magnitudes of electron and muon momenta, pointing in the incoming muon direction
    auto muon = inc_frame;
    muon *= muonP;

    auto electron = inc_frame;
    electron *= electronP;

    //vector perpendicular to the incoming muon frame and z axis
	//note that this will break if incoming muon goes exactly along (0,0,1) direction (theta = 0)
	//but that shouldn't really ever happen
    auto norm = inc_frame.Cross(TVector3(0,0,1));

    //rotate outgoing vectors towards z axis, both in the same direction
    muon.Rotate(muonTheta, norm);	
    electron.Rotate(electronTheta, norm);

    //rotate them around the incoming muon vector with opposite angles
    muon.Rotate(muonPhi, inc_frame);  	
    electron.Rotate(electronPhi, inc_frame);

	outgoingPDG.push_back(11);
	outgoingMomenta.emplace_back(electron);

	outgoingPDG.push_back(13);
	outgoingMomenta.emplace_back(muon);

	return 0;
}



void MUonEVertexGeneratorLOSimple::generateCrossSectionData(Double_t energy, Int_t iterations) {

	std::cout << "Recalculating cross-section data for energy: " << energy << std::endl;
	if(m_crossSection) delete m_crossSection;

	Double_t tmax = 2. * ELECTRON_MASS * (ELECTRON_MASS - m_configuration.crossSectionElectronEnergyCut()); 
	Double_t tmin = 2. * ELECTRON_MASS * (ELECTRON_MASS - energy - 5 * energy * 3.75e-2); 

	m_crossSection = new TH1D("hcs","",10000,tmin,tmax);
	
	Int_t mode = 2;

	for(Int_t i = 0; i < iterations; ++i) {

		Double_t incomingEnergy = gRandom->Gaus(energy, energy * 3.75e-2);
		tmin = 2 * ELECTRON_MASS * (ELECTRON_MASS - incomingEnergy);

		Double_t t = gRandom->Uniform(tmin, tmax);
		Double_t s = ELECTRON_MASS_SQUARED + MUON_MASS_SQUARED + 2*ELECTRON_MASS * incomingEnergy;

		Double_t lambda = s*s + MUON_MASS_SQUARED * MUON_MASS_SQUARED + ELECTRON_MASS_SQUARED * ELECTRON_MASS_SQUARED - 2.* s * MUON_MASS_SQUARED - 2. * ELECTRON_MASS_SQUARED * MUON_MASS_SQUARED - 2. * ELECTRON_MASS_SQUARED * s; 
		Double_t alpha = alpha_running_(&t,&mode);
		Double_t CS = 4. * M_PI * alpha * alpha *(( s - MUON_MASS_SQUARED - ELECTRON_MASS_SQUARED) * ( s - MUON_MASS_SQUARED - ELECTRON_MASS_SQUARED) / t / t + s/t + 0.5); 	
		
		m_crossSection->Fill(t, CS/lambda);
	}
}


Bool_t MUonEVertexGeneratorLOSimple::setConfiguration(const MUonEGeneratorConfiguration* config) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;
	m_configuredCorrectly = config->configuredCorrectly();

	m_configuration = *dynamic_cast<const MUonEVertexGeneratorLOSimpleConfiguration*>(config);

    if(!m_configuredCorrectly)
        LOG(info) << "LOSimple configuration contains errors. Please fix them and try again.";

	return m_configuredCorrectly;
}

void MUonEVertexGeneratorLOSimple::logCurrentConfiguration() const {

	LOG(info) << "name: " << m_name;
	m_configuration.logCurrentConfiguration();
}

void MUonEVertexGeneratorLOSimple::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_configuration.resetDefaultConfiguration();

	if(m_crossSection) {
        delete m_crossSection;
        m_crossSection = nullptr;
    }    
}


ClassImp(MUonEVertexGeneratorLOSimple)

