      double precision function alpha_running(q2,i)
      implicit double precision (a-h,o-z)
      common/constants/alpha,pi
      
      pi    = 3.1415926535897932384626433832795029d0      
      alpha = 1.d0/137.0359895d0      

      alpha_running = alpha*vpol(q2,i)
            
      return
      end
*==============================================================================================
      function vpol(q2,i)
      implicit double precision (a-h,m,o-z)
      common/constants/alpha,pi
      common/dalphaleptonicoeadronico/dalphalep,der
      
* E,MU,TAU,TOP MASSES: 
      DIMENSION AMASSES(9)
      DATA AMASSES /0.510999D-3,.1056584D0,1.777D0,175.6D0,
     . 0.066d0,! u
     . 0.066d0,! d
     . 1.2d0,  ! c
     . 0.150d0,! s 
     . 4.6d0  /! b

      dalphalep = 0.d0
      vpol = 1.d0
      if (i.eq.0) return

* leptonic part      
      SOMMA=0.D0
      DO J=1,4
         SOMMA=SOMMA+SUMMA(AMASSES(J),Q2,J)
      ENDDO
      dalpha = ALPHA/PI*SOMMA

      dalphalep = dalpha

      if (i.eq.1) then
         vpol = 1.d0/(1.d0-dalpha)      
         return
      endif

* hadronic part (with Fred's routine)
      ST2 = 0.2322d0
      qin = q2/abs(q2) * sqrt(abs(q2))

      CALL DHADR5n12(QIN,ST2,DER,ERRDER,DEG,ERRDEG)

      dalpha = dalpha + der
      
      vpol = 1.d0/(1.d0-dalpha)
      return
      end
*==============================================================================================
* LEPTONIC AND TOP CONTRIBUTION TO VACUUM POLARIZATION
      FUNCTION SUMMA(AM,Q2,I)
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 NC(9),QF2(9)
      common/ncqf2/nc,qf2
      data nc  /1.d0,1.d0,1.d0,3.d0,
     . 3.d0, !u
     . 3.d0, !d
     . 3.d0, !c
     . 3.d0, !s
     . 3.d0 /!b
      data qf2 /1.d0,1.d0,1.d0,0.44444444444444444444d0,
     . 0.44444444444444444444d0, !u
     . 0.11111111111111111111d0, !d
     . 0.44444444444444444444d0, !c
     . 0.11111111111111111111d0, !s
     . 0.11111111111111111111d0 /!b
* NC AND QF ARE COLOR FACTOR (1 FOR LEPTONS, 
* 3 FOR TOP) AND CHARGE
      AM2=AM**2
      IF (Q2.GE.0.D0.AND.Q2.LT.(4.D0*AM2)) THEN
         SQ=SQRT(4.D0*AM2/Q2-1.D0)
         SUMMA=NC(I)*QF2(I)*(-5.D0/9.D0-(4.D0/3.D0)*(AM2/Q2)+
     >        (4.D0/3.D0*(AM2/Q2)**2+1.D0/3.D0*AM2/Q2-1.D0/6.D0)*
     >        4.D0/SQ*ATAN(1.D0/SQ))
      ELSE
           SQ=SQRT(1.D0-4.D0*AM2/Q2)
           ARGLOG=ABS((1.D0-SQ)/(1.D0+SQ))
*
           SUMMA=NC(I)*QF2(I)*(-5.D0/9.D0-(4.D0/3.D0)*(AM2/Q2)+
     >          (4.D0/3.D0*(AM2/Q2)**2+1.D0/3.D0*AM2/Q2-1.D0/6.D0)*
     >          2.D0/SQ*LOG(ARGLOG))
       ENDIF
       RETURN
       END
