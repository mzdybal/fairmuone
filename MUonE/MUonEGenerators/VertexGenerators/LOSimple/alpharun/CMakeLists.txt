set(target alpharun)

set(sources 
hadr5n12.f 
alpha_run.F
)

list(APPEND headers
)


add_library(${target} SHARED ${sources} ${headers})
add_library(MUonE::${target} ALIAS ${target})
set_target_properties(${target} PROPERTIES LINKER_LANGUAGE Fortran)

target_include_directories(${target} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
)

target_link_directories(${target} PUBLIC
)

target_link_libraries(${target} PUBLIC
)


install(TARGETS ${target} LIBRARY DESTINATION ${PROJECT_INSTALL_LIBDIR})
install(FILES ${headers} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})