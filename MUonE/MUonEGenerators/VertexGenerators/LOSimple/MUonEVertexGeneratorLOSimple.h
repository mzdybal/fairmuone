#ifndef MUONEVERTEXLOSIMPLE_H
#define MUONEVERTEXLOSIMPLE_H

#include "MUonEVertexGenerator.h"

#include "TH1D.h"
#include "MUonEVertexGeneratorLOSimpleConfiguration.h"


class MUonEVertexGeneratorLOSimple : public MUonEVertexGenerator {

public:

    MUonEVertexGeneratorLOSimple();
    ~MUonEVertexGeneratorLOSimple();

    virtual Int_t generateOutgoingParticlesKinematics(Double_t beamEnergy, TVector3 const& beamMomentum, std::vector<Int_t>& outgoingPDG, std::vector<TVector3>& outgoingMomenta) override;

    virtual Bool_t setConfiguration(const MUonEGeneratorConfiguration* config) override;
    virtual void logCurrentConfiguration() const override;

    virtual void resetDefaultConfiguration() override;

    virtual Bool_t Init() {return true;}
    virtual Bool_t Finalize(Int_t numberOfEvents = 0, Int_t numberOfVertices = 0) {return true;}

private:

    void generateCrossSectionData(Double_t energy, Int_t iterations = 1e7);

    MUonEVertexGeneratorLOSimpleConfiguration m_configuration;
    
    TH1D* m_crossSection{nullptr};


    ClassDef(MUonEVertexGeneratorLOSimple, 2)
};


#endif//MUONEVERTEXLOSIMPLE_H

