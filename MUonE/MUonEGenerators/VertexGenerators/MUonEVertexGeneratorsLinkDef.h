#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonEVertexGeneratorLOSimple+;
#pragma link C++ class MUonEVertexGeneratorMesmer+;
#pragma link C++ class MUonEForceCollisionBiasingOperator+;
#pragma link C++ class MUonEVertexGeneratorForceCollisionBiasing+;
#pragma link C++ class MUonEVertexGenerator+;

#endif