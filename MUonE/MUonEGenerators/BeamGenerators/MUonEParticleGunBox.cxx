#include "MUonEParticleGunBox.h"


#include "TRandom.h"
#include <fairlogger/Logger.h>

#include <iostream>
MUonEParticleGunBox::MUonEParticleGunBox()
    : MUonEBeamGenerator("MUonEParticleGunBox")
{
    resetDefaultConfiguration();
}

Bool_t MUonEParticleGunBox::setConfiguration(const MUonEBeamGeneratorConfiguration* config) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;
    m_configuredCorrectly = config->configuredCorrectly();

    m_configuration = *dynamic_cast<const MUonEParticleGunBoxConfiguration*>(config);

    if(!m_configuredCorrectly)
        LOG(info) << "ParticleGunBox configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEParticleGunBox::logCurrentConfiguration() const {

    LOG(info) << "name: " << m_name;
    m_configuration.logCurrentConfiguration();
}


Bool_t MUonEParticleGunBox::generateBeam(Int_t& pdg, Double_t& energy, TVector3& beamPosition, TVector3& beamMomentum) {

    pdg = m_configuration.pdg();
    if(m_configuration.isEnergyValid())
        energy = m_configuration.energy();
    else
        energy = 0;
    auto p = m_configuration.momentum();

    beamPosition.SetXYZ(gRandom->Uniform(m_configuration.xMin(), m_configuration.xMax()), gRandom->Uniform(m_configuration.yMin(), m_configuration.yMax()), m_configuration.z());

    Double_t theta = gRandom->Uniform(m_configuration.thetaMin(), m_configuration.thetaMax()) * TMath::DegToRad();
    Double_t phi = gRandom->Uniform(0., 360.) * TMath::DegToRad();

	Double_t pt = p*TMath::Sin(theta);

    beamMomentum.SetXYZ(pt*TMath::Cos(phi), pt*TMath::Sin(phi), p*TMath::Cos(theta));    	

    return true;
}

Double_t MUonEParticleGunBox::getNominalBeamEnergy() const {

    if(m_configuration.isEnergyValid())
        return m_configuration.energy();
    else
        return 0;
}


void MUonEParticleGunBox::resetDefaultConfiguration() {

    m_configuration.resetDefaultConfiguration();
}


ClassImp(MUonEParticleGunBox)