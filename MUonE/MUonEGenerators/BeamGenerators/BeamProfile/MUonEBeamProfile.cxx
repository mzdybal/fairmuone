#include "MUonEBeamProfile.h"

#include <fairlogger/Logger.h>
#include "TH1D.h"

MUonEBeamProfile::~MUonEBeamProfile() {

    if(m_file != nullptr) {

        m_file->Close();
        m_file = nullptr;
    }

    if(m_beamProfile != nullptr) {

        m_beamProfile = nullptr;
    }    
}

Bool_t MUonEBeamProfile::loadBeamProfile(TString profilename) {

    m_configuredCorrectly = true;

    if(m_file != nullptr) {

        m_file->Close();
        m_file = nullptr;
    }

    if(m_beamProfile != nullptr) {

        m_beamProfile = nullptr;
    }

    if(!profilename.EndsWith(".root") && !profilename.EndsWith(".ROOT"))
        profilename.Append(".root");

    try {

        m_filePath = "./" + profilename;
        m_file = new TFile(m_filePath);
        if(!m_file->IsOpen()) 
            throw std::invalid_argument("");

        m_file->GetObject("combined", m_beamProfile);
        if(m_beamProfile == nullptr) 
            throw std::invalid_argument("");
            
    } catch(...) {

        TString beamprofile_dir = getenv("BEAMPROFILEPATH");
        beamprofile_dir.ReplaceAll("//", "/"); //taken from FR examples, not sure if necessary
        if(!beamprofile_dir.EndsWith("/"))
            beamprofile_dir.Append("/");

        TString full_path = beamprofile_dir + profilename;

        LOG(info) << "Beam profile file in the given path not found or failed to open. Trying in BEAMPROFILEPATH directory (" << full_path << ").";


        try {

            m_filePath = full_path;

            m_file = new TFile(full_path);
            if(!m_file->IsOpen()) 
                throw std::invalid_argument("");

            m_file->GetObject("combined", m_beamProfile);
            if(m_beamProfile == nullptr)                 
                throw std::invalid_argument("");

            LOG(info) << "File found." <<  "\"";

        } catch(...) {

            LOG(error) << "Failed to load beam profile.";
            m_configuredCorrectly = false;
        }
    }

    return m_configuredCorrectly;
}


void MUonEBeamProfile::generateBeamKinematics(TVector3& beamPosition, TVector3& beamMomentum, Double_t xOffset, Double_t yOffset) {

    m_beamProfile->GetRandom(m_beamParameters.data());

    auto x = m_beamParameters[0] * 0.1; //mm -> cm
    auto y = m_beamParameters[1] * 0.1; //mm -> cm
    auto xp = m_beamParameters[2] * 0.001; //mrad -> rad
    auto yp = m_beamParameters[3] * 0.001; //mrad -> rad
    auto& p = m_beamParameters[4];

    beamMomentum.SetXYZ(0,0,p);
    beamMomentum.RotateY(xp);
    beamMomentum.RotateX(-yp);

    beamPosition.SetXYZ(x + xOffset, y + yOffset, 0);
}

Double_t MUonEBeamProfile::getNominalBeamMomentum() const {

    if(nullptr != m_beamProfile) {

        return m_beamProfile->Projection(4)->GetMean();

    } else
        return 0;
}

ClassImp(MUonEBeamProfile)


