#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonEBeamGenerator+;
#pragma link C++ class MUonEParticleGunBeamProfile+;
#pragma link C++ class MUonEParticleGunBox+;

#endif