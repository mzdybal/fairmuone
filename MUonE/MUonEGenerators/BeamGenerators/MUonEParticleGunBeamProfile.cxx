#include "MUonEParticleGunBeamProfile.h"

#include <fairlogger/Logger.h>

MUonEParticleGunBeamProfile::MUonEParticleGunBeamProfile()
    : MUonEBeamGenerator("ParticleGunBeamProfile")
{

    resetDefaultConfiguration();
}

Bool_t MUonEParticleGunBeamProfile::setConfiguration(const MUonEBeamGeneratorConfiguration* config) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;
    m_configuredCorrectly = config->configuredCorrectly();

    m_configuration = *dynamic_cast<const MUonEParticleGunBeamProfileConfiguration*>(config);

    if(!m_beamProfile.loadBeamProfile(m_configuration.beamProfile()))
        m_configuredCorrectly = false;

    if(!m_configuredCorrectly)
        LOG(info) << "MuonGunBeamProfile configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEParticleGunBeamProfile::logCurrentConfiguration() const {

    LOG(info) << "name: " << m_name;
    LOG(info) << "Beam profile: " << m_beamProfile.filePath();
}

Bool_t MUonEParticleGunBeamProfile::generateBeam(Int_t& pdg, Double_t& energy, TVector3& beamPosition, TVector3& beamMomentum) {

    pdg = m_configuration.pdg();
    m_beamProfile.generateBeamKinematics(beamPosition, beamMomentum, m_configuration.xOffset(), m_configuration.yOffset());

    if(m_configuration.isEnergyValid())
        energy = TMath::Sqrt(beamMomentum.Mag2() + m_configuration.mass() * m_configuration.mass());
    else
        energy = 0;

    return true;
}

Double_t MUonEParticleGunBeamProfile::getNominalBeamEnergy() const {

    if(m_configuration.isEnergyValid()) {

        Double_t momentum = m_beamProfile.getNominalBeamMomentum(); 
        return TMath::Sqrt(momentum * momentum + m_configuration.mass() * m_configuration.mass());
    }
    else
        return 0;
}

void MUonEParticleGunBeamProfile::resetDefaultConfiguration() {

    m_configuredCorrectly = false;
    m_configuration.resetDefaultConfiguration();
    m_beamProfile = MUonEBeamProfile();
}


ClassImp(MUonEParticleGunBeamProfile)

