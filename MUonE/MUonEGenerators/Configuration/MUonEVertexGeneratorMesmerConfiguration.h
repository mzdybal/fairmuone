#ifndef MUONEVERTEXGENERATORMESMERCONFIGURATION_H
#define MUONEVERTEXGENERATORMESMERCONFIGURATION_H

//refer to mesmer documentation for parameter description

#include "MUonEGeneratorConfiguration.h"
#include <yaml-cpp/yaml.h>
#include <string>

class MUonEVertexGeneratorMesmerConfiguration : public MUonEGeneratorConfiguration {

public:

    MUonEVertexGeneratorMesmerConfiguration();

    Bool_t suppressPerEventPrintout() const {return m_suppressPerEventPrintout;}

    Bool_t QmuSet() const {return m_QmuSet;}
    Int_t Qmu() const {return m_Qmu;}

    Bool_t EeminSet() const {return m_EeminSet;}
    Double_t Eemin() const {return m_Eemin;}

    Bool_t EemaxSet() const {return m_EemaxSet;}
    Double_t Eemax() const {return m_Eemax;}

    Bool_t theminSet() const {return m_theminSet;}
    Double_t themin() const {return m_themin;}

    Bool_t themaxSet() const {return m_themaxSet;}
    Double_t themax() const {return m_themax;}

    Bool_t thmuminSet() const {return m_thmuminSet;}
    Double_t thmumin() const {return m_thmumin;}

    Bool_t thmumaxSet() const {return m_thmumaxSet;}
    Double_t thmumax() const {return m_thmumax;}

    Bool_t acoplcutSet() const {return m_acoplcutSet;}
    std::string acoplcutDecision() const {return m_acoplcutDecision;}
    Double_t acoplcut() const {return m_acoplcut;}

    Bool_t elastcutSet() const {return m_elastcutSet;}
    std::string elastcutDcision() const {return m_elastcutDcision;}
    Double_t elastcut() const {return m_elastcut;}

    Bool_t EthrSet() const {return m_EthrSet;}
    Double_t Ethr() const {return m_Ethr;}

    Bool_t ththrSet() const {return m_ththrSet;}
    Double_t ththr() const {return m_ththr;}

    Bool_t ordSet() const {return m_ordSet;}
    std::string ord() const {return m_ord;}

    Bool_t arunSet() const {return m_arunSet;}
    std::string arun() const {return m_arun;}

    Bool_t hadoffSet() const {return m_hadoffSet;}
    std::string hadoff() const {return m_hadoff;}

    Bool_t modeSet() const {return m_modeSet;}
    std::string mode() const {return m_mode;}

    Bool_t nwarmupSet() const {return m_nwarmupSet;}
    Int_t nwarmup() const {return m_nwarmup;}

    Bool_t nwriteSet() const {return m_nwriteSet;}
    Int_t nwrite() const {return m_nwrite;}

    Bool_t sdmaxSet() const {return m_sdmaxSet;}
    Double_t sdmax() const {return m_sdmax;}

    Bool_t wnormSet() const {return m_wnormSet;}
    Double_t wnorm() const {return m_wnorm;}

    Bool_t syncSet() const {return m_syncSet;}
    Bool_t sync() const {return m_sync;}

    Bool_t radchsSet() const {return m_radchsSet;}
    Bool_t radchsDecision1() const {return m_radchsDecision1;}
    Bool_t radchsDecision2() const {return m_radchsDecision2;}

    Bool_t epsSet() const {return m_epsSet;}
    Double_t eps() const {return m_eps;}

    Bool_t phmassSet() const {return m_phmassSet;}
    Double_t phmass() const {return m_phmass;}

    Bool_t nphotSet() const {return m_nphotSet;}
    Int_t nphot() const {return m_nphot;}

    Bool_t ndistrSet() const {return m_ndistrSet;}
    Int_t ndistr() const {return m_ndistr;}

    Bool_t NEmuminSet() const {return m_NEmuminSet;}
    Double_t NEmumin() const {return m_NEmumin;}

    Bool_t NQcutSet() const {return m_NQcutSet;}
    Double_t NQcut() const {return m_NQcut;}

    Bool_t NZSet() const {return m_NZSet;}
    Int_t NZ() const {return m_NZ;}

    Bool_t NFFSet() const {return m_NFFSet;}
    Int_t NFF() const {return m_NFF;}
  
    virtual Bool_t readConfiguration(YAML::Node const& config, std::string opt = "");
    virtual void logCurrentConfiguration() const;
    virtual void resetDefaultConfiguration();

    MUonEVertexGeneratorMesmerConfiguration& operator=(const MUonEVertexGeneratorMesmerConfiguration* cfg)
    {

        m_configuredCorrectly = cfg->configuredCorrectly();

        m_suppressPerEventPrintout = cfg->suppressPerEventPrintout();

        m_QmuSet = cfg->QmuSet();
        m_Qmu = cfg->Qmu();

        m_EeminSet = cfg->EeminSet();
        m_Eemin = cfg->Eemin();

        m_EemaxSet = cfg->EemaxSet();
        m_Eemax = cfg->Eemax();

        m_theminSet = cfg->theminSet();
        m_themin = cfg->themin();

        m_themaxSet = cfg->themaxSet();
        m_themax = cfg->themax();

        m_thmuminSet = cfg->thmuminSet();
        m_thmumin = cfg->thmumin();

        m_thmumaxSet = cfg->thmumaxSet();
        m_thmumax = cfg->thmumax();

        m_acoplcutSet = cfg->acoplcutSet();
        m_acoplcutDecision = cfg->acoplcutDecision();
        m_acoplcut = cfg->acoplcut();

        m_elastcutSet = cfg->elastcutSet();
        m_elastcutDcision = cfg->elastcutDcision();
        m_elastcut = cfg->elastcut();

        m_EthrSet = cfg->EthrSet();
        m_Ethr = cfg->Ethr();

        m_ththrSet = cfg->ththrSet();
        m_ththr = cfg->ththr();

        m_ordSet = cfg->ordSet();
        m_ord = cfg->ord();

        m_arunSet = cfg->arunSet();
        m_arun = cfg->arun();

        m_hadoffSet = cfg->hadoffSet();
        m_hadoff = cfg->hadoff();

        m_modeSet = cfg->modeSet();
        m_mode = cfg->mode();

        m_nwarmupSet = cfg->nwarmupSet();
        m_nwarmup = cfg->nwarmup();

        m_nwriteSet = cfg->nwriteSet();
        m_nwrite = cfg->nwrite();

        m_sdmaxSet = cfg->sdmaxSet();
        m_sdmax = cfg->sdmax();

        m_wnormSet = cfg->wnormSet();
        m_wnorm = cfg->wnorm();

        m_syncSet = cfg->syncSet();
        m_sync = cfg->sync();

        m_radchsSet = cfg->radchsSet();
        m_radchsDecision1 = cfg->radchsDecision1();
        m_radchsDecision2 = cfg->radchsDecision2();

        m_epsSet = cfg->epsSet();
        m_eps = cfg->eps();

        m_phmassSet = cfg->phmassSet();
        m_phmass = cfg->phmass();

        m_nphotSet = cfg->nphotSet();
        m_nphot = cfg->nphot();

        m_ndistrSet = cfg->ndistrSet();
        m_ndistr = cfg->ndistr(); 

	m_NEmuminSet = cfg->NEmuminSet();
	m_NEmumin = cfg->NEmumin();

	m_NQcutSet = cfg->NQcutSet();
	m_NQcut = cfg->NQcut();

	m_NZSet = cfg->NZSet();
	m_NZ = cfg->NZ();

	m_NFFSet = cfg->NFFSet();
	m_NFF = cfg->NFF();

        return *this;
    }

private:

    Bool_t m_suppressPerEventPrintout{true};

    Bool_t m_QmuSet{false};
    Int_t m_Qmu;

    Bool_t m_EeminSet{false};
    Double_t m_Eemin;

    Bool_t m_EemaxSet{false};
    Double_t m_Eemax;

    Bool_t m_theminSet{false};
    Double_t m_themin;
  
    Bool_t m_themaxSet{false};
    Double_t m_themax;

    Bool_t m_thmuminSet{false};
    Double_t m_thmumin;

    Bool_t m_thmumaxSet{false};
    Double_t m_thmumax;

    Bool_t m_acoplcutSet{false};
    std::string m_acoplcutDecision;
    Double_t m_acoplcut;

    Bool_t m_elastcutSet{false};
    std::string m_elastcutDcision;
    Double_t m_elastcut;

    Bool_t m_EthrSet{false};
    Double_t m_Ethr;

    Bool_t m_ththrSet{false};
    Double_t m_ththr;

    Bool_t m_ordSet{false};
    std::string m_ord;

    Bool_t m_arunSet{false};
    std::string m_arun;

    Bool_t m_hadoffSet{false};
    std::string m_hadoff;

    Bool_t m_modeSet{false};
    std::string m_mode;

    Bool_t m_nwarmupSet{false};
    Int_t m_nwarmup;

    Bool_t m_nwriteSet{false};
    Int_t m_nwrite;

    Bool_t m_sdmaxSet{false};
    Double_t m_sdmax;

    Bool_t m_wnormSet{false};
    Double_t m_wnorm;

    Bool_t m_syncSet{false};
    Bool_t m_sync;

    Bool_t m_radchsSet{false};
    Bool_t m_radchsDecision1;
    Bool_t m_radchsDecision2;

    Bool_t m_epsSet{false};
    Double_t m_eps;

    Bool_t m_phmassSet{false};
    Double_t m_phmass;

    Bool_t m_nphotSet{false};
    Int_t m_nphot;

    Bool_t m_ndistrSet{false};
    Int_t m_ndistr;

    Bool_t m_NEmuminSet{false};
    Double_t m_NEmumin;

    Bool_t m_NQcutSet{false};
    Double_t m_NQcut;

    Bool_t m_NZSet{false};
    Int_t m_NZ;

    Bool_t m_NFFSet{false};
    Int_t m_NFF;

    ClassDef(MUonEVertexGeneratorMesmerConfiguration, 4)

};

#endif //MUONEVERTEXGENERATORMESMERCONFIGURATION_H

