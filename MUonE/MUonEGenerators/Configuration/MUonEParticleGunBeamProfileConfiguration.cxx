#include "MUonEParticleGunBeamProfileConfiguration.h"


#include <fairlogger/Logger.h>
#include "TMath.h"

MUonEParticleGunBeamProfileConfiguration::MUonEParticleGunBeamProfileConfiguration()
    : MUonEBeamGeneratorConfiguration("BeamProfile")
{
    resetDefaultConfiguration();
}

Bool_t MUonEParticleGunBeamProfileConfiguration::readConfiguration(YAML::Node const& config, std::string opt) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(0 == opt.compare("muon")) {

        m_pdg = 13;
        m_massSet = true;
        m_mass = 105.6583715 * 1.e-3;        

    } else if(0 == opt.compare("pion")) {

        m_pdg = 211;
        m_massSet = true;
        m_mass = 134.9766 * 1.e-3;
    }
    else
    {
        if(config["pdg"])
            m_pdg = config["pdg"].as<Int_t>();
        else {

            LOG(error) << "Variable 'pdg' not set.";
            m_configuredCorrectly = false;
        }

        if(config["mass"]) {
            m_mass = config["mass"].as<Double_t>();
            m_massSet = true;
        }
        else {
            m_massSet = false;
        }    
    }

    if(config["beamProfile"]) {
        m_beamProfile = config["beamProfile"].as<std::string>();
    }
    else {
        LOG(error) << "Variable beamProfile not set.";
        m_configuredCorrectly = false;
    }    

    if(config["xOffset"]) {
        m_xOffset = config["xOffset"].as<Double_t>();
    }

    if(config["yOffset"]) {
        m_yOffset = config["yOffset"].as<Double_t>();
    }

    if(!m_massSet) {

        LOG(info) << "Mass not set. Can't calculate energy. Using vertex generator impossible.";
    }


    if(!m_configuredCorrectly)
        LOG(info) << "ParticleGunBeamProfile configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEParticleGunBeamProfileConfiguration::logCurrentConfiguration() const {

    LOG(info) << "pdg: " << m_pdg;
    if(m_massSet)
        LOG(info) << "mass: " << m_mass << " GeV";

    LOG(info) << "beamProfile: " << m_beamProfile;
    LOG(info) << "xOffset: " << m_xOffset;
    LOG(info) << "yOffset: " << m_yOffset;

}


void MUonEParticleGunBeamProfileConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;
    m_pdg = 0;

    m_massSet = false;
    m_mass = 0;

    m_beamProfile = "";   
    m_xOffset = 0;
    m_yOffset = 0;
}

ClassImp(MUonEParticleGunBeamProfileConfiguration)