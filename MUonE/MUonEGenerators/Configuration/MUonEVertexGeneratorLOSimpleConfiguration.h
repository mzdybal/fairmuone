#ifndef MUONEVERTEXGENERATORLOSIMPLECONFIGURATION_H
#define MUONEVERTEXGENERATORLOSIMPLECONFIGURATION_H

#include "MUonEGeneratorConfiguration.h"
#include <yaml-cpp/yaml.h>
#include <string>

class MUonEVertexGeneratorLOSimpleConfiguration : public MUonEGeneratorConfiguration {

public:

    MUonEVertexGeneratorLOSimpleConfiguration();

    Bool_t constantEnergy() const {return m_constantEnergy;}

    Double_t electronMomentumCut() const {return m_electronMomentumCut;}
    Double_t crossSectionElectronEnergyCut() const {return m_crossSectionElectronEnergyCut;}

    virtual Bool_t readConfiguration(YAML::Node const& config, std::string opt = "");
    virtual void logCurrentConfiguration() const;
    virtual void resetDefaultConfiguration();

    MUonEVertexGeneratorLOSimpleConfiguration& operator=(const MUonEVertexGeneratorLOSimpleConfiguration* cfg)
    {

        m_configuredCorrectly = cfg->configuredCorrectly();
        m_constantEnergy = cfg->constantEnergy();

        m_electronMomentumCut = cfg->electronMomentumCut();
        m_crossSectionElectronEnergyCut = cfg->crossSectionElectronEnergyCut();

        return *this;
    }

private:

    Bool_t m_constantEnergy{false};

    Double_t m_electronMomentumCut{0};
    Double_t m_crossSectionElectronEnergyCut{1};


    ClassDef(MUonEVertexGeneratorLOSimpleConfiguration, 2)

};

#endif //MUONEVERTEXGENERATORLOSIMPLECONFIGURATION_H

