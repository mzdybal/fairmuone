#include "MUonEVertexGeneratorLOSimpleConfiguration.h"

#include <fairlogger/Logger.h>

MUonEVertexGeneratorLOSimpleConfiguration::MUonEVertexGeneratorLOSimpleConfiguration()
    : MUonEGeneratorConfiguration("LOSimple")
{
    resetDefaultConfiguration();
}

Bool_t MUonEVertexGeneratorLOSimpleConfiguration::readConfiguration(YAML::Node const& config, std::string opt) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;


    if(config["constantEnergy"])
        m_constantEnergy = config["constantEnergy"].as<Bool_t>();
    else {

        LOG(error) << "Variable 'constantEnergy' not set.";
        m_configuredCorrectly = false;
    }

    if(config["electronMomentumCut"])
        m_electronMomentumCut = config["electronMomentumCut"].as<Double_t>();
    else {

        LOG(error) << "Variable 'electronMomentumCut' not set.";
        m_configuredCorrectly = false;
    }

    if(config["crossSectionElectronEnergyCut"])
        m_crossSectionElectronEnergyCut = config["crossSectionElectronEnergyCut"].as<Double_t>();
    else {

        LOG(error) << "Variable 'crossSectionElectronEnergyCut' not set.";
        m_configuredCorrectly = false;
    }

    if(!m_configuredCorrectly)
        LOG(info) << "LOSimple configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEVertexGeneratorLOSimpleConfiguration::logCurrentConfiguration() const {

	LOG(info) << "constantEnergy: " << m_constantEnergy;
	LOG(info) << "electronMomentumCut: " << m_electronMomentumCut << " GeV";
	LOG(info) << "crossSectionElectronEnergyCut: " << m_crossSectionElectronEnergyCut << " GeV";

}


void MUonEVertexGeneratorLOSimpleConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

	m_constantEnergy = false;

	m_electronMomentumCut = 0;
	m_crossSectionElectronEnergyCut = 1;
  
}

ClassImp(MUonEVertexGeneratorLOSimpleConfiguration)

