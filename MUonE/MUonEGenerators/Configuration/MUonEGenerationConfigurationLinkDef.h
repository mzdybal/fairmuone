#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonEGenerationConfiguration+;
#pragma link C++ class MUonEGeneratorConfiguration+;
#pragma link C++ class MUonEBeamGeneratorConfiguration+;
#pragma link C++ class MUonEParticleGunBeamProfileConfiguration+;
#pragma link C++ class MUonEParticleGunBoxConfiguration+;
#pragma link C++ class MUonEVertexGeneratorLOSimpleConfiguration+;
#pragma link C++ class MUonEVertexGeneratorMesmerConfiguration+;
#pragma link C++ class MUonEVertexGeneratorForceCollisionBiasingConfiguration+;

#endif