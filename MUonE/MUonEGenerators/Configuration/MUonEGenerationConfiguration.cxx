#include "MUonEGenerationConfiguration.h"

#include <fairlogger/Logger.h>

#include "MUonEParticleGunBoxConfiguration.h"
#include "MUonEParticleGunBeamProfileConfiguration.h"
#include "MUonEVertexGeneratorLOSimpleConfiguration.h"
#include "MUonEVertexGeneratorMesmerConfiguration.h"
#include "MUonEVertexGeneratorForceCollisionBiasingConfiguration.h"

MUonEGenerationConfiguration::MUonEGenerationConfiguration() {

    resetDefaultConfiguration();
}

MUonEGenerationConfiguration::~MUonEGenerationConfiguration() {

    if(m_beamGeneratorConfiguration) {

        delete m_beamGeneratorConfiguration;
        m_beamGeneratorConfiguration = nullptr;
    }

    if(m_vertexGeneratorConfiguration) {

        delete m_vertexGeneratorConfiguration;
        m_vertexGeneratorConfiguration = nullptr;
    }
}

Bool_t MUonEGenerationConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    if(config["saveTrackerPoints"])
        m_saveTrackerPoints = config["saveTrackerPoints"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'saveTrackerPoints' not set. They will be saved by default.";
        m_saveTrackerPoints = true;            
    }

    if(config["saveCalorimeterPoints"])
        m_saveCalorimeterPoints = config["saveCalorimeterPoints"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'saveCalorimeterPoints' not set. They will be saved by default.";
        m_saveCalorimeterPoints = true;            
    }

    if(config["saveMCTracks"])
        m_saveMCTracks = config["saveMCTracks"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'saveMCTracks' not set. They will be saved by default.";
        m_saveMCTracks = true;            
    }

    if(config["savePileupTracks"])
        m_savePileupTracks = config["savePileupTracks"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'savePileupTracks' not set. They will be saved by default.";
        m_savePileupTracks = true;            
    }

    if(config["pileupMean"])
        m_pileupMean = config["pileupMean"].as<Double_t>();

    if(config["saveSignalTracks"])
        m_saveSignalTracks = config["saveSignalTracks"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'saveSignalTracks' not set. They will be saved by default.";
        m_saveSignalTracks = true;            
    }

    if(config["geantConfiguration"]){
        m_geantConfiguration = config["geantConfiguration"].as<std::string>();
        m_hasGeantConfiguration = true;
    }
    else {
        LOG(info) << "Variable 'geantConfiguration' not set. geant4DefaultConfiguration will be used.";
        m_hasGeantConfiguration = false;
    }       

    if(config["beamGenerator"])
        m_beamGenerator = config["beamGenerator"].as<std::string>();
    else {
        LOG(error) << "Variable 'beamGenerator' not set. Generation will not be possible.";
        m_configuredCorrectly = false;
    }

    if(config["beamGeneratorConfiguration"]) {

        loadBeamGeneratorConfiguration(config["beamGeneratorConfiguration"]);
    }
    else {
        LOG(warning) << "Variable 'beamGeneratorConfiguration' not set. This is fine if generator does not need any additional settings.";
        loadBeamGeneratorConfiguration(YAML::Node());
    }

    if(config["forceInteraction"])
        m_forceInteraction = config["forceInteraction"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'forceInteraction' not set. MinBias generation assumed.";
    }    

    if(m_forceInteraction) {

        if(config["vertexGenerator"]){
            m_vertexGenerator = config["vertexGenerator"].as<std::string>();
        }
        else {
            LOG(error) << "forceInteraction set to true, but no vertexGenerator provided.";
            m_configuredCorrectly = false;
        }   

        if(config["vertexStationIndex"])
            m_vertexStationIndex = config["vertexStationIndex"].as<Int_t>();
        else {
            LOG(error) << "forceInteraction set to true, but 'vertexStationIndex' not set.";
            m_configuredCorrectly = false;
        }           

        if(config["vertexGeneratorConfiguration"]) {

            loadVertexGeneratorConfiguration(config["vertexGeneratorConfiguration"]);
        }
        else {
            LOG(warning) << "Variable 'vertexGeneratorConfiguration' not set. This is fine if generator does not need any additional settings.";
            loadVertexGeneratorConfiguration(YAML::Node());
        }        
    }

    if(m_forceInteraction && m_hasBeamGeneratorConfiguration && !m_beamGeneratorConfiguration->isEnergyValid()) {

        LOG(error) << "forceInteraction set to true, but beamGenerator doesn't have enough information to provide beam energy.";
        m_configuredCorrectly = false;
    }

    if(!m_configuredCorrectly)
        LOG(info) << "Generation configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEGenerationConfiguration::logCurrentConfiguration() const {

    if(m_forceInteraction)
        LOG(info) << "Signal events will be produced with " << m_beamGenerator << " beam generator and " << m_vertexGenerator << " vertex generator in target of station " << m_vertexStationIndex << ".";
    else
        LOG(info) << "MinBias events will be produced with " << m_beamGenerator << " beam generator.";

    LOG(info) << "Beam generator configuration:";
    m_beamGeneratorConfiguration->logCurrentConfiguration();

    if(m_forceInteraction) {

        LOG(info) << "Vertex generator configuration:";
        m_vertexGeneratorConfiguration->logCurrentConfiguration();
    }

    LOG(info) << m_geantConfiguration << " Geant configuration will be used";

    if(m_saveTrackerPoints)
        LOG(info) << "Tracker points will be saved.";
    else
        LOG(info) << "Tracker points will not be saved."; 

    if(m_saveCalorimeterPoints)
        LOG(info) << "Calorimeter points will be saved.";
    else
        LOG(info) << "Calorimeter points will not be saved."; 

    if(m_saveMCTracks)
        LOG(info) << "MC tracks will be saved.";
    else
        LOG(info) << "MC tracks will not be saved.";     

    if(m_saveSignalTracks)
        LOG(info) << "Signal tracks will be saved.";
    else
        LOG(info) << "Signal tracks will not be saved."; 

    if(m_pileupMean > 0) {

        LOG(info) << "Pileup mean: " << m_pileupMean;

        if(m_savePileupTracks)
            LOG(info) << "Pileup tracks will be saved.";
        else
            LOG(info) << "Pileup tracks will not be saved."; 
    }
}

void MUonEGenerationConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_saveTrackerPoints = true;
    m_saveCalorimeterPoints = true;
    m_saveMCTracks = true;    
    m_saveSignalTracks = true;
    m_savePileupTracks = true;

    m_hasGeantConfiguration = false;
    m_geantConfiguration = "";

    m_beamGenerator = "";
    m_hasBeamGeneratorConfiguration = false;

    if(m_beamGeneratorConfiguration) {

        delete m_beamGeneratorConfiguration;
        m_beamGeneratorConfiguration = nullptr;
    }

    m_forceInteraction = false;
    m_biasingActive = false;
    m_vertexStationIndex = -1;
    m_vertexGenerator = "";
    m_hasVertexGeneratorConfiguration = false;

    if(m_vertexGeneratorConfiguration) {

        delete m_vertexGeneratorConfiguration;
        m_vertexGeneratorConfiguration = nullptr;
    }
   
}

void MUonEGenerationConfiguration::loadBeamGeneratorConfiguration(YAML::Node const& config) {

    auto readPGBConfig = [](YAML::Node const& cfg, std::string opt, Bool_t& configuredCorrectly, Bool_t& hasBeamGeneratorConfiguration) {

        auto tmp_ptr = new MUonEParticleGunBoxConfiguration();
        if(!tmp_ptr->readConfiguration(cfg, opt)) {
            configuredCorrectly = false;
            hasBeamGeneratorConfiguration = false;
            delete tmp_ptr;
            tmp_ptr = nullptr;
        }
        else {
            hasBeamGeneratorConfiguration = true;
        }        
        return dynamic_cast<MUonEBeamGeneratorConfiguration*>(tmp_ptr);
    };

    auto readPGBPConfig = [](YAML::Node const& cfg, std::string opt, Bool_t& configuredCorrectly, Bool_t& hasBeamGeneratorConfiguration) {

        auto tmp_ptr = new MUonEParticleGunBeamProfileConfiguration();
        if(!tmp_ptr->readConfiguration(cfg, opt)) {
            configuredCorrectly = false;
            hasBeamGeneratorConfiguration = false;
            delete tmp_ptr;
            tmp_ptr = nullptr;
        }
        else {
            hasBeamGeneratorConfiguration = true;
        }        
        return dynamic_cast<MUonEBeamGeneratorConfiguration*>(tmp_ptr);
    };

    if(0 == m_beamGenerator.compare("ParticleGunBox"))
        m_beamGeneratorConfiguration = readPGBConfig(config, "", m_configuredCorrectly, m_hasBeamGeneratorConfiguration);
    else if(0 == m_beamGenerator.compare("MuonGunBox"))
        m_beamGeneratorConfiguration = readPGBConfig(config, "muon", m_configuredCorrectly, m_hasBeamGeneratorConfiguration);
    else if(0 == m_beamGenerator.compare("PionGunBox"))
        m_beamGeneratorConfiguration = readPGBConfig(config, "pion", m_configuredCorrectly, m_hasBeamGeneratorConfiguration);
    else if(0 == m_beamGenerator.compare("ParticleGunBeamProfile"))
        m_beamGeneratorConfiguration = readPGBPConfig(config, "", m_configuredCorrectly, m_hasBeamGeneratorConfiguration);
    else if(0 == m_beamGenerator.compare("MuonGunBeamProfile"))
        m_beamGeneratorConfiguration = readPGBPConfig(config, "muon", m_configuredCorrectly, m_hasBeamGeneratorConfiguration);
    else if(0 == m_beamGenerator.compare("PionGunBeamProfile"))
        m_beamGeneratorConfiguration = readPGBPConfig(config, "pion", m_configuredCorrectly, m_hasBeamGeneratorConfiguration);
    else {

        LOG(error) << "Beam generator " << m_beamGenerator << " not known.";
        m_configuredCorrectly = false;
        m_hasBeamGeneratorConfiguration = false;
    }  

    LOG(info) <<   m_configuredCorrectly;
    LOG(info) <<   m_hasBeamGeneratorConfiguration;
    LOG(info) <<   m_beamGeneratorConfiguration->isEnergyValid();
    
}

void MUonEGenerationConfiguration::loadVertexGeneratorConfiguration(YAML::Node const& config) {

    if(0 == m_vertexGenerator.compare("LOSimple")) {

        auto tmp_ptr = new MUonEVertexGeneratorLOSimpleConfiguration();
        if(!tmp_ptr->readConfiguration(config)) {
            m_configuredCorrectly = false;
            m_hasVertexGeneratorConfiguration = false;
            delete tmp_ptr;
            tmp_ptr = nullptr;
        }
        else {
            m_vertexGeneratorConfiguration = tmp_ptr;
            m_hasVertexGeneratorConfiguration = true;
        }

    } else if(0 == m_vertexGenerator.compare("Mesmer")) {

        auto tmp_ptr = new MUonEVertexGeneratorMesmerConfiguration();
        if(!tmp_ptr->readConfiguration(config)) {
            m_configuredCorrectly = false;
            m_hasVertexGeneratorConfiguration = false;
            delete tmp_ptr;
            tmp_ptr = nullptr;
        }
        else {
            m_vertexGeneratorConfiguration = tmp_ptr;
            m_hasVertexGeneratorConfiguration = true;
        }

    } else if(0 == m_vertexGenerator.compare("ForceCollisionBiasing")) {

        auto tmp_ptr = new MUonEVertexGeneratorForceCollisionBiasingConfiguration();
        if(!tmp_ptr->readConfiguration(config)) {
            m_configuredCorrectly = false;
            m_hasVertexGeneratorConfiguration = false;
            delete tmp_ptr;
            tmp_ptr = nullptr;
        }
        else {
            m_vertexGeneratorConfiguration = tmp_ptr;
            m_hasVertexGeneratorConfiguration = true;
            m_biasingActive = true;
        }        

    } else {

        LOG(error) << "Vertex generator " << m_vertexGenerator << " not known.";
        m_configuredCorrectly = false;	
        m_hasVertexGeneratorConfiguration = false;
    }      
}
    

ClassImp(MUonEGenerationConfiguration)

