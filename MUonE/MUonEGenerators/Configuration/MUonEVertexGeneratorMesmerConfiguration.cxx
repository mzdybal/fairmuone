#include "MUonEVertexGeneratorMesmerConfiguration.h"

#include <fairlogger/Logger.h>

MUonEVertexGeneratorMesmerConfiguration::MUonEVertexGeneratorMesmerConfiguration()
    : MUonEGeneratorConfiguration("Mesmer")
{
    resetDefaultConfiguration();
}

Bool_t MUonEVertexGeneratorMesmerConfiguration::readConfiguration(YAML::Node const& config, std::string opt) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(config["suppressPerEventPrintout"]) {

        m_suppressPerEventPrintout = config["suppressPerEventPrintout"].as<Bool_t>();
    }

    if(config["Qmu"]) {

        m_QmuSet = true;
        m_Qmu = config["Qmu"].as<Int_t>();
    }
    
    if(config["Eemin"]) {

        m_EeminSet = true;
        m_Eemin = config["Eemin"].as<Double_t>();
    }
    
    if(config["Eemax"]) {

        m_EemaxSet = true;
        m_Eemax = config["Eemax"].as<Double_t>();
    }
    
    if(config["themin"]) {

        m_theminSet = true;
        m_themin = config["themin"].as<Double_t>();
    }
    
    if(config["themax"]) {

        m_themaxSet = true;
        m_themax = config["themax"].as<Double_t>();
    }
    
    if(config["thmumin"]) {

        m_thmuminSet = true;
        m_thmumin = config["thmumin"].as<Double_t>();
    }
    
    if(config["thmumax"]) {

        m_thmumaxSet = true;
        m_thmumax = config["thmumax"].as<Double_t>();
    }
    
    if(config["acoplcut"]) {

        auto temp = config["acoplcut"];

        if(2 == temp.size()) {

            m_acoplcutSet = true;
            m_acoplcutDecision = temp[0].as<std::string>();
            m_acoplcut = temp[1].as<Double_t>();

        } else {
            m_configuredCorrectly = false;
            LOG(error) << "Variable acoplcut requires two inputs given as [a, b]";
        }
    }
    
    if(config["elastcut"]) {

        auto temp = config["elastcut"];

        if(2 == temp.size()) {

            m_elastcutSet = true;
            m_elastcutDcision = temp[0].as<std::string>();
            m_elastcut = temp[1].as<Double_t>();

        } else {

            m_configuredCorrectly = false;
            LOG(error) << "Variable elastcut requires two inputs given as [a, b]";
        }
    }
    
    if(config["Ethr"]) {

        m_EthrSet = true;
        m_Ethr = config["Ethr"].as<Double_t>();
    }
    
    if(config["ththr"]) {

        m_ththrSet = true;
        m_ththr = config["ththr"].as<Double_t>();
    }
    
    if(config["ord"]) {

        m_ordSet = true;
        m_ord = config["ord"].as<std::string>();
    }
    
    if(config["arun"]) {

        m_arunSet = true;
        m_arun = config["arun"].as<std::string>();
    }
    
    if(config["hadoff"]) {

        m_hadoffSet = true;
        m_hadoff = config["hadoff"].as<std::string>();
    }
    
    if(config["mode"]) {

        m_modeSet = true;
        m_mode = config["mode"].as<std::string>();
    }
    
    if(config["nwarmup"]) {

        m_nwarmupSet = true;
        m_nwarmup = config["nwarmup"].as<Int_t>();
    }
    
    if(config["nwrite"]) {

        m_nwriteSet = true;
        m_nwrite = config["nwrite"].as<Int_t>();
    }
    
    if(config["sdmax"]) {

        m_sdmaxSet = true;
        m_sdmax = config["sdmax"].as<Double_t>();
    }
    
    if(config["wnorm"]) {

        m_wnormSet = true;
        m_wnorm = config["wnorm"].as<Double_t>();
    }

    if(config["sync"]) {

        m_syncSet = true;
        m_sync = config["sync"].as<Int_t>();
    }
    
    if(config["radchs"]) {

        auto temp = config["radchs"];

        if(2 == temp.size()) {
            m_radchsSet = true;
            m_radchsDecision1 = temp[0].as<Int_t>();
            m_radchsDecision2 = temp[1].as<Int_t>();
        } else {

            m_configuredCorrectly = false;
            LOG(error) << "Variable radchs requires two inputs given as [a, b]";
        }
    }
    
    if(config["eps"]) {

        m_epsSet = true;
        m_eps = config["eps"].as<Double_t>();
    }
    
    if(config["phmass"]) {

        m_phmassSet = true;
        m_phmass = config["phmass"].as<Double_t>();
    }
    
    if(config["nphot"]) {

        m_nphotSet = true;
        m_nphot = config["nphot"].as<Int_t>();
    }

    if(config["ndistr"]) {

        m_ndistrSet = true;
        m_ndistr = config["ndistr"].as<Int_t>();  
    }  

    if(config["NEmumin"]) {

        m_NEmuminSet = true;
	m_NEmumin = config["NEmumin"].as<Double_t>();
    }
    
    if(config["NQcut"]) {

        m_NQcutSet = true;
        m_NQcut = config["NQcut"].as<Double_t>();
    }
    
    if(config["NZ"]) {

        m_NZSet = true;
        m_NZ = config["NZ"].as<Int_t>();
    }

    if(config["NFF"]) {

        m_NFFSet = true;
        m_NFF = config["NFF"].as<Int_t>();
    }

    return m_configuredCorrectly;
}

void MUonEVertexGeneratorMesmerConfiguration::logCurrentConfiguration() const {

    LOG(info) << "Fixed parameters:";
    LOG(info) << "extmubeam yes";
    LOG(info) << "seed, nev, Ebeam taken from job configuration";
    LOG(info) << "bspr 3.5";
    LOG(info) << "path set to JOB_OUTPUT_PATH_mesmer_data";

    LOG(info) << "Custom parameters (if not set, default mesmer values will be used):";

    if(m_QmuSet)
        LOG(info) << "Qmu " << m_Qmu;

    if(m_EeminSet)
        LOG(info) << "Eemin " << m_Eemin;

    if(m_EemaxSet)
        LOG(info) << "Eemax " << m_Eemax;

    if(m_theminSet)
        LOG(info) << "themin " << m_themin;

    if(m_themaxSet)
        LOG(info) << "themax " << m_themax;

    if(m_thmuminSet)
        LOG(info) << "thmumin " << m_thmumin;

    if(m_thmumaxSet)
        LOG(info) << "thmumax " << m_thmumax;

    if(m_acoplcutSet)
        LOG(info) << "acoplcut [" << m_acoplcutDecision << " " << m_acoplcut << "]";

    if(m_elastcutSet)
        LOG(info) << "elastcut [" << m_elastcutDcision << " " << m_elastcut << "]";

    if(m_EthrSet)
        LOG(info) << "Ethr " << m_Ethr;

    if(m_ththrSet)
        LOG(info) << "ththr " << m_ththr;

    if(m_ordSet)
        LOG(info) << "ord " << m_ord;

    if(m_arunSet)
        LOG(info) << "arun " << m_arun;

    if(m_hadoffSet)
        LOG(info) << "hadoff " << m_hadoff;

    if(m_modeSet)
        LOG(info) << "mode " << m_mode;

    if(m_nwarmupSet)
        LOG(info) << "nwarmup " << m_nwarmup;

    if(m_nwriteSet)
        LOG(info) << "nwrite " << m_nwrite;

    if(m_sdmaxSet)
        LOG(info) << "sdmax " << m_sdmax;

    if(m_wnormSet)
        LOG(info) << "wnorm " << m_wnorm;

    if(m_syncSet)
        LOG(info) << "sync " << m_sync;

    if(m_radchsSet)
        LOG(info) << "radchs [" << m_radchsDecision1 << " " << m_radchsDecision2 << "]";

    if(m_epsSet)
        LOG(info) << "eps " << m_eps;

    if(m_phmassSet)
        LOG(info) << "phmass " << m_phmass;

    if(m_nphotSet)
        LOG(info) << "nphot " << m_nphot;

    if(m_ndistrSet)
        LOG(info) << "ndistr " << m_ndistr;

    if(m_NEmuminSet)
        LOG(info) << "NEmumin " << m_NEmumin;

    if(m_NQcutSet)
        LOG(info) << "NQcut " << m_NQcut;

    if(m_NZSet)
        LOG(info) << "NZ " << m_NZ;

    if(m_NFFSet)
        LOG(info) << "NFF " << m_NFF;
}


void MUonEVertexGeneratorMesmerConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_suppressPerEventPrintout = true;

    m_QmuSet = false;
    m_EeminSet = false;
    m_EemaxSet = false;
    m_theminSet = false;
    m_themaxSet = false;
    m_thmuminSet = false;
    m_thmumaxSet = false;
    m_acoplcutSet = false;
    m_elastcutSet = false;
    m_EthrSet = false;
    m_ththrSet = false;
    m_ordSet = false;
    m_arunSet = false;
    m_hadoffSet = false;
    m_modeSet = false;
    m_nwarmupSet = false;
    m_nwriteSet = false;
    m_sdmaxSet = false;
    m_wnormSet = false;
    m_syncSet = false;
    m_radchsSet = false;
    m_epsSet = false;
    m_phmassSet = false;
    m_nphotSet = false;
    m_ndistrSet = false;
    m_NEmuminSet = false;
    m_NQcutSet = false;
    m_NZSet = false;
    m_NFFSet = false;
}

ClassImp(MUonEVertexGeneratorMesmerConfiguration)

