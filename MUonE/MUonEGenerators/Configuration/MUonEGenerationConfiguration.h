#ifndef MUONEGENERATIONCONFIGURATION_H
#define MUONEGENERATIONCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

#include "MUonEGeneratorConfiguration.h"
#include "MUonEBeamGeneratorConfiguration.h"

class MUonEGenerationConfiguration {

public:

    MUonEGenerationConfiguration();
    ~MUonEGenerationConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t saveTrackerPoints() const {return m_saveTrackerPoints;}
    Bool_t saveCalorimeterPoints() const {return m_saveCalorimeterPoints;}
    Bool_t saveMCTracks() const {return m_saveMCTracks;}
    Bool_t saveSignalTracks() const {return m_saveSignalTracks;}
    Bool_t savePileupTracks() const {return m_savePileupTracks;}

    Double_t pileupMean() const {return m_pileupMean;}

    Bool_t hasGeantConfiguration() const {return m_hasGeantConfiguration;}
    std::string geantConfiguration() const {return m_geantConfiguration;}

    std::string beamGenerator() const {return m_beamGenerator;}

    Bool_t hasBeamGeneratorConfiguration() const {return m_hasBeamGeneratorConfiguration;};
    const MUonEBeamGeneratorConfiguration* beamGeneratorConfiguration() const {
        if(m_hasBeamGeneratorConfiguration)
            return m_beamGeneratorConfiguration;
        else
            throw std::logic_error("Trying to access beamGeneratorConfiguration, but it's not present in the configuration file.");
    }

    Bool_t forceInteraction() const {return m_forceInteraction;}
    Bool_t biasingActive() const {return m_biasingActive;}

    Int_t vertexStationIndex() const {
        if(m_forceInteraction)
            return m_vertexStationIndex;
        else
            throw std::logic_error("Trying to access vertexStationIndex, but forceInteraction is set to false.");
    }

    std::string vertexGenerator() const {
        if(m_forceInteraction)
            return m_vertexGenerator;
        else
            throw std::logic_error("Trying to access vertexGenerator, but forceInteraction is set to false.");
    }

    Bool_t hasVertexGeneratorConfiguration() const {return m_hasVertexGeneratorConfiguration;}
    const MUonEGeneratorConfiguration* vertexGeneratorConfiguration() const {
        if(m_forceInteraction && m_hasVertexGeneratorConfiguration)
            return m_vertexGeneratorConfiguration;
        else
            throw std::logic_error("Trying to access vertexGeneratorConfiguration, but forceInteraction is set to false or vertexGeneratorConfiguration section is not present.");
    }


    void resetDefaultConfiguration();

private:

    void loadBeamGeneratorConfiguration(YAML::Node const& config);
    void loadVertexGeneratorConfiguration(YAML::Node const& config);
    

    Bool_t m_configuredCorrectly{false};

    Bool_t m_saveTrackerPoints{true};
    Bool_t m_saveCalorimeterPoints{true};
    Bool_t m_saveMCTracks{true};
    Bool_t m_saveSignalTracks{true};
    Bool_t m_savePileupTracks{true};

    Double_t m_pileupMean{-1};

    Bool_t m_hasGeantConfiguration{false};
    std::string m_geantConfiguration;

    std::string m_beamGenerator;
    Bool_t m_hasBeamGeneratorConfiguration{false};
    MUonEBeamGeneratorConfiguration* m_beamGeneratorConfiguration{nullptr}; //generator specific settings, defined in generator classes

    Bool_t m_forceInteraction{false};
    Bool_t m_biasingActive{false};

    Int_t m_vertexStationIndex{-1};
    std::string m_vertexGenerator;
    Bool_t m_hasVertexGeneratorConfiguration{false};
    MUonEGeneratorConfiguration* m_vertexGeneratorConfiguration{nullptr}; //generator specific settings, defined in generator classes


    ClassDef(MUonEGenerationConfiguration, 2)
};


#endif //MUONEGENERATIONCONFIGURATION_H

