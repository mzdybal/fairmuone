#ifndef MUONEVERTEXGENERATORFORCECOLLISIONBIASINGCONFIGURATION_H
#define MUONEVERTEXGENERATORFORCECOLLISIONBIASINGCONFIGURATION_H

#include "MUonEGeneratorConfiguration.h"
#include <yaml-cpp/yaml.h>
#include <string>
#include <vector>

class MUonEVertexGeneratorForceCollisionBiasingConfiguration : public MUonEGeneratorConfiguration {

public:

    MUonEVertexGeneratorForceCollisionBiasingConfiguration();

    /*
    kept in base class to be accesible from run macro
    
    std::vector<std::string> const& particlesToBias() const {return m_particlesToBias;}
    std::vector<std::vector<std::string>> const& processesToBias() const {return m_processesToBias;}
    */
   
    virtual Bool_t readConfiguration(YAML::Node const& config, std::string opt = "");
    virtual void logCurrentConfiguration() const;
    virtual void resetDefaultConfiguration();

    MUonEVertexGeneratorForceCollisionBiasingConfiguration& operator=(const MUonEVertexGeneratorForceCollisionBiasingConfiguration* cfg)
    {

        m_particlesToBias = cfg->particlesToBias();
        m_processesToBias = cfg->processesToBias();

        return *this;
    }

private:

    /*
    kept in base class to be accesible from run macro

    std::vector<std::string> m_particlesToBias;
    std::vector<std::vector<std::string>> m_processesToBias;
    */

    ClassDef(MUonEVertexGeneratorForceCollisionBiasingConfiguration, 2)

};

#endif //MUONEVERTEXGENERATORFORCECOLLISIONBIASINGCONFIGURATION_H

