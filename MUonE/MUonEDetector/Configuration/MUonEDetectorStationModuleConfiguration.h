#ifndef MUONEDETECTORSTATIONMODULECONFIGURATION_H
#define MUONEDETECTORSTATIONMODULECONFIGURATION_H

#include "Rtypes.h"

#include <string>
#include <yaml-cpp/yaml.h>
#include <unordered_map>

#include "MUonEDetectorProjectionConfiguration.h"

#include "MUonEDetectorModulesConfiguration.h"

class MUonEDetectorStationModuleConfiguration {

public:

    MUonEDetectorStationModuleConfiguration();
    MUonEDetectorStationModuleConfiguration(YAML::Node const& config, std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections, MUonEDetectorModulesConfiguration const& baseConfiguration);

    Bool_t readConfiguration(YAML::Node const& config, std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections, MUonEDetectorModulesConfiguration const& baseConfiguration);
    void logCurrentConfiguration() const;


    MUonEDetectorProjectionConfiguration const& projection() const {return m_projection;}
    Double_t relativePosition() const {return m_relativePosition;}
    Double_t hitResolution() const {return m_hitResolution;}

    MUonEDetectorModulesConfiguration const& configuration() const {return m_configuration;}


    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};

    MUonEDetectorProjectionConfiguration m_projection;
    Double_t m_relativePosition{0};
    Double_t m_hitResolution{0};

    MUonEDetectorModulesConfiguration m_configuration;

    ClassDef(MUonEDetectorStationModuleConfiguration, 2)

};


#endif //MUONEDETECTORSTATIONMODULECONFIGURATION_H

