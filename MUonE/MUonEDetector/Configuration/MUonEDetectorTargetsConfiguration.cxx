#include "MUonEDetectorTargetsConfiguration.h"


#include <fairlogger/Logger.h>

MUonEDetectorTargetsConfiguration::MUonEDetectorTargetsConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorTargetsConfiguration::MUonEDetectorTargetsConfiguration(YAML::Node const& config) {

    readConfiguration(config);
}

Bool_t MUonEDetectorTargetsConfiguration::readConfiguration(YAML::Node const& config) {


    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(config["targetMaterial"])
        m_targetMaterial = config["targetMaterial"].as<std::string>();
    else {

        LOG(error) << "Variable 'targetMaterial' not set.";
        m_configuredCorrectly = false;
    }

    if(config["width"])
        m_width = config["width"].as<Double_t>();
    else {

        LOG(error) << "Variable 'width' not set.";
        m_configuredCorrectly = false;
    }

    if(config["height"])
        m_height = config["height"].as<Double_t>();
    else {

        LOG(error) << "Variable 'height' not set.";
        m_configuredCorrectly = false;
    }

    if(config["thickness"])
        m_thickness = config["thickness"].as<Double_t>();
    else {

        LOG(error) << "Variable 'thickness' not set.";
        m_configuredCorrectly = false;
    }

    if(!m_configuredCorrectly)
        LOG(info) << "targetsConfiguration section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorTargetsConfiguration::updateConfiguration(YAML::Node const& config) {

    if(config["targetMaterial"])
        m_targetMaterial = config["targetMaterial"].as<std::string>();

    if(config["width"])
        m_width = config["width"].as<Double_t>();

    if(config["height"])
        m_height = config["height"].as<Double_t>();

    if(config["thickness"])
        m_thickness = config["thickness"].as<Double_t>();
}

void MUonEDetectorTargetsConfiguration::logCurrentConfiguration() const {

    LOG(info) << "";

    LOG(info) << "Targets configuration:";

    LOG(info) << "material: " << m_targetMaterial;
    LOG(info) << "width: " << m_width << " cm";
    LOG(info) << "height: " << m_height << " cm";
    LOG(info) << "thickness: " << m_thickness << " cm";

}

void MUonEDetectorTargetsConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_targetMaterial = "";
    m_width = 0;
    m_height = 0;
    m_thickness = 0;
}

ClassImp(MUonEDetectorTargetsConfiguration)
