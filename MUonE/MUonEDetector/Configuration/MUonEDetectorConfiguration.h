#ifndef MUONEDETECTORCONFIGURATION_H
#define MUONEDETECTORCONFIGURATION_H

#include "Rtypes.h"
#include "TString.h"

#include <string>
#include <vector>
#include <yaml-cpp/yaml.h>
#include <algorithm>
#include <stdexcept>
	

#include "MUonEDetectorModulesConfiguration.h"
#include "MUonEDetectorTargetsConfiguration.h"
#include "MUonEDetectorCalorimeterConfiguration.h"

#include "MUonEDetectorProjectionConfiguration.h"
#include "MUonEDetectorStationConfiguration.h"


#include <unordered_map>

//to ensure that detector IDs don't overlap
enum DETECTORS {DET_TRACKER, DET_TARGETS, DET_CALORIMETER};


class MUonEDetectorConfiguration {

public:

    MUonEDetectorConfiguration();
    MUonEDetectorConfiguration(TString config_name);

    Bool_t readConfiguration(TString config_name);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t hasModulesConfiguration() const {return m_hasModulesConfiguration;}
    Bool_t hasTargetsConfiguration() const {return m_hasTargetsConfiguration;}
    Bool_t hasCalorimeterConfiguration() const {return m_hasCalorimeterConfiguration;}

    MUonEDetectorModulesConfiguration const& modulesBaseConfiguration() const {
        if(m_hasModulesConfiguration)
            return m_modulesConfiguration;
        else
            throw std::logic_error("Trying to access modulesConfiguration, but it's not defined in the detector configuration file.");
    }

    MUonEDetectorTargetsConfiguration const& targetsBaseConfiguration() const {
        if(m_hasTargetsConfiguration)
            return m_targetsConfiguration;
        else
            throw std::logic_error("Trying to access targetsConfiguration, but it's not defined in the detector configuration file.");
    }

    MUonEDetectorCalorimeterConfiguration const& calorimeterConfiguration() const {
        if(m_hasCalorimeterConfiguration)
            return m_calorimeterConfiguration;
        else
            throw std::logic_error("Trying to access calorimeterConfiguration, but it's not defined in the detector configuration file.");
    }

    std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections() const {return m_projections;}

    std::vector<MUonEDetectorStationConfiguration> const& stations() const {return m_stations;}

    Double_t calorimeterPosition() const {return m_calorimeterPosition;}
    Bool_t hasCalorimeter() const {return m_hasCalorimeter;}

    Bool_t hasStations() const {return !m_stations.empty();}
    Bool_t hasModules() const {return std::any_of(m_stations.begin(), m_stations.end(), [](const auto& s){return s.hasModules();});}
    Bool_t hasTargets() const {return std::any_of(m_stations.begin(), m_stations.end(), [](const auto& s){return s.hasTarget();});}


    void resetDefaultConfiguration();

private:

    Bool_t loadConfigFile(TString config_name, YAML::Node& config);

    Bool_t m_configuredCorrectly{false};

    Bool_t m_hasModulesConfiguration{false};
    Bool_t m_hasTargetsConfiguration{false};
    Bool_t m_hasCalorimeterConfiguration{false};

    MUonEDetectorModulesConfiguration m_modulesConfiguration;
    MUonEDetectorTargetsConfiguration m_targetsConfiguration;
    MUonEDetectorCalorimeterConfiguration m_calorimeterConfiguration;

    std::unordered_map<char, MUonEDetectorProjectionConfiguration> m_projections 
    {
        {'x', {'x', 0}},
        {'y', {'y', 90}},
        {'u', {'u', 135}},
        {'v', {'v', 45}}
    };
    std::vector<MUonEDetectorStationConfiguration> m_stations;
    
    Double_t m_calorimeterPosition{0};
    Bool_t m_hasCalorimeter = false;
    

    ClassDef(MUonEDetectorConfiguration, 2)
};

#endif//MUONEDETECTORCONFIGURATION_H

