#ifndef MUONEDETECTORMODULEGEOMETRYCONFIGURATION_H
#define MUONEDETECTORMODULEGEOMETRYCONFIGURATION_H

#include "Rtypes.h"

#include <string>
#include <yaml-cpp/yaml.h>


class MUonEDetectorModuleGeometryConfiguration {

public:

    MUonEDetectorModuleGeometryConfiguration();
    MUonEDetectorModuleGeometryConfiguration(YAML::Node const& config);

    Bool_t readConfiguration(YAML::Node const& config);
    void updateConfiguration(YAML::Node const& config); //update for per-module settings
    void logCurrentConfiguration() const;


    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t twoSensorsPerModule() const {return m_twoSensorsPerModule;}
    std::string sensorMaterial() const {return m_sensorMaterial;}
    Double_t sensorSizeMeasurementDirection() const {return m_sensorSizeMeasurementDirection;}
    Double_t sensorSizePerpendicularDirection() const {return m_sensorSizePerpendicularDirection;}
    Double_t sensorThickness() const {return m_sensorThickness;}
    Double_t distanceBetweenSensorCenters() const {return m_distanceBetweenSensorCenters;}
    Int_t numberOfStrips() const {return m_numberOfStrips;}
    Double_t stripPitch() const {return m_sensorSizeMeasurementDirection / m_numberOfStrips;}

    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};


    Bool_t m_twoSensorsPerModule{true};
    std::string m_sensorMaterial;
    Double_t m_sensorSizeMeasurementDirection{0};
    Double_t m_sensorSizePerpendicularDirection{0};
    Double_t m_sensorThickness{0};
    Double_t m_distanceBetweenSensorCenters{0};
    Int_t m_numberOfStrips{0};

    ClassDef(MUonEDetectorModuleGeometryConfiguration, 2)
};


#endif //MUONEDETECTORMODULEGEOMETRYCONFIGURATION_H


