#ifndef MUONEDETECTORCALORIMETERCONFIGURATION_H
#define MUONEDETECTORCALORIMETERCONFIGURATION_H

#include "Rtypes.h"

#include <string>
#include <yaml-cpp/yaml.h>
#include <stdexcept>

#include "MUonEDetectorCalorimeterGeometryConfiguration.h"
#include "MUonEDetectorCalorimeterDigitizationConfiguration.h"

class MUonEDetectorCalorimeterConfiguration {

public:

    MUonEDetectorCalorimeterConfiguration();
    MUonEDetectorCalorimeterConfiguration(YAML::Node const& config);

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    MUonEDetectorCalorimeterGeometryConfiguration const& geometry() const {
        if(m_hasGeometry)
            return m_geometry;
        else
            throw std::logic_error("Trying to access geometry from calorimeterConfiguration, but it's not defined in the detector configuration file.");
    }

    MUonEDetectorCalorimeterDigitizationConfiguration const& digitization() const {
        if(m_hasDigitization)
            return m_digitization;
        else
            throw std::logic_error("Trying to access digitization from calorimeterConfiguration, but it's not defined in the detector configuration file.");
    }
    
    Bool_t hasGeometry() const {return m_hasGeometry;}
    Bool_t hasDigitization() const {return m_hasDigitization;}


    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};

    MUonEDetectorCalorimeterGeometryConfiguration m_geometry;
    MUonEDetectorCalorimeterDigitizationConfiguration m_digitization;

    Bool_t m_hasGeometry = false;
    Bool_t m_hasDigitization = false;

    ClassDef(MUonEDetectorCalorimeterConfiguration, 2)
};

#endif //MUONEDETECTORCALORIMETERCONFIGURATION_H

