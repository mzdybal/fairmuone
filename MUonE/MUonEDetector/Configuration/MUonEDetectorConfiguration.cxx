#include "MUonEDetectorConfiguration.h"


#include <fairlogger/Logger.h>

#include <fstream>
#include <algorithm>

MUonEDetectorConfiguration::MUonEDetectorConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorConfiguration::MUonEDetectorConfiguration(TString config_name) {

    readConfiguration(config_name);
}

Bool_t MUonEDetectorConfiguration::readConfiguration(TString config_name) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    LOG(info) << "";
    LOG(info) << "Reading detector configuration: " << config_name.Data();

    YAML::Node config;

    if(!loadConfigFile(config_name, config))
        m_configuredCorrectly = false;

    if(config["modulesConfiguration"]) {

        if(!m_modulesConfiguration.readConfiguration(config["modulesConfiguration"]))
            m_configuredCorrectly = false;
        else
            m_hasModulesConfiguration = true;

    }
    else {

        m_hasModulesConfiguration = false;
        LOG(info) << "'modulesConfiguration' section not found.";
    }

    if(config["targetsConfiguration"]) {

        if(!m_targetsConfiguration.readConfiguration(config["targetsConfiguration"]))
            m_configuredCorrectly = false;
        else
            m_hasTargetsConfiguration = true;
    }
    else {

        m_hasTargetsConfiguration = false;
        LOG(info) << "'targetsConfiguration' section not found.";
    }

    if(config["calorimeterConfiguration"]) {

        if(!m_calorimeterConfiguration.readConfiguration(config["calorimeterConfiguration"]))
            m_configuredCorrectly = false;
        else
            m_hasCalorimeterConfiguration = true;
    }
    else {

        m_hasCalorimeterConfiguration = false;
        LOG(info) << "'calorimeterConfiguration' section not found.";
    }

    if(config["detector"]) {

        auto detector_setup = config["detector"];

        if(detector_setup["projections"]) {

            auto projections = detector_setup["projections"];

            if(projections['x']) {

                if(!m_projections['x'].readConfiguration(projections['x']))
                    m_configuredCorrectly = false;
            }

            if(projections['y']) {

                if(!m_projections['y'].readConfiguration(projections['y']))
                    m_configuredCorrectly = false;
            }            

            if(projections['u']) {

                if(!m_projections['u'].readConfiguration(projections['u']))
                    m_configuredCorrectly = false;
            }

            if(projections['v']) {

                if(!m_projections['v'].readConfiguration(projections['v']))
                    m_configuredCorrectly = false;
            }            
        }
        else {

            LOG(info) << "'detector/projections' section not found.";
        } 

        if(detector_setup["stations"]) {

            for(auto const& s : detector_setup["stations"]) {

                MUonEDetectorStationConfiguration tmp_stat;
                if(!tmp_stat.readConfiguration(s, m_projections, modulesBaseConfiguration(), targetsBaseConfiguration()))
                    m_configuredCorrectly = false;  
                else
                    m_stations.emplace_back(tmp_stat);              
            }

            //sort stations by their origin
            std::sort(m_stations.begin(), m_stations.end(), [](MUonEDetectorStationConfiguration const& lhs, MUonEDetectorStationConfiguration const& rhs){return lhs.origin() < rhs.origin();});
        }
        else {

            LOG(info) << "'detector/stations' section not found.";
        } 

        if(detector_setup["calorimeterPosition"]) {

            m_calorimeterPosition = detector_setup["calorimeterPosition"].as<Double_t>();
            m_hasCalorimeter = true;
        }
        else{

            LOG(info) << "'detector/calorimeterPosition' not set. No calorimeter will be created.";
            m_hasCalorimeter = false;
        }

    }
    else {

        LOG(error) << "'detector' section not found.";
        m_configuredCorrectly = false;
    }

    if(m_configuredCorrectly)
        LOG(info) << "Detector configuration read succesfully.";
    else
        LOG(info) << "Detector configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorConfiguration::logCurrentConfiguration() const {

    LOG(info) << "";
    LOG(info) << "";
    LOG(info) << "Summary of detector configuration:";

    if(m_hasModulesConfiguration)
        m_modulesConfiguration.logCurrentConfiguration();
    else
        LOG(info) << "Modules configuration not defined.";

    if(m_hasTargetsConfiguration)
        m_targetsConfiguration.logCurrentConfiguration();
    else
        LOG(info) << "Targets configuration not defined.";

    if(m_hasCalorimeterConfiguration)
        m_calorimeterConfiguration.logCurrentConfiguration();
    else
        LOG(info) << "Calorimeter configuration not defined.";

    if(!m_projections.empty()) {

        LOG(info) << "Defined projections:";
        for(auto const& p : m_projections) {
            p.second.logCurrentConfiguration();
            LOG(info) << "";
        }
    } else
        LOG(info) << "No projections defined.";

    LOG(info) << "";

    if(!m_stations.empty()) {

        LOG(info) << "Detector has " << m_stations.size() << " stations.";
        LOG(info) << "";

        for(Int_t station_index = 0; station_index < m_stations.size(); ++station_index) {
            LOG(info) << "Configuration of station " << station_index << ":";
            m_stations[station_index].logCurrentConfiguration();
            LOG(info) << "";
        }
    } else
        LOG(info) << "Detector has no stations.";

    if(m_hasCalorimeter)
        LOG(info) << "Calorimeter position: " << m_calorimeterPosition << " cm";
    else   
        LOG(info) << "Detector has no calorimeter.";
 
}

void MUonEDetectorConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_hasModulesConfiguration = false;
    m_hasTargetsConfiguration = false;
    m_hasCalorimeterConfiguration = false;

    m_modulesConfiguration.resetDefaultConfiguration();
    m_targetsConfiguration.resetDefaultConfiguration();
    m_calorimeterConfiguration.resetDefaultConfiguration();

    //m_projections.clear(); //projections vector is now predefined in header file
    m_stations.clear();

    m_calorimeterPosition = 0;
    m_hasCalorimeter = false;
}

Bool_t MUonEDetectorConfiguration::loadConfigFile(TString config_name, YAML::Node& config) {

    if(!config_name.EndsWith(".yaml") && !config_name.EndsWith(".YAML"))
        config_name.Append(".yaml");

    try {

        config = YAML::LoadFile(config_name.Data());
        LOG(info) << "Configuration file found in the path provided.";

    } catch(...) {

        TString geometry_dir = getenv("GEOMPATH");
        geometry_dir.ReplaceAll("//", "/"); //taken from FR examples, not sure if necessary
        if(!geometry_dir.EndsWith("/"))
            geometry_dir.Append("/");  

        TString full_path = geometry_dir + config_name;

        LOG(info) << "Configuration file in the given path not found or failed to open. Trying in GEOMPATH directory (" << full_path << ").";

        try {

            config = YAML::LoadFile(full_path.Data());
            LOG(info) << "File found.";

        } catch(...) {

            LOG(fatal) << "Failed to load detector configuration.";
            return false;
        }
    }   

    return true; 
}

ClassImp(MUonEDetectorConfiguration)
