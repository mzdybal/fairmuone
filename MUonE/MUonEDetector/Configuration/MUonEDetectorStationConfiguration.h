#ifndef MUONEDETECTORSTATIONCONFIGURATION_H
#define MUONEDETECTORSTATIONCONFIGURATION_H

#include "Rtypes.h"

#include <string>
#include <yaml-cpp/yaml.h>
#include <vector>

#include "MUonEDetectorProjectionConfiguration.h"
#include "MUonEDetectorStationModuleConfiguration.h"

#include "MUonEDetectorModulesConfiguration.h"
#include "MUonEDetectorTargetsConfiguration.h"


class MUonEDetectorStationConfiguration {

public:

    MUonEDetectorStationConfiguration();
    MUonEDetectorStationConfiguration(YAML::Node const& config, std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections, MUonEDetectorModulesConfiguration const& baseConfiguration, MUonEDetectorTargetsConfiguration const& targetBaseConfiguration);

    Bool_t readConfiguration(YAML::Node const& config, std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections, MUonEDetectorModulesConfiguration const& baseConfiguration, MUonEDetectorTargetsConfiguration const& targetBaseConfiguration);
    void logCurrentConfiguration() const;


    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Double_t origin() const {return m_origin;}

    Bool_t hasTarget() const {return m_hasTarget;}
    Double_t targetRelativePosition() const {return m_targetRelativePosition;}
    Double_t targetPosition() const {return m_origin + m_targetRelativePosition;}

    MUonEDetectorTargetsConfiguration const& targetConfiguration() const {return m_targetConfiguration;}

    std::vector<MUonEDetectorStationModuleConfiguration> const& modules() const {return m_modules;}

    Double_t modulePosition(MUonEDetectorStationModuleConfiguration const& module) const {return m_origin + module.relativePosition();}
    Bool_t hasModules() const {return !m_modules.empty();}


    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};

    Double_t m_origin{0};

    Bool_t m_hasTarget{false};    
    Double_t m_targetRelativePosition{0};
    MUonEDetectorTargetsConfiguration m_targetConfiguration;

    std::vector<MUonEDetectorStationModuleConfiguration> m_modules;


    ClassDef(MUonEDetectorStationConfiguration, 2)
};

#endif //MUONEDETECTORSTATIONCONFIGURATION_H

