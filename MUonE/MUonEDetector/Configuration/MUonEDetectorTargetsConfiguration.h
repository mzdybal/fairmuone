#ifndef MUONEDETECTORTARGETSCONFIGURATION_H
#define MUONEDETECTORTARGETSCONFIGURATION_H


#include "Rtypes.h"

#include <string>
#include <yaml-cpp/yaml.h>


class MUonEDetectorTargetsConfiguration {

public:

    MUonEDetectorTargetsConfiguration();
    MUonEDetectorTargetsConfiguration(YAML::Node const& config);

    Bool_t readConfiguration(YAML::Node const& config);
    void updateConfiguration(YAML::Node const& config); //update for per-station settings
    void logCurrentConfiguration() const;


    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    std::string targetMaterial() const {return m_targetMaterial;}
    Double_t width() const {return m_width;}
    Double_t height() const {return m_height;}
    Double_t thickness() const {return m_thickness;}


    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};

    std::string m_targetMaterial;
    Double_t m_width{0};
    Double_t m_height{0};
    Double_t m_thickness{0};

    ClassDef(MUonEDetectorTargetsConfiguration, 2)
};

#endif //MUONEDETECTORTARGETSCONFIGURATION_H
 