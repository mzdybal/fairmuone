#include "MUonEDetectorCalorimeterDigitizationConfiguration.h"


#include <fairlogger/Logger.h>

MUonEDetectorCalorimeterDigitizationConfiguration::MUonEDetectorCalorimeterDigitizationConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorCalorimeterDigitizationConfiguration::MUonEDetectorCalorimeterDigitizationConfiguration(YAML::Node const& config) {

    readConfiguration(config);
}

Bool_t MUonEDetectorCalorimeterDigitizationConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(config["resolution"])
        m_resolution = config["resolution"].as<Double_t>();
    else {
        LOG(info) << "Variable 'resolution' not set.  A perfect calorimeter will be assumed.";
        m_resolution = 0;
    }


    if(!m_configuredCorrectly)
        LOG(info) << "calorimeterConfiguration/digitization section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorCalorimeterDigitizationConfiguration::logCurrentConfiguration() const {

    LOG(info) << "A calorimeter crystal resolution of " << m_resolution << "%/sqrt(E) will be applied.";

}

void MUonEDetectorCalorimeterDigitizationConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_resolution = 0;
}

ClassImp(MUonEDetectorCalorimeterDigitizationConfiguration)
