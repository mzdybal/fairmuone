#include "MUonEDetectorProjectionConfiguration.h"

#include <fairlogger/Logger.h>

MUonEDetectorProjectionConfiguration::MUonEDetectorProjectionConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorProjectionConfiguration::MUonEDetectorProjectionConfiguration(char name, Double_t angle) 
    : m_name(name), m_angle(angle)
    {}

Bool_t MUonEDetectorProjectionConfiguration::readConfiguration(YAML::Node const& config) {


    resetDefaultConfiguration();

    m_configuredCorrectly = true;


    if(config["tilt"])
        m_tilt = config["tilt"].as<Double_t>();
    else {

        LOG(info) << "Variable 'tilt' not set for projection " << m_name << ". Assumed to be 0.";
    }

    if(config["negateDataHitPosition"])
        m_negateDataHitPosition = config["negateDataHitPosition"].as<Bool_t>();


    if(!m_configuredCorrectly)
        LOG(info) << "One of the projections in detector/projections section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorProjectionConfiguration::logCurrentConfiguration() const {

    LOG(info) << "name: " << m_name;
    LOG(info) << "angle: " << m_angle << " degrees";
    LOG(info) << "tilt: " << m_tilt << " degrees";
    LOG(info) << "negateDataHitPosition: " << m_negateDataHitPosition;
}

void MUonEDetectorProjectionConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_tilt = 0;   
    m_negateDataHitPosition = false; 
}

ClassImp(MUonEDetectorProjectionConfiguration)
