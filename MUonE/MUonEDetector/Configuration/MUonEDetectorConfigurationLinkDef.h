#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonEDetectorCalorimeterConfiguration+;
#pragma link C++ class MUonEDetectorCalorimeterDigitizationConfiguration+;
#pragma link C++ class MUonEDetectorCalorimeterGeometryConfiguration+;
#pragma link C++ class MUonEDetectorConfiguration+;
#pragma link C++ class MUonEDetectorModuleDigitizationConfiguration+;
#pragma link C++ class MUonEDetectorModuleGeometryConfiguration+;
#pragma link C++ class MUonEDetectorModulesConfiguration+;
#pragma link C++ class MUonEDetectorProjectionConfiguration+;
#pragma link C++ class MUonEDetectorStationConfiguration+;
#pragma link C++ class MUonEDetectorStationModuleConfiguration+;
#pragma link C++ class MUonEDetectorTargetsConfiguration+;


#endif