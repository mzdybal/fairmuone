#include "MUonETracker.h"

#include "MUonEStack.h"

#include "FairVolume.h"
#include "FairGeoVolume.h"
#include "FairGeoNode.h"
#include "FairRootManager.h"
#include "FairGeoLoader.h"
#include "FairGeoMedia.h"
#include "FairGeoBuilder.h"
#include "FairGeoInterface.h"
#include "FairRun.h"
#include "FairRuntimeDb.h"

#include "TClonesArray.h" 
#include "TGeoManager.h" 


#include "TGeoBBox.h"
#include "TGeoTrd2.h"
#include "TVirtualMC.h"
#include "TParticle.h"


MUonETracker::MUonETracker()
    : FairDetector("MUonETracker", true, DET_TRACKER), //name, active, id (see MUonEDetectorConfiguration.h)
    m_trackerPoints(new TClonesArray("MUonETrackerPoint"))
    {}


MUonETracker::MUonETracker(MUonEDetectorConfiguration const& detectorConfiguration, Bool_t saveTrackerPoints)
    : FairDetector("MUonETracker", true, DET_TRACKER), //name, active, id (see MUonEDetectorConfiguration.h)
    m_trackerPoints(new TClonesArray("MUonETrackerPoint"))
    {
        setConfiguration(detectorConfiguration, saveTrackerPoints);
    }


MUonETracker::~MUonETracker() {

    if (m_trackerPoints) {
        m_trackerPoints->Delete();
        delete m_trackerPoints;
        m_trackerPoints = nullptr;
    }
}

void MUonETracker::setConfiguration(MUonEDetectorConfiguration const& detectorConfiguration, Bool_t saveTrackerPoints) {
    
    m_detectorConfiguration = detectorConfiguration;
    m_saveTrackerPoints = saveTrackerPoints;
}



void MUonETracker::Register() {

	FairRootManager::Instance()->Register("TrackerPoints", FairDetector::GetName(), m_trackerPoints, m_saveTrackerPoints ? kTRUE : kFALSE); //kTRUE = store in ntuple
}


TClonesArray* MUonETracker::GetCollection(Int_t iColl) const {

    if(0 == iColl)
        return m_trackerPoints;
    else
        return NULL;
}


void MUonETracker::ConstructGeometry() {

    m_idMap.clear();

    if(!m_detectorConfiguration.hasModules())
        return;


    //avoid creating unnecessary volumes
    auto const& stations = m_detectorConfiguration.stations();


    std::vector<TGeoVolume*> volumes;
    volumes.reserve(5);

    std::vector<std::vector<Int_t>> station_module_volume_map;
    station_module_volume_map.resize(stations.size());

    for(Int_t station_index = 0; station_index < stations.size(); ++station_index) {

        auto const& station_modules = stations[station_index].modules();
        station_module_volume_map[station_index].reserve(10);

        for(Int_t module_index = 0; module_index < station_modules.size(); ++module_index) {

            auto const& module = station_modules[module_index];
            auto const& module_geometry = module.configuration().geometry();
            auto const& sensor_material_name = module_geometry.sensorMaterial().c_str();

            auto sensor_size_measurement_direction = module_geometry.sensorSizeMeasurementDirection();
            auto sensor_size_perpendicular_direction = module_geometry.sensorSizePerpendicularDirection();
            auto sensor_thickness = module_geometry.sensorThickness();

            //InitMedium has check to not initialize medium multiple times
            InitMedium(sensor_material_name);
            auto sensor_material = gGeoManager->GetMedium(sensor_material_name);

            //check if volume would be the same as one of the existing ones
            //the modules will be mostly uniform, so we can brute force it
            bool sameAsExisting = false;
            for(Int_t test_volume_id = 0; test_volume_id < volumes.size(); ++test_volume_id) {

                auto const& test_volume = volumes[test_volume_id];

                auto const& shape = static_cast<TGeoBBox*>(test_volume->GetShape());
                auto const& test_dx = shape->GetDX();
                auto const& test_dy = shape->GetDY();
                auto const& test_dz = shape->GetDZ();
                auto const& test_medium = test_volume->GetMedium();

                if(fabs(0.5 * sensor_size_measurement_direction - test_dx) < 1e-5
                && fabs(0.5 * sensor_size_perpendicular_direction - test_dy) < 1e-5
                && fabs(0.5 * sensor_thickness - test_dz) < 1e-5
                && strcmp(test_medium->GetName(), sensor_material->GetName()) == 0) {

                    sameAsExisting = true;
                    station_module_volume_map[station_index].push_back(test_volume_id);
                    break;
                }
            }

            if(!sameAsExisting) {

                volumes.emplace_back(new TGeoVolume(("sensor_volume" + std::to_string(volumes.size())).c_str(), 
                                        new TGeoBBox(0.5 * sensor_size_measurement_direction,
                                        0.5 * sensor_size_perpendicular_direction, 
                                        0.5 * sensor_thickness),
                                        sensor_material));

                AddSensitiveVolume(volumes.back());
                volumes.back()->SetLineColor(4); //blue

                station_module_volume_map[station_index].push_back(volumes.size() - 1);
            }
        }
    }


    //get pointer to detector cave
    TGeoVolume* top = gGeoManager->GetTopVolume();


    Int_t sensor_copy_id = 0;

    for(Int_t station_index = 0; station_index < stations.size(); ++station_index) {

        auto const& station = stations[station_index];
        auto const& station_modules = station.modules();

        //setup modules geometry
        for(Int_t module_index = 0; module_index < station_modules.size(); ++module_index) {

            auto const& module = station_modules[module_index];
            auto const& module_projection = module.projection();

            auto const& module_geometry = module.configuration().geometry();

            auto sensor_volume = volumes[station_module_volume_map[station_index][module_index]];

            //assume measurement direction to be (1,0,0)
            //rotate around Y axis by tilt angle
            //note that Y component will still be 0, but the measurement direction will be tilted towards Z axis
            TGeoRotation* rotation_matrix = new TGeoRotation();
            rotation_matrix->RotateY(module_projection.tilt());
            //rotate around Z axis to get correct projection
            rotation_matrix->RotateZ(module_projection.angle());            

            if(module_geometry.twoSensorsPerModule()) {
                //add nodes to cave and ids to map
                //all geometry other than sensors should have negative IDs
                TGeoVolumeAssembly* sensor_pair = new TGeoVolumeAssembly("sensor_pair");

                //first sensor
                sensor_pair->AddNode(sensor_volume, sensor_copy_id, new TGeoTranslation(0,0, -0.5 * module_geometry.distanceBetweenSensorCenters()));
                m_idMap.insert({sensor_copy_id, MUonETrackerVolumeIDs(station_index, module_index, 0)});
                ++sensor_copy_id;

                //second sensor
                sensor_pair->AddNode(sensor_volume, sensor_copy_id, new TGeoTranslation(0,0, 0.5 * module_geometry.distanceBetweenSensorCenters()));
                m_idMap.insert({sensor_copy_id, MUonETrackerVolumeIDs(station_index, module_index, 1)});
                ++sensor_copy_id;     

                top->AddNode(sensor_pair, 0, new TGeoCombiTrans(0, 0, station.modulePosition(module), rotation_matrix));      
            
            } else {

                m_idMap.insert({sensor_copy_id, MUonETrackerVolumeIDs(station_index, module_index, 0)});
                top->AddNode(sensor_volume, sensor_copy_id, new TGeoCombiTrans(0, 0, station.modulePosition(module), rotation_matrix));
                ++sensor_copy_id; 
            }


/*
            //first sensor
            auto first_sensor_transformation = new TGeoCombiTrans(0, 0, station.modulePosition(module) - 0.5 * module_geometry.distanceBetweenSensorCenters(), rotation_matrix);
            top->AddNode(sensor_volume, sensor_copy_id, first_sensor_transformation);
            m_idMap.insert({sensor_copy_id, MUonETrackerVolumeIDs(station_index, module_index, 0)});
            ++sensor_copy_id;

            //second sensor
            auto second_sensor_transformation = new TGeoCombiTrans(0, 0, station.modulePosition(module) + 0.5 * module_geometry.distanceBetweenSensorCenters(), rotation_matrix);
            top->AddNode(sensor_volume, sensor_copy_id, second_sensor_transformation);
            m_idMap.insert({sensor_copy_id, MUonETrackerVolumeIDs(station_index, module_index, 1)});
            ++sensor_copy_id;    

*/               
        }
    }
}

Bool_t MUonETracker::ProcessHits(FairVolume *v) {

    /** This method is called from the MC stepping */

    //all geometry other than sensors should have negative IDs
    Int_t volume_copy_number = v->getCopyNo();
    if(volume_copy_number < 0)
        return kTRUE;

	// track entering volume
	if ( TVirtualMC::GetMC()->IsTrackEntering()) {

		m_trackTotalEnergyLoss  = 0;

        Double_t global_position[3];
        Double_t local_position[3];
        
		TVirtualMC::GetMC()->TrackPosition(global_position[0], global_position[1], global_position[2]);
        m_trackEnteringPositionGlobalCoordinates = TVector3(global_position[0], global_position[1], global_position[2]);

        TVirtualMC::GetMC()->Gmtod(global_position, local_position, 1);
        m_trackEnteringPositionLocalCoordinates = TVector3(local_position[0], local_position[1], local_position[2]);
	}

    // track inside volume, add energy loss from current step
    m_trackTotalEnergyLoss += TVirtualMC::GetMC()->Edep();
    
    // track leaving volume
    if( TVirtualMC::GetMC()->IsTrackExiting()    ||
        TVirtualMC::GetMC()->IsTrackStop()       ||
        TVirtualMC::GetMC()->IsTrackDisappeared()) {


			if(m_trackTotalEnergyLoss == 0. ) {return kFALSE;}

            //get copy number (this is the number we set during geometry construction)
            Int_t sensor_copy_number = volume_copy_number;
            auto const& ids = m_idMap[sensor_copy_number];

            Int_t track_ID = TVirtualMC::GetMC()->GetStack()->GetCurrentTrackNumber();
			Int_t track_pdg_code = TVirtualMC::GetMC()->GetStack()->GetCurrentTrack()->GetPdgCode();


            //calculate track entering position
            Double_t global_position[3];
            Double_t local_position[3];
            
            TVirtualMC::GetMC()->TrackPosition(global_position[0], global_position[1], global_position[2]);
            auto track_exiting_position_global_coordinates = TVector3(global_position[0], global_position[1], global_position[2]);

            TVirtualMC::GetMC()->Gmtod(global_position, local_position, 1);
            auto track_exiting_position_local_coordinates = TVector3(local_position[0], local_position[1], local_position[2]);

            //add point to container
	        TClonesArray& clref = *m_trackerPoints;
	        Int_t size = clref.GetEntriesFast();

            new(clref[size]) MUonETrackerPoint(track_ID, track_pdg_code, ids, 
                m_trackEnteringPositionGlobalCoordinates, track_exiting_position_global_coordinates,
                m_trackEnteringPositionLocalCoordinates, track_exiting_position_local_coordinates,
                m_trackTotalEnergyLoss);
    }
    
    return kTRUE;
}

void MUonETracker::Reset() {

    m_trackerPoints->Clear();
}

void MUonETracker::EndOfEvent() {

    m_trackerPoints->Clear();
}

//taken from examples, no need to read into it too much
Int_t MUonETracker::InitMedium(const char* name)
{
	static FairGeoLoader* geoLoad = FairGeoLoader::Instance();
	static FairGeoInterface* geoFace = geoLoad->getGeoInterface();
	static FairGeoMedia* media = geoFace->getMedia();
	static FairGeoBuilder* geoBuild = geoLoad->getGeoBuilder();
    
	FairGeoMedium* ShipMedium = media->getMedium(name);
    
	if (!ShipMedium)
	{
		Fatal("InitMedium","Material %s not defined in media file.", name);
		return -1111;
	}

	TGeoMedium* medium=gGeoManager->GetMedium(name);

	if (medium!=NULL)
		return ShipMedium->getMediumIndex();

    return geoBuild->createMedium(ShipMedium);
}


ClassImp(MUonETracker)


