#ifndef MUONETRACKER_H
#define MUONETRACKER_H

#include "FairDetector.h"
#include "TLorentzVector.h"

#include <unordered_map>

#include "MUonETrackerVolumeIDs.h"
#include "MUonEDetectorConfiguration.h"
#include "MUonETrackerPoint.h"

class FairVolume;
class TClonesArray;

class MUonETracker : public FairDetector {

public:

    MUonETracker();
    MUonETracker(MUonEDetectorConfiguration const& detectorConfiguration, Bool_t saveTrackerPoints = true);

    virtual ~MUonETracker() override;

    void setConfiguration(MUonEDetectorConfiguration const& detectorConfiguration, Bool_t saveTrackerPoints = true);


    //registers output containers
	virtual void   Register() override;

    //returns containers
	virtual TClonesArray* GetCollection(Int_t iColl) const override;

    //register detector geometry
    virtual void ConstructGeometry() override;

    //called at each step through the volume
    virtual Bool_t ProcessHits(FairVolume *v=0) override;

    //resets containers after each event
    virtual void   Reset() override;

    //cleanup
    virtual void EndOfEvent() override;

private:

    //detector information
    MUonEDetectorConfiguration m_detectorConfiguration;
    Bool_t m_saveTrackerPoints{true};

    //output container
    TClonesArray* m_trackerPoints{nullptr};

    //map sensor copy ID to detector IDs
    std::unordered_map<Int_t, MUonETrackerVolumeIDs> m_idMap;

	//temporary information stored between 
    //processHits calls for a single track
	TVector3 m_trackEnteringPositionGlobalCoordinates;
	TVector3 m_trackEnteringPositionLocalCoordinates;
	Double32_t m_trackTotalEnergyLoss{0};    

    //initialize materials
	Int_t InitMedium(const char* name);


	ClassDef(MUonETracker, 2)

};

#endif//MUONETRACKER_H

