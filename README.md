# FairMUonE

A preliminary work to introduce FairRoot simulation into the MUonE experiment.

## SETUP

1. Clone, build and install FairSoft (please refer to the installation instructions on git):

	`https://github.com/FairRootGroup/FairSoft`

2. Clone FairRoot repository.

	`https://github.com/FairRootGroup/FairRoot`

3. Clone this repository.

4. Place MUonE directory from this repository inside your cloned FairRoot directory.

5. Edit FairRoot/CMakeLists.txt, find the lines where add_subdirectory commands are called and add line:

	`add_subdirectory(MUonE)`

	![image.png](./image.png)

6. Follow instructions in FairRoot repository to compile and install FairRoot.

## SETUP ON LXPLUS

Make sure to login on CentOS8 machine ("@lxplus8", using CentOS7 will result in missing dependencies errors). Instead of building FairSoft directly, set SIMPATH to `/cvmfs/fairsoft.gsi.de/centos8/fairsoft/<version you want to use>`, then follow setup instructions starting from point 2.

WARNING

Since v18.8.0, FairRoot is set to force C++17 standard, which does not work smoothly with the default version of GCC compiler (8) and centos8 is not really officially supported by FS and FR as far as I can tell.
You may encounter these problems (thanks Eugenia):
1) When running

``` cmake -DCMAKE_INSTALL_PREFIX=~/fair_install/FairRootInst" .. ```

error about FairRoot tutorial may appear:

```

CMake Error at examples/simulation/Tutorial4/src/CMakeLists.txt:38 (add_library):

  Target "ExSimulation4" links to target "FairRoot::EventDisplay" but the

  target was not found.  Perhaps a find_package() call is missing for an

  IMPORTED target, or an ALIAS target is missing?

```

To fix it, remove the directory of tutorial number 4 in ```examples/simulation/```. This applies to any errors related to tutorials, as we're not using them.

 
2) When running

``` make ```

you may get an error about linking to std::filesystem (similar message repeated multiple times):

 
```
[ 31%] Linking CXX shared library ../../lib/libMUonEProductionJob.so
CMakeFiles/MUonEProductionJob.dir/MUonEProductionJob.cxx.o: In function `MUonEProductionJob::readConfiguration(TString)':
/usr/include/c++/8/bits/fs_path.h:185: undefined reference to `std::filesystem::__cxx11::path::_M_split_cmpts()'

```

add ```link_libraries(stdc++fs)```  to ```FairRoot/CMakeLists.txt``` right after line 14.
Unfortunately, this fix seems to be specific to the combination of C++17 standard and GCC8 compiler and fixing it centrally would be problematic (filesystem library has an experimental implementation pre-GCC8 and is part of the standard and linked automatically post-GCC8). The problem should also be fixed by switching to a higher version of the compiler.

## CURRENTLY KNOWN PROBLEMS

This version is compatible only with FairRoot releases newer than v18.6.9 (i.e. v18.8.0 and up).

Event display works only with detector geometry, as the macro has to be adapted to new track classes.

## LIST OF COMMANDS

Beware that it depends on possible future changes in how the packages are built. In particular, the line at which "add_subdirectory" is inserted may vary between FairRoot versions, so it's better to do it manually. If anything stops working, let me know.

I am assuming bash shell. If using different one, adapt accordingly or work within bash by typing "bash" in the console first. Master branch of each repository always contains the newest release. If you want to use a different one, use "--branch xyz" when cloning. The build and make commands can also be appended with "-jN" to use more CPU threads.

```
mkdir muone
cd muone
```


%%%%%%%%%IGNORE THIS PART WHEN USING LXPLUS%%%%%%%%%%

Install appropriate dependencies for your system, if not done already -- https://github.com/FairRootGroup/FairSoft/blob/master/legacy/dependencies.md

```
git clone https://github.com/FairRootGroup/FairSoft.git

mkdir fsbuild
cmake -S FairSoft/ -B fsbuild/ -C FairSoft/FairSoftConfig.cmake
cmake --build fsbuild/
```


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

locally:
`export SIMPATH=<starting path>/muone/FairSoft/install`

on lxplus:
`export SIMPATH=/cvmfs/fairsoft.gsi.de/centos8/fairsoft/<version you want to use>`

```
mkdir fairroot

git clone https://github.com/FairRootGroup/FairRoot.git
git clone https://gitlab.cern.ch/mgoncerz/fairmuone
cp -r fairmuone/MUonE FairRoot/
sed -i '283 i add_subdirectory(MUonE)' FairRoot/CMakeLists.txt

cd FairRoot
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX="<starting path>/muone/fairroot" ..
make && make install
```



The software will be installed in `<starting path>/muone/fairroot`.

## USAGE

1. Run FairRootConfig.sh (or .csh, depending on shell) script from bin subdirectory in FairRoot installation directory.

	`source FairRootConfig.sh`

2. Enter share/MUonE/macros.

3. To run example production job, run `root -l 'runProductionJob.C("exampleProductionJob")'`

4. To run event display, run `root -l 'runEventDisplay.C("exampleProductionJob.root")'`

Be aware that neither MCTracks, nor GeoTracks (required to draw tracks in event display) are saved by default, as the EMZ Geant physics list produces accurate cascades in the calorimeter and thus A LOT of tracks.
To see them in the event display, you need to set saveGeoTracks to true in the production job configuration and then run event display with an additional argument, i.e. `root -l 'runEventDisplay.C("exampleProductionJob.root", true)'`.
It's recommended do to such runs for a rather limited number of events, at least with the default Geant settings.

5. To run analysis job, run `root -l 'runAnalysisJob.C("exampleAnalysisJob")'`

The example job files (MUonE/common/jobs) contain all available settings. All configuration files are first looked for in the run directory/provided path and if not found, in the respective default directories. It's enough to give the name, without extension.
The analysis plots can be opened in TBrowser. Run `root -l` first and then `new TBrowser()` and navigate to the analysis root file. 

